package com.okaygo.worker.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.JobRoleResponse
import java.util.*

class JobRoleSearchAdapter(
    private val mActivity: Activity,
    private var mResult: ArrayList<JobRoleResponse>?,
    private val onRoleClick: (JobRoleResponse?, Int?) -> Unit?

) : RecyclerView.Adapter<JobRoleSearchAdapter.MyHolder>(), Filterable {

    private var tempList: List<JobRoleResponse>? = null
    private var filterData: FilterData? = null

    init {
        tempList = mResult
        this.filterData = FilterData()
    }

    override fun getFilter(): Filter {
        return filterData!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val inflater = LayoutInflater.from(mActivity)
        val viewHolder: View = inflater.inflate(R.layout.item_text, parent, false)
        return MyHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.txtName.setText(mResult?.get(position)?.categorySubType)
        holder.txtName.setOnClickListener {
            onRoleClick(mResult?.get(position), position)
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mResult?.size ?: 0
    }

    inner class MyHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtName: AppCompatTextView

        init {
            txtName = itemView.findViewById(R.id.txtItemName)
        }
    }

    /**
     * inner class to filter hotels with name
     */
    private inner class FilterData : Filter() {
        override fun performFiltering(constraint: CharSequence): Filter.FilterResults {
            val filterString = constraint.toString().toLowerCase()
            val results = FilterResults()
            if (filterString.trim { it <= ' ' }.isEmpty()) {
                results.values = tempList
                results.count = tempList?.size ?: 0
                return results
            }
            val noData = JobRoleResponse(
                "Not Found",
                "",
                231,
                0,
                "",
                "1354",
                "",
                null,
                0,
                "",
                "",
                "",
                0,
                ""
            )
            val nlist = ArrayList<JobRoleResponse>()
            var filterableString: String
            for (i in tempList?.indices!!) {
                filterableString = tempList?.get(i)?.categorySubType?.toLowerCase() ?: ""
                if (filterableString.toLowerCase().contains(filterString)) {
                    if (nlist.contains(noData)) {
                        nlist.remove(noData)
                    }
                    tempList?.get(i)?.let { nlist.add(it) }
                } else {
                    if (!nlist.contains(noData)) {
                        nlist.add(noData)
                    }
                }
            }
            results.values = nlist
            results.count = nlist.size
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            mResult = results.values as ArrayList<JobRoleResponse>
            notifyDataSetChanged()
        }
    }
}