package com.okaygo.worker.adapters

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.okaygo.worker.ui.fragments.earning.EarningSubTabFragment
import com.okaygo.worker.ui.fragments.earning.ReferralSubTabFragment

class PaymentViewPagerAdapter(@NonNull fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {
    @NonNull
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return EarningSubTabFragment()
            1 -> return ReferralSubTabFragment()
        }
        return EarningSubTabFragment()
    }

    override fun getItemCount(): Int {
        return 2
    }
}