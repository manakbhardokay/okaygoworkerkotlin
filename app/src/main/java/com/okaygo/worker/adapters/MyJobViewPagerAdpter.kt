package com.okaygo.worker.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.*

class MyJobViewPagerAdpter(
    fm: FragmentManager?
) : FragmentPagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val mFragmentList = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return mFragmentList?.get(position)!!
    }

    override fun getCount(): Int {
        return mFragmentList?.size ?: 0
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList?.add(fragment)
        mFragmentTitleList?.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList?.get(position)
    }
}