package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.ReferralPayment
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.item_referral_payment.view.*

class ReferralPaymentAdapter(var context: Context?, val mList: ArrayList<ReferralPayment>?) :
    RecyclerView.Adapter<ReferralPaymentAdapter.PaymentHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val inflater = LayoutInflater.from(context)
        val myownholder: View = inflater.inflate(R.layout.item_referral_payment, parent, false)
        return PaymentHolder(myownholder)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        holder.bind(mList?.get(position))
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class PaymentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ReferralPayment?) {
            itemView.txtName?.text = item?.referralName
            itemView.txtRefId?.text = "Ref: #${item?.referralId}"
            itemView.txtJobName?.text =
                "${item?.referralJopType}, ${item?.brandName ?: item?.companyName}"
            itemView.txtDate?.text = "${Utilities.getFormatedDate(
                item?.insertedOn?.split(" ")?.get(0)
            )}"

            itemView.txtAmount?.text = "₹ ${Math.round(item?.totalAmount?.toFloat() ?: 0F)}"

            if (item?.isWorkerPaid == 1) {
                itemView.txtStatusPaid?.visibility = View.VISIBLE
                itemView.txtStatusPending?.visibility = View.GONE
            } else {
                itemView.txtStatusPaid?.visibility = View.GONE
                itemView.txtStatusPending?.visibility = View.VISIBLE
            }


        }
    }
}