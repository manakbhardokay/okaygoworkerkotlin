package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.adapters.PaymentAdapter.PaymentHolder
import com.okaygo.worker.data.modal.reponse.PaymentResponse
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.item_payment.view.*

class PaymentAdapter(
    var context: Context?,
    val mList: ArrayList<PaymentResponse>?,
    private val onInvoiceClick: (PaymentResponse?, Int?) -> Unit?
) :
    RecyclerView.Adapter<PaymentHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val inflater = LayoutInflater.from(context)
        val myownholder: View = inflater.inflate(R.layout.item_payment, parent, false)
        return PaymentHolder(myownholder)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        holder.bind(mList?.get(position))
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class PaymentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: PaymentResponse?) {
            itemView.txtBrandName?.text = item?.brandName ?: item?.companyName
//            itemView.txtJobName?.text = item?.jobTitle
            if (item?.invoiceDate?.isEmpty() == false) {
                itemView.txtDate?.visibility = View.VISIBLE
                itemView.txtDate?.text = "${
                    Utilities.getFormatedDate(
                        item.invoiceDate.split(" ").get(0)
                    )
                }"
            } else {
                itemView.txtDate?.visibility = View.GONE
            }

            if (item?.payoutMonth?.isEmpty() == false) {
                itemView.txtPayout?.visibility = View.VISIBLE
                itemView?.txtPayout?.text = "Payout for " + item.payoutMonth
            } else {
                itemView.txtPayout?.visibility = View.GONE
            }

            itemView.txtInvoice?.visibility = View.VISIBLE
            itemView.viewOutLine?.visibility = View.VISIBLE
            itemView.txtDot?.visibility = View.VISIBLE

            if (item?.invoiceFilePath == null || item.invoiceFilePath.isEmpty()) {
                itemView.txtInvoice?.visibility = View.GONE
                itemView.viewOutLine?.visibility = View.GONE
                itemView.txtDot?.visibility = View.GONE

//                if (item?.insertedOn?.isEmpty() == false) {
//                    itemView.txtDate?.text = "${
//                        Utilities.getFormatedDate(
//                            item.insertedOn.split(" ").get(0)
//                        )
//                    }"
//                }
//            }else{
//                itemView.txtInvoice?.visibility = View.VISIBLE
//                itemView.viewOutLine?.visibility = View.VISIBLE
//                itemView.txtDot?.visibility = View.VISIBLE
            }

            itemView.txtAmount?.text = "₹ ${Math.round(item?.totalAmount?.toFloat() ?: 0F)}"

            if (item?.paymentStatus?.isEmpty() == false) {
//                itemView.txtApprovalStatus?.visibility = View.GONE
                if (item.isWorkerPaid.equals("1")) {
                    itemView.txtStatusPaid?.visibility = View.VISIBLE
                    itemView.txtStatusPending?.visibility = View.GONE
                } else {
                    itemView.txtStatusPaid?.visibility = View.GONE
                    itemView.txtStatusPending?.visibility = View.VISIBLE
                }
            } else if ((item?.approvalStatus ?: 0) > 0) {
//                itemView.txtApprovalStatus?.visibility = View.VISIBLE
                itemView.txtStatusPaid?.visibility = View.GONE
                itemView.txtStatusPending?.visibility = View.VISIBLE
                when (item?.approvalStatus) {
                    1 -> itemView.txtStatusPending?.text = "Awating Client payment"
                    2 -> itemView.txtStatusPending?.text = "Not approved"
                    3 -> itemView.txtStatusPending?.text = "Approved"
                    4 -> itemView.txtStatusPending?.text = "In Approval"
                }
            } else {
//                itemView.txtApprovalStatus?.visibility = View.GONE
                itemView.txtStatusPaid?.visibility = View.GONE
                itemView.txtStatusPending?.visibility = View.VISIBLE
            }

            if (item?.companyLogo != null) {
                Utilities.setImageByGlideRounded(
                    context as FragmentActivity,
                    item.companyLogo,
                    itemView.imgCompany
                )
            } else {
                Utilities.setImageByGlideRounded(
                    context as FragmentActivity,
                    context?.resources?.getDrawable(R.drawable.ic_group_431),
                    itemView.imgCompany
                )
            }

            itemView.txtInvoice?.setOnClickListener {
                onInvoiceClick(
                    item,
                    absoluteAdapterPosition
                )
            }
        }
    }
}