package com.okaygo.worker.adapters

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.okaygo.worker.ui.fragments.earning.JobsReferralFragment
import com.okaygo.worker.ui.fragments.referral.ReferralFragment

class ReferralTabPagerAdapter(@NonNull fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    @NonNull
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return JobsReferralFragment()
            1 -> return JobsReferralFragment(0)
            2 -> return JobsReferralFragment(1)
        }
        return ReferralFragment()
    }

    override fun getItemCount(): Int {
        return 3
    }
}