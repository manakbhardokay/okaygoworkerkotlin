package com.okaygo.worker.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.okaygo.worker.R
import kotlinx.android.synthetic.main.item_image_slider.view.*

class SliderImageAdapter(val context: Context?, val mImages: ArrayList<String>?) :
    RecyclerView.Adapter<SliderImageAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SliderImageAdapter.MyViewHolder {
        val inflater = LayoutInflater.from(context)
        val myownholder: View = inflater.inflate(R.layout.item_image_slider, parent, false)
        return MyViewHolder(myownholder)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mImages?.get(position))
    }

    override fun getItemCount(): Int {
        return mImages?.size ?: 0
    }

    inner class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(item: String?) {
            Log.e("Url", item + "")
            context?.let {
//                itemView.imgSlider?.let { it1 ->
                Glide.with(it)
                    .load(item)
                    .override(500, 300)
                    .into(itemView.imgSlider)
//                }
            }
        }
    }
}