package com.okaygo.worker.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.ReferralWorkerData
import kotlinx.android.synthetic.main.item_referral_child.view.*

class ReferralAdtapter(
    val context: Context?,
    val mChildList: ArrayList<ReferralWorkerData>?,
    private val onRaiseEnqueryClick: (ReferralWorkerData?, Int?) -> Unit?,
    private val onClaimClick: (ReferralWorkerData?, Int?) -> Unit?

) : RecyclerView.Adapter<ReferralAdtapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_referral_child, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mChildList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mChildList?.get(position))
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ReferralWorkerData?) {
            if (absoluteAdapterPosition == mChildList?.size?.minus(1)) {
                itemView.viewDivider?.visibility = View.GONE
            }
            if (item?.referral_id?.isNotEmpty() == true) {
                itemView.txtReference?.text = "Ref #${item?.referral_id}"
                itemView.txtReference?.visibility = View.VISIBLE
            }
            itemView.txtWorkerName?.text = item?.firstName + " " + item?.lastName
            if (item?.joiningStatus != null && item.joiningDate != null) {
//                val expDate = Utilities.getDateAfterParticularDays(
//                    (item?.joiningDate).split(" ").get(0),
//                    45
//                )
//                if (Utilities.compareDates(
//                        (item?.joiningDate).split(" ").get(0),
//                        45
//                    )
//                )
                if (item.claimStatus == null && item.toBeClaimed == 1) {
                    itemView.txtClaim?.visibility = View.VISIBLE
                    itemView.txtAmtStatus?.visibility = View.GONE
                } else if (item.claimStatus != null) {
                    itemView.txtClaim?.visibility = View.GONE
                    itemView.txtClaimDone?.visibility = View.VISIBLE
                    itemView.txtAmtStatus?.visibility = View.GONE
//                    itemView.txtClaimDone?.text = item.claimStatus
                } else {
                    itemView.txtClaim?.visibility = View.GONE
//                    itemView.txtAmtStatus?.visibility = View.VISIBLE
                    itemView.txtClaimDone?.visibility = View.GONE
                }
            }

            var feedback: String? = null
            if (item?.feedbackStatus?.isEmpty() == false) {
                if (item.feedbackStatus.equals("RESULT_AWAITED")) {
                    feedback = "Interview done"
                } else {
                    feedback = item.feedbackStatus
                }
            }

            val status: String? =
                item?.joiningStatus ?: feedback ?: item?.approvalStatus ?: "In process"
            if (status?.contains("_") == true) {
                itemView.txtStatus?.text = status.replace("_", " ")
            } else {
                itemView.txtStatus?.text = status
            }

            itemView.txtAmount?.text = "\u20B9 " + (item?.referralClaimAmount ?: 0)

            itemView.txtClaim?.setOnClickListener {
                onClaimClick(item, absoluteAdapterPosition)
            }

            itemView.imgMenu?.setOnClickListener {

                val popup = context?.let { it1 -> PopupMenu(it1, itemView.imgMenu) }
                //Inflating the Popup using xml file
                popup?.getMenuInflater()
                    ?.inflate(R.menu.referral_menu, popup?.getMenu())
                popup?.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(menuItem: MenuItem?): Boolean {
                        Log.e("menu click", item.toString() + "")
                        when (menuItem.toString()) {
                            "Raise Enquire" -> onRaiseEnqueryClick(item, absoluteAdapterPosition)
                        }
                        return true
                    }
                })
                popup?.show()
            }
        }
    }
}