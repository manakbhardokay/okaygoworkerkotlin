package com.okaygo.worker.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.Availability
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.item_availability.view.*
import java.util.*

class AvailabilityAdapter(
    private val mActivity: Activity,
    private val isForOnBoarding: Boolean,
    private val mList: ArrayList<Availability>?,
    private val onEditClick: (Availability?, Int?) -> Unit?,
    private val onDeleteClick: (Availability?, Int?) -> Unit?,
    private val onTggleClick: (Int?, Int?, Int?) -> Unit?

) : RecyclerView.Adapter<AvailabilityAdapter.MyJobHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyJobHolder {
        val inflater = LayoutInflater.from(mActivity)
        val viewHolder: View = inflater.inflate(R.layout.item_availability, parent, false)
        return MyJobHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: MyJobHolder, position: Int) {
        val data: Availability? = mList?.get(position)
        holder.bind(data)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class MyJobHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var days: String? = ""
        fun bind(item: Availability?) {
            if (isForOnBoarding) {
                itemView.swToggle?.visibility = View.GONE
            }

            itemView.txtTime?.text =
                "${Utilities.convertTimeToAmPm(item?.loginTime)} - ${Utilities.convertTimeToAmPm(
                    item?.logoutTime
                )}"


            val jsonParser = JsonParser()
            val jsonObject: JsonObject = jsonParser.parse(item?.workdaysDetails).getAsJsonObject()
//            tileHolder.days_details = jsonObject
            if (jsonObject["2"].asBoolean) {
                days = days + "Mon | "
            }
            if (jsonObject["3"].asBoolean) {
                days = days + "Tue | "
            }
            if (jsonObject["4"].asBoolean) {
                days = days + "Wed | "
            }
            if (jsonObject["5"].asBoolean) {
                days = days + "Thu | "
            }
            if (jsonObject["6"].asBoolean) {
                days = days + "Fri | "
            }
            if (jsonObject["7"].asBoolean) {
                days = days + "Sat | "
            }
            if (jsonObject["1"].asBoolean) {
                days = days + "Sun | "
            }
            itemView.txtDays?.text = days
            if (item?.active_status == 1) {
                itemView.swToggle?.setChecked(true)
            } else {
                itemView.swToggle?.setChecked(false)
            }

            itemView.txtEdit?.setOnClickListener {
                onEditClick(item, adapterPosition)
            }
            itemView.txtDelete?.setOnClickListener {
                onDeleteClick(item, adapterPosition)
            }

            itemView.swToggle?.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
                if (b) {
                    onTggleClick(item?.availablityId, 1, adapterPosition)
                } else {
                    onTggleClick(item?.availablityId, 0, adapterPosition)
                }
            })

        }
    }

}