package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.Experiences
import kotlinx.android.synthetic.main.item_exp.view.*
import java.util.*

class ExperienceAdapter(
    val context: Context?,
    val mItemList: ArrayList<Experiences>?,
    private val OnEditClick: (Experiences?, Int?) -> Unit?,
    private val OnDeleteClick: (Experiences?, Int?) -> Unit?
) : RecyclerView.Adapter<ExperienceAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(context)
        val viewHolder = inflater.inflate(R.layout.item_exp, parent, false)
        return MyViewHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val expData: Experiences? = mItemList?.get(position)
        holder.bind(expData)
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mItemList?.size ?: 0
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Experiences?) {
            itemView.txtJobCat?.text = item?.industryName
            itemView.txtJob?.text = "${item?.jobTypeName}, ${item?.companyName}"
            if (item?.workLocation?.isNotEmpty() == true) {
                itemView.txtJobLoc?.text = item.workLocation
            } else {
                itemView.txtJobLoc?.text = "NA"
            }

            if (item?.toDate?.isNotEmpty() == true) {
//                itemView.txtJobDuration?.setText(
//                    "${Utilities.getDateInMonthYear(item?.fromDate)} - ${Utilities.getDateInMonthYear(
//                        item?.toDate
//                    )}"
//                )
                itemView.txtJobDuration?.setText("No")
            } else {
//                itemView.txtJobDuration?.setText("${Utilities.getDateInMonthYear(item?.fromDate)}  - present")
                itemView.txtJobDuration?.setText("Yes")
            }

            itemView.imgMenu?.setOnClickListener {
                onMenuClick(
                    itemView.imgMenu,
                    item,
                    adapterPosition
                )
            }
        }
    }

    /**
     * menu btn click handling
     */
    private fun onMenuClick(imgMenu: AppCompatImageView, item: Experiences?, pos: Int?) {
        val popupMenu = PopupMenu(context, imgMenu)
        popupMenu.menuInflater.inflate(R.menu.experince_menu, popupMenu.menu)

        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.edit -> OnEditClick(item, pos)
                R.id.delete -> OnDeleteClick(item, pos)
            }
            true
        }
        popupMenu.show()
    }
}