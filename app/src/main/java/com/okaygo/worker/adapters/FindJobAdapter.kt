package com.okaygo.worker.adapters

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonParser
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.JobContent
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.fragment_job_detail.*
import kotlinx.android.synthetic.main.item_find_jobs.view.*
import java.util.*

class FindJobAdapter(
    val context: Context?, val mItemList: ArrayList<JobContent>?,
    private val OnApplyJob: (JobContent?, Int?) -> Unit?,
    private val onJobDetailClick: (JobContent?, Int?) -> Unit?,
    private val onJobRejectClick: (JobContent?, Int?) -> Unit?,
    private val OnJobRefferAndEarn: (JobContent?, Int?) -> Unit?
) : RecyclerView.Adapter<FindJobAdapter.MyViewHolder>() {

    //    var okayGoFirebaseAnalytics: OkayGoFirebaseAnalytics
    var calendar: Calendar

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(context)
        val myownholder = inflater.inflate(R.layout.item_find_jobs, parent, false)
        return MyViewHolder(myownholder)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val homeData: JobContent? = mItemList?.get(position)
        holder.bind(homeData)
        if (homeData?.jobTitle?.isEmpty() == true) {
            holder.itemView.txtJobName?.text = homeData.jobType
        } else {
            holder.itemView.txtJobName?.text = homeData?.jobTitle
        }

        if (homeData?.referralClaimAmount ?: 0 > 0) {
            holder.itemView.constraintReferAmount?.visibility = View.VISIBLE
            holder.itemView.txtReferAmt?.text =
                "\u20B9 " + homeData?.referralClaimAmount?.toString()
        } else {
            holder.itemView.constraintReferAmount?.visibility = View.GONE
        }

        if (homeData?.brandName != null && homeData.brandName.length > 0) {
            holder.itemView.txtCompany?.text = homeData.brandName
        } else {
            holder.itemView.txtCompany?.text = homeData?.company
        }

//        val minSalary = homeData?.min_salary;
//        val maxSalary = homeData?.max_salary
//
//        val minSal = minSalary?.div(1000)
//        val maxSal = maxSalary?.div(1000)
//
//        var salaryType = "p.m."
//        if (homeData?.workType?.toUpperCase() == "ON DEMAND") {
//            salaryType = "Daily"
//            holder.itemView.txtSalary?.text =
//                "\u20B9${
//                    String.format(
//                        "%.2f",
//                        homeData?.amount
//                    )
//                }/ ${(homeData?.amountPer ?: salaryType)}"
//        }
//
//        if (homeData?.workType?.toUpperCase() != "ON DEMAND") {
//            if (maxSal ?: 0 < 1) {
//                holder.itemView.txtSalary?.text =
//                    "\u20B9${homeData?.amount}/ ${(homeData?.amountPer ?: salaryType)}"
//            } else {
//                if (minSal ?: 0 > 99) {
//                    val mMinSal = minSal?.div(100.0)
//                    val mMaxSal = maxSal?.div(100.0)
//                    if (mMinSal == mMaxSal) {
//                        holder.itemView.txtSalary?.text =
//                            "\u20B9" + mMaxSal + "L /" + (homeData?.amountPer
//                                ?: salaryType)
//                    } else {
//                        holder.itemView.txtSalary?.text =
//                            "\u20B9" + mMinSal + "L - \u20B9" + mMaxSal + "L /" + (homeData?.amountPer
//                                ?: salaryType)
//                    }
//                } else {
//                    if (minSal == maxSal) {
//                        holder.itemView.txtSalary?.text =
//                            "\u20B9" + minSal + "k /" + (homeData?.amountPer
//                                ?: salaryType)
//                    } else {
//                        holder.itemView.txtSalary?.text =
//                            "\u20B9" + minSal + "k - \u20B9" + maxSal + "k /" + (homeData?.amountPer
//                                ?: salaryType)
//                    }
//                }
//            }
//        }

        if ((homeData?.noOfHrs ?: 0) > 0) {
            holder.itemView.txtSalary?.text =
                "\u20B9${(homeData?.noOfHrs ?: 0) * 70} + ₹2 per KM petrol charges"
        } else {
            holder.itemView.txtSalary?.text = "₹70 per hour + ₹2 per KM petrol charges"
        }

        if (homeData?.address?.isNotEmpty() == true) {
            holder.itemView.txtAddress?.text = homeData.address
        } else if (homeData?.job_google_location?.isNotEmpty() == true) {
            holder.itemView.txtAddress?.text = homeData.job_google_location
        } else if (homeData?.city_name?.isNotEmpty() == true) {
            holder.itemView.txtAddress?.text = homeData.city_name
        } else if (homeData?.city?.isNotEmpty() == true) {
            holder.itemView.txtAddress?.text = homeData.city
        }

        var jobType = "Single Day"
        if (homeData?.boost_job == 1) {
            jobType = "Hot Job!"
//            holder.itemView.txtHotJobs?.setVisibility(View.VISIBLE)
//        } else {
//            holder.itemView.txtHotJobs?.setVisibility(View.GONE)
        } else if (homeData?.recurringJob == 1) {
            jobType = "Weekly"
        } else if (homeData?.multiDay == 1) {
            jobType = "Multiday"
        }
        holder.itemView.txtHotJobs?.text = jobType

        if (homeData?.workdays_details != null && !homeData.workdays_details.equals("{}")) {
            holder.itemView.linearWeekDays?.visibility = View.VISIBLE
            setDays(holder, homeData.workdays_details)
        } else {
            holder.itemView.linearWeekDays?.visibility = View.GONE
        }
//        if (homeData?.experience?.isNotEmpty() == true) {
//            holder.itemView.txtEntryLevel?.setVisibility(View.VISIBLE);
//            holder.itemView.imgEntryLevel?.setVisibility(View.VISIBLE);
//            holder.itemView.txtEntryLevel?.setText(homeData?.experience)
//        } else {
//            holder.itemView.txtEntryLevel?.setVisibility(View.GONE);
//            holder.itemView.imgEntryLevel?.setVisibility(View.GONE);
//        }
        val startDate = Utilities.convertStringDateToDayMonth(homeData?.startDate)
        val endDate = Utilities.convertStringDateToDayMonth(homeData?.endDate)
        Log.e("Strst", startDate + "")
        if (startDate?.isEmpty() == false) {
            holder.itemView.txtDateTitle?.visibility = View.VISIBLE
            holder.itemView.imgCalender?.visibility = View.VISIBLE
            holder.itemView.txtDate?.visibility = View.VISIBLE
            holder.itemView.txtDate?.text = startDate
//            if (homeData?.workType != null && homeData.workType.toUpperCase() == "ON DEMAND") {
//                if (homeData?.workdays_count ?: 0 > 0) {
//                    holder.itemView.linearWeekDays?.setVisibility(View.VISIBLE);
//                    setDaysForOD(holder, homeData?.startDate ?: "", homeData?.workdays_count ?: 0);
//                } else {
//                    holder.itemView.linearWeekDays?.setVisibility(View.GONE);
//                }
//            }
        } else {
            holder.itemView.txtDateTitle?.visibility = View.GONE
            holder.itemView.txtDate?.visibility = View.GONE
            //            holder.imgCalender.setVisibility(View.GONE);
        }
        if (endDate?.isEmpty() == false) {
            holder.itemView.txtEDateTitle?.visibility = View.VISIBLE
            holder.itemView.imgCalender?.visibility = View.VISIBLE
            holder.itemView.txtEndDate?.visibility = View.VISIBLE
            holder.itemView.txtEndDate?.text = endDate
//            if (homeData?.workType != null && homeData.workType.toUpperCase() == "ON DEMAND") {
//                if (homeData?.workdays_count ?: 0 > 0) {
//                    holder.itemView.linearWeekDays?.setVisibility(View.VISIBLE);
//                    setDaysForOD(holder, homeData?.startDate ?: "", homeData?.workdays_count ?: 0);
//                } else {
//                    holder.itemView.linearWeekDays?.setVisibility(View.GONE);
//                }
//            }
        } else {
            holder.itemView.txtEDateTitle?.visibility = View.GONE
            holder.itemView.txtEndDate?.visibility = View.GONE
            //            holder.imgCalender.setVisibility(View.GONE);
        }
        val loginTime = Utilities.convertTimeInAmPm(homeData?.loginTime)
        val logoutTime = Utilities.convertTimeInAmPm(homeData?.logoutTime)
        Log.e("loginTime", loginTime + "")
        Log.e("logoutTime", logoutTime + "")
        if (loginTime?.isEmpty() == false && logoutTime?.isEmpty() == false) {
            holder.itemView.txtTimeTitle?.visibility = View.VISIBLE
            holder.itemView.imgWatch?.visibility = View.VISIBLE
            holder.itemView.txtTime?.visibility = View.VISIBLE
            holder.itemView.txtTime?.text = "$loginTime - $logoutTime"
        } else {
            holder.itemView.txtTimeTitle?.visibility = View.GONE
            holder.itemView.txtTime?.visibility = View.GONE
            holder.itemView.imgWatch?.visibility = View.GONE
        }

        if ((homeData?.cuisine) ?: 0 == 0) {
            holder.itemView.imgCusin?.setVisibility(View.GONE);
            holder.itemView.txtIndianCuisine?.setVisibility(View.GONE);
        } else {
            holder.itemView.imgCusin?.setVisibility(View.VISIBLE);
            holder.itemView.txtIndianCuisine?.setVisibility(View.VISIBLE);
            holder.itemView.txtIndianCuisine?.setText(
                Utilities.getCuisineFromId(
                    homeData?.cuisine ?: 0
                )
            )
        }
//        setJobType(homeData?.workType, holder)
    }

    private fun setJobType(jobType: String?, holder: MyViewHolder) {
        holder.itemView.txtJobType?.text = jobType
        context?.let {
            var colorCode = ContextCompat.getColor(it, R.color.kprogresshud_grey_color)
            when (jobType?.toUpperCase()) {
                "ON DEMAND" -> colorCode = ContextCompat.getColor(it, R.color.on_demand_color)

                "PART TIME" -> {
                    colorCode = ContextCompat.getColor(it, R.color.part_time_color_new)
                    holder.itemView.txtDateTitle?.visibility = View.GONE
                    holder.itemView.txtDate?.visibility = View.GONE
                }
                "FULL TIME" -> {
                    holder.itemView.txtTimeTitle?.visibility = View.GONE
                    holder.itemView.txtTime?.visibility = View.GONE
                    holder.itemView.imgWatch?.setVisibility(View.GONE);
                    holder.itemView.txtDateTitle?.visibility = View.GONE
                    holder.itemView.txtDate?.visibility = View.GONE
                    holder.itemView.imgCalender?.visibility = View.GONE
                    holder.itemView.linearWeekDays?.visibility = View.GONE
                }
                else -> ContextCompat.getColor(it, R.color.full_time_color)
            }
            holder.itemView.txtJobType?.setBackgroundColor(colorCode)
        }
    }

    private fun setDaysForOD(holder: MyViewHolder, sDate: String, workDays: Int) {
        val day = Utilities.getWeekdayFromDate(sDate)
        val mDays = Utilities.getNextFourDays(day, workDays)
        selectUnselectDay(holder.itemView.txtMonday, false)
        selectUnselectDay(holder.itemView.txtThursday, false)
        selectUnselectDay(holder.itemView.txtTuesday, false)
        selectUnselectDay(holder.itemView.txtWednesday, false)
        selectUnselectDay(holder.itemView.txtFriday, false)
        selectUnselectDay(holder.itemView.txtSaturday, false)
        selectUnselectDay(holder.itemView.txtSunday, false)
        for (i in 0 until (mDays?.size ?: 0)) {
            Log.e("DAY", mDays?.get(i).toString() + "")
            when (mDays?.get(i)) {
                "Monday" -> selectUnselectDay(holder.itemView.txtMonday, true)
                "Tuesday" -> selectUnselectDay(holder.itemView.txtTuesday, true)
                "Wednesday" -> selectUnselectDay(holder.itemView.txtWednesday, true)
                "Thursday" -> selectUnselectDay(holder.itemView.txtThursday, true)
                "Friday" -> selectUnselectDay(holder.itemView.txtFriday, true)
                "Saturday" -> selectUnselectDay(holder.itemView.txtSaturday, true)
                "Sunday" -> selectUnselectDay(holder.itemView.txtSunday, true)
            }
        }
    }

    private fun setDays(holder: MyViewHolder, response: String?) {
        if (response == null || response.equals("null")) {
            return
        }
        val jsonParser = JsonParser()
        val jsonObject = jsonParser.parse(response).asJsonObject
        for (key in jsonObject.keySet()) {
            if (key == "0") {
                selectUnselectDay(holder.itemView.txtMonday, true)
                selectUnselectDay(holder.itemView.txtTuesday, true)
                selectUnselectDay(holder.itemView.txtWednesday, true)
                selectUnselectDay(holder.itemView.txtThursday, true)
                selectUnselectDay(holder.itemView.txtFriday, true)
                selectUnselectDay(holder.itemView.txtSaturday, true)
                selectUnselectDay(holder.itemView.txtSunday, true)
            } else if (key == "1") {
                selectUnselectDay(holder.itemView.txtMonday, jsonObject[key].asBoolean)
            } else if (key == "2") {
                selectUnselectDay(holder.itemView.txtTuesday, jsonObject[key].asBoolean)
            } else if (key == "3") {
                selectUnselectDay(holder.itemView.txtWednesday, jsonObject[key].asBoolean)
            } else if (key == "4") {
                selectUnselectDay(holder.itemView.txtThursday, jsonObject[key].asBoolean)
            } else if (key == "5") {
                selectUnselectDay(holder.itemView.txtFriday, jsonObject[key].asBoolean)
            } else if (key == "6") {
                selectUnselectDay(holder.itemView.txtSaturday, jsonObject[key].asBoolean)
            } else if (key == "7") {
                selectUnselectDay(holder.itemView.txtSunday, jsonObject[key].asBoolean)
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    private fun selectUnselectDay(textView: AppCompatTextView, isSelected: Boolean) {
        if (isSelected) {
            textView.setBackgroundDrawable(context?.resources?.getDrawable(R.drawable.sp_orange_circle))
            context?.let { textView.setTextColor(ContextCompat.getColor(it, R.color.white)) }
        } else {
            textView.setBackgroundDrawable(context?.resources?.getDrawable(R.drawable.sp_grey_circle))
            context?.let {
                textView.setTextColor(
                    ContextCompat.getColor(
                        it,
                        R.color.charcol_black
                    )
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return mItemList?.size ?: 0
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        fun bind(item: JobContent?) {
            itemView.txtCompany?.setOnClickListener(this)
//            itemView.txtApply?.setOnClickListener(this)
//            itemView.txtRefferEarn?.setOnClickListener(this)
            itemView.txtAccept?.setOnClickListener(this)
            itemView.txtReject?.setOnClickListener(this)
            itemView.constraintRoot?.setOnClickListener(this)
            itemView.imgMore?.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            when (v.id) {
                R.id.constraintRoot -> {
//                    okayGoFirebaseAnalytics.job_detail_view(
//                        mItemList?.get(adapterPosition)?.workType,
//                        mItemList?.get(adapterPosition)?.jobId.toString() + "",
//                        mItemList?.get(adapterPosition)?.jobType,
//                        "",
//                        "findjobs"
//                    )
                    onJobDetailClick(mItemList?.get(adapterPosition), adapterPosition)
                }

                R.id.txtReject -> rejectJobPopUp()
//                    OnJobRefferAndEarn(
//                    mItemList?.get(adapterPosition),
//                    adapterPosition
//                )
                R.id.txtAccept -> showDailog()
                R.id.txtCompany -> OkayGoFirebaseAnalytics.company_detail_view(
                    mItemList?.get(
                        absoluteAdapterPosition
                    )?.employerId.toString() + "", "findjobs"
                )
                R.id.imgMore -> {
                    val popup = context?.let { it1 -> PopupMenu(it1, itemView.imgMore) };
                    //Inflating the Popup using xml file
                    popup?.getMenuInflater()
                        ?.inflate(R.menu.find_job_menu, popup?.getMenu());
                    popup?.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                        override fun onMenuItemClick(item: MenuItem?): Boolean {
                            Log.e("menu click", item.toString() + "")
                            when (item.toString()) {
                                "View detail" -> onJobDetailClick(
                                    mItemList?.get(adapterPosition),
                                    adapterPosition
                                )
                                "Not interested" -> rejectJobPopUp()
//                                else -> OnJobRefferAndEarn(
//                                    mItemList?.get(adapterPosition),
//                                    adapterPosition
//                                )
                            }
                            return true;
                        }
                    })
                    popup?.show();
                }
            }
        }

        private fun showDailog() {
            val dialog = context?.let { Dialog(it) }
            dialog?.setContentView(R.layout.dialog_confirmation)
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.setCancelable(false)
            val title = dialog?.findViewById<TextView>(R.id.title)
            val message = dialog?.findViewById<TextView>(R.id.txtMsg)
            val cancel = dialog?.findViewById<TextView>(R.id.txtCancel)
            val confirm = dialog?.findViewById<TextView>(R.id.txtConfirm)
            title?.setText(R.string.apply_job)
            message?.setText(R.string.want_to_apply_job)
            cancel?.text = "NO"
            confirm?.text = "YES"
            cancel?.setOnClickListener { dialog.dismiss() }
            confirm?.setOnClickListener(View.OnClickListener {
                if (absoluteAdapterPosition < 0) {
                    return@OnClickListener
                }
                OnApplyJob(mItemList?.get(absoluteAdapterPosition), absoluteAdapterPosition)
                dialog.dismiss()
            })
            dialog?.show()
        }

        private fun rejectJobPopUp() {
            val dialog = context?.let { AlertDialog.Builder(it) }
            dialog?.setMessage("Are you not interested in this job?")
            dialog?.setCancelable(true)
            dialog?.setPositiveButton(
                "Yes"
            ) { dialog, id ->
                onJobRejectClick(mItemList?.get(adapterPosition), adapterPosition)
//                matchJob(mItemList?.get(adapterPosition)?.jobId.toString())
            }
            dialog?.setNegativeButton(
                "No"
            ) { dialog, id -> dialog.cancel() }
            val alert11 = dialog?.create()
            alert11?.show()
        }
    }

    init {
        calendar = Calendar.getInstance()
//        okayGoFirebaseAnalytics = OkayGoFirebaseAnalytics(context)
    }
}