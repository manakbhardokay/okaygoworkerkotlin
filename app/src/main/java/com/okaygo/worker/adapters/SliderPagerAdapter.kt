package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

class SliderPagerAdapter(private val context: Context?, private val layouts: IntArray?) :
    PagerAdapter() {

    private var layoutInflater: LayoutInflater? = null

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun getCount(): Int {
        return layouts?.size ?: 0
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater =
            context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val view: View? = layoutInflater?.inflate(layouts?.get(position) ?: 0, container, false)
        container.addView(view)

        return view!!
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        val view = obj as View
        container.removeView(view)
    }

}