package com.okaygo.worker.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.adapters.SearchAdapter.MyJobHolder
import com.okaygo.worker.data.modal.reponse.CitySearch
import java.util.*

class SearchAdapter(
    private val mActivity: Activity,
    private val cityList: ArrayList<CitySearch>?,
    private val onCitySelection: (CitySearch?, Int?) -> Unit?

) : RecyclerView.Adapter<MyJobHolder>() {
    private val mResult: ArrayList<CitySearch>?
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyJobHolder {
        val inflater = LayoutInflater.from(mActivity)
        val viewHolder: View = inflater.inflate(R.layout.item_text, parent, false)
        return MyJobHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: MyJobHolder, position: Int) {
        holder.txtName.setText(mResult?.get(position)?.district)
        holder.txtName.setOnClickListener {
            onCitySelection(mResult?.get(position), position)

        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mResult?.size ?: 0
    }

    inner class MyJobHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtName: AppCompatTextView

        init {
            txtName = itemView.findViewById(R.id.txtItemName)
        }
    }

    init {
        mResult = cityList
    }
}