package com.okaygo.worker.adapters

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.okaygo.worker.ui.fragments.earning.JobsEarningFragment

class EarningTabPagerAdapter(@NonNull fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    @NonNull
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return JobsEarningFragment()
            1 -> return JobsEarningFragment(0)
            2 -> return JobsEarningFragment(1)
        }
        return JobsEarningFragment()
    }

    override fun getItemCount(): Int {
        return 3
    }
}