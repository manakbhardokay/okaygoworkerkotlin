package com.okaygo.worker.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.Reviews
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.item_rating_review.view.*

class RatingReviewAdtapter(
    val context: Context?, val mReviewList: ArrayList<Reviews>?
) : RecyclerView.Adapter<RatingReviewAdtapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_rating_review, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mReviewList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mReviewList?.get(position))
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Reviews?) {
            itemView.txtCompanyName?.text = item?.companyName ?: item?.brandName
            itemView.txtRating?.text = item?.ratings?.toString() ?: ""
            itemView.txtReview?.text = item?.review ?: ""
            Utilities.setImageByGlide(context as Activity, item?.companyLogo, itemView.imgLogo)
        }
    }
}