package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.Youtube
import kotlinx.android.synthetic.main.item_help_videos.view.*

class HelpVideoAdapter(
    val context: Context?,
    val mVideoList: ArrayList<Youtube>?,
    private val onVideoClick: (Youtube?, Int?) -> Unit?

) : RecyclerView.Adapter<HelpVideoAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_help_videos, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mVideoList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mVideoList?.get(position))
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Youtube?) {
            itemView.txtVideo?.text = item?.file_name

            itemView.constraintRoot?.setOnClickListener {
                item?.let { it1 ->
                    onVideoClick(
                        mVideoList?.get(adapterPosition),
                        adapterPosition
                    )
                }
            }
        }

    }

}