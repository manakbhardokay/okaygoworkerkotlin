package com.okaygo.worker.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.JobCategories
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.ui.activity.NavigationActivity
import kotlinx.android.synthetic.main.item_header.view.*

class HomeAdtapter(
    val context: Context?, val mHeaderList: ArrayList<JobCategories>?
) : RecyclerView.Adapter<HomeAdtapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_header, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mHeaderList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mHeaderList?.get(position))
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: JobCategories?) {
            itemView.txtHeader?.text = item?.type_desc
            itemView.txtCount?.text = item?.count?.toString()

            if (item?.type_desc?.contains("Chef") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_cook)
            } else if (item?.type_desc?.contains("Restaurant") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_rr)
            } else if (item?.type_desc?.contains("Hotel") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_trolly)
            } else if (item?.type_desc?.contains("Office Job") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic__office_job)
            } else if (item?.type_desc?.contains("Delivery") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_delivery)
                val intent: Intent? = Intent(context, NavigationActivity::class.java)
                intent?.putExtra(Constants.NAV_SCREEN, Constants.JOB_LIST)
                intent?.putExtra(Constants.SELECTED_JOB_CAT_DATA, item)
                context?.startActivity(intent)
            } else if (item?.type_desc?.contains("Warehouse") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_warehouse)
            } else if (item?.type_desc?.contains("Event") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_events)
            } else if (item?.type_desc?.contains("Sales") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_sales)
            } else if (item?.type_desc?.contains("Customer Support") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_customer_support)
            } else if (item?.type_desc?.contains("Education") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_education)
            } else if (item?.type_desc?.contains("Healthcare") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_health_care)
            } else if (item?.type_desc?.contains("IT") == true) {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_it)
            } else {
                itemView.imgHeaderIcon?.setImageResource(R.drawable.ic_other_jobs)
            }

            itemView.constraintRoot?.setOnClickListener {

                val intent: Intent? = Intent(context, NavigationActivity::class.java)
                intent?.putExtra(Constants.NAV_SCREEN, Constants.JOB_LIST)
                intent?.putExtra(Constants.SELECTED_JOB_CAT_DATA, item)
                context?.startActivity(intent)
            }
        }
    }
}