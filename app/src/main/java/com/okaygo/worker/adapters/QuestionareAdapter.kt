package com.okaygo.worker.adapters

import android.content.Context
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.QuestionareResponseItem
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.item_questionare.view.*

class QuestionareAdapter(
    private var mContext: Context?,
    private var mList: ArrayList<QuestionareResponseItem>?,
    private val onYesNoClick: (QuestionareResponseItem?, Int?, Int?) -> Unit?
) : RecyclerView.Adapter<QuestionareAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(mContext)
        val myownholder: View = inflater.inflate(R.layout.item_questionare, parent, false)
        return MyViewHolder(myownholder)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mList?.get(position))
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(item: QuestionareResponseItem?) {

            var s: SpannableString? = SpannableString(item?.question)
            if (item?.first_parameter?.isEmpty() == false) {
                s = mContext?.let {
                    Utilities.setSpecificTextColor(
                        it,
                        s,
                        item.question,
                        item.first_parameter
                    )
                }
                if (item.second_parameter?.isEmpty() == false) {
                    s = mContext?.let {
                        Utilities.setSpecificTextColor(
                            it,
                            s,
                            item.question,
                            item.second_parameter
                        )
                    }
                    if (item.third_parameter?.isEmpty() == false) {
                        s = mContext?.let {
                            Utilities.setSpecificTextColor(
                                it,
                                s,
                                item.question,
                                item.third_parameter
                            )
                        }
                        if (item.forth_parameter?.isEmpty() == false) {
                            s = mContext?.let {
                                Utilities.setSpecificTextColor(
                                    it,
                                    s,
                                    item.question,
                                    item.forth_parameter
                                )
                            }
                            if (item.fifth_parameter?.isEmpty() == false) {
                                s = mContext?.let {
                                    Utilities.setSpecificTextColor(
                                        it,
                                        s,
                                        item.question,
                                        item.fifth_parameter
                                    )
                                }
                            }
                        }
                    }
                }
            }

            itemView.txtQuestion?.setText(s, TextView.BufferType.SPANNABLE)

            itemView.txtYes?.setOnClickListener {
                mContext?.resources?.getColor(R.color.theme)?.let { it1 ->
                    itemView.txtYes?.setTextColor(
                        it1
                    )
                }
                itemView.txtYes?.background =
                    mContext?.resources?.getDrawable(R.drawable.sp_rounded_blue_light_fill_40dp)

                mContext?.resources?.getColor(R.color.text_black)?.let { it1 ->
                    itemView.txtNo?.setTextColor(
                        it1
                    )
                }
                itemView.txtNo?.background =
                    mContext?.resources?.getDrawable(R.drawable.sp_rounded_white_light_fill_40dp)

                onYesNoClick(item, absoluteAdapterPosition, 1)
            }

            itemView.txtNo?.setOnClickListener {
                mContext?.resources?.getColor(R.color.red)?.let { it1 ->
                    itemView.txtNo?.setTextColor(
                        it1
                    )
                }
                itemView.txtNo?.background =
                    mContext?.resources?.getDrawable(R.drawable.sp_rounded_red_light_fill_40dp)

                mContext?.resources?.getColor(R.color.text_black)?.let { it1 ->
                    itemView.txtYes?.setTextColor(
                        it1
                    )
                }
                itemView.txtYes?.background =
                    mContext?.resources?.getDrawable(R.drawable.sp_rounded_white_light_fill_40dp)

                onYesNoClick(item, absoluteAdapterPosition, 0)
            }
        }
    }
}