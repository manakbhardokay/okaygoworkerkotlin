//package com.okaygo.worker.download
//
//import android.content.Context
//import android.os.Environment
//import android.util.Log
//import androidx.work.Worker
//import androidx.work.WorkerParameters
//import com.okaygo.worker.help.utils.Utilities
//import java.io.File
//import java.io.FileOutputStream
//import java.io.InputStream
//import java.net.HttpURLConnection
//import java.net.URL
//
//class DownloadWorker(ctx: Context, params: WorkerParameters) : Worker(ctx, params) {
//    override fun doWork(): Result {
//        var apkStorage: File? = null
//        var outputFile: File? = null
//        try {
//            val url = URL(url) //Create Download URl
//            val c: HttpURLConnection =
//                url.openConnection() as HttpURLConnection //Open Url Connection
//            c.setRequestMethod("GET") //Set Request Method to "GET" since we are grtting data
//            c.connect() //connect the URL Connection
//
//            //If Connection response is not OK then show Logs
//            if (c.getResponseCode() !== HttpURLConnection.HTTP_OK) {
//                Log.e(
//                    "Downlaod File", "Server returned HTTP " + c.getResponseCode()
//                        .toString() + " " + c.getResponseMessage()
//                )
//            }
//
//
//            //Get File if SD card is present
////            if (CheckForSDCard().isSDCardPresent()) {
//            apkStorage = File(
//                Environment.getExternalStorageDirectory().toString()
//                        + "/load"
//            )
////            )
////            } else Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show()
//
//            //If File is not present create directory
//            if (!apkStorage.exists()) {
//                apkStorage.mkdir()
//                Log.e("Download dir", "Directory Created.")
//            }
//            outputFile =
//                File(
//                    apkStorage,
//                    Utilities.getCurrentTime() + " CV"
//                ) //Create Output file in Main File
//
//            //Create New File if not present
//            if (!outputFile.exists()) {
//                outputFile.createNewFile()
//                Log.e("File created", "File Created")
//            }
//            val fos =
//                FileOutputStream(outputFile) //Get OutputStream for NewFile Location
//            val ist: InputStream = c.getInputStream() //Get InputStream for connection
//            val buffer = ByteArray(1024) //Set buffer type
//            var len1 = 0 //init length
//            while (ist.read(buffer).also({ len1 = it }) != -1) {
//                fos.write(buffer, 0, len1) //Write new file
//            }
//
//            //Close all connection after doing task
//            fos.close()
//            ist.close()
//           return Result.success()
//        } catch (e: java.lang.Exception) {
//
//            //Read exception if something went wrong
//            e.printStackTrace()
//            outputFile = null
//            Log.e("download error", "Download Error Exception " + e.message)
//           return Result.failure()
//        }
//    }
//
//
//}