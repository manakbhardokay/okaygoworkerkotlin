package com.okaygo.worker.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.okaygo.worker.R
import com.okaygo.worker.adapters.SliderPagerAdapter
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.ui.activity.login_onboarding.LoginOnBoardingActivity
import com.openkey.guest.help.Preferences
import kotlinx.android.synthetic.main.fragment_tutorial.*
import java.util.*

class TutorialFragment : BaseFragment(), View.OnClickListener {
    private val sliderRunnable: Runnable = Runnable { run { viewpager?.currentItem = (1) } }
    private var sliderLayouts: IntArray? = null
    private var mSliderPagerAdapter: SliderPagerAdapter? = null
    private var dots: Array<TextView?>? = null
    private var handler: Handler? = null
    var referrerClient: InstallReferrerClient? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sliderLayouts = intArrayOf(
            R.layout.job_flexibility
//            R.layout.earn_more
        )

        mSliderPagerAdapter = SliderPagerAdapter(activity, sliderLayouts)
        viewpager?.adapter = mSliderPagerAdapter
        addBottomDots(0)
        handler = Handler()
        handler?.postDelayed(sliderRunnable, 4000)
        viewpager?.addOnPageChangeListener(viewPagerPageChangeListener)
        connectToGoogelPlayServiceForAnalyticsUTMCodes()
        setListeners()
    }

    private fun setListeners() {
        btnNext?.setOnClickListener(this)
    }

    //  viewpager change listener
    var viewPagerPageChangeListener: ViewPager.OnPageChangeListener = object :
        ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            addBottomDots(position)
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
        override fun onPageScrollStateChanged(arg0: Int) {}
    }

    // add custom dot indigatores
    private fun addBottomDots(currentPage: Int?) {
        dots = arrayOfNulls(sliderLayouts?.size ?: 0)
        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)
        layoutDots?.removeAllViews()
        for (i in 0..(dots?.size ?: 0) - 1) {
            dots!![i] = TextView(activity)
            dots!!.get(i)?.text = (Html.fromHtml("&#8226;"))
            dots!!.get(i)?.setTextSize(35f)
            dots!!.get(i)?.setTextColor(colorsInactive[currentPage ?: 0])
            layoutDots?.addView(dots?.get(i))
        }
        if (dots?.size ?: 0 > 0) dots?.get(currentPage ?: 0)
            ?.setTextColor(colorsActive[currentPage ?: 0])
    }

    override fun onDestroy() {
        super.onDestroy()
        handler?.removeCallbacks(sliderRunnable)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnNext -> {
                Preferences.prefs?.saveValue(Constants.IS_FIRST_TIME_LAUNCH, true)
                startActivity(Intent(activity, LoginOnBoardingActivity::class.java))
                activity?.finish()
            }
        }
    }


    fun connectToGoogelPlayServiceForAnalyticsUTMCodes() {
        referrerClient = activity?.let { InstallReferrerClient.newBuilder(it).build() }
        referrerClient?.startConnection(object : InstallReferrerStateListener {
            override fun onInstallReferrerSetupFinished(responseCode: Int) {
                when (responseCode) {
                    InstallReferrerClient.InstallReferrerResponse.OK -> {
                        try {
                            val response: ReferrerDetails? = referrerClient?.getInstallReferrer()
                            Log.e("JobResponse", response.toString() + "")
                            val referrerUrl: String? = response?.installReferrer
                            saveUTMCodesLocally(referrerUrl)
                            Log.e("referrerUrl", referrerUrl + "")
                            val referrerClickTime = response?.referrerClickTimestampSeconds
                            Log.e("referrerClickTime", referrerClickTime.toString() + "")
                            val appInstallTime = response?.installBeginTimestampSeconds
                            Log.e("appInstallTime", appInstallTime.toString() + "")
//                            val instantExperienceLaunched: Boolean = response?.getGooglePlayInstantParam()
//                            Log.d("instantExperience", instantExperienceLaunched.toString() + "")
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        Preferences.prefs?.saveValue(Constants.UTM_ACCESS_STATUS, true)
                    }
                    InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {
                    }
                    InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                    }
                }
            }

            override fun onInstallReferrerServiceDisconnected() {
                Log.d("Play Disconnected", "Starting the request again.")
                if (referrerClient != null) {
                    referrerClient?.endConnection()
                }
                connectToGoogelPlayServiceForAnalyticsUTMCodes()
            }
        })
    }

    /**
     * save all utm data to local
     */
    fun saveUTMCodesLocally(code_string: String?) {
        val codes_with_values: Array<String>? = code_string?.split("&".toRegex())?.toTypedArray()
        val Codes = HashMap<String, String?>()
        if (codes_with_values != null) {
            for (i in codes_with_values) {
                val key_value = i.split("=".toRegex()).toTypedArray()
                Codes[key_value[0]] = key_value[1]
            }
        }
        if (Codes.containsKey("utm_source")) {
            Preferences.prefs?.saveValue(Constants.UTM_SOURCE, Codes["utm_source"])
        } else {
            Preferences.prefs?.saveValue(Constants.UTM_SOURCE, "unknown")
        }
        if (Codes.containsKey("utm_medium")) {
            Preferences.prefs?.saveValue(Constants.UTM_MEDIUM, Codes["utm_medium"])
        } else {
            Preferences.prefs?.saveValue(Constants.UTM_MEDIUM, "unknown")
        }
        if (Codes.containsKey("utm_content")) {
            Preferences.prefs?.saveValue(Constants.UTM_CONTENT, Codes["utm_content"])
        } else {
            Preferences.prefs?.saveValue(Constants.UTM_CONTENT, "unknown")
        }
        if (Codes.containsKey("utm_campaign")) {
            Preferences.prefs?.saveValue(Constants.UTM_CAMPAIGN, Codes["utm_campaign"])
        } else {
            Preferences.prefs?.saveValue(Constants.UTM_CAMPAIGN, "unknown")
        }
        if (Codes.containsKey("utm_term")) {
            Preferences.prefs?.saveValue(Constants.UTM_TERM, Codes["utm_term"])
        } else {
            Preferences.prefs?.saveValue(Constants.UTM_TERM, "unknown")
        }
        if (Codes.containsKey("anid")) {
            Preferences.prefs?.saveValue(Constants.ANID, Codes["anid"])
        } else {
            Preferences.prefs?.saveValue(Constants.ANID, "unknown")
        }
    }
}