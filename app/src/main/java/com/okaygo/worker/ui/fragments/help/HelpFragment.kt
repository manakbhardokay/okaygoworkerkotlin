package com.okaygo.worker.ui.fragments.help

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.freshchat.consumer.sdk.*
import com.freshchat.consumer.sdk.exception.MethodNotAllowedException
import com.google.firebase.messaging.FirebaseMessaging
import com.okaygo.worker.R
import com.okaygo.worker.adapters.FaqAdapter
import com.okaygo.worker.adapters.HelpVideoAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.FAQ
import com.okaygo.worker.data.modal.reponse.Youtube
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.NavigationActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.HelpModel
import kotlinx.android.synthetic.main.fragment_help.*
import java.util.*
import kotlin.collections.ArrayList

class HelpFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: HelpModel

    private var mFaqAdapter: FaqAdapter? = null
    private var mFaqList: ArrayList<FAQ>? = null

    private var mVideoAdapter: HelpVideoAdapter? = null
    private var mVideoList: ArrayList<Youtube>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HelpModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_help, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OkayGoFirebaseAnalytics.clickOnHelp()
        Constants.BOTTOM_MENU_ITEM_SELECTED = 4
        setListeners()
        txtVesrion?.text = "V ${Utilities.getAppVersion(activity)}"
        viewModel.getYoutubeVideos()
        activity?.let {
            Freshchat.getInstance(it)
                .getUnreadCountAsync { freshchatCallbackStatus, unreadCount ->
                    if (unreadCount > 0) {
                        txtCount?.visibility = View.VISIBLE
                        txtCount?.text = unreadCount.toString()
                    }
                }
        }
    }

    private fun setListeners() {
        txtTermCondition?.setOnClickListener(this)
        txtPolicy?.setOnClickListener(this)
        txtChat?.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getFaq()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtChat -> chatClickHandling()
            R.id.txtTermCondition -> {
                Utilities.showAlertTermsPolicy(
                    activity,
                    resources.getString(R.string.tc),
                    resources.getString(R.string.tc_all)
                )
            }
            R.id.txtPolicy -> {
//                Utilities.showAlertTermsPolicy(
//                    activity,
//                    resources.getString(R.string.pp),
//                    resources.getString(R.string.pp_text)
//                )

                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.WEBVIEW)
                intent.putExtra(Constants.WEB_URL, "https://www.okaygo.in/policy/")
                startActivity(intent)
            }
        }
    }

    private fun chatClickHandling() {
        activity?.let {
            val freshchatConfig: FreshchatConfig? = FreshchatConfig(
                resources.getString(R.string.freshchat_app_id),
                resources.getString(R.string.freshchat_app_key)
            )
            freshchatConfig?.isCameraCaptureEnabled = true
            freshchatConfig?.isGallerySelectionEnabled = true
            Freshchat.setImageLoader(object : FreshchatImageLoader {
                override fun load(
                    freshchatImageLoaderRequest: FreshchatImageLoaderRequest,
                    imageView: ImageView
                ) {
                    Glide.with(it).load(freshchatImageLoaderRequest.uri).into(imageView)
                }

                override fun get(freshchatImageLoaderRequest: FreshchatImageLoaderRequest): Bitmap? {
                    return null
                }

                override fun fetch(freshchatImageLoaderRequest: FreshchatImageLoaderRequest) {}
            })
            freshchatConfig?.let { it1 ->
                Freshchat.getInstance(it).init(
                    it1
                )
            }
            var token = ""
            FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
                token = result
            }
            Freshchat.getInstance(it).setPushRegistrationToken(token!!)
            viewModel.getUserDetail(userId)
        }

    }

    private fun setVideoAdapter() {
        mVideoAdapter = HelpVideoAdapter(
            activity,
            mVideoList,
            onVideoClick
        )
        val linearLayoutManager = LinearLayoutManager(activity)
        recyclerVideos?.adapter = mVideoAdapter
        recyclerVideos?.layoutManager = linearLayoutManager
    }

    private val onVideoClick: (Youtube?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        OkayGoFirebaseAnalytics.clickOnHelpVideo(it?.file_name)
        val intent = Intent(activity, NavigationActivity::class.java)
        intent.putExtra(Constants.NAV_SCREEN, Constants.VIDEO_PLAYER)
        intent.putExtra(Constants.YOUTUBE_RESPONSE, it)
        activity?.startActivity(intent)
    }


    private val onFaqClick: (FAQ?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        val intent: Intent? = Intent(activity, NavigationActivity::class.java)
        intent?.putExtra(Constants.NAV_SCREEN, Constants.FAQ)
        Constants.FAQ_TITLE = it?.typeValue ?: ""
        Constants.FAQ_DESC = it?.typeDesc ?: ""
        startActivity(intent)
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseFaq.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    val lang: String? = Preferences.prefs?.getString(Constants.SELECTED_LANG, "en")
                    if (mFaqList == null) {
                        mFaqList = ArrayList()
                    } else {
                        mFaqList?.clear()
                    }
                    if (lang == "en") {
                        for (i in 0 until it.response?.content?.size!!) {
                            if (it.response?.content?.get(i).typeKey?.equals("en") == true) {
                                mFaqList?.add(it.response?.content?.get(i))
                            }
                        }
                    } else {
                        for (i in 0 until it.response?.content?.size!!) {
                            if (it.response?.content?.get(i).typeKey?.equals("hi") == true) {
                                mFaqList?.add(it.response?.content?.get(i))
                            }
                        }
                    }
                    mFaqAdapter = FaqAdapter(activity, mFaqList, onFaqClick)
                    val linearLayoutManager = LinearLayoutManager(activity)
                    recylerrFaq?.setAdapter(mFaqAdapter)
                    recylerrFaq?.setLayoutManager(linearLayoutManager)
                }
            }
        })

        viewModel.responseYouetube.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    if (it.code == Constants.SUCCESS) {
                        progressVideo?.visibility = View.GONE
                        recyclerVideos?.visibility = View.VISIBLE
                        if (mVideoList == null) {
                            mVideoList = ArrayList()
                        } else {
                            mVideoList?.clear()
                        }
                        it.response?.let { it1 -> mVideoList?.addAll(it1) }
                        setVideoAdapter()
                    }
                }
            }
        })

        viewModel.responseUserDetail.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    if (it.code == Constants.SUCCESS) {
                        val freshUser = activity?.let { it1 -> Freshchat.getInstance(it1).user }
                        freshUser?.firstName = it.response?.content?.get(0)?.firstName
                        freshUser?.lastName = it.response?.content?.get(0)?.lastName
                        freshUser?.setPhone(
                            "+91",
                            it.response?.content?.get(0)?.phoneNumber
                        )
                        try {
                            activity?.let { it1 -> Freshchat.getInstance(it1).user = freshUser!! }
                        } catch (e: MethodNotAllowedException) {
                            e.printStackTrace()
                        }

                        val userMeta: MutableMap<String, String>? = HashMap()
                        userMeta?.put("User Type", "Worker")
                        userMeta?.put("Gender", it.response?.content?.get(0)?.gender ?: "")
                        userMeta?.put(
                            "User Id",
                            it.response?.content?.get(0)?.userId?.toString() ?: ""
                        )
                        userMeta?.put("Worker Id", workerId?.toString() ?: "")

                        try {
                            userMeta?.let { it1 ->
                                activity?.let { it2 ->
                                    Freshchat.getInstance(it2)
                                        .setUserProperties(it1)
                                }
                            }
                        } catch (e: MethodNotAllowedException) {
                            e.printStackTrace()
                        }

                        val tags: MutableList<String> =
                            java.util.ArrayList()
                        tags.add("Og_worker")
                        val options = ConversationOptions()
                            .filterByTags(tags, "OkayGo Support")
                        activity?.let { it1 ->
                            Freshchat.getInstance(it1)
                                .identifyUser(userId?.toString() ?: "", null)
                        }
                        activity?.let { it1 -> Freshchat.showConversations(it1, options) }
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}