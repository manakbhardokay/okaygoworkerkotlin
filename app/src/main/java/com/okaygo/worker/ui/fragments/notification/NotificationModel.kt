package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.NotificationResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class NotificationModel(app: Application) : MyViewModel(app) {
    var responseNotification = MutableLiveData<NotificationResponse>()
    var responseDelNotification = MutableLiveData<NotificationResponse>()
    var responseAssignedDetail = MutableLiveData<SuccessResponse>()

    /**
     * getNotifications for particular user
     */
    fun getNotifications(userId: Int?, isAlert: Int?) {
        isLoading.value = true
        NotificationRepository.getNotifications({
            isLoading.value = false
            responseNotification.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, isAlert)
    }

    /**
     * deleteNotification for particular user
     */
    fun deleteNotification(userId: Int?, notiId: Int?) {
        isLoading.value = true
        NotificationRepository.deleteNotification({
            isLoading.value = false
            responseDelNotification.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, notiId)
    }

    fun getAssignedJobDetail(userId: Int?, jobDetailId: Int?) {
        isLoading.value = true
        NotificationRepository.getAssignedJobDetail({
            isLoading.value = false
            responseAssignedDetail.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, jobDetailId)
    }
}