package com.okaygo.worker.ui.fragments.forgot_pass

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ForgotPasswordContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.ForgotPasswordHandler
import com.okaygo.worker.R
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import java.security.InvalidParameterException

class ForgotPassword : BaseFragment(), View.OnClickListener {
    private var isPasswordVisible = false
    private var username: String? = null
    private var mForgotPasswordContinuation: ForgotPasswordContinuation? = null

    /**
     * forgot password handler on cognito
     */
    var forgotPasswordHandler: ForgotPasswordHandler = object : ForgotPasswordHandler {
        override fun onSuccess() {
            Utilities.hideLoader()
            Toast.makeText(activity, "Password changed!", Toast.LENGTH_SHORT).show()
            activity?.onBackPressed()
        }

        override fun getResetCode(forgotPasswordContinuation: ForgotPasswordContinuation) {
            mForgotPasswordContinuation = forgotPasswordContinuation
        }

        override fun onFailure(e: Exception) {
            Utilities.hideLoader()
            if (e is InvalidParameterException) {
                showDialogMessage("Forgot password failed", AuthHelper.formatException(e))
            } else {
                showDialogMessage("Forgot password failed", AuthHelper.formatException(e))
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle: Bundle? = arguments
        username = bundle?.getString(Constants.USERNAME_FORGOT)
        txtOtpTitle?.setText(R.string.otp)
        setListener()
        forgotpasswordUser()
    }

    private fun setListener() {
        imgEye?.setOnClickListener(this)
        txtChangePass?.setOnClickListener(this)
    }

    private fun forgotpasswordUser() {
//        Utilities.showLoader(activity)
        AuthHelper.getPool().getUser(username)
            .forgotPasswordInBackground(forgotPasswordHandler)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgEye -> {
                if (isPasswordVisible) {
                    isPasswordVisible = false
                    imgEye?.setImageResource(R.drawable.ic_eye_cl)
                    edtNewPass?.setTransformationMethod(PasswordTransformationMethod.getInstance())
                } else {
                    isPasswordVisible = true
                    imgEye?.setImageResource(R.drawable.ic_eye_op)
                    edtNewPass?.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
                }
            }
            R.id.txtChangePass -> {
                val otp = edtOtp?.text?.toString()?.trim()
                val newPass = edtNewPass?.text?.toString()?.trim()
                val confPass = edtConfPass?.text?.toString()?.trim()
                if (otp?.length != 6) {
                    if (otp?.length == 0) {
                        edtOtp?.setError("Enter OTP")
                    } else {
                        edtOtp?.setError("Invalid OTP")
                    }
                }

                if (newPass?.length ?: 0 >= 6 && newPass?.length ?: 0 <= 12
                    && confPass?.length ?: 0 >= 6 &&
                    confPass?.length ?: 0 <= 12 &&
                    confPass?.length == newPass?.length
                ) {
                    if (newPass.equals(confPass) && otp?.length == 6) {
                        Utilities.showLoader(activity)
                        mForgotPasswordContinuation?.setPassword(newPass)
                        mForgotPasswordContinuation?.setVerificationCode(otp)
                        mForgotPasswordContinuation?.continueTask()
                    } else {
                        edtConfPass?.setError("Password did not match")
                    }
                } else if (newPass?.length ?: 0 < 6) {
                    edtConfPass?.setError("Password Length 6 to 12 is required")
                } else if (confPass?.length ?: 0 < 6) {
                    edtConfPass?.setError("Password did not match")
                } else if (confPass?.length != newPass?.length) {
                    edtConfPass?.setError("Password did not match")
                }
            }
        }
    }

    private fun showDialogMessage(title: String, body: String) {
        var alert: AlertDialog? = null
        val builder = activity?.let { AlertDialog.Builder(it) }
        builder?.setTitle(title)?.setMessage(body)?.setNeutralButton("OK") { dialog, which ->
            try {
                alert?.dismiss()
            } catch (e: java.lang.Exception) {
            }
        }
        alert = builder?.create()
        alert?.show()
    }

}
