package com.okaygo.worker.ui.fragments.upcoming

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.okaygo.worker.R
import com.okaygo.worker.adapters.MyJobAdapter
import com.okaygo.worker.data.modal.reponse.AppliedJob
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.NavigationActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.ui.fragments.verification.AppliedModel
import com.openkey.guest.ui.fragments.verification.JobDetailModel
import com.openkey.guest.ui.fragments.verification.UpcomingModel
import kotlinx.android.synthetic.main.fragment_applied_job.*

class UpcomingFragment(val tabs: TabLayout?) : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: UpcomingModel
    private lateinit var viewModelApplied: AppliedModel
    private lateinit var viewModelJobDetailModel: JobDetailModel

    private var mAdapter: MyJobAdapter? = null
    private var mJobList: ArrayList<AppliedJob>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(UpcomingModel::class.java)
        viewModelApplied = ViewModelProvider(this).get(AppliedModel::class.java)
        viewModelJobDetailModel = ViewModelProvider(this).get(JobDetailModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_applied_job, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()

    }

    private fun setListeners() {
        swipeRefresh?.setOnRefreshListener {
            viewModel.getUpcomingJobs(userId)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUpcomingJobs(userId)
    }

    private fun setAdapter() {
        val linearLayoutManager = LinearLayoutManager(context)
        mAdapter = MyJobAdapter(
            activity, 2, mJobList, onInterviewTimeClick, onJobDetailClick,
            onJobRemoveClick, onInterviewDetailClick, onJoiningDetailClick, onEnterOtpClick,
            onAddressClick, onYesClick, onNoClick, onDirectionClick, onConstraintClick
        )
        recylerAppliedJob?.layoutManager = linearLayoutManager
        recylerAppliedJob?.adapter = mAdapter
    }

    private val onConstraintClick: (AppliedJob?, Int?) -> Unit = { it, currentStatus ->
        Log.e("Pstion", currentStatus.toString())
        if (it?.workType.equals("On Demand")) {
            val gson = Gson()
            val intent = Intent(context, NavigationActivity::class.java)
            intent.putExtra(Constants.NAV_SCREEN, Constants.INTERVIEW_SCHEDUAL_OD)
            intent.putExtra(Constants.NAVIGATION_DATA, it)
            activity?.startActivity(intent)
        }
    }
    private val onDirectionClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Utilities.openAddressOnMap(
            activity,
            it?.address ?: (it?.locationLat + ", " + it?.locationLong)
        )
    }
    private val onNoClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }
    private val onYesClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }
    private val onAddressClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        val lat = it?.locationLat
        val lng = it?.locationLong
        activity?.let {
            if (ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(context, "Permission not Granted.", Toast.LENGTH_SHORT).show()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                        201
                    )
                } else {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ),
                        202
                    )
                }
            } else {
                Utilities.openAddressOnMap(
                    it,
                    lat + "," + lng
                )
            }
        }
    }
    private val onEnterOtpClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }
    private val onJoiningDetailClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }
    private val onInterviewTimeClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        viewModelApplied.getInterviewTimeSlot(it?.jobDetailId, userId)
    }

    private val onInterviewDetailClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }

    private val onJobDetailClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        val intent: Intent? = Intent(activity, NavigationActivity::class.java)
        intent?.putExtra(Constants.NAV_SCREEN, Constants.JOB_DETAIL)
        intent?.putExtra(Constants.JOB_DETAIL_ID, it?.jobId?.toString())
        startActivity(intent)
    }

    private val onJobRemoveClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        viewModelApplied.deleteJob(it?.assignId)
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseUpcomingJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    swipeRefresh?.isRefreshing = false
                    if (it.response?.content?.isNotEmpty() == true) {
                        recylerAppliedJob?.setVisibility(View.VISIBLE)
                        imgNoData?.setVisibility(View.GONE)
                        txtWarning?.setVisibility(View.GONE)
                        if (mJobList == null) {
                            mJobList = ArrayList()
                        } else {
                            mJobList?.clear()
                        }
                        mJobList?.addAll(it.response.content)
                        tabs?.getTabAt(1)?.text = "Upcoming (${mJobList?.size})"
                        setAdapter()
                        mAdapter?.notifyDataSetChanged()
                    } else {
                        tabs?.getTabAt(1)?.text = "Upcoming"

                        imgNoData?.setVisibility(View.VISIBLE)
                        imgNoData?.setImageResource(R.drawable.ic_no_records)
                        txtWarning?.setText("No jobs found")
                        txtWarning?.setVisibility(View.VISIBLE)
                        recylerAppliedJob?.setVisibility(View.GONE)
                    }
                }
            }
        })
        viewModelJobDetailModel.responseJobDetail.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                }
            }
        })
        viewModelApplied.responseDelete.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.getUpcomingJobs(userId, false)
                }
            }
        })
        viewModelApplied.responseNotJoining.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Utilities.showToast(context, "Job Rejected.")
                    viewModel.getUpcomingJobs(userId)
//                    UpdateTabLabel()
                }
            }
        })

        viewModelApplied.responseOfferAccpted.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
//                    val salary: Int = job_offered_amount.toInt()
//                    okayGoFirebaseAnalytics.on_accept_job_offer(
//                        job_details_id,
//                        job_type,
//                        "screen",
//                        (salary * 0.36).toInt()
//                    )
                    Utilities.showSuccessToast(activity, "Job Accepted.")
                    viewModel.getUpcomingJobs(userId)
                    Utilities.jobSuccessDialog(
                        context as Activity?,
                        context?.resources?.getString(R.string.job_conf),
                        context?.resources?.getString(R.string.applied_confrmd_msg),
                        false, onOKClick
                    )

//                    UpdateTabLabel()
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelJobDetailModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModelJobDetailModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelApplied.apiError.observe(this, Observer {
            if (isAdded) {
                swipeRefresh?.isRefreshing = false
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModelApplied.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                swipeRefresh?.isRefreshing = false
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private val onOKClick: () -> Unit = {
    }
}