package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.QuestionareResponse
import com.okaygo.worker.data.modal.reponse.SaveQuestionareResponse
import com.okaygo.worker.data.modal.request.QuestionareRequestItem
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class QuestionareModel(app: Application) : MyViewModel(app) {
    var responseJobQuestions = MutableLiveData<QuestionareResponse>()
    var responseSaveJobQuestions = MutableLiveData<SaveQuestionareResponse>()


    /**
     *get referral header data from server
     */
    fun getJobQuestions(jobId: Int?) {
        isLoading.value = true
        QuestionareRepository.getJobQuestions({
            isLoading.value = false
            responseJobQuestions.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, jobId)
    }

    /**
     *get referral header data from server
     */
    fun saveJobQuestions(
        jobId: Int?,
        jobDetailId: Int?,
        userId: Int?,
        list: ArrayList<QuestionareRequestItem?>
    ) {
        isLoading.value = true
        QuestionareRepository.saveJobQuestions({
            isLoading.value = false
            responseSaveJobQuestions.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, jobId, jobDetailId, userId, list)
    }
}