package com.okaygo.worker.ui.fragments.rating_review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.okaygo.worker.R
import com.okaygo.worker.adapters.RatingReviewAdtapter
import com.okaygo.worker.data.modal.reponse.Reviews
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_rating_review.*

class RatingReviewFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: RatingModel
    private var mList: ArrayList<Reviews>? = null
    private var mAdapter: RatingReviewAdtapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RatingModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_rating_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getRatingReviews(workerId)
        setListeners()
    }

    /**
     * set click listeners for required views
     */
    private fun setListeners() {
        imgBackIcon?.setOnClickListener(this)
    }

    /**
     * set adapter for referral list outer part
     */
    private fun setAdapter() {
        val linearLayoutManager = LinearLayoutManager(activity)
        mAdapter =
            RatingReviewAdtapter(
                activity,
                mList
            )
        recyclerRating?.layoutManager = linearLayoutManager
        recyclerRating?.adapter = mAdapter
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseRatingReview.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.reviews?.content?.isEmpty() == false) {
                        txtTotalRating?.text = String.format("%.1f", it.response.averageRating)
                        ratingBarTotal?.rating = it.response.averageRating?.toFloat() ?: 0f
                        txtRatingTag?.text =
                            "based on ${it.response?.reviews?.content?.size} reviews"
                        linearNoResult?.visibility = View.GONE
                        txtTotalRating?.visibility = View.VISIBLE
                        ratingBarTotal?.visibility = View.VISIBLE
                        txtRatingTag?.visibility = View.VISIBLE
                        recyclerRating?.visibility = View.VISIBLE

                        if (mList == null) {
                            mList = ArrayList()
                        } else {
                            mList?.clear()
                        }
                        mList?.addAll(it.response.reviews.content)
                        setAdapter()
                    } else {
                        txtTotalRating?.visibility = View.GONE
                        ratingBarTotal?.visibility = View.GONE
                        txtRatingTag?.visibility = View.GONE
                        recyclerRating?.visibility = View.GONE
                        linearNoResult?.visibility = View.VISIBLE
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBackIcon -> activity?.onBackPressed()
        }
    }
}