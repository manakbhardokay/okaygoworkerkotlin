package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.CliamResponse
import com.okaygo.worker.data.modal.reponse.RaiseEnquiryResponse
import com.okaygo.worker.data.modal.reponse.ReferralDataResponse
import com.okaygo.worker.data.modal.reponse.ReferralWorkerDataResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object ReferralRepository {
    private val mService = ApiHelper.getService()


    /**
     *get refer jobs data from server
     */
    fun getReferralData(
        successHandler: (ReferralDataResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getReferralData(userId)?.enqueue(object : Callback<ReferralDataResponse> {
            override fun onResponse(
                call: Call<ReferralDataResponse>?,
                response: Response<ReferralDataResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<ReferralDataResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("referral failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *get refer worker list from server
     */
    fun getReferralWorkerData(
        successHandler: (ReferralWorkerDataResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        jobId: Int?
    ) {
        mService.getReferralWorkerData(userId, jobId)
            ?.enqueue(object : Callback<ReferralWorkerDataResponse> {
                override fun onResponse(
                    call: Call<ReferralWorkerDataResponse>?,
                    response: Response<ReferralWorkerDataResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<ReferralWorkerDataResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("referral failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *claim referral amount
     */
    fun claimReferral(
        successHandler: (CliamResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        referrBy: Int?,
        claimAmount: Int?,
        jobId: Int?
    ) {
        mService.claimReferral(userId, referrBy, claimAmount, jobId)
            ?.enqueue(object : Callback<CliamResponse> {
                override fun onResponse(
                    call: Call<CliamResponse>?,
                    response: Response<CliamResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<CliamResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("referral failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *raise Enquiry for particular referral
     */
    fun raiseEnquiry(
        successHandler: (RaiseEnquiryResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        comment: String?,
        request: String?,
        referralId: String?
    ) {
        mService.raiseEnquiry(userId, comment, request, referralId)
            ?.enqueue(object : Callback<RaiseEnquiryResponse> {
                override fun onResponse(
                    call: Call<RaiseEnquiryResponse>?,
                    response: Response<RaiseEnquiryResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<RaiseEnquiryResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("RaiseEnquiry failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }
}