package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.AllJobRoleResponse
import com.okaygo.worker.data.modal.reponse.WorkerDetailResponse
import com.okaygo.worker.data.modal.request.UpdateWorkerRequest
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class ExperinceModel(app: Application) : MyViewModel(app) {
    var responseSaveData = MutableLiveData<WorkerDetailResponse>()
    var responseAllJobRole = MutableLiveData<AllJobRoleResponse>()

    /**
     *
     */
    fun saveWorkerData(workerId: Int?, request: UpdateWorkerRequest?) {
        isLoading.value = true
        ExperienceRepository.saveWorkerData({
            isLoading.value = false
            responseSaveData.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId, request)
    }

    /**
     *
     */
    fun getAllJobRole() {
        isLoading.value = true
        ExperienceRepository.getAllJobRole({
            isLoading.value = false
            responseAllJobRole.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        })
    }

}