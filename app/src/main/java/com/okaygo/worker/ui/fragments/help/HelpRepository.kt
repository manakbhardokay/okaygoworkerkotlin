package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.FaqResponse
import com.okaygo.worker.data.modal.reponse.UserDetailResponse
import com.okaygo.worker.data.modal.reponse.YoueTubeResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object HelpRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun getFaq(
        successHandler: (FaqResponse) -> Unit,
        failureHandler: (String) -> Unit
    ) {
        mService.getFaq()?.enqueue(object : Callback<FaqResponse> {
            override fun onResponse(
                call: Call<FaqResponse>?,
                response: Response<FaqResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<FaqResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("due failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun getYoutubeVideos(
        successHandler: (YoueTubeResponse) -> Unit,
        failureHandler: (String) -> Unit
    ) {
        mService.getYoutubeVideos()?.enqueue(object : Callback<YoueTubeResponse> {
            override fun onResponse(
                call: Call<YoueTubeResponse>?,
                response: Response<YoueTubeResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<YoueTubeResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("youtube failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun getUserDetail(
        successHandler: (UserDetailResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getUserDetail(userId)?.enqueue(object : Callback<UserDetailResponse> {
            override fun onResponse(
                call: Call<UserDetailResponse>?,
                response: Response<UserDetailResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<UserDetailResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("userDetail failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}