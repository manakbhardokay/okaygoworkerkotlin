package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.InAppResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class LoginModel(app: Application) : MyViewModel(app) {
    var response = MutableLiveData<InAppResponse>()

    fun getInAppData(userId: Int?) {
        isLoading.value = true
        SplashRepository.getInAppData({
            isLoading.value = false
            response.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }
}