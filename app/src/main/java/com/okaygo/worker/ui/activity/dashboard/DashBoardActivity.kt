package com.okaygo.worker.ui.activity.dashboard

import android.Manifest
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.bumptech.glide.Glide
import com.freshchat.consumer.sdk.*
import com.freshchat.consumer.sdk.exception.MethodNotAllowedException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.okaygo.worker.R
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.data.modal.BottomNavScreenChange
import com.okaygo.worker.data.modal.CheckOdJob
import com.okaygo.worker.data.modal.LocatonPermissionOD
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.TrackingUtility
import com.okaygo.worker.help.utils.TrackingUtility.POWERMANAGER_INTENTS
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.services.location.LocationTrackerService
import com.okaygo.worker.ui.activity.BaseActivity
import com.okaygo.worker.ui.activity.NavigationActivity
import com.openkey.guest.help.Dialogs
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.DashBoardModel
import com.openkey.guest.ui.fragments.verification.HelpModel
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DashBoardActivity : BaseActivity(), EasyPermissions.PermissionCallbacks {
    private lateinit var viewModel: DashBoardModel
    private lateinit var viewModelHelp: HelpModel
    private var isBackClicked = false
    private var lastTime: Long? = 0L
    private var mNavController: NavController? = null
    private var mCurrentScreen: Int? = 0
    private var mToken: String? = null
    private var alreadyNumber: String? = null                   //already subscribed
    private var notificationBadge: AppCompatTextView? = null
    private var isNotificationCalled = false
    private var companyLatLng: String? = null
    private var mJobId: Int? = 0
    private var mEta: String? = "Calculating..."
    private var mDistance: String? = "Calculating..."
    private var mLat: Double? = 0.0
    private var mLng: Double? = 0.0
    private var mSpeed: Float? = 0.0f
    private val REQUEST_CODE_LOCATION = 100

    private var isGpsDialogShowed: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        viewModel = ViewModelProvider(this).get(DashBoardModel::class.java)
        viewModelHelp = ViewModelProvider(this).get(HelpModel::class.java)
        attachObservers()
        mNavController = Navigation.findNavController(this, R.id.nav_host_fragment)

        mNavController?.let {
            NavigationUI.setupWithNavController(nav_view, it)
        }
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        try {
            Preferences.prefs?.saveValue(
                Constants.ACCESS_TOKEN,
                AuthHelper.getCurrSession().accessToken.jwtToken
            )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            } else {
                mToken = task.result
                Preferences.prefs?.saveValue(Constants.FCM_TOKEN, mToken)
                Log.e("FCM Token", mToken ?: "")
            }
        })

        val isWhatsAppDialogShown: Boolean? =
            Preferences.prefs?.getBoolean(Constants.IS_WHATSAPP_SUB_DIALOG, false)
        if (isWhatsAppDialogShown == false) {
            viewModel.getWhatsAppSubscription(Preferences.prefs?.getInt(Constants.ID, 0))
        }
        handleChat()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.home_tool_bar, menu)
        val menuItem = menu.findItem(R.id.navigation_notification)
        val actionView = menuItem.actionView
        if (actionView != null) {
            notificationBadge = actionView?.findViewById<AppCompatTextView>(R.id.cart_badge)
            notificationBadge?.setVisibility(View.GONE)
            viewModel.getNotificationCount(Preferences.prefs?.getInt(Constants.ID, 0))
            actionView.setOnClickListener { onOptionsItemSelected(menuItem) }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.navigation_notification) {
//            mOkayGoFirebaseAnalytics.inapp_alert_view()
            val intent: Intent? = Intent(this, NavigationActivity::class.java)
            intent?.putExtra(Constants.NAV_SCREEN, 11)
            startActivity(intent)
            return true
        }
        if (id == R.id.navigation_settings) {
//            mOkayGoFirebaseAnalytics.availability_view(screenName())
            val intent = Intent(this, NavigationActivity::class.java)
            intent?.putExtra(Constants.NAV_SCREEN, 12)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isBackClicked) {
            super.onBackPressed()
            return
        }
        isBackClicked = true
        Utilities.showSuccessToast(this, "Please click BACK again to exit.")
        Handler().postDelayed({ isBackClicked = false }, 2000)
    }


    private val onAlertDenyClick: () -> Unit = {

    }

    private val onAlertAllowBtnClick: () -> Unit = {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            5011
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                12121
            )
        }
    }

    fun locationPermisiion(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Dialogs.showAlertDialog(
                    this,
                    "Alert!",
                    "This app collects location data so as to allow tracking your location before job start time even if the app is in background.",
                    "Allow",
                    "Deny",
                    onAlertAllowBtnClick,
                    onAlertDenyClick
                )


//                else {
//                    ActivityCompat.requestPermissions(
//                        this,
//                        arrayOf(
//                            Manifest.permission.ACCESS_FINE_LOCATION,
//                            Manifest.permission.ACCESS_COARSE_LOCATION
//                        ),
//                        5011
//                    )
//                }
                return false
            }
            return true
        }
        return true

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            5011 -> {
                var isPermission = false
                var foreground = false
                var background = false
                Log.e("Permission size", permissions?.size?.toString() ?: "")
                for (i in permissions.indices) {
                    if ((permissions[i].equals(
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            ignoreCase = true
                        )) || (permissions[i].equals(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            ignoreCase = true
                        ))
                    ) {
                        isPermission = true
                        //foreground permission allowed
                    }
                    if (permissions[i].equals(
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                            ignoreCase = true
                        )
                    ) {
                        isPermission = true

                    }

//                    requestBackgroundPermission()

                }
                if (isPermission) {
                    EventBus.getDefault().post(LocatonPermissionOD())
                }
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLoactionEvent(event: Location) {
        Log.e("Event", event?.latitude?.toString())
        mLat = event.latitude
        mLng = event.longitude
        mSpeed = event.speed

//        var locationA: Location = Location("Point A")
//        locationA.setLatitude(event?.latitude ?: 0.0);
//        locationA.setLongitude(event?.longitude ?: 0.0);
//
//        var locationB = Location("Point B");
//        locationB.setLatitude(event?.latitude ?: 0.0);
//        locationB.setLongitude(event?.longitude ?: 0.0);

//        var distance = locationA.distanceTo(locationB);
//        Utilities.showToast(this, "distance in m :: $distance")
//        val locationData: LocationDTO? = LocationDTO(
//            Preferences.prefs?.getInt(Constants.ID, 0),
//            Preferences.prefs?.getString(Constants.userName, ""),
//            event.latitude,
//            event.longitude,
//            event.speed,
//            System.currentTimeMillis(),
//            mEta, mDistance
//        )
//        val diff = (System.currentTimeMillis() - (lastTime ?: 0L)).div(60000)
//        Log.e("Diff", diff.toString() + "")
//        if (diff > 4 || (lastTime ?: 0) == 0L) {
//            lastTime = System.currentTimeMillis()
//            viewModel.saveLocationForOD(
//                Preferences.prefs?.getInt(Constants.ID, 0),
//                event.latitude,
//                event.longitude,
//                event.speed,
//                Preferences.prefs?.getString(Constants.userName, ""),
//                System.currentTimeMillis(), mJobId, mDistance, mEta
//            )
//
//        val orgin = event.latitude.toString() + "," + event.longitude
//        Log.e("origin", orgin + "")
//        Log.e("companyLatLng", companyLatLng + "")
//        if (companyLatLng != null && companyLatLng?.equals("0.0") == false) {
//            viewModel.getDirectionDuration(
//                orgin,
//                companyLatLng,
//                resources.getString(R.string.map_api_key)
//            )
//        } else {
//            viewModel.saveLocationForOD(
//                Preferences.prefs?.getInt(Constants.ID, 0),
//                event.latitude,
//                event.longitude,
//                event.speed,
//                Preferences.prefs?.getString(Constants.userName, ""),
//                System.currentTimeMillis(), mJobId, mDistance, mEta
//            )
//        }
//        }
//
//        val ref =
//            FirebaseDatabase.getInstance().getReference("locations")
//                .child(Preferences.prefs?.getInt(Constants.ID, 0)?.toString() ?: "")
//
//        Log.e("location update", "location update $locationData")
//        ref.setValue(locationData)
    }

    private fun handleChat() {
//        Freshchat.getInstance(applicationContext)
//            .getUnreadCountAsync { freshchatCallbackStatus, unreadCount ->
//                if (unreadCount > 0) {
//                    txtCount.setText(Integer.toString(unreadCount))
//                    txtCount.setVisibility(View.VISIBLE)
//                } else {
//                    txtCount.setVisibility(View.GONE)
//                }
//            }
        floatChat.setOnClickListener {
            Log.e("Click", "chat")
            val freshchatConfig = FreshchatConfig(
                resources.getString(R.string.freshchat_app_id),
                resources.getString(R.string.freshchat_app_key)
            )
            freshchatConfig.isCameraCaptureEnabled = true
            freshchatConfig.isGallerySelectionEnabled = true
            //                Freshchat.setImageLoader(com.freshchat.consumer.sdk.j.af.aw(getContext()));
            Freshchat.setImageLoader(object : FreshchatImageLoader {
                override fun load(
                    freshchatImageLoaderRequest: FreshchatImageLoaderRequest,
                    imageView: ImageView
                ) {
                    Glide.with(this@DashBoardActivity).load(freshchatImageLoaderRequest.uri)
                        .into(imageView)
                }

                override fun get(freshchatImageLoaderRequest: FreshchatImageLoaderRequest): Bitmap? {
                    return null
                }

                override fun fetch(freshchatImageLoaderRequest: FreshchatImageLoaderRequest) {}
            })
            Freshchat.getInstance(applicationContext).init(freshchatConfig)
            var token = ""
            FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
                token = result
            }
            Freshchat.getInstance(this).setPushRegistrationToken(token ?: "")
            viewModelHelp.getUserDetail(Preferences.prefs?.getInt(Constants.ID, 0))
        }
    }


    override fun onResume() {
        super.onResume()
        Log.e("OnResume Called", "Dashboard")
        if (isNotificationCalled) {
            viewModel.getNotificationCount(Preferences.prefs?.getInt(Constants.ID, 0))
        }
        viewModel.updateAppRequest()
        Preferences.prefs?.saveValue(Constants.IS_LOGGED_IN, true)
        Log.e("Inside Screen", mCurrentScreen.toString() + "")
        handleBottomNav(0)
        handleDeeplink()
        getUpcomingOdJobs()
    }

    fun getUpcomingOdJobs() {
        Log.e("getUpcomingOdJobs", "called")
        viewModel.getUpcomingOdJobs(Preferences.prefs?.getInt(Constants.ID, 0))
    }

    private fun handleBottomNav(screen: Int?) {
        if (screen == 0) {
            mCurrentScreen = Preferences.prefs?.getInt(Constants.CURRENT_SCREEN, 0)
        } else {
            mCurrentScreen = screen
        }
        if (mCurrentScreen != 0) {
            when (mCurrentScreen) {
                1 -> mNavController?.navigate(R.id.navigation_home)
                2 -> mNavController?.navigate(R.id.navigation_payment)
                3 -> mNavController?.navigate(R.id.navigation_my_jobs)
                4 -> mNavController?.navigate(R.id.navigation_help)
                5 -> mNavController?.navigate(R.id.navigation_profile)
            }
            Preferences.prefs?.saveValue(Constants.CURRENT_SCREEN, 0)
        } else {
            Log.e("Outside", "screen")
        }
    }

    private fun handleDeeplink() {
        val jobId: String? = Preferences.prefs?.getString(Constants.JOB_ID, "")
        val jobDetailsId: String? = Preferences.prefs?.getString(Constants.JOB_DETAIL_ID, "")
        val wId: String? = Preferences.prefs?.getString(Constants.WORKER_ID, "")
        val appDirect: String? = Preferences.prefs?.getString(Constants.APP_DIRECT, "")
        val currentWorkerId: Int? = Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0)

        Preferences.prefs?.clearValue(Constants.IS_FOR_DEEPLINK)
        if (jobId?.isEmpty() == false) {
            if (wId?.isEmpty() == false) {
                if (wId.toInt() == currentWorkerId) {
                    if (appDirect?.isEmpty() == true || appDirect == Constants.DOWNLOAD_AND_APPLY) {
                        val intent: Intent? = Intent(this, NavigationActivity::class.java)
                        intent?.putExtra(Constants.NAV_SCREEN, Constants.JOB_DETAIL)
                        intent?.putExtra(Constants.JOB_ID, jobId)
                        startActivity(intent)
                    } else {
                        if (appDirect == Constants.INTERVIEW_DETAILS) {
                            Constants.IS_FOR_INTERVIEW = true
                            Constants.IS_DEEPLINK = 1
                        } else if (appDirect == Constants.OFFER_DETAIL) {
                            Constants.IS_FOR_INTERVIEW = false
                            Constants.IS_DEEPLINK = 3
                        } else if (appDirect == Constants.INTERVIEW_SLOT) {
                            Constants.IS_DEEPLINK = 2
                        }
                        mNavController?.navigate(R.id.navigation_my_jobs)
                    }
                }
            } else {
                val intent: Intent? = Intent(this, NavigationActivity::class.java)
                intent?.putExtra(Constants.NAV_SCREEN, Constants.JOB_DETAIL)
                intent?.putExtra(Constants.JOB_ID, jobId)
                startActivity(intent)
            }
        }
    }


    /**
     * handle api reposne
     */
    private fun attachObservers() {
        viewModel.responseAppUpdateResponse.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    try {
                        val updatedVersion: Int? =
                            it.response?.content?.version_code
                        val pInfo =
                            packageManager.getPackageInfo(packageName, 0)
                        val currentVersion = pInfo.versionCode
                        if ((updatedVersion ?: 0) > currentVersion) {
                            Constants.IS_UPDATE_DIALOG_DISPLAYED = true
                            Utilities.appUpdateAlert(
                                this,
                                it.response?.content?.is_force_update ?: false,
                                it.response?.content?.update_msg ?: ""
                            )
                        }
                    } catch (e: PackageManager.NameNotFoundException) {
                        e.printStackTrace()
                    }
                }
            }
        })

        viewModel.responseODJobs.observe(this, Observer {
            if (it?.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {
                for (i in 0..(it.response.content.size - 1)) {


                    val startDate = it.response.content.get(i).startDate
//                val startDate = "2020-09-28"
                    val loginTime = it.response.content.get(i).loginTime
                    val logOutTime = it.response.content.get(i).logoutTime


                    val currentTime = Calendar.getInstance().time
                    val sdf = SimpleDateFormat("HH:mm:ss")
                    val currentDateTimeString: String = sdf.format(currentTime)

                    val sTime = loginTime?.split(":")?.get(0)?.toInt()
                    val eTime = logOutTime?.split(":")?.get(0)?.toInt()
                    val cTime = currentDateTimeString.split(":").get(0).toInt()
                    Log.e("startDate", startDate?.toString() + "")
                    Log.e("loginTime", loginTime?.toString() + "")
                    Log.e("curent time", currentDateTimeString.toString() + "")

                    if (Utilities.isThisDateIsToday(startDate) == true) {
                        Log.e("today", "true")
                        if ((cTime >= (sTime ?: 1) - 1) && cTime < (eTime ?: cTime)) {
                            val companyLat = it.response.content.get(i).locationLat
                            val companyLng = it.response.content.get(i).locationLong
                            companyLatLng = companyLat + "," + companyLng
                            mJobId = it.response.content.get(i).jobId
                            Preferences.prefs?.saveValue(Constants.TRACK_JOB_ID, mJobId)

                            Preferences.prefs?.saveValue(Constants.JOB_DEST, companyLatLng)
                            Preferences.prefs?.saveValue(
                                Constants.MAP_KEY,
                                resources.getString(R.string.map_api_key)
                            )

//                            Dialogs.showAlertDialog(
//                                this,
//                                "Alert!",
//                                "OkayGo needs your location permission for live tracking to your OnDemand Job. Please click on 'Allow' button.",
//                                "Allow",
//                                "Deny",
//                                onAlertPosBtnClick,
//                                onAlertNagBtnClick
//                            )
                            startLocTracking()

                            break
                        } else
                            if ((cTime >= (eTime ?: 0) + 1)) {

                                stopCommandService()

                            }
                    } else {
                        stopCommandService()
                    }
                }
            }
        })

        viewModel.responseWhatsappSubscription.observe(this, Observer
        {
            if (it?.code == Constants.SUCCESS) {
                alreadyNumber = it?.response?.whatsappNumber
                if (it.response?.permission == null || it.response.permission == 0) {
                    showAlertForWhatsapp(alreadyNumber)
                }
            }
        })

        viewModel.responseODSaveLocation.observe(this, Observer
        {
            if (it?.code == Constants.SUCCESS) {

            }
        })

        viewModel.responseDirection.observe(this, Observer
        {
            if (it?.routes?.isEmpty() == false) {
                if (it.routes?.get(0)?.legs?.isEmpty() == false) {
                    mEta = it.routes?.get(0)?.legs?.get(0)?.duration?.text
                    mDistance = it.routes?.get(0)?.legs?.get(0)?.distance?.text
                }
                viewModel.saveLocationForOD(
                    Preferences.prefs?.getInt(Constants.ID, 0),
                    mLat,
                    mLng,
                    mSpeed,
                    Preferences.prefs?.getString(Constants.userName, ""),
                    System.currentTimeMillis(), mJobId, mDistance, mEta
                )
            }
        })

        viewModel.responseWhatsappSubscribed.observe(
            this, Observer
            {
                if (it?.code == Constants.SUCCESS) {
                    Preferences.prefs?.saveValue(Constants.IS_WHATSAPP_SUB_DIALOG, true)
                    if (it?.response?.message?.equals("SUCCESS", true) == true) {
                        Utilities.showToast(
                            this,
                            getString(R.string.whatsapp_subscribe_on) + it.response.whatsappNumber
                        )
                    } else {
                        Utilities.showToast(this, "Invalid user. WhatsApp subscription failed")
                    }
                }
            })

        viewModel.responseNotificatonCount.observe(this, Observer
        {
            if (it?.code == Constants.SUCCESS) {
                isNotificationCalled = true
                if (it.response?.toInt() ?: 0 > 0) {
                    notificationBadge?.visibility = View.VISIBLE
                    if ((it.response?.toInt()) ?: 0 > 99) {
                        notificationBadge?.text = "99+"
                    } else {
                        notificationBadge?.text = it.response ?: ""
                    }
                } else {
                    notificationBadge?.visibility = View.GONE
                }
            }
        })

        viewModelHelp.responseUserDetail.observe(this, Observer
        {
            if (it?.code == Constants.SUCCESS) {
                val freshUser = Freshchat.getInstance(applicationContext).user
                freshUser.firstName = it.response?.content?.get(0)?.firstName
                freshUser.lastName = it.response?.content?.get(0)?.lastName
                freshUser.setPhone(
                    "+91", it.response?.content?.get(0)?.phoneNumber
                )
                try {
                    Freshchat.getInstance(applicationContext).user = freshUser
                } catch (e: MethodNotAllowedException) {
                    e.printStackTrace()
                }
                val userMeta: MutableMap<String, String> =
                    HashMap()
                userMeta.put("User Type", "Worker")
                userMeta.put("Gender", it.response?.content?.get(0)?.gender?.toString() ?: "")
                userMeta.put("User Id", it.response?.content?.get(0)?.userId?.toString() ?: "")
                userMeta.put(
                    "Worker Id",
                    "" + Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0)?.toString()
                )

                try {
                    Freshchat.getInstance(applicationContext).setUserProperties(userMeta)
                } catch (e: MethodNotAllowedException) {
                    e.printStackTrace()
                }
                val tags: MutableList<String> = ArrayList()
                tags.add("Og_worker")
                val options = ConversationOptions()
                    .filterByTags(tags, "OkayGo Support")
//                        Freshchat.showConversations(getContext(), options);
                //                        Freshchat.showConversations(getContext(), options);
                Freshchat.getInstance(applicationContext)
                    .identifyUser(
                        Preferences.prefs?.getInt(Constants.ID, 0)?.toString() ?: "",
                        null
                    )
                Freshchat.showConversations(applicationContext, options)
            }
        })

        viewModel.apiError.observe(this, Observer
        {
            it?.let {
                Utilities.showToast(this, it)
            }
        })

        viewModel.isLoading.observe(this, Observer
        {
            it.let {
                if (it == true) {
                    Utilities.showLoader(this)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private fun showAlertForWhatsapp(mobile: String?) {
        // Create custom dialog object
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_whatsapp_subscription)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // Set dialog title

        // set values for custom dialog components - text, image and button
        val subscribe: AppCompatTextView = dialog.findViewById(R.id.txtSubscribe)
        val edtMobile: AppCompatEditText = dialog.findViewById(R.id.edtMobile)
        val close: AppCompatImageView = dialog.findViewById(R.id.imgClose)
        if (mobile == null || mobile.isEmpty()) {
            edtMobile.setText(Preferences.prefs?.getString(Constants.MOBILE_NO, ""))
        } else {
            edtMobile.setText(mobile)
        }
        dialog.setCancelable(false)
        dialog.show()

        // if decline button is clicked, close the custom dialog
        close.setOnClickListener { // Close dialog
            Preferences.prefs?.saveValue(Constants.IS_WHATSAPP_SUB_DIALOG, true)
            dialog.dismiss()
        }

        // if decline button is clicked, close the custom dialog
        subscribe.setOnClickListener { // Close dialog
            val mobile = edtMobile.text.toString().trim { it <= ' ' }
            if (mobile.isEmpty()) {
                Utilities.showToast(this, resources.getString(R.string.enter_number))
            }
            if (mobile.length < 10) {
                Utilities.showToast(this, getString(R.string.enter_correct_number))
            } else {
                viewModel.whatsAppSubscribe(mobile, 1, Preferences.prefs?.getInt(Constants.ID, 0))
                dialog.dismiss()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
        Constants.IS_UPDATE_DIALOG_DISPLAYED = false
    }


//
//    private fun stopLocationService() {
//        val i = Intent(this, CurrentLocationService::class.java)
////        val i = Intent(this, LocationTrackerService::class.java)
//        stopService(i)
//    }

    /**
     * commented on reivew hit api to refresh count
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun changeBottomNavigation(bottomScreen: BottomNavScreenChange?) {
        Log.e("Screen", bottomScreen?.screen?.toString() ?: "")
        handleBottomNav(bottomScreen?.screen)
    }

    /**
     * commented on reivew hit api to refresh count
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun checkOdJobs(od: CheckOdJob?) {
        Log.e("Screen", od?.isLoader?.toString() ?: "")
        getUpcomingOdJobs()
    }


    private fun sendCommandService() {
        if (!TrackingUtility.isServiceRunning(
                this,
                LocationTrackerService::class.java
            )
        ) {
            Intent(this, LocationTrackerService::class.java).also {
                this.startService(it)
            }
        }
    }

    private fun stopCommandService() {
        if (TrackingUtility.isServiceRunning(
                this,
                LocationTrackerService::class.java
            )
        ) {
            Intent(this, LocationTrackerService::class.java).also {
                this.stopService(it)
            }
        }
    }

    fun requestBackgroundPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val backPermList = arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            AlertDialog.Builder(this)
                .setTitle("Background location permission")
                .setMessage("Allow location permission to get location updates in background")
                .setPositiveButton("Allow") { _, _ ->

                    requestPermissions(backPermList, 121212)

                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
        }
    }

    private fun startLocTracking() {
        requestPermissions()
    }


    private fun checkGpsStatus() {
        val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && isGpsDialogShowed == false) {
            isGpsDialogShowed = true
            Dialogs.showAlert(
                this,
                "Please turn on your location.",
                "Settings",
                onLocationSetting,
                onNagLocationSetting
            )
        }

    }

    private val onLocationSetting: () -> Unit = {
        isGpsDialogShowed = false
        val locIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivity(locIntent)
    }
    private val onNagLocationSetting: () -> Unit = {
        isGpsDialogShowed = false
    }

    private fun requestPermissions() {
        if (TrackingUtility.hasLocationPermissions(this)) {
            checkGpsStatus()
            sendCommandService()
            checkAutoStart()
            return
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            EasyPermissions.requestPermissions(
                this,
                "You need to accept location permissions to use this app",
                REQUEST_CODE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )

        } else {
            EasyPermissions.requestPermissions(
                this,
                "You need to accept location permissions to use this app",
                REQUEST_CODE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )

        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestBackgroundPermission()
                sendCommandService()
            }
        } else if (requestCode == 121212) {
            checkAutoStart()
        }
    }


    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        } else {
            requestPermissions()
        }
    }

    private fun checkAutoStart() {
        val currentTime = System.currentTimeMillis()
        val time = Preferences.prefs?.getLong(Constants.IS_AUTO_START_ENABLED, currentTime)
        val duration: Long = currentTime - time!!
        val days = TimeUnit.MILLISECONDS.toDays(duration)

        if (duration == 0L || days >= 1) {
            for (intent in POWERMANAGER_INTENTS)
                if (packageManager.resolveActivity(
                        intent,
                        PackageManager.MATCH_DEFAULT_ONLY
                    ) != null
                ) {
                    val dialog = AlertDialog.Builder(this)
                    dialog.setMessage("On this device you must allow us to run services in background. If enabled please click cancel.")
                        .setPositiveButton("Yes") { paramDialogInterface, paramInt ->
                            try {
                                Preferences.prefs?.saveValue(
                                    Constants.IS_AUTO_START_ENABLED,
                                    System.currentTimeMillis()
                                )
                                startActivityForResult(intent, 200)
                            } catch (ex: Exception) {
                                Log.e("TAG", ex.message!!)
                            }
                        }
                        .setNegativeButton("Cancel") { paramDialogInterface, paramInt -> paramDialogInterface.dismiss() }
                    dialog.show()
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 || requestCode == 200) {
            sendCommandService()
            if (requestCode == 200) {
                Preferences.prefs?.saveValue(
                    Constants.IS_AUTO_START_ENABLED,
                    System.currentTimeMillis()
                )
            }
        }
    }
}