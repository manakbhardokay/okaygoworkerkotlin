package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.AddUserRolePhoneNumberResponse
import com.okaygo.worker.data.modal.reponse.AllIdResponse
import com.okaygo.worker.data.modal.reponse.WorkerResponse
import com.okaygo.worker.data.modal.reponse.WorkerSession
import com.okaygo.worker.data.modal.request.AddUserRoleRequest
import com.okaygo.worker.data.modal.request.CreateSessionRequest
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class PasswordModel(app: Application) : MyViewModel(app) {
    var response = MutableLiveData<AddUserRolePhoneNumberResponse>()
    var responseAllIds = MutableLiveData<AllIdResponse>()
    var responseWorker = MutableLiveData<WorkerResponse>()
    var responseCreateSession = MutableLiveData<WorkerSession>()

    /**
     * add user on local server
     */
    fun addUserRolePhoneNumber(request: AddUserRoleRequest?, isForPassword: Boolean? = false) {
        isLoading.value = true
        PasswordRepository.addUserRolePhoneNumber({
            if (isForPassword == false) {
                isLoading.value = false
            }
            response.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, request)
    }

    /**
     * get all related id for paricular user
     */
    fun getAllIds(userId: Int?) {
//        isLoading.value = true
        PasswordRepository.getAllIds({
//            isLoading.value = false
            responseAllIds.value = it
        }, {
//            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     * get worker detail
     */
    fun getWorker(userId: Int?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        PasswordRepository.getWorker({
            if (isLoader == true) {
                isLoading.value = false
            }
            responseWorker.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     * create session for particular user
     */
    fun createWorkerSession(request: CreateSessionRequest?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        PasswordRepository.createWorkerSession({
            isLoading.value = false
            responseCreateSession.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, request)
    }
}