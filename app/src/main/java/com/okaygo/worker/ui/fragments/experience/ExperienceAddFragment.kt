package com.okaygo.worker.ui.fragments.experience

import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import com.okaygo.worker.R
import com.okaygo.worker.adapters.JobRoleSearchAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.JobApplySuccess
import com.okaygo.worker.data.modal.reponse.*
import com.okaygo.worker.data.modal.request.UpdateWorkerRequest
import com.okaygo.worker.data.modal.request.WorkerExperienceUpdate
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.questionare.QuestionareFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.ui.fragments.verification.*
import kotlinx.android.synthetic.main.fragment_experience_new.*
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class ExperienceAddFragment : BaseFragment(), View.OnClickListener, TextWatcher {
    private lateinit var viewModel: OnBoardingModel
    private lateinit var viewModelQues: QuestionareModel
    private lateinit var viewModelExperince: ExperinceModel
    private lateinit var viewModelJobList: JobListModel
    private lateinit var viewModelProfile: ProfileModel
    private lateinit var viewModelPaymentDetail: PaymentDetailModel

    private var mExpList: ArrayList<WorkerExperienceUpdate>? = null
    private var mQuestList: ArrayList<QuestionareResponseItem>? = null
    private var mJobData: JobContent? = null
    private var isDialogForUpi = false
    private var upi: String? = ""

    //    private var mLang: Int? = null
//    private var mEdu: Int? = null
//    private var mAdhaar: Int? = null
//    private var mRC: Int? = null
//    private var mDL: Int? = null
//    private var mLAPTOP: Int? = null
//    private var mSMARTPHONE: Int? = null
//    private var mWifi: Int? = null
//    private var mBike: Int? = null
//    private var mPan: Int? = null
    private var isLastSalary: Boolean? = null
    private var isLastSalaryOpt: Boolean? = true
    private var isCVOpt: Boolean? = true

    //    var mSkillList: ArrayList<UpdateSkill>? = null
    private var mNoticeList: ArrayList<String>? = null
    private var mSelectedNotice: String? = null
    private var mTotalExpYear: ArrayList<String>? = null
    private var mTotalExpMonth: ArrayList<String>? = null
    private var mAllJobRoleList: ArrayList<JobRoleResponse>? = null
    private var mTotalExp: String? = null
    private var isTotalExpOpt: Boolean? = true
    private var isNoticeOpt: Boolean? = true
    private var mTotalMonth: String? = null
    private var mTotalYear: String? = null
    private var mCvName: String? = null
    private val FILE_CHOOSER: Int = 5101
    private var cvPath: String? = null
    var mRequiredData: ReqiredSkillsResponse? = null
    private var mAssignId: Int? = null
    private var isOtherInd = false                      //for other interested cat
    private var mSearchAdapter: JobRoleSearchAdapter? = null
    private var expYear = "Years"
    private var expMonth = "Months"
    private var notice = "Select your notice"
    private var sDate: String? = null
    private var eDate: String? = null
    private var mCvLink: String? = null
    private var mCvFielName: String? = null

    //    private var isOtherJobType = false
//    private var mJobTypesList: ArrayList<String>? = null
//    private var jobTypeList: ArrayList<JobType>? = null
//    private var mJobTypeListOther: java.util.ArrayList<JobType>? = null
    private lateinit var viewModelAdd: AddExpModel

    //    private var mapJobTypes: HashMap<String, String>? = null
//    private var ind = ""
    private var selectedJobType: String? = null
    private var selectedJobTypeID: String? = null
    private var expData: Experiences? = null
    private var mExpId: Int? = null                         //if their is any exp, like: for edit
    private var job = ""
//    private var industry_str = ""
//    private var job_type_str = ""
//    private var spnAdapter: ArrayAdapter<String>? = null
//    private var isJobTypeOther = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        viewModelExperince = ViewModelProvider(this).get(ExperinceModel::class.java)
        viewModelAdd = ViewModelProvider(this).get(AddExpModel::class.java)
        viewModelQues = ViewModelProvider(this).get(QuestionareModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModelPaymentDetail = ViewModelProvider(this).get(PaymentDetailModel::class.java)

        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_experience_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle: Bundle? = arguments
//        mapJobTypes = HashMap()
        if (bundle?.containsKey(Constants.POST_APPLY_JOB_DATA) == true) {
            mJobData = bundle.getSerializable(Constants.POST_APPLY_JOB_DATA) as JobContent
        }
        if (bundle?.containsKey(Constants.POST_APPLY_REQUIRED_DATA) == true) {
            mAssignId = bundle.getInt(Constants.POST_Assign_id, 0)
            mRequiredData =
                bundle.getSerializable(Constants.POST_APPLY_REQUIRED_DATA) as ReqiredSkillsResponse
        }
//        if (bundle?.containsKey(Constants.LANG_ID) == true) {
//            mLang = bundle.getInt(Constants.LANG_ID)
//        }
//        if (bundle?.containsKey(Constants.EDU_ID) == true) {
//            mEdu = bundle.getInt(Constants.EDU_ID)
//        }
//        if (bundle?.containsKey(Constants.SKILL_LIST) == true) {
//            mSkillList = bundle.getParcelableArrayList<UpdateSkill>(Constants.SKILL_LIST)
//        }
//        if (bundle?.containsKey(Constants.ADHAAR_STATUS) == true) {
//            mAdhaar = bundle.getInt(Constants.ADHAAR_STATUS)
//        }
//        if (bundle?.containsKey(Constants.RC_STATUS) == true) {
//            mRC = bundle.getInt(Constants.RC_STATUS)
//        }
//        if (bundle?.containsKey(Constants.DL_STATUS) == true) {
//            mDL = bundle.getInt(Constants.DL_STATUS)
//        }
//        if (bundle?.containsKey(Constants.BIKE_STATUS) == true) {
//            mBike = bundle.getInt(Constants.BIKE_STATUS)
//        }
//        if (bundle?.containsKey(Constants.PAN_STATUS) == true) {
//            mPan = bundle.getInt(Constants.PAN_STATUS)
//        }
//        if (bundle?.containsKey(Constants.WIFI_STATUS) == true) {
//            mWifi = bundle.getInt(Constants.WIFI_STATUS)
//        }
//        if (bundle?.containsKey(Constants.LAPTOP_STATUS) == true) {
//            mLAPTOP = bundle.getInt(Constants.LAPTOP_STATUS)
//        }
//        if (bundle?.containsKey(Constants.SMARTPHONE_STATUS) == true) {
//            mSMARTPHONE = bundle.getInt(Constants.SMARTPHONE_STATUS)
//        }
        viewModel.getWorkerExp(workerId)
        viewModelExperince.getAllJobRole()
        viewModelProfile.getWorkerByUserId(userId, false)
        if (Constants.IS_FROM_PROFILE == false) {
            viewModelQues.getJobQuestions(mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID)
        }

        val yr = Calendar.getInstance().get(Calendar.YEAR);
        val monthOfYear = Calendar.getInstance().get(Calendar.MONTH);
        sDate = yr.toString() + "-" + (monthOfYear + 1) + "-" + "01"
        showHideViews()
        setListeners()
        setJobRoleAdapter()
    }

    /**
     * set adapter for searched list
     */
    private fun setJobRoleAdapter() {
        if (mAllJobRoleList == null) {
            mAllJobRoleList = ArrayList<JobRoleResponse>()
        }
        val linearLayoutManager = LinearLayoutManager(activity)
        mSearchAdapter = activity?.let {
            JobRoleSearchAdapter(
                it, mAllJobRoleList,
                onJobRoleClick
            )
        }
        recyclerJobRole?.layoutManager = linearLayoutManager
        recyclerJobRole?.adapter = mSearchAdapter
    }

    /**
     * click listenr when click on any job role
     */
    private val onJobRoleClick: (JobRoleResponse?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        selectedJobType = it?.mapped_category.toString()
        selectedJobTypeID = it?.id.toString()
        Log.e("JobRoleResponse", it?.categorySubType.toString() + "")
        edtJobRole?.setText(it?.categorySubType.toString())
        recyclerJobRole?.visibility = View.GONE
    }

    private fun showHideViews() {
        if (Constants.IS_FROM_PROFILE == false) {
            for (i in 0 until (mRequiredData?.response?.size ?: 0)) {
                when (mRequiredData?.response?.get(i)?.skillId) {
                    Constants.USER_CV -> {
                        linearUplaodCv?.visibility = View.VISIBLE
                        if (mJobData != null && mJobData?.cv_name?.isEmpty() == false) {
                            linearFileName?.visibility = View.VISIBLE
                            chFileName?.visibility = View.VISIBLE
                            chFileName?.text = mJobData?.cv_name ?: ""
                            txtUploadCV?.text = "UPDATE CV"
                        }
                        if (mRequiredData?.response?.get(i)?.mandatory == true) {
                            isCVOpt = false
                        }
                    }
                    Constants.USER_LAST_SALARY -> {
                        isLastSalary = true
                        if (mRequiredData?.response?.get(i)?.mandatory == true) {
                            isLastSalaryOpt = false
                        }
                        txtLastSalary?.visibility = View.VISIBLE
                        edtLastSalary?.visibility = View.VISIBLE
                    }
                    Constants.USER_NOTICE -> {
                        imgDropDownNotice?.visibility = View.VISIBLE
                        spnNotice?.visibility = View.VISIBLE
                        txtNoticeTitle?.visibility = View.VISIBLE
                        if (mRequiredData?.response?.get(i)?.mandatory == true) {
                            isNoticeOpt = false
                        }
//                        mSelectedNotice = "Select notice period"
//                        setNoticeSpnAdapter()
                    }
//                Constants.USER_EXP -> {
////                    constraintAddExp?.visibility = View.VISIBLE
//                    if (mExpList == null) {
//                        mExpList = ArrayList()
//                    } else {
//                        mExpList?.clear()
//                    }
//                }
                    Constants.USER_EXP,
                    Constants.USER_TOTAL_EXP -> {
                        constraintTotalExp?.visibility = View.VISIBLE
//                        mTotalExp = "Years,Months"
                        if (mRequiredData?.response?.get(i)?.mandatory == true) {
                            isTotalExpOpt = false
                        }
//                        setTotalExpSpnAdapter()
                    }
                }
            }
        } else {

            handleEdit(true)
            isTotalExpOpt = false
            isNoticeOpt = false
            isLastSalary = false
            txtEdit?.visibility = View.VISIBLE
            txtBack?.visibility = View.GONE
            txtDone?.visibility = View.GONE
            txtBack?.text = "Discard"
            txtDone?.text = "Save"
            constraintTotalExp?.visibility = View.VISIBLE

            txtLastSalary?.visibility = View.VISIBLE
            edtLastSalary?.visibility = View.VISIBLE
            imgDropDownNotice?.visibility = View.VISIBLE
            spnNotice?.visibility = View.VISIBLE
            txtNoticeTitle?.visibility = View.VISIBLE

        }
    }


    fun selectSpinnerItemByValue(spnr: Spinner, value: String?, spnAdapter: ArrayAdapter<String>) {
        for (position in 0 until spnAdapter.getCount()) {
            if (spnAdapter.getItem(position) == value) {
                spnr.setSelection(position)
                return
            }
        }
    }

    /**
     * set click listeners for required views
     */
    private fun setListeners() {
        txtBack?.setOnClickListener(this)
        txtDone?.setOnClickListener(this)
        txtEdit?.setOnClickListener(this)
        imgDownload?.setOnClickListener(this)
        txtUploadCV?.setOnClickListener(this)
        edtJobRole?.addTextChangedListener(this)

        spnNotice?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mSelectedNotice = parent?.getItemAtPosition(position).toString()
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        spnYears?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mTotalYear = parent?.getItemAtPosition(position).toString()
                mTotalExp = mTotalYear + "," + mTotalMonth
                if ((mTotalExp.equals("0 yrs") && mTotalMonth.equals("0 month")) ||
                    (mTotalExp.equals("0 yrs") && mTotalMonth.equals("Months")) ||
                    (mTotalExp.equals("Years")) ||
                    mTotalMonth.equals("Months") ||
                    mTotalExp.equals("Years,Months")
                ) {
                    constraintAddExp?.visibility = View.GONE
                } else {
                    recyclerJobRole?.visibility = View.GONE
                    constraintAddExp?.visibility = View.VISIBLE
                }
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        spnMonth?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mTotalMonth = parent?.getItemAtPosition(position).toString()
                mTotalExp = mTotalYear + "," + mTotalMonth
                if (mTotalExp.equals("0 yrs,0 month") ||
                    mTotalExp.equals("Years,Months") ||
                    mTotalYear?.equals("Years") == true
                ) {
                    constraintAddExp?.visibility = View.GONE
                } else {
                    recyclerJobRole?.visibility = View.GONE
                    constraintAddExp?.visibility = View.VISIBLE
                }
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        chkStillWorking?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                eDate = null
            } else {
                eDate = sDate
            }
        }

        chFileName?.setOnCloseIconClickListener(View.OnClickListener {
            txtUploadCV?.text = "UPLOAD CV"
            chFileName?.text = ""
            chFileName?.visibility = View.GONE
            imgDownload?.visibility = View.GONE
        })
    }


    /**
     * set adapter for lan spinner
     */
    private fun setNoticeSpnAdapter() {
        mNoticeList = ArrayList()
        mNoticeList?.add("Select notice period")
        mNoticeList?.add("No Notice")
        mNoticeList?.add("15 Days")
        mNoticeList?.add("30 Days")
        mNoticeList?.add("45 Days")
        mNoticeList?.add("60 Days")

        val spnAdapter: ArrayAdapter<String>? = activity?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                mNoticeList!!
            )
        }
        spnAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnNotice?.setAdapter(spnAdapter)
        spnAdapter?.let { selectSpinnerItemByValue(spnNotice, mSelectedNotice, it) }
    }

    /**
     * set adapter for lan spinner
     */
    private fun setTotalExpSpnAdapter() {
        mTotalExpYear = ArrayList()
        mTotalExpYear?.add("Years")
        for (i in 0..35) {
            mTotalExpYear?.add(i.toString() + " yrs")
        }
        val spnAdapter: ArrayAdapter<String>? = activity?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                mTotalExpYear!!
            )
        }
        spnAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnYears?.setAdapter(spnAdapter)
        spnAdapter?.let { selectSpinnerItemByValue(spnYears, expYear, it) }

        mTotalExpMonth = ArrayList()
        mTotalExpMonth?.add("Months")
        for (i in 0..11) {
            mTotalExpMonth?.add(i.toString() + " month")
        }
        val spnAdapterMonth: ArrayAdapter<String>? = activity?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                mTotalExpMonth!!
            )
        }
        spnAdapterMonth?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnMonth?.setAdapter(spnAdapterMonth)
        spnAdapterMonth?.let { selectSpinnerItemByValue(spnMonth, expMonth, it) }
    }

    private fun handleEdit(isDisable: Boolean) {
        if (isDisable) {
            chkStillWorking?.isEnabled = false
            spnYears?.isEnabled = false
            spnMonth?.isEnabled = false
            spnNotice?.isEnabled = false
            edtLastSalary?.isEnabled = false
            edtCompany?.isEnabled = false
            edtDesignation?.isEnabled = false
            edtJobRole?.isEnabled = false
        } else {
            chkStillWorking?.isEnabled = true
            spnYears?.isEnabled = true
            spnMonth?.isEnabled = true
            spnNotice?.isEnabled = true
            edtLastSalary?.isEnabled = true
            edtCompany?.isEnabled = true
            edtDesignation?.isEnabled = true
            edtJobRole?.isEnabled = true
        }
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgDownload -> {
                Utilities.downloadAnyFile(activity, mCvLink, mCvFielName)
            }
            R.id.txtEdit -> {
                handleEdit(false)
                txtEdit?.visibility = View.GONE
                txtBack?.visibility = View.VISIBLE
                txtDone?.visibility = View.VISIBLE
            }
            R.id.txtBack -> {
                if (Constants.IS_FROM_PROFILE) {
                    handleEdit(true)
                    viewModel.getWorkerExp(workerId, false)
                    viewModelProfile.getWorkerByUserId(userId, false)
                    txtEdit?.visibility = View.VISIBLE
                    txtBack?.visibility = View.GONE
                    txtDone?.visibility = View.GONE
//                    mTotalExp = "$expYear,$expMonth"
//                    mSelectedNotice = notice
//                    setTotalExpSpnAdapter()
//                    setNoticeSpnAdapter()
//                    edtLastSalary?.setText(Constants.LAST_UPDATED_SALARY?.toString())
                } else {
                    activity?.onBackPressed()
                }
            }
            R.id.txtUploadCV -> showFileChooserCV()
            R.id.txtDone -> {
                if (Constants.IS_FROM_PROFILE == false) {
                    if (cvPath != null && cvPath?.isEmpty() == false) {
                        val file = File(cvPath)
                        viewModel.getDocLink(userId, file)
                    } else {
                        saveData()
                    }
                } else {
                    saveData()
                }

            }
        }
    }

    /**
     * set data to view if it for edit page
     */
    private fun setExpData() {
        mExpId = expData?.experienceId
        if ((mExpId ?: 0) < 1 || expData?.companyName?.isEmpty() == true) {
            txtEdit?.performClick()
        }
//        isOtherInd = !(expData?.industryType ?: 0 in 105..115)
//        ind = expData?.industryType?.toString() ?: ""
        job = expData?.jobType?.toString() ?: ""
        edtCompany?.setText(expData?.companyName)
        edtDesignation?.setText(expData?.designation)
        edtJobRole?.setText(expData?.jobTypeName)
        recyclerJobRole?.visibility = View.GONE
        selectedJobType = expData?.industryType?.toString()
        selectedJobTypeID = expData?.jobType?.toString()
        if (expData?.toDate == null || expData?.toDate?.isEmpty() == true) {
            chkStillWorking?.setChecked(true)
        }

//        if (!isOtherInd) {
//            viewModel.fetchInterestedJobTypes()
//
//        } else {
//            industry_str = expData?.industryName ?: ""
//        edtJobRole?.setText(expData?.jobTypeName ?: "")
//            chgJobCat?.setVisibility(View.GONE);
//            spnJobRole?.setVisibility(View.GONE);
//            txtJobRoleTitle?.setVisibility(View.VISIBLE)
//        }
    }

    private fun saveData() {
        if (mExpList == null) {
            mExpList = ArrayList()
        } else {
            mExpList?.clear()
        }
        if ((mTotalExp.equals("0 yrs,0 month") == false && mTotalExp.equals("Years,Months") == false)) {

            val cName = edtCompany?.text?.toString()?.trim()
            val cDesg = edtDesignation?.text?.toString()?.trim()
            if (cName?.isEmpty() == true) {
                Utilities.showToast(activity, "Please enter your last company name.")
                return
            } else if (cDesg?.isEmpty() == true) {
                Utilities.showToast(activity, "Please enter your last designation.")
                return
            }
//            if (!isOtherInd) {
//                if (chgJobCat?.getCheckedChipId() == -1) {
//                    Utilities.showToast(activity, "Select a industry.")
//                    return
//                }

            if (selectedJobType == null || selectedJobType?.isEmpty() == true) {
                Utilities.showToast(activity, "Select a Job Type.")
                return
//                }
            }

            val expData = WorkerExperienceUpdate(
                experienceId = mExpId,
                companyName = cName,
                designation = cDesg,
//                fromDate = sDate,
                toDate = eDate,
                industryType = selectedJobType?.toInt(),
                jobType = selectedJobTypeID?.toInt()
            )
            mExpList?.add(expData)
        }

        if (isTotalExpOpt == false && mTotalExp.equals("Years,Months")) {
            Utilities.showToast(activity, "Please select your total experience.")
            return
        }

        if (isTotalExpOpt == false && mTotalMonth.equals("Months")) {
            Utilities.showToast(activity, "Please select your total months experience.")
            return
        }

        if (isTotalExpOpt == false && mTotalYear?.contains("Years") == true) {
            Utilities.showToast(activity, "Please select your total years experience.")
            return
        }

        if (isNoticeOpt == false && mSelectedNotice.equals("Select notice period")) {
            Utilities.showToast(activity, "Please select your notice period.")
            return
        }

        if (isCVOpt == false && chFileName?.text?.isEmpty() == true) {
            Utilities.showToast(activity, "Please upload your cv.")
            return
        }

        var sal: Int? = null
        if (isLastSalary == true || Constants.IS_FROM_PROFILE) {
            val salary = edtLastSalary?.text?.toString()?.trim()
            if (salary?.isEmpty() == true) {
                sal = 0
            } else {
                sal = salary?.toInt() ?: 0
            }
            if (isLastSalaryOpt == false && sal <= 0) {
                Utilities.showToast(activity, "Please enter your last salary.")
                return
            }

        }

        if (Constants.IS_FROM_PROFILE) {
            handleEdit(true)
            txtEdit?.visibility = View.VISIBLE
            txtBack?.visibility = View.GONE
            txtDone?.visibility = View.GONE
        }
        Constants.TOTAL_EXP = mTotalExp
        Constants.NOTICE_PERIOD = mSelectedNotice
        Constants.LAST_UPDATED_SALARY = sal

        val request: UpdateWorkerRequest? = UpdateWorkerRequest(
            requestedBy = userId,
            notice_period = mSelectedNotice,
            total_experience = mTotalExp,
            last_salary = sal,
            workerExperiences = mExpList
        )
//        val request: UpdateWorkerRequest? = UpdateWorkerRequest(
//            requestedBy = userId,
//            skills = mSkillList,
//            english_known_level = mLang,
//            qualificationId = mEdu,
//            own_aadhar_card = mAdhaar,
//            own_bike = mBike,
//            own_laptop = mLAPTOP,
//            own_smartphone = mSMARTPHONE,
//            own_pan_card = mPan,
//            own_vehicle_rc = mRC,
//            own_wifi = mWifi,
//            bike_license = mDL,
//            notice_period = mSelectedNotice,
//            total_experience = mTotalExp,
//            last_salary = sal,
//            workerExperiences = mExpList
//        )
        viewModelExperince.saveWorkerData(workerId, request)
    }


    private fun setProfileData(response: WorkerDetailResponse?) {
        if (response?.response?.content?.isEmpty() == true) {
            return
        }
        Constants.LAST_UPDATED_SALARY = response?.response?.content?.get(0)?.last_salary
        Constants.NOTICE_PERIOD = response?.response?.content?.get(0)?.notice_period
        Constants.TOTAL_EXP = response?.response?.content?.get(0)?.total_experience

        edtLastSalary?.setText(Constants.LAST_UPDATED_SALARY?.toString() ?: "0")
        if (Constants.TOTAL_EXP?.contains(",") == true) {
            val value = Constants.TOTAL_EXP?.split(",")
            expYear = value?.get(0) ?: "Years"
            expMonth = value?.get(1) ?: "Months"
        }
        mSelectedNotice = Constants.NOTICE_PERIOD ?: "Select notice period"
        notice = Constants.NOTICE_PERIOD ?: "Select notice period"
        mTotalMonth = expMonth
        mTotalYear = expYear
        mTotalExp = "$expYear,$expMonth"
        mCvFielName = response?.response?.content?.get(0)?.cv_file_name
        mCvLink = response?.response?.content?.get(0)?.cv_link
        setTotalExpSpnAdapter()
        setNoticeSpnAdapter()
    }

    /**
     * handle all api's response
     */
    private fun attachObservers() {
        viewModel.responseGetDocLink.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.uplaodCV(
                        it.response,
                        workerId,
                        mCvName
                    )
                }
            }
        })

        viewModel.responseUploadCV.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    saveData()
                    Utilities.showSuccessToast(activity, "CV Uploaded Successfully.")
                }
            }
        })

        viewModel.responseSaveCat.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.setNoExp(workerId, false)
                }
            }
        })

        viewModelExperince.responseAllJobRole.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.isEmpty() == false) {
                    if (mAllJobRoleList == null) {
                        mAllJobRoleList = ArrayList()
                    } else {
                        mAllJobRoleList?.clear()
                    }
                    mAllJobRoleList?.addAll(it.response)
                    mSearchAdapter?.notifyDataSetChanged()
                }
            }
        })

        viewModelProfile.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    setProfileData(it)
                }
            }
        })

        viewModel.responseGetExp.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.content?.isEmpty() == false) {
                        expData = it.response.content.get(it.response.content.size - 1)
                        setExpData()
                    }
//                    viewModel.fetchInterestedJobTypes()
//                    setSpinnerForJobType()
                }
            }
        })

        viewModelExperince.responseSaveData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (Constants.IS_FROM_PROFILE) {
                        Utilities.showToast(activity, "Data has been updated.")
                    } else {
                        if (mQuestList?.isEmpty() == false) {
                            val bundle: Bundle? = Bundle()
                            val fragment = QuestionareFragment()
                            if (mJobData != null) {
                                bundle?.putSerializable(Constants.POST_APPLY_JOB_DATA, mJobData)
                            }
                            bundle?.putSerializable(
                                Constants.POST_APPLY_REQUIRED_DATA,
                                mRequiredData
                            )
                            bundle?.putInt(Constants.POST_Assign_id, mAssignId ?: 0)
                            fragment.arguments = bundle
                            addFragment(fragment, true)
                        } else {
                            viewModelJobList.applyJob(
                                mAssignId,
                                userId,
                                Constants.DEEPLINK_REFFER_BY
                            )
                        }
                    }
                }
            }
        })
        viewModelJobList.responseApplyJob.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    if (it.code == Constants.SUCCESS) {
                        var workType: String? = ""
                        if (mJobData == null) {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                Constants.DEEPLINK_WORK_TYPE,
                                Constants.DEEPLINK_JOB_TYPE,
                                Constants.DEEPLINK_JOB_DETAIL_ID?.toString(),
                                "apply"
                            )
                            workType = Constants.DEEPLINK_WORK_TYPE
                        } else {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                mJobData?.workType,
                                mJobData?.jobType,
                                mJobData?.jobDetailId?.toString(),
                                "apply"
                            )
                            workType = mJobData?.workType
                        }
                        if (workType.equals("ON DEMAND", true) && upi?.isEmpty() == true) {
                            isDialogForUpi = false
                            Dialogs.showAlertDialog(
                                activity,
                                "Job applied",
                                activity?.resources?.getString(R.string.applied_msg_od),
                                "UPDATE",
                                "LATER",
                                onAlertPosBtnClick,
                                onAlertNagClick
                            )
                        } else {
                            Utilities.jobSuccessDialog(
                                activity,
                                activity?.resources?.getString(R.string.job_applied),
                                activity?.resources?.getString(R.string.applied_success_descriotion),
                                true, onOKClick
                            )
                        }
                    }
                }
            }
        })
//        viewModel.responseIntrestedJob.observe(this, Observer {
//            it?.let {
//                if (it.code == Constants.SUCCESS) {
//                    handleJobCat(it.response)
//                }
//            }
//        })
//
//        viewModelAdd.responseJobTypeInd.observe(this, Observer {
//            it?.let {
//                if (it.code == Constants.SUCCESS) {
//                    if (mJobTypesList == null) {
//                        mJobTypesList = java.util.ArrayList<String>()
//                    } else {
//                        mJobTypesList?.clear()
//                    }
//
//                    if (jobTypeList == null) {
//                        jobTypeList = java.util.ArrayList<JobType>()
//                    } else {
//                        jobTypeList?.clear()
//                    }
//                    it.response?.content?.let { it1 -> jobTypeList?.addAll(it1) }
//                    for (index in 0 until it.response?.content?.size!!) {
//                        it.response?.content?.get(index).categorySubType?.let { it1 ->
//                            mJobTypesList?.add(
//                                it1
//                            )
//                        }
//                    }
//
//                    spnJobRole?.visibility = View.VISIBLE
//                    isJobTypeOther = false
//
//                    spnAdapter!!.notifyDataSetChanged()
//                    if (jobTypeList?.isEmpty() == false) {
//                        selectedJobTypeID = jobTypeList?.get(0)?.id.toString() + ""
//                    }
//                }
//            }
//        })
//
//        viewModelAdd.responseIntrestedJobType.observe(this, Observer {
//            it?.let {
//                if (it.code == Constants.SUCCESS) {
//                    if (mJobTypesList == null) {
//                        mJobTypesList = java.util.ArrayList<String>()
//                    } else {
//                        mJobTypesList?.clear()
//                    }
//
//                    if (mJobTypeListOther == null) {
//                        mJobTypeListOther = java.util.ArrayList<JobType>()
//                    } else {
//                        mJobTypeListOther?.clear()
//                    }
//                    it.response?.content?.let { it1 ->
//                        mJobTypeListOther?.addAll(
//                            it1
//                        )
//                    }
//
//                    for (i in 0 until it.response?.content?.size!!) {
//                        it.response.content.get(i).categorySubType?.let { it1 ->
//                            mJobTypesList?.add(
//                                it1
//                            )
//                        }
//                    }
//                    spnJobRole?.visibility = View.VISIBLE
//                    isJobTypeOther = true
//
//                    spnAdapter?.notifyDataSetChanged()
//                    if (mJobTypeListOther?.isEmpty() == false) {
//                        selectedJobTypeID = mJobTypeListOther?.get(0)?.id.toString() + ""
//                    }
//                }
//            }
//        })

        viewModelQues.responseJobQuestions.observe(this, Observer {
            it?.let {
                if (!it.isEmpty()) {
                    if (mQuestList == null) {
                        mQuestList = ArrayList()
                    } else {
                        mQuestList?.clear()
                    }
                    mQuestList?.addAll(it)
//                    if (mQuestList?.size == 1) {
//                        txtNext?.text = "APPLY"
//                    }
//                    mAdapter?.notifyDataSetChanged()
//                    setProgress()
                } else {
                    viewModelPaymentDetail.getUPI(workerId, false)
                    txtDone?.text = "APPLY"
                }
            }
        })

        viewModelPaymentDetail.responseSaveUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response
                    Utilities.showToast(activity, "UPI Id is updated.")
                    EventBus.getDefault().post(JobApplySuccess(true))
                    activity?.finish()
                }
            }
        })
        viewModelPaymentDetail.responseGetUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response ?: ""
                }
            }
        })

        viewModelPaymentDetail.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelPaymentDetail.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelJobList.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelJobList.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelExperince.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelExperince.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private fun showUpiDialog(savedUpi: String?) {
        activity?.let {
            val dialog = Dialog(it)
            dialog.setContentView(R.layout.dialog_salary)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val title = dialog.findViewById<TextView>(R.id.title)
            val edtUpi = dialog.findViewById<TextInputEditText>(R.id.salary)
            val cancel = dialog.findViewById<TextView>(R.id.cancel)
            val confirm = dialog.findViewById<TextView>(R.id.confirm)
            title.text = "Enter your valid UPI id"
            confirm.text = "Save"
            edtUpi?.setHint("")
            edtUpi?.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(50)))
            edtUpi?.gravity = Gravity.START
            edtUpi?.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            edtUpi?.setText(savedUpi ?: "")

            cancel.setOnClickListener {
                dialog.dismiss()
                EventBus.getDefault().post(JobApplySuccess(true))
                activity?.finish()
            }
            confirm.setOnClickListener {
                upi = edtUpi?.text?.trim()?.toString()
                val count = upi?.split("@")
                if (upi?.isEmpty() == true) {
                    Utilities.showToast(activity, "Enter your UPI id.")
                } else if ((upi?.length) ?: 0 < 3 || upi?.contains("@") == false || (count?.size
                        ?: 0) != 2
                ) {
                    edtUpi?.setError("Invalid UPI id.")
                } else {
                    dialog.dismiss()
                    isDialogForUpi = true
                    Dialogs.showAlertDialog(
                        activity,
                        "Check your UPI ID",
                        "Entered UPI ID is $upi. Do you want to save?",
                        "Yes",
                        "Edit",
                        onAlertPosBtnClick,
                        onAlertNagClick
                    )
                }
            }
            dialog.show()
        }
    }

    private val onAlertNagClick: () -> Unit = {
        if (isDialogForUpi) {
            showUpiDialog(upi)
        } else {
            EventBus.getDefault().post(JobApplySuccess(true))
            activity?.finish()
        }
    }

    private val onAlertPosBtnClick: () -> Unit = {
        if (isDialogForUpi == false) {
            showUpiDialog(upi)
        } else {
            viewModelPaymentDetail.updateUPI(workerId, upi)
        }
    }

    private val onOKClick: () -> Unit = {
        EventBus.getDefault().post(JobApplySuccess(true))
        activity?.finish()
    }

    /**
     * file choser for cv
     */
    fun showFileChooserCV() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        val mimeTypes = arrayOf(
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "image/*",
            "application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        //        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                FILE_CHOOSER
            )
        } catch (ex: ActivityNotFoundException) {
            // Potentially direct the user to the Market with a Dialog
            Utilities.showToast(activity, "Please install a File Manager.")
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == FILE_CHOOSER) {
            // Get the Uri of the selected file
            val uri = data?.data
            cvPath = activity?.let { Utilities.getFilePathForN(uri, it) }
            if (cvPath != null) {
                val file = File(cvPath)
                val file_size: Int = (file.length() / (1024 * 1024)).toString().toInt()
                if (file_size >= 1) {
                    Utilities.showToast(activity, getString(R.string.file_size))
                    cvPath = null
                } else {
                    val name: Array<String>? =
                        cvPath?.split("/".toRegex())?.toTypedArray()
                    mCvName = name?.get(name.size - 1)
                    linearFileName?.visibility = View.VISIBLE
                    imgDownload?.visibility = View.GONE
                    chFileName?.visibility = View.VISIBLE
                    chFileName?.text = mCvName
                    txtUploadCV?.text = "UPDATE CV"

                }
                Log.e("Work Experience", "File Path: $cvPath")
                Log.e("Work Experience", "File file_size: $file_size")
            }
        }
    }

    override fun afterTextChanged(editable: Editable?) {
        Log.e("afterTextChanged", "true")
        if (editable.toString().isEmpty() == false) {
            recyclerJobRole?.visibility = View.VISIBLE
        }
        mSearchAdapter?.filter?.filter(editable.toString().trim({ it <= ' ' }))
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        Log.e("beforeTextChanged", "true")
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        Log.e("onTextChanged", "true")
    }
}