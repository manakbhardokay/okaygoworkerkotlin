package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class JobPreferenceModel(app: Application) : MyViewModel(app) {
    var responseSaveJobCat = MutableLiveData<SuccessResponse>()

    fun saveJobCat(workerId: Int?, cat: String?) {
        isLoading.value = true
        JobPreferenceRepository.saveJobCat({
            isLoading.value = false
            responseSaveJobCat.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId, cat)
    }
}