package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.GetCurrentEduResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.okaygo.worker.data.modal.reponse.UserDetailResponse
import com.okaygo.worker.data.modal.reponse.WorkerDetailResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class ProfileModel(app: Application) : MyViewModel(app) {
    var responseWorkerData = MutableLiveData<WorkerDetailResponse>()
    var responseLanguage = MutableLiveData<SuccessResponse>()
    var responseProfilePic = MutableLiveData<WorkerDetailResponse>()
    var responseUpdateStatus = MutableLiveData<UserDetailResponse>()
    var responseCurrentEdu = MutableLiveData<GetCurrentEduResponse>()


    /**
     * create session for particular user
     */
    fun getWorkerByUserId(userId: Int?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        ProfileRepository.getWorkerByUserId({
            isLoading.value = false
            responseWorkerData.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     *
     */
    fun updateLang(langId: Int?, userId: Int?) {
        isLoading.value = true
        ProfileRepository.updateLang({
            isLoading.value = false
            responseLanguage.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, langId, userId)
    }

    /**
     *
     */
    fun updateStatus(status: String?, userId: Int?) {
        isLoading.value = true
        ProfileRepository.updateStatus({
            isLoading.value = false
            responseUpdateStatus.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, status, userId)
    }

    /**
     *
     */
    fun getCurrentEdu(workerId: Int?) {
        isLoading.value = true
        ProfileRepository.getCurrentEdu({
//            isLoading.value = false
            responseCurrentEdu.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId)
    }

    /**
     *
     */
    fun uploadProfilePic(usertId: Int?, path: String?) {
        isLoading.value = true
        ProfileRepository.uploadProfilePic({
            isLoading.value = false
            responseProfilePic.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, usertId, path)
    }
}