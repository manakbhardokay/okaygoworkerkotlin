package com.okaygo.worker.ui.fragments.login.model

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession

data class AuthenticationSuccess(
    val cognitoUserSession: CognitoUserSession?,
    val device: CognitoDevice?
)