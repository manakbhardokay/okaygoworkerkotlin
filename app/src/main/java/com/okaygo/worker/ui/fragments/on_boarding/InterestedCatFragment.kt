package com.okaygo.worker.ui.fragments.on_boarding

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.Chip
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.CategoryResponse
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import kotlinx.android.synthetic.main.fragment_skills.*

class InterestedCatFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: OnBoardingModel
    private var mSelectedCat: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtTitle?.text = "Interests"
        txtSkillTag?.text = "Select upto 3 job types you are interested in"
        linearBottomBtns?.visibility = View.VISIBLE
        lnrBottomLayout?.visibility = View.GONE
        txtChipOther?.visibility = View.GONE
        txtDiscard?.text = "BACK"
        txtSave?.text = "FIND JOBS"
        viewModel.fetchInterestedJobTypes()
        setListeners()
    }

    private fun setListeners() {
        txtSave?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)
    }

    /**
     * handle all interested job cat from local server
     */
    private fun handleIntrestedJobCat(response: CategoryResponse?) {
        for (i in 0 until response?.content?.size!!) {
            val tagName: String? = response.content.get(i).typeDesc ?: ""
            val chip = Chip(activity)

            if (i == 0) {
                chip.isChecked = true
            }
            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            chip.text = tagName
            chip.id = response.content.get(i).id ?: 0
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 1f
            chip.isClickable = true
            chip.isChipIconVisible = true

            activity?.let {
                if (tagName?.contains("Chef") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_cook)
                } else if (tagName?.contains("Restaurant") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_resturants)
                } else if (tagName?.contains("Hotel") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_hotel_services)
                } else if (tagName?.contains("Office Job") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic__office_job)
                } else if (tagName?.contains("Delivery") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_delivery)
                } else if (tagName?.contains("Warehouse") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_warehouse)
                } else if (tagName?.contains("Event") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_events)
                } else if (tagName?.contains("Sales") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_sales)
                } else if (tagName?.contains("Customer Support") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_customer_support)
                } else if (tagName?.contains("Education") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_education)
                } else {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_other_jobs)
                }
            }
            chip.isCloseIconVisible = true
            chip.isCheckedIconVisible = false
            chgSkills?.addView(chip)
        }
    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {

        viewModel.responseIntrestedJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    handleIntrestedJobCat(it.response)
                }
            }
        })

        viewModel.responseSaveCat.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    OkayGoFirebaseAnalytics.on_boarding_interested_cat()
                    Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, true)
                    activity?.startActivity(Intent(activity, DashBoardActivity::class.java))
                    activity?.finish()
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtSave -> {
                if (Utilities.isOnline(activity)) {
                    saveJobInterest()
                } else {
                    Utilities.showToast(activity, "No Internet")
                }
            }
            R.id.txtDiscard -> {
                activity?.onBackPressed()
            }
        }
    }

    private fun saveJobInterest() {
        val sb = StringBuilder()
        for (i in 0 until chgSkills?.childCount!!) {
            val chip: Chip? = chgSkills?.findViewById(chgSkills?.getChildAt(i)?.getId() ?: 0)
            if (chip?.isChecked == true) {
                sb.append(chip.id.toString() + "")
                sb.append(",")
            }
        }
        Log.e("selected chips", sb.toString() + "")
        mSelectedCat = sb.toString().replace(",$".toRegex(), "")
        if (mSelectedCat?.isEmpty() == true) {
            Utilities.showToast(activity, "Select atleast  one job interest.")
        } else if (mSelectedCat?.contains(",") == true) {
            val selectedJob: Array<String>? = mSelectedCat?.split(",".toRegex())?.toTypedArray()
            if (selectedJob?.size ?: 0 > 3) {
                Utilities.showToast(activity, "You can select maximum 3 job interests.")
            } else {
                viewModel.saveInterestedCat(
                    workerId,
                    mSelectedCat
                )
            }
        } else {
            viewModel.saveInterestedCat(
                workerId,
                mSelectedCat
            )
        }
    }
}