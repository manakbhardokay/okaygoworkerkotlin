package com.okaygo.worker.ui.fragments.on_boarding

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.okaygo.worker.R
import com.okaygo.worker.adapters.SearchAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.LocationEvent
import com.okaygo.worker.data.modal.reponse.CitySearch
import com.okaygo.worker.data.modal.reponse.WorkerDetails
import com.okaygo.worker.data.modal.request.UserDetailRequest
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.spinner_date_picker.DatePicker
import com.okaygo.worker.spinner_date_picker.DatePickerDialogCustom
import com.okaygo.worker.spinner_date_picker.SpinnerDatePickerDialogBuilder
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.okaygo.worker.ui.activity.map.CustomMapActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.application.OkayGo.Companion.appContext
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import com.openkey.guest.ui.fragments.verification.PasswordModel
import com.openkey.guest.ui.fragments.verification.ProfileModel
import com.openkey.guest.ui.fragments.verification.SearchModel
import kotlinx.android.synthetic.main.fragment_personal_detail.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.IOException
import java.util.*

class PersonalDetailFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModelSearch: SearchModel
    private lateinit var viewModelWorker: PasswordModel  // this is for handle/get worker detail
    private lateinit var viewModel: OnBoardingModel
    private lateinit var viewModelProfile: ProfileModel

    //    private var mGender: Int? = 0
    private var mCityId: Int? = 0
    var myCalendar: Calendar? = null
    private var dob: String? = null
    private var isProfileUpdate: Boolean? = false
    private var mTempList: ArrayList<CitySearch>? = null
    private var mSearchAdapter: SearchAdapter? = null


    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var current_latitude: Double? = 0.0
    private var current_longitude: Double? = 0.0
    var geocoder: Geocoder? = null
    var addresses: List<Address>? = null
    private var mCityName: String? = null
    private var mGoogleAddress: String? = null
    private var isCurrentLocClicked: Boolean? = false
    private val AUTOCOMPLETE_REQUEST_CODE = 110

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        viewModelWorker = ViewModelProvider(this).get(PasswordModel::class.java)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModelSearch = ViewModelProvider(this).get(SearchModel::class.java)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(appContext)
        geocoder = Geocoder(activity, Locale.getDefault())
        Places.initialize(
            activity as FragmentActivity,
            activity?.resources?.getString(R.string.map_api_key)!!
        )
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        attachObservers()
        locationPermission()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_personal_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myCalendar = Calendar.getInstance()
        setListeners()
        handleForProfile()
    }

    private fun handleForProfile() {
        if (Constants.IS_FROM_PROFILE == true) {
            isProfileUpdate = true
            constraintBottom?.visibility = View.GONE
            imgBackIcon?.visibility = View.VISIBLE
            linearBottomBtns?.visibility = View.VISIBLE
            txtEdit?.visibility = View.VISIBLE
            lnrBottomLayout?.visibility = View.GONE

            edtFName?.setEnabled(false)
            edtLName?.isEnabled = false
            txtDateOfBirth?.isEnabled = false
            edtEmail?.isEnabled = false
//            txtCityName?.isEnabled = false
            edtManualLocation?.isEnabled = false
            txtCurrentLoc?.isEnabled = false
            chMale?.isEnabled = false
            chFemale?.isEnabled = false

//            viewModelProfile.getWorkerByUserId(userId, false)
        } else {
            var langId = 1
            if (Preferences.prefs?.getString(Constants.SELECTED_LANG, "en").equals("en")) {
                langId = 2
            }
            viewModelProfile.updateLang(langId, userId)
        }
        viewModelProfile.getWorkerByUserId(userId, false)

    }

    /**
     * set listeners to required views
     */
    private fun setListeners() {
//        btnSaveContinue?.setOnClickListener(this)
        txtNext?.setOnClickListener(this)
        imgBackIcon?.setOnClickListener(this)
        imgBack?.setOnClickListener(this)
//        txtCityName?.setOnClickListener(this)
        txtDateOfBirth?.setOnClickListener(this)
        txtEdit?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)
//        handleEdtSearch()
        txtCurrentLoc?.setOnClickListener(this)
        edtManualLocation?.setOnClickListener(this)
        setupFusedApi(false)
    }


    fun locationPermission(): Boolean {
        activity?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ),
                        2011
                    )
//                    Dialogs.showAlert(
//                        it,
//                        resources?.getString(R.string.loctaion_permission_msg),
//                        getString(R.string.allow),
//                        onAlertAllowBtnClick
//                    )


                    return false
                }
                return true
            }
            return true
        }
        return false
    }

    private fun setupFusedApi(isFromButtonClick: Boolean) {
        if (locationPermission()) {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener {
                try {
                    current_latitude = it?.latitude
                    current_longitude = it?.longitude
                    addresses =
                        geocoder?.getFromLocation(
                            current_latitude ?: 0.0,
                            current_longitude ?: 0.0,
                            1
                        )

                    mGoogleAddress = addresses?.get(0)?.getAddressLine(0)
                    val city = addresses?.get(0)?.subAdminArea ?: addresses?.get(0)?.adminArea
                    viewModelSearch.getCityWithText(city)
                    if (isFromButtonClick) {
                        edtManualLocation?.setText(mGoogleAddress ?: "")
                    }

                    Log.e("Location", current_latitude?.toString() + " \n " + current_longitude)
                    Log.e("Location Add", mGoogleAddress ?: "")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun locationEvent(location: LocationEvent) {
        Log.e("EventBus location", location.address?.toString() + "")
        current_latitude = location.lat
        current_longitude = location.lng
        mGoogleAddress = location.address
        val city = location.city
        viewModelSearch.getCityWithText(city)
        edtManualLocation?.setText(mGoogleAddress ?: "")
    }


//    private fun getCityByString(text: String) {
//        if (!Utilities.isOnline(activity)) {
//            Utilities.showToast(activity, resources.getString(R.string.no_internet))
//            return
//        }
//        progressSearch?.visibility = View.VISIBLE
//        viewModelSearch.getCityWithText(text)                 //city api call
//    }
//
//    /**
//     * click listenr when click on any city
//     */
//    private val onCityClick: (CitySearch?, Int?) -> Unit = { it, pos ->
//        Log.e("Pstion", pos.toString())
//        Log.e("CityName", it?.district.toString() + "")
//        txtCityName?.setText(it?.district.toString())
//        mCityId = it?.city_id
////        EventBus.getDefault().post(it)
////        onBackPressed()
//    }

    var onDateSetListener: DatePickerDialogCustom.OnDateSetListener =
        object : DatePickerDialogCustom.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker?,
                year: Int,
                monthOfYear: Int,
                dayOfMonth: Int
            ) {
                txtDateOfBirth?.setText(Utilities.getFormatedDate(year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth))
                if (monthOfYear + 1 < 10) {
                    if (dayOfMonth < 10) {
                        dob = year.toString() + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth
                    } else {
                        dob = year.toString() + "-0" + (monthOfYear + 1) + "-" + dayOfMonth
                    }
                } else {
                    if (dayOfMonth < 10) {
                        dob = year.toString() + "-" + (monthOfYear + 1) + "-0" + dayOfMonth
                    } else {
                        dob = year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth
                    }
                }
            }
        }

    fun getGenderId(): String? {
        if (chMale.isChecked()) {
            return "26"
        }
        return if (chFemale.isChecked()) {
            "27"
        } else "26"
    }

    private fun setDataToViews(response: WorkerDetails?) {
        edtFName?.setText(response?.firstName ?: "")
        edtLName?.setText(response?.lastName ?: "")
        edtEmail?.setText(response?.emailId ?: "")
//        txtCityName?.setText(response?.cityName ?: "")
        mCityName = response?.cityName
        mCityId = response?.cityId

        if (response?.addressLine1?.isEmpty() == false) {
            edtManualLocation?.setText(response.addressLine1)
        }

        dob = response?.birthDate
        txtDateOfBirth?.setText(Utilities.getFormatedDate(dob))
        if (response?.gender.equals("26")) {
            chMale.setChecked(true)
        } else if (response?.gender.equals("27")) {
            chFemale.setChecked(true)
        }
    }

//    /**
//     * handle search while typing
//     */
//    private fun handleEdtSearch() {
//        edtCityName?.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(
//                s: CharSequence,
//                start: Int,
//                count: Int,
//                after: Int
//            ) {
//            }
//
//            override fun onTextChanged(
//                s: CharSequence,
//                start: Int,
//                before: Int,
//                count: Int
//            ) {
//            }
//
//            override fun afterTextChanged(s: Editable) {
//                if (s.toString().length > 1) {
//                    getCityByString(s.toString())
//                } else {
//                    txtNotFound?.visibility = View.GONE
//                    recylerSearch?.visibility = View.GONE
//                }
//            }
//        })
//    }

//    /**
//     * set adapter for searched list
//     */
//    private fun setCityAdapter() {
////        if (mTempList == null) {
////            mTempList = ArrayList<CitySearch>()
////        }
//        val linearLayoutManager = LinearLayoutManager(activity)
//        mSearchAdapter = activity?.let {
//            SearchAdapter(
//                it, mTempList,
//                onCityClick
//            )
//        }
//        recylerSearch?.layoutManager = linearLayoutManager
//        recylerSearch?.adapter = mSearchAdapter
//        scrollView?.scrollTo(0, scrollView?.getBottom() ?: 0)
//    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {

//        viewModelWorker.responseWorker.observe(this, Observer {
//            it?.let {
//                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty()==false ) {
//                    Log.e("Worker id session api", it.response?.content?.get(0)?.workerId?.toString())
//                    Preferences.prefs?.saveValue(
//                        Constants.EMPLOYER_ID,
//                        it.response?.content?.get(0)?.workerId
//                    )
//                }
//            }
//        })

        viewModel.responsePersonalDetail.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Preferences.prefs?.saveValue(
                        Constants.userName,
                        it.response?.content?.get(0)?.firstName + " " + it.response?.content?.get(0)?.lastName
                    )
                    if (Constants.IS_FROM_PROFILE) {
                        Utilities.showToast(activity, "Profile updated.")
                        handleForProfile()
                    } else {
                        OkayGoFirebaseAnalytics.on_boarding_personal_details()
//                        attachFragment(SelfyFragment(), false)
//                        attachFragment(InterestedCatFragment(), true)
                        Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, true)
                        activity?.startActivity(Intent(activity, DashBoardActivity::class.java))
                        activity?.finish()
                    }
                }
            }
        })

        viewModelProfile.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {
                    Preferences.prefs?.saveValue(
                        Constants.EMPLOYER_ID,
                        it.response.content.get(0).workerId
                    )
                    setDataToViews(it.response.content.get(0))

                }
            }
        })

        viewModelSearch.response.observe(this, androidx.lifecycle.Observer {

            if (it.code == Constants.SUCCESS) {
                mCityName = it.response?.get(0)?.district
                mCityId = it.response?.get(0)?.city_id
            }
        })
//
//        viewModelSearch.response.observe(this, androidx.lifecycle.Observer {
//
//            if (it.code == Constants.SUCCESS) {
//                progressSearch?.visibility = View.GONE
//
//                txtNotFound?.visibility = View.GONE
//                recylerSearch?.visibility = View.VISIBLE
//                if (mTempList == null) {
//                    mTempList = ArrayList<CitySearch>()
//                } else {
//                    mTempList?.clear()
//                }
//                var i = 0
//                while (i < it.response?.size ?: 0) {
//                    it.response?.get(i)?.let { it1 -> mTempList?.add(it1) }
//                    i++
//                }
//                if (mTempList?.isEmpty() == true) {
//                    txtNotFound?.visibility = View.VISIBLE
//                    recylerSearch?.visibility = View.GONE
//                }
//
//                Log.e("City Set", mTempList.toString() + "")
////                setCityAdapter()
//            }
//        })
//
//        viewModelSearch.apiError.observe(this, androidx.lifecycle.Observer {
//            progressSearch?.visibility = View.GONE
//
//            txtNotFound?.visibility = View.VISIBLE
//            recylerSearch?.visibility = View.VISIBLE
//        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelProfile.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelProfile.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    /**
     * handel save button click
     */
    private fun handleSaveClick() {
        val fName: String? = edtFName?.text.toString().trim()
        val lName: String? = edtLName?.text.toString().trim()
//        val dob: String? = txtDateOfBirth?.text.toString().trim()
        val email: String? = edtEmail?.text.toString().trim()
//        val city: String? = txtCityName?.text.toString().trim()
        val manualAddress = edtManualLocation?.text?.toString()?.trim()
        var googleAdd = manualAddress
        if (googleAdd == null || googleAdd.isEmpty()) {
            googleAdd = mGoogleAddress ?: ""
        }
        if (fName?.length ?: 0 < 1) {
            edtFName?.error = "Enter First Name"
        } else if (lName?.length ?: 0 < 1) {
            edtLName?.error = "Enter Last Name"
        } else if (dob == null || dob?.length ?: 0 < 8) {
            Utilities.showToast(activity, "Select date of birth")
        } else if (email?.isNotEmpty() == true && Utilities.isValidEmail(email) == false) {
            Utilities.showToast(activity, "Invalid Email id")
        } else if (chgGender?.checkedChipId == -1) {
            Utilities.showToast(activity, "Select gender")
        } else if (googleAdd.length ?: 0 < 1) {
            Utilities.showToast(activity, "Please select or enter your address.")
        } else {
            val request: UserDetailRequest? = UserDetailRequest(
                mCityId.toString(),
                dob,
                fName,
                lName,
                getGenderId(),
                userId.toString(),
                email,
                mCityName,
                null,
                current_latitude, current_longitude,
                mGoogleAddress,
                edtManualLocation?.text?.toString()
            )
            viewModel.saveUserDetail(request)                       //save user api call
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        Log.e("places", "Place: ${place.address}, ${place.id}")
                        val ltLng = place.latLng
                        current_latitude = ltLng?.latitude
                        current_longitude = ltLng?.longitude
                        try {
                            addresses =
                                geocoder?.getFromLocation(
                                    current_latitude ?: 0.0,
                                    current_longitude ?: 0.0,
                                    1
                                )
                            if (addresses?.isEmpty() == false) {
                                mGoogleAddress = addresses?.get(0)?.getAddressLine(0)
                                val city =
                                    addresses?.get(0)?.subAdminArea ?: addresses?.get(0)?.adminArea
                                viewModelSearch.getCityWithText(city)
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        edtManualLocation?.text = mGoogleAddress ?: ""
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.e("place error", status.statusMessage)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtSave,
            R.id.txtNext -> {
                if (userId == 0) {
                    Utilities.logoutUser(userId, activity)
                } else {
//                    isProfileUpdate = true
                    handleSaveClick()
                }
            }
            R.id.imgBackIcon,
            R.id.imgBack -> {
                activity?.onBackPressed()
            }
            R.id.txtEdit -> {
                handleEdit()
            }

            R.id.txtDiscard -> {
                handleForProfile()
            }

//            R.id.txtCityName -> {
//                val intent = Intent(activity, SearchActivity::class.java)
//                startActivity(intent)
//            }

            R.id.edtManualLocation -> {
                // Set the fields to specify which types of place data to
                // return after the user has made a selection.
                val fields = listOf(
                    Place.Field.ID,
                    Place.Field.NAME,
                    Place.Field.LAT_LNG,
                    Place.Field.ADDRESS_COMPONENTS
                )

                // Start the autocomplete intent.
                val intent = activity?.let {
                    Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(it)
                }
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
            }

            R.id.txtCurrentLoc -> {
                if (locationPermission()) {
//                    isCurrentLocClicked = true
//                    setupFusedApi(true)
                    val intent = Intent(activity, CustomMapActivity::class.java)
                    intent.putExtra(Constants.CUR_LAT, current_latitude)
                    intent.putExtra(Constants.CUR_LNG, current_longitude)
                    startActivity(intent)
                }
            }

            R.id.txtDateOfBirth -> {
                SpinnerDatePickerDialogBuilder()
                    .context(activity)
                    .callback(onDateSetListener)
                    .spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true)
                    .showDaySpinner(true)
                    .defaultDate(
                        myCalendar?.get(Calendar.YEAR)?.minus(18) ?: 0,
                        myCalendar?.get(Calendar.MONTH) ?: 0,
                        myCalendar?.get(Calendar.DAY_OF_MONTH)?.minus(1) ?: 0
                    )

                    .minDate(
                        myCalendar?.get(Calendar.YEAR)?.minus(65) ?: 0,
                        myCalendar?.get(Calendar.MONTH) ?: 0,
                        myCalendar?.get(Calendar.DAY_OF_MONTH) ?: 0
                    )
                    .maxDate(
                        myCalendar?.get(Calendar.YEAR)?.minus(18) ?: 0,
                        myCalendar?.get(Calendar.MONTH) ?: 0,
                        myCalendar?.get(Calendar.DAY_OF_MONTH) ?: 0
                    )
                    .build()
                    .show()
            }
        }
    }

    private fun handleEdit() {
        constraintBottom?.visibility = View.VISIBLE
        txtEdit?.visibility = View.GONE
        edtFName?.setEnabled(true)
        edtLName?.isEnabled = true
        txtDateOfBirth?.isEnabled = true
        edtEmail?.isEnabled = true
//        txtCityName?.isEnabled = true
        edtManualLocation?.isEnabled = true
        txtCurrentLoc?.isEnabled = true

        chMale?.isEnabled = true
        chFemale?.isEnabled = true
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun cityName(cityData: CitySearch) {
        Log.e("EventBus Cityname", cityData?.district ?: "")
        mCityId = cityData.city_id
//        txtCityName?.requestFocus()
//        txtCityName?.text = cityData.district.toString()
    }
}