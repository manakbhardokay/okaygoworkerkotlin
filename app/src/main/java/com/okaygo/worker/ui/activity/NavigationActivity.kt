package com.okaygo.worker.ui.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.LocatonPermissionOD
import com.okaygo.worker.data.modal.reponse.Youtube
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.FaqDetailFragment
import com.okaygo.worker.ui.fragments.VideoPlayerFragment
import com.okaygo.worker.ui.fragments.add_exp.AddExperienceFragment
import com.okaygo.worker.ui.fragments.availability.AvailabilityDashBoardFragment
import com.okaygo.worker.ui.fragments.change_password.ChangePasswordFragment
import com.okaygo.worker.ui.fragments.experience.ExperienceAddFragment
import com.okaygo.worker.ui.fragments.job_detail.JobDetailFragment
import com.okaygo.worker.ui.fragments.job_list.JobListFragment
import com.okaygo.worker.ui.fragments.job_prefrence.JobPreferenceFragment
import com.okaygo.worker.ui.fragments.notification.NotificationFragment
import com.okaygo.worker.ui.fragments.on_boarding.EducationLanguageFragment
import com.okaygo.worker.ui.fragments.on_boarding.PersonalDetailFragment
import com.okaygo.worker.ui.fragments.on_boarding.SkillsFragment
import com.okaygo.worker.ui.fragments.rating_review.RatingReviewFragment
import com.okaygo.worker.ui.fragments.referral.DocumentDetailFragment
import com.okaygo.worker.ui.fragments.referral.PaymentDetailFragment
import com.okaygo.worker.ui.fragments.referral.ReferralFragment
import com.okaygo.worker.ui.fragments.schedual_interview.SchedualInterviewFragment
import com.okaygo.worker.ui.fragments.web_view.WebViewFragment
import com.openkey.guest.help.Dialogs
import org.greenrobot.eventbus.EventBus

class NavigationActivity : BaseActivity() {
    private var mScreen = 0
    private var mIntent: Intent? = null
    private var mYoutubeVideoResponse: Youtube? = null
    private var isApiCalled = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        mIntent = intent
        if (mIntent != null && mIntent?.hasExtra(Constants.NAV_SCREEN) == true) {
            mScreen = mIntent!!.getIntExtra(Constants.NAV_SCREEN, 0)
            if (mScreen == 1) {
                mYoutubeVideoResponse =
                    mIntent?.getParcelableExtra(Constants.YOUTUBE_RESPONSE) as? Youtube
            }
            navigateToScreen()
        } else {
            onBackPressed()
        }
    }

    private fun navigateToScreen() {
        val bundle = Bundle()
        var baseFragment: BaseFragment? = null
        when (mScreen) {
            Constants.VIDEO_PLAYER -> {
                baseFragment = VideoPlayerFragment()
                bundle.putParcelable(Constants.YOUTUBE_RESPONSE, mYoutubeVideoResponse)
            }
            Constants.JOB_LIST -> {
                if (mIntent?.extras != null && mIntent?.hasExtra(Constants.SELECTED_JOB_CAT_DATA) == true) {
                    bundle.putParcelable(
                        Constants.NAVIGATION_DATA,
                        mIntent?.getParcelableExtra(Constants.SELECTED_JOB_CAT_DATA)
                    )
                    baseFragment = JobListFragment()
                }
            }
            Constants.SKILLs -> baseFragment = SkillsFragment()

            Constants.PERSONAL_DETAIL -> baseFragment = PersonalDetailFragment()
            Constants.DOCUMENT_DETAIL -> baseFragment = DocumentDetailFragment()
            Constants.EDUCATION_LANGUAGE -> baseFragment = EducationLanguageFragment()
//            Constants.EXPERIENCE -> baseFragment = ExperienceFragment()
            Constants.EXPERIENCE -> baseFragment = ExperienceAddFragment()
            Constants.JOB_PREFREBCE -> baseFragment = JobPreferenceFragment()
            Constants.CHANGE_PASSWORD -> baseFragment = ChangePasswordFragment()
            Constants.FAQ -> baseFragment = FaqDetailFragment()
            Constants.JOB_DETAIL -> {
                if (mIntent?.extras != null) {
                    if (mIntent?.hasExtra(Constants.NAVIGATION_DATA) == true) {
                        bundle.putSerializable(
                            Constants.JOB_DETAIL_DATA,
                            mIntent?.getSerializableExtra(Constants.NAVIGATION_DATA)
                        )
                    } else if (mIntent?.hasExtra(Constants.JOB_ID) == true) {
                        bundle?.putString(
                            Constants.JOB_ID,
                            mIntent?.getStringExtra(Constants.JOB_ID)
                        )
                    } else if (mIntent?.hasExtra(Constants.JOB_DETAIL_ID) == true) {
                        bundle?.putString(
                            Constants.JOB_DETAIL_ID,
                            mIntent?.getStringExtra(Constants.JOB_DETAIL_ID)
                        )
                    }
                    baseFragment = JobDetailFragment()
                }
            }
            Constants.NOTIFICATION -> baseFragment = NotificationFragment()
            Constants.AVAILABILITY_DASHBOARD -> baseFragment = AvailabilityDashBoardFragment()
            Constants.REFFERAL -> baseFragment = ReferralFragment()
            Constants.PAYMENT_DETAIL -> baseFragment = PaymentDetailFragment()
            Constants.RATING_REVIEW -> baseFragment = RatingReviewFragment()
            Constants.INTERVIEW_SCHEDUAL_OD -> {
                if (mIntent?.extras != null && mIntent?.hasExtra(Constants.NAVIGATION_DATA) == true) {
                    bundle.putParcelable(
                        Constants.NAVIGATION_DATA,
                        mIntent?.getParcelableExtra(Constants.NAVIGATION_DATA)
                    )

                    baseFragment = SchedualInterviewFragment()
                }
            }

            Constants.WEBVIEW -> {
                if (mIntent?.hasExtra(Constants.WEB_URL) == true) {
                    bundle?.putString(
                        Constants.WEB_URL,
                        mIntent?.getStringExtra(Constants.WEB_URL)
                    )
                }
                baseFragment = WebViewFragment()
            }
        }

        baseFragment?.arguments = bundle
        baseFragment?.let { addFragment(it, false) }
    }

    override fun onBackPressed() {
        val fragment: Fragment? = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment !is AddExperienceFragment) {
            finish()
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("Navigation destroy", "Called")
        Constants.IS_FROM_PROFILE = false
    }

    private val onAlertDenyClick: () -> Unit = {
        Utilities.showToast(this, "You have to allow location permission to apply for OD jobs.")
    }

    private val onAlertAllowBtnClick: () -> Unit = {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            5012
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                5012
            )
        }
    }

    fun loactionPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Dialogs.showAlertDialog(
                    this,
                    "Alert!",
                    "This app collects location data so as to allow tracking your location before job start time even if the app is in background.",
                    "Allow",
                    "Deny",
                    onAlertAllowBtnClick,
                    onAlertDenyClick
                )


//                else {
//                    ActivityCompat.requestPermissions(
//                        this,
//                        arrayOf(
//                            Manifest.permission.ACCESS_FINE_LOCATION,
//                            Manifest.permission.ACCESS_COARSE_LOCATION
//                        ),
//                        5011
//                    )
//                }
                return false
            }
            return true
        }
        return true

    }


//    fun loactionPermission(): Boolean {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_COARSE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
//                || ContextCompat.checkSelfPermission(
//                    this,
//                    Manifest.permission.ACCESS_FINE_LOCATION
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                    ActivityCompat.requestPermissions(
//                        this, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
//                        5012
//                    )
//                } else {
//                    ActivityCompat.requestPermissions(
//                        this,
//                        arrayOf(
//                            Manifest.permission.ACCESS_FINE_LOCATION,
//                            Manifest.permission.ACCESS_COARSE_LOCATION
//                        ),
//                        5012
//                    )
//                }
//                return false
//            }
//            return true
//        }
//        return true
//    }

    private val onAlertNagClick: () -> Unit = {
        Log.e("permission onNagClick", "true")
//        EventBus.getDefault().post(LocatonPermissionOD())
        Utilities.showToast(this, "You have to allow location permission to apply for OD jobs.")
    }

    private val onAlertPosBtnClick: () -> Unit = {
        Log.e("permission onPosClick", "true")
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", getPackageName(), null)
        intent.data = uri
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 5012) {
            Log.e("permission size", permissions.size.toString() + " ")
            for (i in 0..permissions.size - 1) {
                val permission = permissions[i]
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    Log.e("permission", "deny")
                    // user rejected the permission
                    val showRationale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        shouldShowRequestPermissionRationale(permission)
                    } else {
                        Log.e("permission", "not m")
                    }
                    if (showRationale == false && !Constants.isApiCalled) {
                        Log.e("showRationale", showRationale.toString() + " false case")
                        Constants.isApiCalled = true
                        Dialogs.showAlertDialog(
                            this,
                            "Required Location permission",
                            "For On Demand Jobs, we will be needing permission to access your location. Therefore, please provide the Location permission from settings.",
                            "Settings",
                            "Deny",
                            onAlertPosBtnClick,
                            onAlertNagClick
                        )

                    } else if ((Manifest.permission.ACCESS_FINE_LOCATION.equals(permission) || Manifest.permission.ACCESS_COARSE_LOCATION.equals(
                            permission
                        ) || Manifest.permission.ACCESS_BACKGROUND_LOCATION.equals(permission)) && !Constants.isApiCalled
                    ) {
                        Constants.isApiCalled = true
                        Log.e("showRationale", showRationale.toString() + " DENY case")
//                        EventBus.getDefault().post(LocatonPermissionOD())
                        Utilities.showToast(
                            this,
                            "You have to allow location permission to apply for OD jobs."
                        )
                    }
                } else if (grantResults[i] == PackageManager.PERMISSION_GRANTED && !Constants.isApiCalled) {
                    Constants.isApiCalled = true
                    Log.e("permission", "grant")
                    EventBus.getDefault().post(LocatonPermissionOD())
                }
            }
        }
    }
}