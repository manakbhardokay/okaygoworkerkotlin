package com.okaygo.worker.ui.fragments.earning

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.okaygo.worker.R
import com.okaygo.worker.adapters.PaymentAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.PaymentResponse
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.ui.fragments.verification.PaymentModel
import kotlinx.android.synthetic.main.fragment_earning_tab.*

class JobsEarningFragment(val paymentStatus: Int? = null) : BaseFragment() {
    private lateinit var viewModel: PaymentModel
    private var mPaymentList: ArrayList<PaymentResponse>? = null
    private var mTempList: ArrayList<PaymentResponse>? = null
    private var mAdapter: PaymentAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PaymentModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_earning_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
    }

    override fun onResume() {
        super.onResume()
        if (Utilities.isOnline(activity)) {
            viewModel.getPaymentTransection(userId, paymentStatus)
        } else {
            imgNoData?.visibility = View.VISIBLE
            txtMsg?.visibility = View.VISIBLE
        }
    }

    private fun setAdapter() {
        if (mPaymentList == null) {
            mPaymentList = ArrayList()
        }
        val linearLayoutManager = LinearLayoutManager(activity)
        mPaymentList?.let {
            mAdapter = PaymentAdapter(context, it, onInvoiceClick)
            recylerTabs?.layoutManager = linearLayoutManager
            recylerTabs?.adapter = mAdapter
        }
    }


    private val onInvoiceClick: (PaymentResponse?, Int?) -> Unit = { it, pos ->
        if (it?.invoiceFilePath?.isEmpty() == false) {
            OkayGoFirebaseAnalytics.viewInvoice(it.invoiceNo?.toString() ?: "")
            Utilities.openPdf(activity, it.invoiceFilePath)
        } else {
            Utilities.showToast(activity, "No invoice generated for this yet.")
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseTransection.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mTempList == null) {
                        mTempList = ArrayList()
                    } else {
                        mTempList?.clear()
                    }
                    it.response?.content?.let { it1 -> mTempList?.addAll(it1) }
//                    viewModel.getPaymentTransectionPT_FT(userId)
//                    recylerTabs?.visibility = View.VISIBLE
//                    imgNoData?.visibility = View.GONE
//                    txtMsg?.visibility = View.GONE
//                    mPaymentList?.addAll(it.response.content)
//                    mAdapter?.notifyDataSetChanged()

                } else {
                    recylerTabs?.visibility = View.GONE
                    imgNoData?.visibility = View.VISIBLE
                    txtMsg?.visibility = View.VISIBLE
                    txtMsg?.text = "No data found"
                    imgNoData?.background = activity?.let { it1 ->
                        ContextCompat.getDrawable(
                            it1,
                            R.drawable.ic_no_records
                        )
                    }
                }
            }
        })

        viewModel.responseTransectionPT_FT.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.content?.isEmpty() == false) {
                        if (mTempList == null) {
                            mTempList = ArrayList()
//                    } else {
//                        mPaymentList?.clear()
                        }
                        it.response?.content?.let { it1 -> mTempList?.addAll(it1) }

//                    val sortedList = mTempList?.sortedWith(compareBy({ it.insertedOn }))
                        val sortedList =
                            mTempList?.sortedWith(compareByDescending<PaymentResponse> { it.insertedOn }
                                .thenByDescending { it.insertedOn })

                        if (mPaymentList == null) {
                            mPaymentList = ArrayList()
                        } else {
                            mPaymentList?.clear()
                        }
                        recylerTabs?.visibility = View.VISIBLE
                        imgNoData?.visibility = View.GONE
                        txtMsg?.visibility = View.GONE
//                    it.response?.content?.let { it1 -> mPaymentList?.addAll(it1) }
                        if (paymentStatus == null) {
                            sortedList?.let {
                                mPaymentList?.addAll(it)
                            }
                        } else {
                            for (i in 0 until (sortedList?.size ?: 0)) {
                                sortedList?.let {
                                    if ((it.get(i).isWorkerPaid) ?: "0" == paymentStatus.toString()) {
                                        mPaymentList?.add(it.get(i))
                                    }
                                }
                            }
                        }
                        mAdapter?.notifyDataSetChanged()

                    } else {
                        recylerTabs?.visibility = View.GONE
                        imgNoData?.visibility = View.VISIBLE
                        txtMsg?.visibility = View.VISIBLE
                        txtMsg?.text = "No data found"
                        imgNoData?.background = activity?.let { it1 ->
                            ContextCompat.getDrawable(
                                it1,
                                R.drawable.ic_no_records
                            )
                        }
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}