package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object JobPreferenceRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun saveJobCat(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?,
        cat: String?
    ) {
        mService.saveJobCat(workerId, cat)?.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(
                call: Call<SuccessResponse>?,
                response: Response<SuccessResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("save cat failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}