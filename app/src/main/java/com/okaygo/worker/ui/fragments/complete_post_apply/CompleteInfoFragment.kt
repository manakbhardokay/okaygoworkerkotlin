package com.okaygo.worker.ui.fragments.complete_post_apply

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.Chip
import com.google.android.material.textfield.TextInputEditText
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.api.response.SkillResponse
import com.okaygo.worker.data.api.response.UserSkills
import com.okaygo.worker.data.modal.JobApplySuccess
import com.okaygo.worker.data.modal.reponse.JobContent
import com.okaygo.worker.data.modal.reponse.QuestionareResponseItem
import com.okaygo.worker.data.modal.reponse.ReqiredSkillsResponse
import com.okaygo.worker.data.modal.reponse.WorkerDetails
import com.okaygo.worker.data.modal.request.UpdateSkill
import com.okaygo.worker.data.modal.request.UpdateWorkerRequest
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.experience.ExperienceAddFragment
import com.okaygo.worker.ui.fragments.questionare.QuestionareFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.ui.fragments.verification.*
import kotlinx.android.synthetic.main.fragment_complete_application_info.*
import org.greenrobot.eventbus.EventBus

class CompleteInfoFragment : BaseFragment(), View.OnClickListener {
    private var mCurentEduId: Int? = null
    private var mJobData: JobContent? = null
    private var mAssignId: Int? = null

    private lateinit var viewModel: OnBoardingModel
    private lateinit var viewModelProfile: ProfileModel
    private lateinit var viewModelExperince: ExperinceModel
    private lateinit var viewModelPaymentDetail: PaymentDetailModel
    private lateinit var viewModelJobList: JobListModel
    private lateinit var viewModelQues: QuestionareModel

    private var mUserSkills: ArrayList<Int>? = null
    private var mCurentLangId: Int? = null
    private var mSelectedCat: String? = null
    private var mOtherSkills: ArrayList<SkillResponse>? = null
    private var mQuestList: ArrayList<QuestionareResponseItem>? = null
    private var mLang: Int? = null
    private var mAdhaar: Int? = null
    private var mRC: Int? = null
    private var mDL: Int? = null
    private var mLAPTOP: Int? = null
    private var mSMARTPHONE: Int? = null
    private var mWifi: Int? = null
    private var mBike: Int? = null
    private var mPan: Int? = null
    private var isUserSkills: Boolean? = false
    private var isUserEdu: Boolean? = false
    private var isNavToExp: Boolean? = false
    private var isUserLang: Boolean? = false
    private var isUserSkillsOpt: Boolean? = true
    private var isUserEduOpt: Boolean? = true
    private var isUserExpOpt: Boolean? = true
    private var isUserLangOpt: Boolean? = true
    private var mEdu: Int? = null
    var mSkillList: ArrayList<UpdateSkill>? = null
    var mRequiredData: ReqiredSkillsResponse? = null
    private var upi: String? = ""
    private var isDialogForUpi = false

    var bundle: Bundle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        viewModelExperince = ViewModelProvider(this).get(ExperinceModel::class.java)
        viewModelPaymentDetail = ViewModelProvider(this).get(PaymentDetailModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        viewModelQues = ViewModelProvider(this).get(QuestionareModel::class.java)

        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_complete_application_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle: Bundle? = arguments
        if (bundle?.containsKey(Constants.POST_APPLY_JOB_DATA) == true) {
            mJobData = bundle.getSerializable(Constants.POST_APPLY_JOB_DATA) as JobContent
        }
        if (bundle?.containsKey(Constants.POST_APPLY_REQUIRED_DATA) == true) {
            mRequiredData =
                bundle.getSerializable(Constants.POST_APPLY_REQUIRED_DATA) as ReqiredSkillsResponse
            mAssignId = bundle.getInt(Constants.POST_Assign_id)
        }
        mOtherSkills = ArrayList()
        showHideFields()
        setListeners()
        OkayGoFirebaseAnalytics.postApplicationEditInfo(
            mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID
        )
        viewModelProfile.getWorkerByUserId(userId)
        viewModelQues.getJobQuestions(mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID)

    }


    private fun setListeners() {
        txtChipOther?.setOnClickListener(this)
        txtAddSkill?.setOnClickListener(this)
        txtNext?.setOnClickListener(this)

        txtWifiYes?.setOnClickListener(this)
        txtWifiNo?.setOnClickListener(this)
        txtLaptopYes?.setOnClickListener(this)
        txtLaptopNo?.setOnClickListener(this)
        txtAdhaarYes?.setOnClickListener(this)
        txtAdhaarNo?.setOnClickListener(this)
        txtPanNo?.setOnClickListener(this)
        txtPanYes?.setOnClickListener(this)
        txtSmartYes?.setOnClickListener(this)
        txtSmartNo?.setOnClickListener(this)
        txtBikeYes?.setOnClickListener(this)
        txtBikeNo?.setOnClickListener(this)
        txtRCYes?.setOnClickListener(this)
        txtRCNo?.setOnClickListener(this)
        txtLicenceYes?.setOnClickListener(this)
        txtLicenceNo?.setOnClickListener(this)

    }

    private fun showHideFields() {
        if (mRequiredData?.response?.isEmpty() == false) {
            for (i in 0..(mRequiredData?.response?.size ?: 0) - 1) {
                when (mRequiredData?.response?.get(i)?.skillId) {
                    Constants.USER_EDUCATION -> {
                        isUserEdu = true
                        if (mRequiredData?.response?.get(i)?.mandatory == true) {
                            isUserEduOpt = false
                        }
                        linearEducation?.visibility = View.VISIBLE
                    }

                    Constants.USER_LANG -> {
                        isUserLang = true
                        if (mRequiredData?.response?.get(i)?.mandatory == true) {
                            isUserLangOpt = false
                        }
                        linearLang?.visibility = View.VISIBLE
                    }

                    Constants.USER_LAPTOP -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtLaptop?.visibility = View.VISIBLE
                        txtLaptopYes?.visibility = View.VISIBLE
                        txtLaptopNo?.visibility = View.VISIBLE
                    }
                    Constants.USER_CV,
                    Constants.USER_TOTAL_EXP,
                    Constants.USER_NOTICE,
                    Constants.USER_LAST_SALARY,
                    Constants.USER_EXP -> {
                        isNavToExp = true
                    }
                    Constants.USER_SMARTPHONE -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtSmartPhone?.visibility = View.VISIBLE
                        txtSmartYes?.visibility = View.VISIBLE
                        txtSmartNo?.visibility = View.VISIBLE
                    }
                    Constants.USER_WIFI -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtWifi?.visibility = View.VISIBLE
                        txtWifiYes?.visibility = View.VISIBLE
                        txtWifiNo?.visibility = View.VISIBLE
                    }
                    Constants.USER_BIKE -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtBike?.visibility = View.VISIBLE
                        txtBikeNo?.visibility = View.VISIBLE
                        txtBikeYes?.visibility = View.VISIBLE
                    }
                    Constants.USER_DL -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtLicence?.visibility = View.VISIBLE
                        txtLicenceYes?.visibility = View.VISIBLE
                        txtLicenceNo?.visibility = View.VISIBLE
                    }
                    Constants.USER_RC -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtRC?.visibility = View.VISIBLE
                        txtRCNo?.visibility = View.VISIBLE
                        txtRCYes?.visibility = View.VISIBLE
                    }
                    Constants.USER_ADHAAR -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtAdhaar?.visibility = View.VISIBLE
                        txtAdhaarNo?.visibility = View.VISIBLE
                        txtAdhaarYes?.visibility = View.VISIBLE
                    }
                    Constants.USER_PAN -> {
                        constraintOwnerShip?.visibility = View.VISIBLE
                        txtPan?.visibility = View.VISIBLE
                        txtPanNo?.visibility = View.VISIBLE
                        txtPanYes?.visibility = View.VISIBLE
                    }

                    Constants.USER_SKILLS -> {
                        isUserSkills = true
                        if (mRequiredData?.response?.get(i)?.mandatory == true) {
                            isUserSkillsOpt = false
                        }
                        linearSkills?.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun addLanguage() {
        chgLang?.removeAllViews()
        var chip: Chip? = null
        var tagName: String
        for (index in 1..3) {
            when (index) {
                1 -> {
                    chip = Chip(activity)
                    tagName = getString(R.string.no_eng)
                }
                2 -> {
                    chip = Chip(activity)
                    tagName = getString(R.string.thoda_eng)
                }
                3 -> {
                    chip = Chip(activity)
                    tagName = getString(R.string.good_eng)
                }
                else -> {
                    chip = Chip(activity)
                    tagName = ""
                }
            }
            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            chip.text = tagName
            chip.id = index
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 2f
            if (mCurentLangId != null && index == mCurentLangId) {
                chip.isChecked = true
            }
            chgLang?.addView(chip)
        }
//        if (Constants.IS_FROM_PROFILE) {
//            for (i in 0 until chgLang?.getChildCount()!!) {
//                chgLang?.getChildAt(i)?.setEnabled(false)
//            }
//        }
    }

    private fun getData() {
        if (isUserSkills == true) {
            saveSkills()
        }
        mLang = chgLang?.checkedChipId
        mEdu = chgEdu?.checkedChipId
    }

    private fun saveSkills() {
        if (checkSelectedSkillCount()) {
            if (mSkillList == null) {
                mSkillList = ArrayList()
            } else {
                mSkillList?.clear()
            }

            var skills: UpdateSkill

            for (i in 0 until chgSkills?.getChildCount()!!) {
                val chip: Chip? = chgSkills?.findViewById(
                    chgSkills?.getChildAt(i)?.getId()
                        ?: 0
                )
                if (chip?.isChecked == true) {
                    skills = UpdateSkill(chip.text.toString(), chip.id, userId)
                    mSkillList?.add(skills)
                }
            }

            for (i in 0 until mOtherSkills?.size!!) {
                skills = UpdateSkill(
                    mOtherSkills?.get(i)?.skills_name?.toString(),
                    mOtherSkills?.get(i)?.skills_type_id,
                    userId
                )
                mSkillList?.add(skills)
            }
        }
    }

    private fun checkSelectedSkillCount(): Boolean {
        val tempList: ArrayList<String>? = ArrayList()

        for (i in 0 until (chgSkills?.getChildCount() ?: 0)) {
            val chip: Chip = chgSkills.findViewById(chgSkills.getChildAt(i).getId())
            if (chip.isChecked) {
                tempList?.add(chip.id.toString() + "")
            }
        }

        for (i in 0 until (mOtherSkills?.size ?: 0)) {
            tempList?.add(mOtherSkills?.get(i)?.skills_type_id?.toString() + "")
        }


        if (Utilities.isOnline(activity) == false) {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show()
        } else
            if (tempList?.size ?: 0 > 3) {
                Toast.makeText(activity, "You can select maximum 3 job skills.", Toast.LENGTH_SHORT)
                    .show()
                return false

            } else if (tempList?.size == 0) {
                Toast.makeText(activity, "Select atlest one job category.", Toast.LENGTH_SHORT)
                    .show()
                return false
            }
        return true
    }

    private fun setYesActive(txt: AppCompatTextView?, selected: Boolean) {
        if (selected) {
            txt?.background = activity?.resources?.getDrawable(R.drawable.sp_eclips_blue_filled_40)
            activity?.let { ContextCompat.getColor(it, R.color.white) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        } else {
            txt?.background =
                activity?.resources?.getDrawable(R.drawable.sp_eclips_blue_border_40dp)
            activity?.let { ContextCompat.getColor(it, R.color.theme) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        }
    }

    private fun setNoActive(txt: AppCompatTextView?, selected: Boolean) {
        if (selected) {
            txt?.background = activity?.resources?.getDrawable(R.drawable.sp_eclips_blue_filled_40)
            activity?.let { ContextCompat.getColor(it, R.color.white) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        } else {
            txt?.background = null
            activity?.let { ContextCompat.getColor(it, R.color.text_black) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        }
    }

    private fun setDataToInfo(response: WorkerDetails?) {
        mCurentLangId = response?.english_known_level
        if (isUserLang == true) {
            addLanguage()
        }
        mCurentEduId = response?.qualificationId
        if (isUserSkills == true) {
//            for (i in 0..(response?.workerExperiences?.size ?: 0) - 1) {
//                if (mSelectedCat?.isEmpty() == true) {
//                    mSelectedCat = response?.workerExperiences?.get(i)?.industryType?.toString()
//                } else {
//                    mSelectedCat =
//                        mSelectedCat + "," + response?.workerExperiences?.get(i)?.industryType?.toString()
//                }
//            }
//            if (mSelectedCat?.isEmpty() == true) {
//                mSelectedCat = response?.interested_cat
//            } else {
//                mSelectedCat = mSelectedCat + "," + response?.interested_cat
//            }
            viewModel.getUserSkills(userId)
        }

        if (isUserEdu == true) {
            viewModel.getEducation()
        }

        if (response?.own_bike == 1) {
            setYesActive(txtBikeYes, true)
            mBike = 1
        } else {
            setNoActive(txtBikeNo, true)
            mBike = 0
        }

        if (response?.bike_license == 1) {
            mDL = 1
            setYesActive(txtLicenceYes, true)
        } else {
            mDL = 0
            setNoActive(txtLicenceNo, true)
        }

//        if (response?.car_license == 1) {
//            mDL = 1
//            setYesActive(txtLicenceYes, true)
//        } else {
//            mDL = 0
//            setNoActive(txtLicenceNo, true)
//        }

        if (response?.own_laptop == 1) {
            mLAPTOP = 1
            setYesActive(txtLaptopYes, true)
        } else {
            mLAPTOP = 0
            setNoActive(txtLaptopNo, true)
        }

        if (response?.own_aadhar_card == 1) {
            mAdhaar = 1
            setYesActive(txtAdhaarYes, true)
        } else {
            mAdhaar = 0
            setNoActive(txtAdhaarNo, true)
        }
        if (response?.own_pan_card == 1) {
            mPan = 1
            setYesActive(txtPanYes, true)
        } else {
            mPan = 0
            setNoActive(txtPanNo, true)
        }

        if (response?.own_smartphone == 1) {
            mSMARTPHONE = 1
            setYesActive(txtSmartYes, true)
        } else {
            mSMARTPHONE = 0
            setNoActive(txtSmartNo, true)
        }

        if (response?.own_vehicle_rc == 1) {
            mRC = 1
            setYesActive(txtRCYes, true)
        } else {
            mRC = 0
            setNoActive(txtRCNo, true)
        }

        if (response?.own_wifi == 1) {
            mWifi = 1
            setYesActive(txtWifiYes, true)
        } else {
            mWifi = 0
            setNoActive(txtWifiNo, true)
        }
    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {

        viewModel.responseEducation.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    chgEdu?.removeAllViews()

                    for (index in 0 until ((it.response?.content?.size) ?: 0)) {
                        val tagName: String = it.response?.content?.get(index)?.typeValue ?: ""
                        val chip = Chip(activity)
                        chip.isCheckable = true
                        chip.isCheckedIconVisible = false
                        chip.text = tagName
                        chip.id = it.response?.content?.get(index)?.id ?: 0
                        chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
                        chip.setChipStrokeColorResource(R.color.theme)
                        chip.chipStrokeWidth = 2f
                        if (mCurentEduId != null) {
                            if (it.response?.content?.get(index)?.id == mCurentEduId) {
                                chip.isChecked = true
                            }
                        }
                        chgEdu?.addView(chip)
                    }

                }
            }
        })
        viewModel.responseAddSkills.observe(this, Observer {
            it?.let {

                if (it.code == Constants.SUCCESS) {
                    it.response?.let { it1 -> mOtherSkills?.add(it1) }
                    cpgOthers?.removeAllViews()
                    edtOtherJobType?.setText("")
                    if (mOtherSkills?.size ?: 0 > 0) {
                        for (i in 0..(mOtherSkills?.size ?: 0) - 1) {

                            val tagName: String = mOtherSkills?.get(i)?.skills_name
                                ?: ""
                            val chip = Chip(activity)

                            chip.isCheckable = true
                            chip.isCheckedIconVisible = false

                            chip.text = tagName
                            chip.id = mOtherSkills?.get(i)?.skills_type_id ?: 0
                            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
                            chip.setChipStrokeColorResource(R.color.theme)
                            chip.chipStrokeWidth = 1f
//                                        chip.isClickable = true
                            chip.isChipIconVisible = true

                            chip.isCloseIconVisible = true
                            chip.isCheckedIconVisible = false

                            //Added click listener on close icon to remove tag from ChipGroup
                            chip.setOnCloseIconClickListener {
                                if (mOtherSkills?.size ?: 0 > i) {
                                    mOtherSkills?.removeAt(i)
                                }
                                cpgOthers.removeView(chip)
                            }

                            cpgOthers?.addView(chip)

                        }
                    }
                }
            }
        })

        viewModelQues.responseJobQuestions.observe(this, Observer {
            it?.let {
                if (!it.isEmpty()) {
                    if (mQuestList == null) {
                        mQuestList = ArrayList()
                    } else {
                        mQuestList?.clear()
                    }
                    mQuestList?.addAll(it)
//                    if (mQuestList?.size == 1) {
//                        txtNext?.text = "APPLY"
//                    }
//                    mAdapter?.notifyDataSetChanged()
//                    setProgress()
                } else {
                    if (isNavToExp == false) {
                        viewModelPaymentDetail.getUPI(workerId, false)
                        txtNext?.text = "APPLY"
                    } else {
                    }
                }
            }
        })

        viewModelPaymentDetail.responseSaveUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response
                    Utilities.showToast(activity, "UPI Id is updated.")
                    EventBus.getDefault().post(JobApplySuccess(true))
                    activity?.finish()
                }
            }
        })
        viewModelPaymentDetail.responseGetUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response ?: ""
                }
            }
        })

        viewModelExperince.responseSaveData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (isNavToExp == true) {
                        OkayGoFirebaseAnalytics.postApplicationEditExp(
                            mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID
                        )
                        Constants.IS_FROM_PROFILE = false
                        val fragment = ExperienceAddFragment()
                        fragment.arguments = bundle
                        fragment.let { attachFragment(it, true) }
                    } else if (mQuestList?.isEmpty() == false) {
                        val fragment = QuestionareFragment()
                        fragment.arguments = bundle
                        fragment.let { attachFragment(it, true) }
                    } else {
                        viewModelJobList.applyJob(
                            mAssignId,
                            userId,
                            Constants.DEEPLINK_REFFER_BY
                        )
                    }
                }
            }
        })

        viewModelJobList.responseApplyJob.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    if (it.code == Constants.SUCCESS) {
                        var workType: String? = ""
                        if (mJobData == null) {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                Constants.DEEPLINK_WORK_TYPE,
                                Constants.DEEPLINK_JOB_TYPE,
                                Constants.DEEPLINK_JOB_DETAIL_ID?.toString(),
                                "apply"
                            )
                            workType = Constants.DEEPLINK_WORK_TYPE
                        } else {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                mJobData?.workType,
                                mJobData?.jobType,
                                mJobData?.jobDetailId?.toString(),
                                "apply"
                            )
                            workType = mJobData?.workType
                        }
                        if (workType.equals("ON DEMAND", true) && upi?.isEmpty() == true) {
                            isDialogForUpi = false
                            Dialogs.showAlertDialog(
                                activity,
                                "Job applied",
                                activity?.resources?.getString(R.string.applied_msg_od),
                                "UPDATE",
                                "LATER",
                                onAlertPosBtnClick,
                                onAlertNagClick
                            )
                        } else {
                            Utilities.jobSuccessDialog(
                                activity,
                                activity?.resources?.getString(R.string.job_applied),
                                activity?.resources?.getString(R.string.applied_success_descriotion),
                                true, onOKClick
                            )
                        }
                    }
                }
            }
        })

        viewModelProfile.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {

                    setDataToInfo(it.response.content.get(0))


                }
            }
        })
        viewModel.responseAllSkills.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.size ?: 0 > 0) {
                        for (i in 0..(it.response?.size ?: 0) - 1) {
                            setChips(it.response?.get(i), null, false)
                        }
                    }
                }
            }
        })

        viewModel.responseUserSkills.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mUserSkills == null) {
                        mUserSkills = ArrayList()
                    } else {
                        mUserSkills = ArrayList()
                    }
                    chgSkills?.removeAllViews()
                    cpgOthers?.removeAllViews()
                    edtOtherJobType?.setText("")
                    if (it.response?.size ?: 0 > 0) {
                        for (i in 0..(it.response?.size ?: 0) - 1) {
                            mUserSkills?.add(it.response?.get(i)?.skillsTypeId ?: 0)
                            setChips(null, it.response?.get(i), true)
                        }
                    }
//                    viewModel.getAllSkills(mSelectedCat, mSelectedCat, false)
                    if (mJobData == null) {
                        viewModel.getAllSkills(
                            Constants.DEEPLINK_JOB_CAT_ID.toString(),
                            Constants.DEEPLINK_JOB_TYPE_ID.toString(),
                            false
                        )
                    } else {
                        viewModel.getAllSkills(
                            mJobData?.job_category_id?.toString(),
                            mJobData?.job_type_id?.toString(),
                            false
                        )
                    }
                }
            }
        })

        viewModelProfile.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelProfile.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelJobList.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelJobList.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelExperince.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelExperince.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelPaymentDetail.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelPaymentDetail.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private val onOKClick: () -> Unit = {
        EventBus.getDefault().post(JobApplySuccess(true))
        activity?.finish()
    }

    private fun setChips(
        response: SkillResponse?,
        userResponse: UserSkills?,
        isUserSkills: Boolean
    ) {
        var chip: Chip? = null
        if (isUserSkills == false) {
            val tagName: String = response?.skills_name
                ?: ""
            chip = Chip(activity)

            chip.isCheckable = true
            chip.isCheckedIconVisible = false

            chip.text = tagName
            chip.id = response?.skills_type_id
                ?: 0
        } else {
            val tagName: String = userResponse?.skillsName
                ?: ""
            chip = Chip(activity)

            chip.isCheckable = true
            chip.isCheckedIconVisible = false

            chip.text = tagName
            chip.id = userResponse?.skillsTypeId
                ?: 0
            chip.isChecked = true
        }
//        if (Constants.IS_FROM_PROFILE_SKILLS == true) {
//            chip.isEnabled = false
//            txtChipOther?.isEnabled = false
//        }
        chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
        chip.setChipStrokeColorResource(R.color.theme)
        chip.chipStrokeWidth = 1f
        chip.isClickable = true
        chip.isChipIconVisible = true

        chip.isCloseIconVisible = false
        chip.isCheckedIconVisible = false

        chgSkills?.addView(chip)
        if (isUserSkills == false) {
            if (mUserSkills?.isNotEmpty() == true && mUserSkills?.contains(response?.skills_type_id) == true) {
                chgSkills?.removeView(chip)
            }
        }
    }

    private val onAlertNagClick: () -> Unit = {
        if (isDialogForUpi) {
            showUpiDialog(upi)
        } else {
            EventBus.getDefault().post(JobApplySuccess(true))
            activity?.finish()
        }
    }

    private val onAlertPosBtnClick: () -> Unit = {
        if (isDialogForUpi == false) {
            showUpiDialog(upi)
        } else {
            viewModelPaymentDetail.updateUPI(workerId, upi)
        }
    }


    private fun showUpiDialog(savedUpi: String?) {
        activity?.let {
            val dialog = Dialog(it)
            dialog.setContentView(R.layout.dialog_salary)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val title = dialog.findViewById<TextView>(R.id.title)
            val edtUpi = dialog.findViewById<TextInputEditText>(R.id.salary)
            val cancel = dialog.findViewById<TextView>(R.id.cancel)
            val confirm = dialog.findViewById<TextView>(R.id.confirm)
            title.text = "Enter your valid UPI id"
            confirm.text = "Save"
            edtUpi?.setHint("")
            edtUpi?.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(50)))
            edtUpi?.gravity = Gravity.START
            edtUpi?.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            edtUpi?.setText(savedUpi ?: "")

            cancel.setOnClickListener {
                dialog.dismiss()
                EventBus.getDefault().post(JobApplySuccess(true))
                activity?.finish()
            }
            confirm.setOnClickListener {
                upi = edtUpi?.text?.trim()?.toString()
                val count = upi?.split("@")
                if (upi?.isEmpty() == true) {
                    Utilities.showToast(activity, "Enter your UPI id.")
                } else if ((upi?.length) ?: 0 < 3 || upi?.contains("@") == false || (count?.size
                        ?: 0) != 2
                ) {
                    edtUpi?.setError("Invalid UPI id.")
                } else {
                    dialog.dismiss()
                    isDialogForUpi = true
                    Dialogs.showAlertDialog(
                        activity,
                        "Check your UPI ID",
                        "Entered UPI ID is $upi. Do you want to save?",
                        "Yes",
                        "Edit",
                        onAlertPosBtnClick,
                        onAlertNagClick
                    )
                }
            }
            dialog.show()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtNext -> {
                getData()
                if (isUserSkills == true && isUserSkillsOpt == false) {
                    if (checkSelectedSkillCount() == false) {
                        return
                    }
                }

                if (isUserEdu == true && isUserEduOpt == false) {
                    if ((chgEdu?.checkedChipId ?: 0) <= 0) {
                        Utilities.showToast(activity, "Select your education first.")
                        return
                    }
                }

                if (isUserLang == true && isUserLangOpt == false) {
                    if ((chgLang?.checkedChipId ?: 0) <= 0) {
                        Utilities.showToast(activity, "Select your language level.")
                        return
                    }
                }


                bundle = Bundle()
//                var fragment: BaseFragment? = null
                if (mJobData != null) {
                    bundle?.putSerializable(Constants.POST_APPLY_JOB_DATA, mJobData)
                }
                bundle?.putSerializable(Constants.POST_APPLY_REQUIRED_DATA, mRequiredData)
                bundle?.putInt(Constants.POST_Assign_id, mAssignId ?: 0)

//                if (isNavToExp == true) {
//                    bundle?.putParcelableArrayList(Constants.SKILL_LIST, mSkillList)
//                    bundle?.putInt(Constants.EDU_ID, mEdu ?: 0)
//                    bundle?.putInt(Constants.LANG_ID, mLang ?: 0)
//                    bundle?.putInt(Constants.ADHAAR_STATUS, mAdhaar ?: 0)
//                    bundle?.putInt(Constants.RC_STATUS, mRC ?: 0)
//                    bundle?.putInt(Constants.DL_STATUS, mDL ?: 0)
//                    bundle?.putInt(Constants.BIKE_STATUS, mBike ?: 0)
//                    bundle?.putInt(Constants.PAN_STATUS, mPan ?: 0)
//                    bundle?.putInt(Constants.WIFI_STATUS, mWifi ?: 0)
//                    bundle?.putInt(Constants.LAPTOP_STATUS, mLAPTOP ?: 0)
//                    bundle?.putInt(Constants.SMARTPHONE_STATUS, mSMARTPHONE ?: 0)
//                    OkayGoFirebaseAnalytics.postApplicationEditExp(
//                        mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID
//                    )
//                    Constants.IS_FROM_PROFILE = false
//                    fragment = ExperienceAddFragment()
//                } else {
                val request: UpdateWorkerRequest? = UpdateWorkerRequest(
                    requestedBy = userId,
                    skills = mSkillList,
                    english_known_level = mLang,
                    qualificationId = mEdu,
                    own_aadhar_card = mAdhaar,
                    own_bike = mBike,
                    own_laptop = mLAPTOP,
                    own_smartphone = mSMARTPHONE,
                    own_pan_card = mPan,
                    own_vehicle_rc = mRC,
                    own_wifi = mWifi,
                    bike_license = mDL
                )
                viewModelExperince.saveWorkerData(workerId, request)
//                }
//                fragment?.arguments = bundle
//                fragment?.let { addFragment(it, true) }
            }

            R.id.txtChipOther -> {
                mOtherSkills?.clear()
                layoutOtherType?.visibility = View.VISIBLE
            }

            R.id.txtAddSkill -> {
                if (!Utilities.isOnline(activity)) {
                    Utilities.showToast(activity, resources.getString(R.string.no_internet))
                    return
                }
                if (edtOtherJobType?.text?.toString()?.trim()
                        ?.isEmpty() == false && mOtherSkills?.size ?: 0 < 3
                ) {
                    viewModel.addSkills(userId, edtOtherJobType?.text?.toString()?.trim())
                } else if (edtOtherJobType?.text?.toString()?.trim()?.isEmpty() == true) {
                    edtOtherJobType?.setError("Enter skill name first.")
                } else {
                    edtOtherJobType?.setError("You can add maximum 3 other skills.")
                }
            }

            R.id.txtWifiYes -> {
                setYesActive(txtWifiYes, true)
                setNoActive(txtWifiNo, false)
                mWifi = 1
            }
            R.id.txtWifiNo -> {
                setYesActive(txtWifiYes, false)
                setNoActive(txtWifiNo, true)
                mWifi = 0
            }
            R.id.txtLaptopYes -> {
                setYesActive(txtLaptopYes, true)
                setNoActive(txtLaptopNo, false)
                mLAPTOP = 1
            }
            R.id.txtLaptopNo -> {
                setYesActive(txtLaptopYes, false)
                setNoActive(txtLaptopNo, true)
                mLAPTOP = 0
            }
            R.id.txtAdhaarYes -> {
                setYesActive(txtAdhaarYes, true)
                setNoActive(txtAdhaarNo, false)
                mAdhaar = 1
            }
            R.id.txtAdhaarNo -> {
                setYesActive(txtAdhaarYes, false)
                setNoActive(txtAdhaarNo, true)
                mAdhaar = 0
            }
            R.id.txtPanYes -> {
                setYesActive(txtPanYes, true)
                setNoActive(txtPanNo, false)
                mPan = 1
            }
            R.id.txtPanNo -> {
                setYesActive(txtPanYes, false)
                setNoActive(txtPanNo, true)
                mPan = 0
            }
            R.id.txtBikeYes -> {
                setYesActive(txtBikeYes, true)
                setNoActive(txtBikeNo, false)
                mBike = 1
            }
            R.id.txtBikeNo -> {
                setYesActive(txtBikeYes, false)
                setNoActive(txtBikeNo, true)
                mBike = 0
            }

            R.id.txtLicenceYes -> {
                setYesActive(txtLicenceYes, true)
                setNoActive(txtLicenceNo, false)
                mDL = 1
            }
            R.id.txtLicenceNo -> {
                setYesActive(txtLicenceYes, false)
                setNoActive(txtLicenceNo, true)
                mDL = 0
            }
            R.id.txtSmartYes -> {
                setYesActive(txtSmartYes, true)
                setNoActive(txtSmartNo, false)
                mSMARTPHONE = 1
            }
            R.id.txtSmartNo -> {
                setYesActive(txtSmartYes, false)
                setNoActive(txtSmartNo, true)
                mSMARTPHONE = 0
            }

            R.id.txtRCYes -> {
                setYesActive(txtRCYes, true)
                setNoActive(txtRCNo, false)
                mRC = 1
            }
            R.id.txtRCNo -> {
                setYesActive(txtRCYes, false)
                setNoActive(txtRCNo, true)
                mRC = 0
            }
        }
    }
}