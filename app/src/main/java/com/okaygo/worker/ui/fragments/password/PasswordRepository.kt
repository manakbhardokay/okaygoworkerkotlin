package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.AddUserRolePhoneNumberResponse
import com.okaygo.worker.data.modal.reponse.AllIdResponse
import com.okaygo.worker.data.modal.reponse.WorkerResponse
import com.okaygo.worker.data.modal.reponse.WorkerSession
import com.okaygo.worker.data.modal.request.AddUserRoleRequest
import com.okaygo.worker.data.modal.request.CreateSessionRequest
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * @author Davinder Goel OkayGo.
 */
object PasswordRepository {
    private val mService = ApiHelper.getService()

    /**
     *add user on local server
     */
    fun addUserRolePhoneNumber(
        successHandler: (AddUserRolePhoneNumberResponse) -> Unit,
        failureHandler: (String) -> Unit,
        request: AddUserRoleRequest?
    ) {
        val params: HashMap<String, String> = HashMap()
        params.put("role_type", "4")
        params.put("country_code", "91")
        params.put("phone_number", request?.phone_number + "")
        params.put("referred_by_code", request?.referred_by_code + "")
        params.put("referral_code", "dfghj")
        mService.addUserRolePhoneNumber(params)
            .enqueue(object : Callback<AddUserRolePhoneNumberResponse> {
                override fun onResponse(
                    call: Call<AddUserRolePhoneNumberResponse>?,
                    response: Response<AddUserRolePhoneNumberResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AddUserRolePhoneNumberResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("addUserRole failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * get all id for particular user on local server
     */
    fun getAllIds(
        successHandler: (AllIdResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getAllIds(userId).enqueue(object : Callback<AllIdResponse> {
            override fun onResponse(
                call: Call<AllIdResponse>?,
                response: Response<AllIdResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<AllIdResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("addUserRole failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *get worker detail from local server
     */
    fun getWorker(
        successHandler: (WorkerResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getWorker(userId).enqueue(object : Callback<WorkerResponse> {
            override fun onResponse(
                call: Call<WorkerResponse>?,
                response: Response<WorkerResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<WorkerResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("worker failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     * create user session for particular user
     */
    fun createWorkerSession(
        successHandler: (WorkerSession) -> Unit,
        failureHandler: (String) -> Unit,
        request: CreateSessionRequest?
    ) {
        mService.createWorkerSession(
            request?.device_registration_id,
            request?.ip_address,
            request?.mac_address,
            request?.user_id,
            request?.last_seen_long,
            request?.last_seen_lat,
            request?.app_version,
            request?.android_version,
            request?.brand
        ).enqueue(object : Callback<WorkerSession> {
            override fun onResponse(
                call: Call<WorkerSession>?,
                response: Response<WorkerSession>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<WorkerSession>?, t: Throwable?) {
                t?.let {
                    Log.e("session failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}