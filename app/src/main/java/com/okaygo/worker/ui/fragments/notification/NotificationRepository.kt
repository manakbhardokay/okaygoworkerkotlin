package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.NotificationResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object NotificationRepository {
    private val mService = ApiHelper.getService()

    /**
     * get all id for particular user on local server
     */

    fun getNotifications(
        successHandler: (NotificationResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        is_alert: Int?
    ) {
        mService.getNotifications(userId, is_alert, 0, 0, 500)
            ?.enqueue(object : Callback<NotificationResponse> {
                override fun onResponse(
                    call: Call<NotificationResponse>?,
                    response: Response<NotificationResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<NotificationResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("noti failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun deleteNotification(
        successHandler: (NotificationResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        notiId: Int?
    ) {
        mService.deleteNotification(notiId, userId)
            ?.enqueue(object : Callback<NotificationResponse> {
                override fun onResponse(
                    call: Call<NotificationResponse>?,
                    response: Response<NotificationResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<NotificationResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("noti failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun getAssignedJobDetail(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        jobDetailId: Int?
    ) {
        mService.getAssignedJobDetail(userId, jobDetailId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("noti failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

}