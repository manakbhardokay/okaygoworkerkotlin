package com.okaygo.worker.ui.fragments

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.Youtube
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.fragment_vedio_player.*

class VideoPlayerFragment : BaseFragment(), View.OnClickListener,
    OnTouchListener {
    private var mUrl = ""
    private var mBundle: Bundle? = null
    private var mYoutubeVideoResponse: Youtube? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_vedio_player, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        mBundle = arguments
        if (mBundle != null) {
            mYoutubeVideoResponse = mBundle?.getParcelable(Constants.YOUTUBE_RESPONSE) as Youtube?
            if (mYoutubeVideoResponse != null) {
                mUrl = mYoutubeVideoResponse?.url ?: ""
                txtVideoTitle?.text = mYoutubeVideoResponse?.title
                txtVideoDesc?.text = mYoutubeVideoResponse?.desc
            }
            if (mUrl.isEmpty()) {
                Utilities.showToast(activity, "Video not found.")
                activity?.onBackPressed()
            } else {
                videoExtract()
            }
        }
        videoPlayer?.setOnTouchListener(this)
        imgPlayPause?.setOnClickListener(this)
        imgBack?.setOnClickListener(this)
    }

    private fun videoExtract() {
        activity?.let {
            if (mUrl.contains("://youtu.be/") || mUrl.contains("youtube.com/watch?v=")) {
                object : YouTubeExtractor(it) {
                    override protected fun onExtractionComplete(
                        ytFiles: SparseArray<YtFile>?,
                        videoMeta: VideoMeta?
                    ) {
                        if (ytFiles != null) {
                            val itag = 18
                            val downloadUrl: String = ytFiles[itag].getUrl()
                            videoPlayerHandling(downloadUrl)
                        }
                    }
                }.extract(mUrl, true, true)
            } else {
                videoPlayerHandling(mUrl)
            }
        }

    }

    /**
     * video player handling
     * play video using url and handle listeners
     */
    private fun videoPlayerHandling(videoUrl: String) {
        Log.e("mVideoUrl", videoUrl + "")
        if (videoUrl.isEmpty()) {
            videoExtract()
            return
        }
        val uri = Uri.parse(videoUrl)
        Log.e("uri", uri.toString() + "")
        videoPlayer?.setVideoURI(uri)
        videoPlayer?.start()
        progressBarVedio?.visibility = View.VISIBLE
        videoPlayer?.setOnPreparedListener { //close the progress dialog when buffering is done
            progressBarVedio?.visibility = View.GONE
        }
        videoPlayer?.setOnErrorListener { mp, what, extra ->
            videoPlayer?.stopPlayback()
            //                mActivity.onBackPressed();
            imgPlayPause?.visibility = View.VISIBLE
            progressBarVedio?.visibility = View.GONE
            true
        }
        videoPlayer?.setOnCompletionListener {
            imgPlayPause?.setImageDrawable(
                activity?.let { it1 ->
                    ContextCompat.getDrawable(
                        it1,
                        R.drawable.ic_play
                    )
                }
            )
            imgPlayPause?.visibility = View.VISIBLE
        }
    }

    override fun onPause() {
        super.onPause()
        videoPlayer?.pause()
    }

    override fun onStart() {
        super.onStart()
        videoPlayer?.start()
    }

    override fun onResume() {
        super.onResume()
        imgPlayPause?.visibility = View.GONE
        videoPlayer?.start()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgPlayPause -> {
                if (videoPlayer?.isPlaying == true) {
                    videoPlayer?.pause()
                    imgPlayPause?.setImageDrawable(
                        activity?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_play
                            )
                        }
                    )
                } else {
                    imgPlayPause?.visibility = View.GONE
                    imgPlayPause?.setImageDrawable(
                        activity?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_pause
                            )
                        }
                    )
                    videoPlayer?.start()
                }
            }
            R.id.imgBack -> activity?.onBackPressed()
        }
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (imgPlayPause.isShown == false) {
            imgPlayPause?.visibility = View.VISIBLE
            val handler = Handler()
            handler.postDelayed({
                if (videoPlayer?.isPlaying == true) {
                    imgPlayPause?.visibility = View.GONE
                }
            }, 3 * 1000.toLong())
        }
        return false
    }
}