package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.eflex.data.modal.reponse.AppUpdateResponse
import com.okaygo.worker.data.modal.direction_api_response.DirectionApiResponse
import com.okaygo.worker.data.modal.reponse.AppliedJobResponse
import com.okaygo.worker.data.modal.reponse.ODLocationResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.okaygo.worker.data.modal.reponse.WhatsAppSubscriptionResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object DashboardRepository {
    private val mService = ApiHelper.getService()
    private val mDirectionService = ApiHelper.getDirectionService()


    /**
     *
     */
    fun getWhatsAppSubscription(
        successHandler: (WhatsAppSubscriptionResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getWhatsAppSubscription(userId)
            ?.enqueue(object : Callback<WhatsAppSubscriptionResponse> {
                override fun onResponse(
                    call: Call<WhatsAppSubscriptionResponse>?,
                    response: Response<WhatsAppSubscriptionResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<WhatsAppSubscriptionResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("whatsapp  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun updateAppRequest(
        successHandler: (AppUpdateResponse) -> Unit,
        failureHandler: (String) -> Unit
    ) {
        mService.getAppUpdateFlag()
            .enqueue(object : Callback<AppUpdateResponse> {
                override fun onResponse(
                    call: Call<AppUpdateResponse>?,
                    response: Response<AppUpdateResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AppUpdateResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("user status failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun whatsAppSubscribe(
        successHandler: (WhatsAppSubscriptionResponse) -> Unit,
        failureHandler: (String) -> Unit,
        mobile: String?,
        permission: Int?,
        userId: Int?
    ) {
        mService.whatsAppSubscribe(mobile, permission, userId)
            ?.enqueue(object : Callback<WhatsAppSubscriptionResponse> {
                override fun onResponse(
                    call: Call<WhatsAppSubscriptionResponse>?,
                    response: Response<WhatsAppSubscriptionResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<WhatsAppSubscriptionResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("whatsAppSubs  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getNotificationCount(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getNotificationCount(userId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("whatsAppSubs  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getUpcomingOdJobs(
        successHandler: (AppliedJobResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getUpcommingODJobs(userId, 3, 100)
            ?.enqueue(object : Callback<AppliedJobResponse> {
                override fun onResponse(
                    call: Call<AppliedJobResponse>?,
                    response: Response<AppliedJobResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AppliedJobResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("whatsAppSubs  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun saveLocationForOD(
        successHandler: (ODLocationResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        latitude: Double?,
        longitude: Double?,
        speed: Float?,
        workerName: String?,
        time_millies: Long?,
        jobId: Int?,
        eta: String?,
        distance: String?

    ) {
        mService.saveLocationForOd(
            userId,
            latitude,
            speed,
            workerName,
            time_millies,
            longitude,
            jobId,
            distance,
            eta
        )
            ?.enqueue(object : Callback<ODLocationResponse> {
                override fun onResponse(
                    call: Call<ODLocationResponse>?,
                    response: Response<ODLocationResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<ODLocationResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("ODLocation failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getDirectionDuration(
        successHandler: (DirectionApiResponse) -> Unit,
        failureHandler: (String) -> Unit,
        origin: String?,
        dest: String?,
        key: String?
    ) {
        mDirectionService.getDirectionApi(origin, dest, key)
            ?.enqueue(object : Callback<DirectionApiResponse> {
                override fun onResponse(
                    call: Call<DirectionApiResponse>?,
                    response: Response<DirectionApiResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<DirectionApiResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("direction  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }
}