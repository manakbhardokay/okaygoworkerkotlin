package com.okaygo.worker.ui.fragments.web_view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.okaygo.worker.R
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_webview.*

class WebViewFragment : BaseFragment() {
    private var mUrl: String? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_webview, container, false)
        val bundle: Bundle? = arguments
        if (bundle?.containsKey(Constants.WEB_URL) == true) {
            mUrl = bundle?.getString(Constants.WEB_URL)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupWebViewClient()
        //        setupPdfView()

    }

    private fun setupPdfView() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(mUrl))
        activity?.startActivity(browserIntent)
    }

    private fun setupWebViewClient() {
        //        webView?.loadUrl("https://docs.google.com/viewer?embedded=true&url=$mUrl")
        webView?.loadUrl(mUrl ?: "")
        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.loadsImagesAutomatically = true
        webView?.webViewClient = MyWebViewClient()
        webView?.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY

    }

    /**
     * inner class for custom web client
     */
    inner class MyWebViewClient : WebViewClient() {

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            // if loading takes time on low end devices
            if (view.progress == 100) {
                // stop progress bar
                if (isVisible)
                    progressBarWebView?.visibility = View.GONE
            }
        }

        override fun onReceivedError(
            view: WebView?,
            errorCode: Int,
            description: String?,
            failingUrl: String?
        ) {
            super.onReceivedError(view, errorCode, description, failingUrl)
            Log.e("webview error", failingUrl.toString() + "")

        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            super.onReceivedError(view, request, error)
            Log.e("webview error", error.toString() + "")
            if (isVisible)
                progressBarWebView?.visibility = View.GONE
        }
    }
}