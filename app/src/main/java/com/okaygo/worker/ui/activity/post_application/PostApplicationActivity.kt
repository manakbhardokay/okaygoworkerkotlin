package com.okaygo.worker.ui.activity.post_application

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.JobContent
import com.okaygo.worker.data.modal.reponse.ReqiredSkillsResponse
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.ui.activity.BaseActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.complete_post_apply.CompleteInfoFragment
import com.okaygo.worker.ui.fragments.experience.ExperienceAddFragment
import com.okaygo.worker.ui.fragments.questionare.QuestionareFragment
import com.okaygo.worker.ui.fragments.show_post_application_info.ShowCompleteInfoFragment

class PostApplicationActivity : BaseActivity() {
    var mScreen: Int? = null
    var mAssignId: Int? = null
    var mJobData: JobContent? = null
    var mRequiredData: ReqiredSkillsResponse? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        mScreen = intent.getIntExtra(Constants.POST_NAV_SCREEN, 0)
        if (intent?.hasExtra(Constants.POST_APPLY_JOB_DATA) == true) {
            mJobData = intent.getSerializableExtra(Constants.POST_APPLY_JOB_DATA) as JobContent
        }
        if (intent?.hasExtra(Constants.POST_APPLY_REQUIRED_DATA) == true) {
            mRequiredData =
                intent.getSerializableExtra(Constants.POST_APPLY_REQUIRED_DATA) as ReqiredSkillsResponse
            mAssignId = intent.getIntExtra(Constants.POST_Assign_id, 0)
        }
        procced()
    }

    private fun procced() {
        val bundle: Bundle? = Bundle()
        var fragment: BaseFragment? = null
        if (mJobData != null) {
            bundle?.putSerializable(Constants.POST_APPLY_JOB_DATA, mJobData)
        }
        bundle?.putSerializable(Constants.POST_APPLY_REQUIRED_DATA, mRequiredData)
        bundle?.putInt(Constants.POST_Assign_id, mAssignId ?: 0)
        when (mScreen) {
            Constants.COMPLETE_INFO -> fragment = ShowCompleteInfoFragment()
            Constants.UNCOMPLETE_INFO -> fragment = CompleteInfoFragment()
            Constants.NO_REQUIREMENT -> fragment = QuestionareFragment()
            Constants.ADD_EXP -> fragment = ExperienceAddFragment()
        }
        fragment?.arguments = bundle
        fragment?.let { addFragment(it, true) }
    }

    override fun onBackPressed() {
        val fragment: Fragment? = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is ShowCompleteInfoFragment ||
            (mScreen == Constants.UNCOMPLETE_INFO && fragment is CompleteInfoFragment) ||
            (mScreen == Constants.NO_REQUIREMENT && fragment is QuestionareFragment)
        ) {
            finish()
        }
        super.onBackPressed()

    }
}