package com.okaygo.worker.ui.activity.login_onboarding

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.okaygo.worker.R
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.BaseActivity
import com.okaygo.worker.ui.fragments.on_boarding.PersonalDetailFragment
import com.openkey.guest.ui.fragments.verification.DashBoardModel

class OnBoardingActivity : BaseActivity() {
    private lateinit var viewModelDash: DashBoardModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activiy_on_boarding)
        viewModelDash = ViewModelProvider(this).get(DashBoardModel::class.java)
        attachObservers()
        Constants.IS_FROM_PROFILE = false
        addFragment(PersonalDetailFragment(), false)
    }

    override fun onResume() {
        super.onResume()
        viewModelDash.updateAppRequest()
    }


    /**
     * handle api reposne
     */
    private fun attachObservers() {
        viewModelDash.responseAppUpdateResponse.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    try {
                        val updatedVersion: Int? =
                            it.response?.content?.version_code
                        val pInfo =
                            packageManager.getPackageInfo(packageName, 0)
                        val currentVersion = pInfo.versionCode
                        if ((updatedVersion ?: 0) > currentVersion) {
                            Utilities.appUpdateAlert(
                                this,
                                it.response?.content?.is_force_update ?: false,
                                it.response?.content?.update_msg ?: ""
                            )
                        }
                    } catch (e: PackageManager.NameNotFoundException) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }
}