package com.okaygo.worker.ui.activity.map

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.okaygo.worker.R
import com.okaygo.worker.data.LocationEvent
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.BaseActivity
import com.openkey.guest.application.OkayGo
import kotlinx.android.synthetic.main.activity_custom_map.*
import org.greenrobot.eventbus.EventBus
import java.io.IOException
import java.util.*


class CustomMapActivity : BaseActivity(), OnMapReadyCallback,
    View.OnClickListener {
    private var mMap: GoogleMap? = null
    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKeyCustom"
    private var mCurrentLat: Double? = null
    private var mCurrentLng: Double? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    var geocoder: Geocoder? = null
    var addresses: List<Address>? = null
    private var mGoogleAddress: String? = null
    private var mCity: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_map)

        val intent = intent
        if (intent?.hasExtra(Constants.CUR_LAT) == true) {
            mCurrentLat = intent.getDoubleExtra(Constants.CUR_LAT, 0.0)
            mCurrentLng = intent.getDoubleExtra(Constants.CUR_LNG, 0.0)
        }

        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }

        mapView?.onCreate(mapViewBundle)
        mapView?.getMapAsync(this)
        mapView?.isClickable = false
        mFusedLocationClient =
            LocationServices.getFusedLocationProviderClient(OkayGo.appContext)

        geocoder = Geocoder(this, Locale.getDefault())
//        setupFusedApi()
        setListeners()
    }

    private fun setListeners() {
        imgBack?.setOnClickListener(this)
        imgForword?.setOnClickListener(this)
        imgCurrentLoc?.setOnClickListener(this)
    }

    private fun setupFusedApi() {
        if (locationPermission()) {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener {
                mCurrentLat = it?.latitude
                mCurrentLng = it?.longitude
                val currentLatLng = LatLng(mCurrentLat ?: 0.0, mCurrentLng ?: 0.0)
                mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                txtAddress?.setText(getAddressFromLatLng())
            }
        }
    }

    fun locationPermission(): Boolean {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ),
                    5011
                )
                return false
            }
            return true
        }
        return true

    }

    private val onAlertAllowBtnClick: () -> Unit = {

        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            5011
        )
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            ActivityCompat.requestPermissions(
//                this, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
//                5011
//            )
//
//        }
    }

    private fun getAddressFromLatLng(): String? {
        try {
            addresses =
                geocoder?.getFromLocation(
                    mCurrentLat ?: 0.0,
                    mCurrentLng ?: 0.0,
                    1
                )
            if (addresses?.isEmpty() == false) {
                mGoogleAddress = addresses?.get(0)?.getAddressLine(0)
                mCity = addresses?.get(0)?.subAdminArea ?: addresses?.get(0)?.adminArea
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return mGoogleAddress ?: ""
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
        mMap?.uiSettings?.isMapToolbarEnabled = false
        mMap?.setMinZoomPreference(12f)
        val currentLatLng = LatLng(mCurrentLat ?: 0.0, mCurrentLng ?: 0.0)
//        mMap?.addMarker(
//            MarkerOptions().position(currentLatLng)
//                .title("You") // below line is use to add custom marker on our map.
//                .icon(
//                    activity?.let {
//                        Utilities.bitmapFromVector(it, R.drawable.ic_map_current)
//                    }
//                ))
        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
        getAddressFromLatLng()
        mMap?.setOnMarkerClickListener { marker ->
            Log.e("Click", "OnMap")
            if (marker.isInfoWindowShown) {
                marker.hideInfoWindow()
            } else {
                marker.showInfoWindow()
            }
            true
        }

        mMap?.setOnCameraIdleListener(object : GoogleMap.OnCameraIdleListener {
            override fun onCameraIdle() {
                val centerPoint = mMap?.cameraPosition?.target
                Log.e("Cneter", "Lat::${centerPoint?.latitude}, Lng::${centerPoint?.longitude}")
                mCurrentLat = centerPoint?.latitude
                mCurrentLng = centerPoint?.longitude
                txtAddress?.setText(getAddressFromLatLng())
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY)
        if (mapViewBundle == null) {
            mapViewBundle = Bundle()
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle)
        }
        mapView?.onSaveInstanceState(mapViewBundle)
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onPause() {
        mapView?.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mapView?.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgCurrentLoc -> {
                setupFusedApi()
            }
            R.id.imgBack -> {
                onBackPressed()
            }
            R.id.imgForword -> {
                if (Utilities.isOnline(this)) {
                    val event = LocationEvent(mCurrentLat, mCurrentLng, mGoogleAddress, mCity)
                    EventBus.getDefault()?.post(event)
                    onBackPressed()
                } else {
                    Utilities.showToast(
                        this,
                        resources?.getString(R.string.no_internet)
                            ?: "Check your internet."
                    )
                }
            }
        }
    }
}