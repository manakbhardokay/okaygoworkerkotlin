package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.InAppResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class SplashModel(app: Application) : MyViewModel(app) {
    var response = MutableLiveData<InAppResponse>()

    /**
     * get in app data from server for particular user
     */
    fun getInAppData(userId: Int?) {
        isLoading.value = false
        SplashRepository.getInAppData({
            isLoading.value = false
            response.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }
}