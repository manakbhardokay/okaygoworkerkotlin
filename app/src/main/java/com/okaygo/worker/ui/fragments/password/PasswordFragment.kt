package com.okaygo.worker.ui.fragments.password

import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.format.Formatter
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.services.cognitoidentityprovider.model.UserNotConfirmedException
import com.freshchat.consumer.sdk.Freshchat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.firebase.messaging.FirebaseMessaging
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.data.modal.request.AddUserRoleRequest
import com.okaygo.worker.data.modal.request.CreateSessionRequest
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.okaygo.worker.ui.activity.login_onboarding.OnBoardingActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.forgot_pass.ForgotPassword
import com.okaygo.worker.ui.fragments.login.model.AuthDetails
import com.okaygo.worker.ui.fragments.login.model.AuthenticationSuccess
import com.okaygo.worker.ui.fragments.otp.OtpFragment
import com.openkey.guest.application.OkayGo.Companion.appContext
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.PasswordModel
import kotlinx.android.synthetic.main.fragment_password.*

class PasswordFragment : BaseFragment(), View.OnClickListener {
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    private var mAlertDialog: AlertDialog? = null
    private var usernameInput: String? = null
    private var exist = false
    private var current_latitude: Double? = 0.0
    private var current_longitude: Double? = 0.0
    private var mOnboardingStatus: Int? = 0

    private var isPasswordVisible = false

    private lateinit var viewModel: PasswordModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PasswordModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(appContext)
        val bundle: Bundle? = arguments
        exist = bundle?.getBoolean(Constants.EXIST, true) ?: false
        usernameInput = bundle?.getString(Constants.USERNAME)
        setListeners()
    }


    private fun setListeners() {
        txtForgotPassword?.setOnClickListener(this)
        imgNext?.setOnClickListener(this)
        imgEye?.setOnClickListener(this)

//        if (Utilities.locationPermisiion(activity)) {
//            mFusedLocationClient?.lastLocation?.addOnSuccessListener {
//                current_latitude = it?.latitude
//                current_longitude = it?.longitude
//                Log.e("Location", current_latitude?.toString() + " \n " + current_longitude)
//            }
//        }
    }

    /**
     * auth success for cognito
     */
    private val onAuthSuccess: (AuthenticationSuccess?) -> Unit = { it ->
        Log.e("TAG", " -- Auth Success")
        AuthHelper.setCurrSession(it?.cognitoUserSession)
        AuthHelper.newDevice(it?.device)
        try {
            Preferences.prefs?.saveValue(
                Constants.ACCESS_TOKEN,
                AuthHelper.getCurrSession().accessToken.jwtToken
            )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        val request = AddUserRoleRequest(phone_number = usernameInput, referred_by_code = "");
        viewModel.addUserRolePhoneNumber(request, true)
    }

    private val onAuthDetails: (AuthDetails?) -> Unit = { it ->
        getUserAuthentication(it?.authenticationContinuation, it?.username)
    }

    private val onAuthFailure: (Exception?) -> Unit = { it ->
        Utilities.hideLoader()
        if (it is UserNotConfirmedException) {
            Toast.makeText(activity, "User Not Confirmed.", Toast.LENGTH_SHORT).show()
            val bundle: Bundle? = Bundle()
            val fragment: BaseFragment? = OtpFragment()
            bundle?.putString("name", usernameInput)
            bundle?.putBoolean("fault", false)
            fragment?.arguments = bundle
            fragment?.let { attachFragment(it, true) }
        } else {
            showDialogMessage("Sign In Failed", "Incorrect Password")
        }
    }

    private fun showDialogMessage(title: String, body: String) {
        val builder = activity?.let { AlertDialog.Builder(it) }
        builder?.setTitle(title)?.setMessage(body)?.setNeutralButton("OK") { dialog, which ->
            try {
                mAlertDialog?.dismiss()
            } catch (e: java.lang.Exception) {
            }
        }
        mAlertDialog = builder?.create()
        mAlertDialog?.show()
    }

    /**
     * get user auth session
     */
    private fun getUserAuthentication(
        continuation: AuthenticationContinuation?,
        username: String?
    ) {
        val authenticationDetails: AuthenticationDetails
        authenticationDetails =
            AuthenticationDetails(usernameInput, edtPassword?.getText().toString(), null)
        continuation?.setAuthenticationDetails(authenticationDetails)
        continuation?.continueTask()
    }

    private fun exit(uname: String?) {
        exit(uname, null)
    }

    private fun exit(uname: String?, password: String?) {
        var uname: String? = uname
        var password = password
        Utilities.hideLoader()
//        val intent = Intent(activity, WorkerOnBoardingActivity::class.java)
//        if (uname == null) {
//            uname = ""
//        }
//        if (password == null) {
//            password = ""
//        }
//        intent.putExtra("name", uname)
//        intent.putExtra("password", password)
//        startActivity(intent)
//        activity?.finish()
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.response.observe(this, Observer {
            it?.let {
                Log.e("pass", " add user role response" + it?.toString())

                if (it.code == Constants.SUCCESS) {
//                    Preferences.prefs?.saveValue(Constants.ID, it.response?.userId)
//                    viewModel.getAllIds(it.response?.userId)
                    if (it.response?.roleType == 3) {

                        val cu = AuthHelper.getPool().getUser(usernameInput)
                        cu.signOut()
                        AuthHelper.setUser(null)
                        Preferences.prefs?.saveValue(Constants.OG_SESSION_ID, null);
                        Preferences.prefs?.saveValue(Constants.USER_ID, null);
                        Preferences.prefs?.saveValue(Constants.ID, 0);
                        Preferences.prefs?.saveValue(Constants.IS_USER_LOGGED_IN, false);
                        Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, false);

                        AuthHelper.clearCurrUserAttributes()
                        Freshchat.resetUser(appContext)
                        val alert = activity?.let { it1 -> AlertDialog.Builder(it1) }
                        alert?.setMessage("This number is already registered as employer.")
                        alert?.setCancelable(false)
                        alert?.setPositiveButton(
                            "OK"
                        ) { dialog, id ->
                            Toast.makeText(
                                activity,
                                "This number is already registered as a employer.",
                                Toast.LENGTH_LONG
                            ).show()
                            Utilities.hideLoader()
                            activity?.onBackPressed()
                        }
                        val alert11 = alert?.create()
                        alert11?.show()
                    } else {
                        Preferences.prefs?.saveValue(
                            Constants.userName,
                            it?.response?.firstName + " " + it?.response?.lastName
                        )
                        Log.e("pass", " add user role response not null" + it.response?.toString())
                        Preferences.prefs?.saveValue(Constants.ID, it.response?.userId)
                        viewModel.getWorker(it.response?.userId, false)
                    }
                }
            }
        })

        viewModel.responseCreateSession.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Preferences.prefs?.saveValue(Constants.OG_SESSION_ID, it.response?.sessionId)
                    Preferences.prefs?.saveValue(Constants.IS_LOGGED_IN, true)
                    if (mOnboardingStatus ?: 0 == 0) {
//                        val userActivity = Intent(activity, SummaryActivity::class.java)
                        Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, false)
                        val userActivity = Intent(activity, OnBoardingActivity::class.java)
                        startActivity(userActivity)
                        activity?.finish()
                    } else {
                        Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, true)
                        Preferences.prefs?.saveValue(Constants.USER_ID, usernameInput);
                        val userActivity = Intent(activity, DashBoardActivity::class.java)
                        startActivity(userActivity)
                        activity?.finish()
                    }
                }
            }
        })

        viewModel.responseWorker.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {
                    mOnboardingStatus = it.response.content.get(0).on_board ?: 0
                    Preferences.prefs?.saveValue(
                        Constants.EMPLOYER_ID,
                        it.response.content.get(0).workerId
                    )

                    try {
                        val manager = activity?.getApplicationContext()
                            ?.getSystemService(Context.WIFI_SERVICE) as WifiManager
                        val info = manager.connectionInfo
                        val mac_address = info.macAddress
                        val ip_address = Formatter.formatIpAddress(info.ipAddress)
                        val appVersion =
                            Utilities.appVersion(activity!!)?.replace("(", "")?.replace(")", "")

                        FirebaseMessaging.getInstance().token.addOnSuccessListener { instanceIdResult ->
                            val request = CreateSessionRequest(
                                instanceIdResult,
                                ip_address,
                                mac_address,
                                it.response.content.get(0).userId,
                                current_longitude,
                                current_latitude,
                                appVersion,
                                Constants.osVersion,
                                Constants.brand.replace(" ", "")
                            )
                            viewModel.createWorkerSession(request, false)
                        }
                    } catch (ex: java.lang.Exception) {
                    }
                }
            }
        })

        viewModel.responseAllIds.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    mOnboardingStatus = it.response?.on_board ?: 0
                    if (it.response?.role_type == 3) {
                        val cu = AuthHelper.getPool().getUser(usernameInput)
                        cu.signOut()
                        AuthHelper.setUser(null)
                        Preferences.prefs?.saveValue(Constants.OG_SESSION_ID, null);
                        Preferences.prefs?.saveValue(Constants.USER_ID, null);
                        Preferences.prefs?.saveValue(Constants.ID, 0);
                        Preferences.prefs?.saveValue(Constants.IS_USER_LOGGED_IN, false);
                        Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, false);

                        AuthHelper.clearCurrUserAttributes()
                        Freshchat.resetUser(appContext)
                        val alert = activity?.let { it1 -> AlertDialog.Builder(it1) }
                        alert?.setMessage("This number is already registered as employer.")
                        alert?.setCancelable(false)
                        alert?.setPositiveButton(
                            "OK"
                        ) { dialog, id ->
                            Toast.makeText(
                                activity,
                                "This number is already registered as a employer.",
                                Toast.LENGTH_LONG
                            ).show()
                            Utilities.hideLoader()
                            activity?.onBackPressed()
                        }
                        val alert11 = alert?.create()
                        alert11?.show()
                    } else {
                        viewModel?.getWorker(it.response?.user_id, false)
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtForgotPassword -> {
                val bundle: Bundle? = Bundle()
                val fragment: BaseFragment? = ForgotPassword()
                bundle?.putString(Constants.USERNAME_FORGOT, usernameInput)
                fragment?.arguments = bundle
                fragment?.let { attachFragment(it, true) }
            }
            R.id.imgNext -> {
                if (edtPassword?.getText().toString().length >= 6 && edtPassword?.getText()
                        .toString().length <= 12
                ) {
                    Utilities.hideKeyboard(activity)
                    OkayGoFirebaseAnalytics.login_password()
                    signInUser()
                } else {
                    Toast.makeText(
                        activity,
                        "Password should be between 6 to 12 character",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            R.id.imgEye -> {
                if (isPasswordVisible) {
                    isPasswordVisible = false
                    imgEye?.setImageResource(R.drawable.ic_eye_cl)
                    edtPassword?.setTransformationMethod(PasswordTransformationMethod.getInstance())
                } else {
                    isPasswordVisible = true
                    imgEye?.setImageResource(R.drawable.ic_eye_op)
                    edtPassword?.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
                }
            }
        }
    }

    /**
     * sign in user on cognito
     */
    private fun signInUser() {
        Utilities.showLoader(activity)
        usernameInput?.let {
            AuthHelper.setUser(usernameInput)

            if (AuthHelper.getPool() != null) {
                AuthHelper.getPool().getUser(usernameInput).getSessionInBackground(
                    Utilities.getAuthHandler(
                        onAuthSuccess,
                        onAuthDetails,
                        onAuthFailure
                    )
                )
            }
        }
    }
}