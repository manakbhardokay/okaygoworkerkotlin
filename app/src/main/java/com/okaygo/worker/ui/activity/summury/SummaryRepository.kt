package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.OnBoardingStatusResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object SummaryRepository {
    private val mService = ApiHelper.getService()

    /**
     * check onBoarding is complete or not
     */
    fun getOnBoardingStatus(
        successHandler: (OnBoardingStatusResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getOnBoardingProcess(userId).enqueue(object : Callback<OnBoardingStatusResponse> {
            override fun onResponse(
                call: Call<OnBoardingStatusResponse>?,
                response: Response<OnBoardingStatusResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<OnBoardingStatusResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("onBoarding  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}