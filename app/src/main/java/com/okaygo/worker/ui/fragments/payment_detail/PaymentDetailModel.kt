package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.UpiRespnse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class PaymentDetailModel(app: Application) : MyViewModel(app) {
    var responseSaveUPI = MutableLiveData<UpiRespnse>()
    var responseGetUPI = MutableLiveData<UpiRespnse>()

    /**
     *get upi from server
     */
    fun getUPI(workerId: Int?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        PaymentDetailRepository.getUPI({
            isLoading.value = false
            responseGetUPI.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId)
    }

    /**
     *get referral child data fro server
     */
    fun updateUPI(workerId: Int?, upi: String?) {
        isLoading.value = true
        PaymentDetailRepository.addUpi({
            isLoading.value = false
            responseSaveUPI.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId, upi)
    }
}