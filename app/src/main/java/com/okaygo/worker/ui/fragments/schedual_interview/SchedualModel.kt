package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.*
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class SchedualModel(app: Application) : MyViewModel(app) {
    var responseTravalMode = MutableLiveData<SuccessResponse>()
    var responseSaveTravalMode = MutableLiveData<SuccessResponse>()
    var responseCheckInData = MutableLiveData<ChekinResonse>()
    var responsesetCheckInData = MutableLiveData<CheckOutResponse>()
    var responsesVerifyOtp = MutableLiveData<SuccessResponse>()
    var responseCheckOut = MutableLiveData<CheckOutResponse>()
    var responseSaveReview = MutableLiveData<SaveReviewResponse>()
    var responseGetEmployerReviewOpt = MutableLiveData<EmployerReviewResponse>()
    var responseSaveEmployerReviewOpt = MutableLiveData<EmployerPointsResponse>()
    var apiOtpError = MutableLiveData<String>()

    /**
     * get in app data from server for particular user
     */
    fun getTravalMode(assignId: Int?) {
        isLoading.value = true
        SchedualRepository.getTravalMode({
            isLoading.value = false
            responseTravalMode.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, assignId)
    }

    fun setTravalMode(
        assignId: Int?,
        userId: Int?,
        workerId: Int?,
        travalMode: Int?
    ) {
        isLoading.value = true
        SchedualRepository.setTravalMode({
            isLoading.value = false
            responseSaveTravalMode.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, assignId, userId, workerId, travalMode)
    }

    fun getCheckInData(
        assignId: Int?,
        userId: Int?
    ) {
        isLoading.value = true
        SchedualRepository.getCheckInData({
            isLoading.value = false
            responseCheckInData.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, assignId, userId)
    }

    fun setCheckInData(
        check_in_id: Int?,
        updated_by: Int?,
        otp: String?
    ) {
        isLoading.value = true
        SchedualRepository.setCheckInData({
            isLoading.value = false
            responsesetCheckInData.value = it
        }, {
            isLoading.value = false
            apiOtpError.value = it
        }, check_in_id, updated_by, otp)
    }

    fun jobCheckOut(
        check_in_id: Int?,
        updated_by: Int?
    ) {
        isLoading.value = true
        SchedualRepository.jobCheckOut({
            isLoading.value = false
            responseCheckOut.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, check_in_id, updated_by)
    }

    fun verifyInterviewOtp(
        interview_id: Int?,
        requestedBy: Int?,
        otp: String?
    ) {
        isLoading.value = true
        SchedualRepository.verifyInterviewOtp({
            isLoading.value = false
            responsesVerifyOtp.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, interview_id, requestedBy, otp)
    }

    fun saveReview(
        employer_id: Int?,
        jobId: Int?,
        ratings: Int?,
        requested_by: Int?,
        review: String?
    ) {
        isLoading.value = true
        SchedualRepository.saveReview({
            isLoading.value = false
            responseSaveReview.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, employer_id, jobId, ratings, requested_by, review)
    }

    fun getEmployerReviewOpt(
        type_key: String?
    ) {
        isLoading.value = true
        SchedualRepository.getEmployerReviewOpt({
            isLoading.value = false
            responseGetEmployerReviewOpt.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, type_key)
    }

    fun saveReviewPt(
        point_type: String?,
        status: String?,
        review_id: String?
    ) {
        isLoading.value = true
        SchedualRepository.saveEmployerReviewPt({
            isLoading.value = false
            responseSaveEmployerReviewOpt.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, point_type, status, review_id)
    }
}