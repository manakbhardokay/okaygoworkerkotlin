package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.DueTransectionResponse
import com.okaygo.worker.data.modal.reponse.PaymentTransectionResponse
import com.okaygo.worker.data.modal.reponse.ReferralPaymentResponse
import com.okaygo.worker.data.modal.reponse.StatsResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class PaymentModel(app: Application) : MyViewModel(app) {
    var responseTransection = MutableLiveData<PaymentTransectionResponse>()
    var responseTransectionPT_FT = MutableLiveData<PaymentTransectionResponse>()
    var responseTransectionReferral = MutableLiveData<ReferralPaymentResponse>()
    var responseDueTransection = MutableLiveData<DueTransectionResponse>()
    var responseTransectionStats = MutableLiveData<StatsResponse>()

    /**
     *
     */
    fun getPaymentTransection(userId: Int?, paymentStatus: Int? = null) {
        isLoading.value = true
        PaymentRepository.getPaymentTransection({
//            isLoading.value = false
            responseTransection.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, paymentStatus)
    }

    /**
     *
     */
    fun getPaymentTransectionPT_FT(userId: Int?) {
//        isLoading.value = true
        PaymentRepository.getPaymentTransectionPT_FT({
            isLoading.value = false
            responseTransectionPT_FT.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     *
     */
    fun getReferralTransection(userId: Int?, paymentStatus: Int? = null) {
        isLoading.value = true
        PaymentRepository.getReferralTransection({
            isLoading.value = false
            responseTransectionReferral.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, paymentStatus)
    }

    /**
     *
     */
    fun getTransectionStats(userId: Int?) {
        isLoading.value = false
        PaymentRepository.getTransectionStats({
            isLoading.value = false
            responseTransectionStats.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     *
     */
    fun getDueTransection(userId: Int?) {
        isLoading.value = false
        PaymentRepository.getDueTransection({
            isLoading.value = false
            responseDueTransection.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }
}