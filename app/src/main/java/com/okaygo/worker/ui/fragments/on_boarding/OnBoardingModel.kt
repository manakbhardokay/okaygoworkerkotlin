package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.api.response.OtherSkills
import com.okaygo.worker.data.api.response.UserSkillsResponse
import com.okaygo.worker.data.modal.reponse.*
import com.okaygo.worker.data.modal.request.AddSkillsRequest
import com.okaygo.worker.data.modal.request.AvailbilityRequest
import com.okaygo.worker.data.modal.request.UserDetailRequest
import com.okaygo.worker.ui.fragments.on_boarding.OnBoardingRepository
import com.openkey.guest.data.MyViewModel
import java.io.File

/**
 * @author Davinder Goel OkayGo.
 */
class OnBoardingModel(app: Application) : MyViewModel(app) {
    var responsePersonalDetail = MutableLiveData<PersonalDetailResponse>()
    var responseSaveEdu = MutableLiveData<SaveEducationResponse>()
    var responseEducation = MutableLiveData<QualificationResponse>()
    var responseIntrestedJob = MutableLiveData<JobCategoryResponse>()
    var responseGetExp = MutableLiveData<ExperienceResponse>()
    var responseDeleteExp = MutableLiveData<SuccessResponse>()
    var responseGetDocLink = MutableLiveData<SuccessResponse>()
    var responseUploadCV = MutableLiveData<SuccessResponse>()
    var responseSaveCat = MutableLiveData<SuccessResponse>()
    var responseNoExp = MutableLiveData<SuccessResponse>()
    var responseUpdateSalry = MutableLiveData<SuccessResponse>()
    var responseSelectedJobCat = MutableLiveData<SelectedJobCatResponse>()
    var responseUserSkills = MutableLiveData<UserSkillsResponse>()
    var responseSaveSkills = MutableLiveData<AllSkillResponse>()
    var responseAllSkills = MutableLiveData<AllSkillResponse>()
    var responseAddSkills = MutableLiveData<OtherSkills>()
    var responseSaveAvailability = MutableLiveData<AvailabilityResponse>()
    var responseGetAvailability = MutableLiveData<GetAvailabilityResponse>()
    var responseGlobalAvailability = MutableLiveData<SuperAvailailityResponse>()
    var responseDeleteAvailability = MutableLiveData<SuccessResponse>()
    var responseUpdateAvailability = MutableLiveData<GetAvailabilityResponse>()

    /**
     * save user detail on local server
     */
    fun saveUserDetail(req: UserDetailRequest?) {
        isLoading.value = true
        OnBoardingRepository.savePersonalDetail({
            isLoading.value = false
            responsePersonalDetail.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, req)
    }

    /**
     * save education detail on local server
     */
    fun saveEducation(req: SaveEducationRequest?) {
        isLoading.value = true
        OnBoardingRepository.saveEducationLang({
            isLoading.value = false
            responseSaveEdu.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, req)
    }

    /**
     * get all available edu detail from local server
     */
    fun getEducation(isLoader: Boolean? = true) {
        isLoading.value = isLoader
        OnBoardingRepository.getEducation({
            isLoading.value = false
            responseEducation.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        })
    }

    /**
     * get all interested job cat from local server
     */
    fun fetchInterestedJobTypes() {
        isLoading.value = true
        OnBoardingRepository.fetchInterestedJobTypes({
            isLoading.value = false
            responseIntrestedJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        })
    }

    /**
     * get all added experience for particular user from local server
     */
    fun getWorkerExp(workerId: Int?, isLoadder: Boolean? = true) {
        isLoading.value = isLoadder
        OnBoardingRepository.getWorkerExp({
            if (isLoadder == true) {
                isLoading.value = false
            }
            responseGetExp.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId)
    }

    /**
     * delete any exp from local server
     */
    fun deleteExp(expId: Int?) {
        isLoading.value = true
        OnBoardingRepository.deleteExp({
            isLoading.value = false
            responseDeleteExp.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, expId)
    }

    /**
     * upload file on local server and get db path
     */
    fun getDocLink(userId: Int?, file: File? = null, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        OnBoardingRepository.getDocLink({
            if (isLoader == true) {
                isLoading.value = false
            }
            responseGetDocLink.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, file, userId)
    }

    /**
     * uplaod file path on local server
     */
    fun uplaodCV(url: String?, workerId: Int?, fileName: String?) {
//        isLoading.value = true
        OnBoardingRepository.uplaodCV({
            isLoading.value = false
            responseUploadCV.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, url, workerId, fileName)
    }

    /**
     * save selected job cat on local server
     */
    fun saveInterestedCat(workerId: Int?, cats: String?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        OnBoardingRepository.saveInterestedCat({
            if (isLoader == true) {
                isLoading.value = false
            }
            responseSaveCat.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId, cats)
    }

    /**
     * set 1 if their is no exp on local server
     */
    fun setNoExp(workerId: Int?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        OnBoardingRepository.setNoExp({
            isLoading.value = false
            responseNoExp.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId)
    }

    /**
     * update about last salary on local server
     */
    fun updateLastSalary(salary: Int?, workerId: Int?) {
        isLoading.value = true
        OnBoardingRepository.updateLastSalary({
            isLoading.value = false
            responseUpdateSalry.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, salary, workerId)
    }

    fun getSelectedJobCat(userId: Int?, isForSkils: Boolean? = false) {
        isLoading.value = true
        OnBoardingRepository.getSelectedJobCat({
            if (isForSkils == false) {
                isLoading.value = false
            }
            responseSelectedJobCat.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    fun getUserSkills(userId: Int?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        OnBoardingRepository.getUserSkills({
            if (isLoader == true) {
                isLoading.value = false
            }
            responseUserSkills.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    fun getAllSkills(jobCatId: String?, jobTypeId: String?, isLoadder: Boolean? = true) {
        isLoading.value = isLoadder
        OnBoardingRepository.getAllSkills({
            isLoading.value = false
            responseAllSkills.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, jobCatId, jobTypeId)
    }

    fun saveSkills(userId: Int?, request: AddSkillsRequest?) {
        isLoading.value = true
        OnBoardingRepository.saveSkills({
            isLoading.value = false
            responseSaveSkills.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, request)
    }

    fun addSkills(userId: Int?, skillName: String?) {
        isLoading.value = true
        OnBoardingRepository.addSkills({
            isLoading.value = false
            responseAddSkills.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, skillName)
    }

    fun saveAvailability(request: AvailbilityRequest?) {
        isLoading.value = true
        OnBoardingRepository.saveAvailability({
            isLoading.value = false
            responseSaveAvailability.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, request)
    }

    fun getAvailability(workerId: Int?, rows: Int?) {
        isLoading.value = true
        OnBoardingRepository.getAvailability({
            isLoading.value = false
            responseGetAvailability.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId, rows)
    }

    fun saveGlobalAvailability(workerId: Int?, gb: Int?, userId: Int?) {
        isLoading.value = true
        OnBoardingRepository.saveGlobalAvailability({
            isLoading.value = false
            responseGlobalAvailability.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, gb, workerId)
    }

    fun updateAvailabilityStatus(availabilityId: Int?, userId: Int?, activeStatus: Int?) {
        isLoading.value = true
        OnBoardingRepository.updateAvailabilityStatus({
            isLoading.value = false
            responseUpdateAvailability.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, availabilityId, userId, activeStatus)
    }

    fun deleteAvailability(availabilityId: Int?) {
        isLoading.value = true
        OnBoardingRepository.deleteAvailability({
            isLoading.value = false
            responseDeleteAvailability.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, availabilityId)
    }
}