package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.CityStateResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object SearchRepository {
    private val mService = ApiHelper.getService()

    /**
     * fetch list of cities from local server accroding to text
     */
    fun getCityWithText(
        successHandler: (CityStateResponse) -> Unit,
        failureHandler: (String) -> Unit,
        text: String?
    ) {
        mService.searchCity(text)?.enqueue(object : Callback<CityStateResponse> {
            override fun onResponse(
                call: Call<CityStateResponse>?,
                response: Response<CityStateResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<CityStateResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("search city  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}