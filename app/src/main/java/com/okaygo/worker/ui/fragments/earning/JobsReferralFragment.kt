package com.okaygo.worker.ui.fragments.earning

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.okaygo.worker.R
import com.okaygo.worker.adapters.ReferralPaymentAdapter
import com.okaygo.worker.data.modal.reponse.ReferralPayment
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.ui.fragments.verification.PaymentModel
import kotlinx.android.synthetic.main.fragment_earning_tab.*

class JobsReferralFragment(val paymentStatus: Int? = null) : BaseFragment() {
    private lateinit var viewModel: PaymentModel
    private var mPaymentList: ArrayList<ReferralPayment>? = null
    private var mAdapter: ReferralPaymentAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PaymentModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_earning_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
    }

    private fun setAdapter() {
        if (mPaymentList == null) {
            mPaymentList = ArrayList()
        }
        val linearLayoutManager = LinearLayoutManager(activity)
        mPaymentList?.let {
            mAdapter = ReferralPaymentAdapter(context, it)
            recylerTabs?.layoutManager = linearLayoutManager
            recylerTabs?.adapter = mAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        if (Utilities.isOnline(activity)) {
            viewModel.getReferralTransection(userId, paymentStatus)
        } else {
            imgNoData?.visibility = View.VISIBLE
            txtMsg?.visibility = View.VISIBLE
        }
    }


    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseTransectionReferral.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {
                    recylerTabs?.visibility = View.VISIBLE
                    imgNoData?.visibility = View.GONE
                    txtMsg?.visibility = View.GONE
                    if (mPaymentList == null) {
                        mPaymentList = ArrayList()
                    } else {
                        mPaymentList?.clear()
                    }
                    if (paymentStatus == null) {
                        it.response.content.let {
                            mPaymentList?.addAll(it)
                        }
                    } else {
                        for (i in 0 until (it.response.content.size ?: 0)) {
                            it.response.content?.let {
                                if ((it.get(i).isWorkerPaid) ?: 0 == paymentStatus) {
                                    mPaymentList?.add(it.get(i))
                                }
                            }
                        }
                    }
//                    it.response.content.let { it1 -> mPaymentList?.addAll(it1) }
                    mAdapter?.notifyDataSetChanged()
                } else {
                    recylerTabs?.visibility = View.GONE
                    imgNoData?.visibility = View.VISIBLE
                    txtMsg?.visibility = View.VISIBLE
                    txtMsg?.text = "No data found"
                    imgNoData?.background = activity?.let { it1 ->
                        ContextCompat.getDrawable(
                            it1,
                            R.drawable.ic_no_records
                        )
                    }
                }
            }
        })


        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}