package com.okaygo.worker.ui.fragments.rating_review

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.RatingReviewResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class RatingModel(app: Application) : MyViewModel(app) {
    var responseRatingReview = MutableLiveData<RatingReviewResponse>()

    /**
     *get rating and review data from server
     */
    fun getRatingReviews(workerId: Int?) {
        isLoading.value = true
        RatingReviewRepository.getRatingReviews({
            isLoading.value = false
            responseRatingReview.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, workerId)
    }

}