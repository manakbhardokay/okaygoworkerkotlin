package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.QuestionareResponse
import com.okaygo.worker.data.modal.reponse.SaveQuestionareResponse
import com.okaygo.worker.data.modal.request.QuestionareRequestItem
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object QuestionareRepository {
    private val mService = ApiHelper.getService()


    /**
     *get refer jobs data from server
     */
    fun getJobQuestions(
        successHandler: (QuestionareResponse) -> Unit,
        failureHandler: (String) -> Unit,
        jobId: Int?
    ) {
        mService.getJobQuestion(jobId)?.enqueue(object : Callback<QuestionareResponse> {
            override fun onResponse(
                call: Call<QuestionareResponse>?,
                response: Response<QuestionareResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<QuestionareResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("referral failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *get refer jobs data from server
     */
    fun saveJobQuestions(
        successHandler: (SaveQuestionareResponse) -> Unit,
        failureHandler: (String) -> Unit,
        jobId: Int?,
        JobDetailId: Int?,
        userId: Int?,
        request: ArrayList<QuestionareRequestItem?>
    ) {
        mService.saveJobQuestion(jobId, JobDetailId, userId, request)
            .enqueue(object : Callback<SaveQuestionareResponse> {
                override fun onResponse(
                    call: Call<SaveQuestionareResponse>?,
                    response: Response<SaveQuestionareResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SaveQuestionareResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("referral failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }
}