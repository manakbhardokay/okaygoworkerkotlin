package com.okaygo.worker.ui.fragments.schedual_interview

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.blankj.utilcode.util.ScreenUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.AppliedJob
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.SchedualModel
import kotlinx.android.synthetic.main.fragment_schedual_interview.*

class SchedualInterviewFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: SchedualModel
    private lateinit var viewModelJobList: JobListModel
    var bottomSheetDialog: BottomSheetDialog? = null
    private var mAppiledJobData: AppliedJob? = null
    private var mBundle: Bundle? = null
    var Current_travel_mode = 0
    var checkInId = 0
    var mode_selected = false
    var comment_group: ChipGroup? = null

    var rating_question: TextView? = null
    var rating = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SchedualModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_schedual_interview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBundle = arguments
        mAppiledJobData = mBundle?.getParcelable(Constants.NAVIGATION_DATA)

        if (mAppiledJobData?.workType == "On Demand") {
            txtJobTime?.setVisibility(View.GONE)
            txtJobDate?.setVisibility(View.GONE)
//            viewModel.getTravalMode(mAppiledJobData?.assignId)
            viewModel.getCheckInData(mAppiledJobData?.assignId, userId)
            txtJobType?.setText("On Demand")
            txtBrandName?.setText(mAppiledJobData?.brandName ?: mAppiledJobData?.company)
            txtJobRole?.setText(mAppiledJobData?.jobType)
            txtJobType?.setText(mAppiledJobData?.workType)
//            txtJobPay?.setText(
//                "₹ " + Math.round(
//                    mAppiledJobData?.amount?.toFloat() ?: 0f
//                ) + "/" + (mAppiledJobData?.amountPer ?: "Daily")
//            )
            txtJobPay?.text = "₹70/per Hour"
            txtDate?.setText(
                Utilities.getFormatedDate(mAppiledJobData?.startDate)
                    .toString() + " " + Utilities.convertTimeInAmPm(mAppiledJobData?.loginTime) + " - " + Utilities.convertTimeInAmPm(
                    mAppiledJobData?.logoutTime
                )
            )
            txtJobAddress?.setText(mAppiledJobData?.address)

        }
        setListeners()
    }

    private fun setListeners() {
        chgTravelOpt?.setOnCheckedChangeListener(ChipGroup.OnCheckedChangeListener { chipGroup, i ->
            if (i != -1) {
                var M = ""
                val chip: Chip? = chipGroup?.findViewById<Chip>(chipGroup.checkedChipId)
                mode_selected = true
                if (chip?.text.toString() == "No Bike") {
                    M = "Bus"
                    Current_travel_mode = 1
                } else if (chip?.text.toString() == "Bike") {
                    M = "Bike"
                    Current_travel_mode = 3
                }
                val builder1 =
                    activity?.let { AlertDialog.Builder(it) }
                builder1?.setMessage("Do you want to confirm $M as travel mode?")
                builder1?.setCancelable(true)
                builder1?.setPositiveButton(
                    "Yes"
                ) { dialog, id ->
                    viewModel.setTravalMode(
                        mAppiledJobData?.assignId,
                        userId,
                        workerId,
                        Current_travel_mode
                    )
                }
                builder1?.setNegativeButton(
                    "No"
                ) { dialog, id -> dialog.cancel() }
                val alert11 = builder1?.create()
                alert11?.show()
            }
        })

        imgOtp?.setOnClickListener(this)
        imgMenu?.setOnClickListener(this)
        txtJobCompleted?.setOnClickListener(this)
        imgMap?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgMap -> {
                Utilities.openAddressOnMap(
                    activity,
                    mAppiledJobData?.address
                        ?: (mAppiledJobData?.locationLat + ", " + mAppiledJobData?.locationLong)
                )
            }
            R.id.txtJobCompleted -> onJobCompletedButtonClicked()
            R.id.imgMenu -> onMenuClicked()
            R.id.imgOtp -> {
//                if (mAppiledJobData?.workType.equals("On Demand")) {
                if (edtOtp.getText()?.length == 4) {
                    viewModel.setCheckInData(checkInId, userId, edtOtp.getText().toString())
                } else {
                    if (edtOtp.getText()?.length == 0) {
                        Utilities.showToast(
                            activity,
                            "Enter OTP"
                        )
                    } else {
                        Utilities.showToast(
                            activity,
                            "Enter 4 digit OTP"
                        )
                    }
                }
//                }
            }
        }
    }

    private fun showRatingBottomSheet() {
        val view = layoutInflater.inflate(R.layout.bottomsheet_rating_layout, null)
        bottomSheetDialog = activity?.let { BottomSheetDialog(it) }
        bottomSheetDialog?.setCancelable(false)
        bottomSheetDialog?.setCancelable(false)
        val ratingBar = view.findViewById<RatingBar>(R.id.rating)
        val close = view.findViewById<AppCompatImageView>(R.id.close)
        val reason = view.findViewById<AppCompatEditText>(R.id.reason_text)
        comment_group = view.findViewById(R.id.comments_group)
        rating_question = view.findViewById<AppCompatTextView>(R.id.rating_question)
        close.setOnClickListener { bottomSheetDialog?.dismiss() }
        ratingBar.onRatingBarChangeListener =
            OnRatingBarChangeListener { ratingBar, v, b ->
                if (b) {
                    if (v < 4) {
                        viewModel?.getEmployerReviewOpt("Bad")
                        rating = v.toInt()
                    } else {
                        viewModel.getEmployerReviewOpt("Good")
                        rating = v.toInt()
                    }
                }
            }
        val heightInPixels = (ScreenUtils.getScreenHeight() / 1f).toInt()
        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
        bottomSheetDialog?.setContentView(view, params)
        bottomSheetDialog?.show()
        val submit_rating = view.findViewById<TextView>(R.id.submit_rating)
        submit_rating.setOnClickListener {
            viewModel.saveReview(
                mAppiledJobData?.employerId,
                mAppiledJobData?.jobId,
                rating,
                userId,
                reason.text.toString() + ""
            )
        }
    }

    fun onJobCompletedButtonClicked() {
        val days: Int? = Utilities.getDaysDiff(mAppiledJobData?.startDate)

        if (((days ?: 0) + 1) >= mAppiledJobData?.workdaysCount ?: 0) {
            val builder = activity?.let { AlertDialog.Builder(it) }
            builder?.setMessage("Do you want to mark this job as complete?")
            builder?.setCancelable(true)
            builder?.setPositiveButton(
                "Yes"
            ) { dialog, id ->
                viewModel.jobCheckOut(checkInId, userId)
                rltJobComplete?.setVisibility(View.GONE)
                imgMenu?.setVisibility(View.GONE)
            }
            builder?.setNegativeButton(
                "No"
            ) { dialog, id -> dialog.cancel() }
            val alert11 = builder?.create()
            alert11?.show()
        } else {
            Utilities.showToast(
                activity,
                "Job will end on in next " + (mAppiledJobData?.workdaysCount?.minus(
                    days ?: 0
                )).toString() + " days"
            )
        }
    }

    private fun onMenuClicked() {
        val popupMenu = PopupMenu(activity, imgMenu)
        if (mAppiledJobData?.workType == "On Demand") {
            popupMenu.menuInflater.inflate(R.menu.on_demand_menu, popupMenu.menu)
        } else {
            popupMenu.menuInflater.inflate(R.menu.schedule_interview_menu, popupMenu.menu)
        }
        popupMenu.setOnMenuItemClickListener { menuItem ->
            if (mAppiledJobData?.workType == "On Demand") {
                when (menuItem.itemId) {
                    R.id.navigation_cancel -> {
                        OkayGoFirebaseAnalytics.interview_cancel(mAppiledJobData?.jobId?.toString())
                        val layoutInflater1 =
                            activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val view2: View = layoutInflater1.inflate(
                            R.layout.dialog_box_custom,
                            null
                        )
                        val message = view2.findViewById<AppCompatTextView>(R.id.txtMsg)
                        val cancel = view2.findViewById<AppCompatTextView>(R.id.txtCancel)
                        val confirm = view2.findViewById<AppCompatTextView>(R.id.txtConfirm)
                        val popupWindow = PopupWindow(
                            view2,
                            ChipGroup.LayoutParams.WRAP_CONTENT,
                            ChipGroup.LayoutParams.WRAP_CONTENT
                        )
                        //display the popup window
                        popupWindow.isOutsideTouchable = true
                        popupWindow.isFocusable = true
                        popupWindow.showAtLocation(constraintRoot, Gravity.CENTER, 0, 0)

                        popupWindow.setOnDismissListener {

                        }
                        cancel.setOnClickListener { popupWindow.dismiss() }
                        confirm.setOnClickListener {
                            viewModelJobList.rejectJob(mAppiledJobData?.assignId, userId)
                        }
                    }
                }
            }
            true
        }
        popupMenu.show()
    }

    private fun stopServiceAndRemoveData() {
//        val i = Intent(activity, CurrentLocationService::class.java)
//        activity?.stopService(i)
//        val ref: DatabaseReference? =
//            FirebaseDatabase.getInstance().getReference("locations")
//                .child(userId?.toString() ?: "")
//        ref?.removeValue()
    }

    private val onOKClick: () -> Unit = {
        activity?.onBackPressed()
    }

    /**
     * handl api's reposne
     */
    private fun attachObservers() {
        viewModel.responseTravalMode.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {

                    if (it.response == null) {
                        rltTravel?.setVisibility(View.VISIBLE)
//                            Toast.makeText(ScheduleInterviewActivity.this, "Null aa raha hai", Toast.LENGTH_SHORT).show();
                    } else {
//                        Current_travel_mode =
//                            travelModeResponseGS.getResponse().getTravelMode().toInt()
//                        modeOfTravelContainer.setVisibility(View.GONE)
//                        otpParent.setVisibility(View.VISIBLE)
                        viewModel.getCheckInData(mAppiledJobData?.assignId, userId)
                    }
                }
            }
        })

        viewModel.responseSaveTravalMode.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
//                    if (travelModeResponseGS.getResponse() != null) {
////                            Toast.makeText(ScheduleInterviewActivity.this, "Mode Saved.", Toast.LENGTH_SHORT).show();
//                        modeOfTravelContainer.setVisibility(View.GONE)
//                        otpParent.setVisibility(View.VISIBLE)
//                        getCheckInData(data.getAssignId())
//                    }
                }
            }
        })

        viewModel.responseCheckInData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it?.response?.content?.isEmpty() == false) {
                    checkInId = it?.response?.content?.get(0)?.checkInId ?: 0
                    if (it?.response?.content?.get(0).isVerifiedByOtp == 1
                    ) {
                        txtOtpTitle?.visibility = View.GONE
                        edtOtp?.visibility = View.GONE
                        txtTagLine?.visibility = View.GONE
                        imgOtp?.visibility = View.GONE
                        rltJobComplete?.setVisibility(View.VISIBLE)
                        imgMenu?.setVisibility(View.GONE)
                    } else {
                        txtOtpTitle?.visibility = View.VISIBLE
                        edtOtp?.visibility = View.VISIBLE
                        txtTagLine?.visibility = View.VISIBLE
                        imgOtp?.visibility = View.VISIBLE
                    }
                }
            }
        })
        viewModel.responsesetCheckInData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it?.response?.isVerifiedByOtp == 1) {
                        Utilities.showToast(
                            activity,
                            "Check In Sucessfully."
                        )
                        stopServiceAndRemoveData()
                        txtOtpTitle?.visibility = View.GONE
                        edtOtp?.visibility = View.GONE
                        txtTagLine?.visibility = View.GONE
                        imgOtp?.visibility = View.GONE
                        rltJobComplete?.setVisibility(View.VISIBLE)
                        imgMenu?.setVisibility(View.GONE)
                        checkInId = it?.response?.checkInId ?: 0
                    }
                } else if (it.code == Constants.API_ERROR) {
                    if (it.message?.isEmpty() == false) {
                        Utilities.showToast(activity, it.message)
                    } else {
                        Utilities.showToast(activity, "Invalid otp, please enter correct otp.")
                    }
                }
            }
        })

        viewModel.responseCheckOut.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.isEnded == 1) {
                        showRatingBottomSheet()

                    }
                }
            }
        })

        viewModel.responseSaveReview.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
//                    if (reviewResponseGS.getCode() === 1000) {
                    for (m in 0 until (comment_group?.getChildCount() ?: 0)) {
                        val chip: Chip? =
                            comment_group?.getChildAt(m)?.getId()?.let { it1 ->
                                comment_group?.findViewById(
                                    it1
                                )
                            }
                        if (chip?.isChecked == true) {
                            viewModel.saveReviewPt(
                                chip.id.toString() + "",
                                "1",
                                it.response?.reviewId.toString() + ""
                            )
                        }
                    }
//                    paymentDone()
                    bottomSheetDialog?.dismiss()

                }
            }
        })

        viewModel.responseSaveEmployerReviewOpt.observe(this, Observer
        {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Utilities.jobSuccessDialog(
                        activity,
                        "Job Completed",
                        "Your job mark as completed, Now you can see it in \"Past\" section",
                        true, onOKClick
                    )
                }
            }
        })

        viewModelJobList.responseRejectJob.observe(this, Observer
        {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Utilities.showToast(
                        activity,
                        "Job Canceled Sucessfully."
                    )
                    activity?.onBackPressed()

                }
            }
        })

        viewModel.responseGetEmployerReviewOpt.observe(this, Observer
        {
            it?.let {
                if (it.code == Constants.SUCCESS) {

                    comment_group?.removeAllViews()
                    val currentLang = Preferences.prefs?.getString(Constants.SELECTED_LANG, "en")
                    for (i in 0 until it.response?.content?.size!!) {
                        if (currentLang.equals(it.response.content.get(i).mapping)) {
                            val tagName: String? = it.response.content.get(i).typeValue
                            rating_question?.setText(it.response.content.get(i).typeDesc)
                            val chip = Chip(activity)
                            chip.isCheckable = true
                            chip.isCheckedIconVisible = false
                            chip.text = tagName
                            chip.id = it.response?.content?.get(i).id ?: 0
                            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
                            chip.setChipStrokeColorResource(R.color.theme)
                            chip.chipStrokeWidth = 1f
                            chip.isClickable = true
                            chip.isCheckedIconVisible = false
                            comment_group?.addView(chip)
                        }
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer
        {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer
        {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelJobList.apiError.observe(this, Observer
        {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.apiOtpError.observe(this, Observer
        {
            if (isAdded) {
                if (it?.isEmpty() == false) {
                    Utilities.showToast(activity, it)
                } else {
                    Utilities.showToast(activity, "Invalid otp, please enter correct otp.")
                }
            }

        })

        viewModelJobList.isLoading.observe(
            this, Observer
            {
                it.let {
                    if (it == true) {
                        Utilities.showLoader(activity)
                    } else {
                        Utilities.hideLoader()
                    }
                }
        })
    }

    fun paymentDone(): BottomSheetDialog? {
        val view1: View = (context as Activity).layoutInflater
            .inflate(R.layout.bottomsheet_layout_payment, null)
        val bottomSheetDialog1 = BottomSheetDialog(context as Activity)
        val close =
            view1.findViewById<ImageView>(R.id.close)
        val profile_image =
            view1.findViewById<ImageView>(R.id.profile_picture)
        val job_role = view1.findViewById<TextView>(R.id.job_role)
        val brand_name = view1.findViewById<TextView>(R.id.brand_name)
        val job_duration = view1.findViewById<TextView>(R.id.job_duration)
        val job_diff = view1.findViewById<TextView>(R.id.job_time_diff)
        val amount = view1.findViewById<TextView>(R.id.amount)
        job_role.setText(mAppiledJobData?.jobType)

        brand_name.setText(mAppiledJobData?.brandName ?: mAppiledJobData?.company)

        job_duration.setText(
            Utilities.convertTimeToAmPm(mAppiledJobData?.loginTime) + " - " + Utilities.convertTimeToAmPm(
                mAppiledJobData?.logoutTime
            )
        )

        job_diff.setText(
            (mAppiledJobData?.logoutTime?.split(":")?.get(0)?.toInt()?.minus(
                mAppiledJobData?.loginTime?.split(":")?.get(0)?.toInt() ?: 0
            )).toString() + " Hrs"
        )
        amount.setText(
            "\u20B9 " + mAppiledJobData?.amount
                .toString() + " / " + mAppiledJobData?.amountPer
        )
        close.setOnClickListener { bottomSheetDialog1.dismiss() }
        val heightInPixels = (ScreenUtils.getScreenHeight() / 1.5f).toInt()
        val params =
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
        bottomSheetDialog1.setContentView(view1, params)
        bottomSheetDialog1.show()
        return bottomSheetDialog1
    }
}