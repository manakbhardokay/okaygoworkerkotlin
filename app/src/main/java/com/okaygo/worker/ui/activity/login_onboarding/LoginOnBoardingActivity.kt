package com.okaygo.worker.ui.activity.login_onboarding

import android.os.Bundle
import com.okaygo.worker.R
import com.okaygo.worker.ui.activity.BaseActivity
import com.okaygo.worker.ui.fragments.login.LoginFragment

class LoginOnBoardingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_onboarding)
//        if (Preferences.prefs?.getBoolean(Constants.IS_CHOSE_PASS, false) == true) {
//            addFragment(ChosePasswordFragment(), false)
//        } else {
        addFragment(LoginFragment(), false)
//        }
//
//        val isOnBoardingDone = Preferences.prefs?.getBoolean(Constants.DONE_ONBOARDING, false);
//        val isLoggedin = Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false);
//        if (isLoggedin == true && isOnBoardingDone == false) {
//            startActivity(Intent(this, OnBoardingActivity::class.java))
//            finish()
//        } else {
//            addFragment(LoginFragment(), false)
//        }

    }
}