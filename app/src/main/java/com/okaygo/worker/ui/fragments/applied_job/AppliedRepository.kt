package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.AppliedJobResponse
import com.okaygo.worker.data.modal.reponse.JobCount
import com.okaygo.worker.data.modal.reponse.OfferAcceptResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object AppliedRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun getAppliedJobs(
        successHandler: (AppliedJobResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getAppliedJobs(userId, 500)
            .enqueue(object : Callback<AppliedJobResponse> {
                override fun onResponse(
                    call: Call<AppliedJobResponse>?,
                    response: Response<AppliedJobResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AppliedJobResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("applied failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun updateNotJoining(
        successHandler: (OfferAcceptResponse) -> Unit,
        failureHandler: (String) -> Unit,
        interviewId: Int?,
        isOfferRejected: Int?,
        userId: Int?
    ) {
        mService.updateForNotJoining(interviewId, isOfferRejected, userId)
            ?.enqueue(object : Callback<OfferAcceptResponse> {
                override fun onResponse(
                    call: Call<OfferAcceptResponse>?,
                    response: Response<OfferAcceptResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<OfferAcceptResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("ofer reject failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun offerAccepted(
        successHandler: (OfferAcceptResponse) -> Unit,
        failureHandler: (String) -> Unit,
        interviewId: Int?,
        isOfferAccepted: Int?,
        userId: Int?
    ) {
        mService.offerAccepted(interviewId, isOfferAccepted, userId)
            ?.enqueue(object : Callback<OfferAcceptResponse> {
                override fun onResponse(
                    call: Call<OfferAcceptResponse>?,
                    response: Response<OfferAcceptResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<OfferAcceptResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("OfferAccept failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun deleteJob(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        assignId: Int?
    ) {
        mService.deleteJob(assignId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("applied failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getInterviewDetail(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        detailId: Int?,
        userId: Int?
    ) {
        mService.getInterviewDetail(detailId, userId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("applied failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getInterviewTimeSlot(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        detailId: Int?,
        userId: Int?
    ) {
        mService.getInterviewTimeSlot(detailId, userId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("applied failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getJobCounts(
        successHandler: (JobCount) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getJobCounts(userId)
            ?.enqueue(object : Callback<JobCount> {
                override fun onResponse(
                    call: Call<JobCount>?,
                    response: Response<JobCount>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<JobCount>?, t: Throwable?) {
                    t?.let {
                        Log.e("applied failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

}