package com.okaygo.worker.ui.fragments.rating_review

import android.util.Log
import com.okaygo.worker.data.modal.reponse.RatingReviewResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object RatingReviewRepository {
    private val mService = ApiHelper.getService()

    /**
     *get review and rating data from server
     */
    fun getRatingReviews(
        successHandler: (RatingReviewResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?
    ) {
        mService.getRatingReview(workerId)?.enqueue(object : Callback<RatingReviewResponse> {
            override fun onResponse(
                call: Call<RatingReviewResponse>?,
                response: Response<RatingReviewResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<RatingReviewResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("referral failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}