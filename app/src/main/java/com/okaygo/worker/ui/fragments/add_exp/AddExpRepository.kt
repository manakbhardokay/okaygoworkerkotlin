package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.AddExpResponse
import com.okaygo.worker.data.modal.reponse.AddJobTypeResponse
import com.okaygo.worker.data.modal.reponse.JobTypeResponse
import com.okaygo.worker.data.modal.request.AddExpRequest
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object AddExpRepository {
    private val mService = ApiHelper.getService()

    /**
     * get in app data for particular user to our server
     */
    fun getInterestedJobType(
        successHandler: (JobTypeResponse) -> Unit,
        failureHandler: (String) -> Unit
    ) {
        mService.getInterestedJobType().enqueue(object : Callback<JobTypeResponse> {
            override fun onResponse(
                call: Call<JobTypeResponse>?,
                response: Response<JobTypeResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<JobTypeResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("intr job type failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     * get in app data for particular user to our server
     */
    fun getJobTypeForInd(
        successHandler: (JobTypeResponse) -> Unit,
        failureHandler: (String) -> Unit,
        inquery: String?
    ) {
        mService.getJobTypeForInd(inquery).enqueue(object : Callback<JobTypeResponse> {
            override fun onResponse(
                call: Call<JobTypeResponse>?,
                response: Response<JobTypeResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<JobTypeResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("intr job type failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     * get in app data for particular user to our server
     */
    fun addTypeToConfigMaster(
        successHandler: (AddJobTypeResponse) -> Unit,
        failureHandler: (String) -> Unit,
        jobType: String?,
        userId: Int?
    ) {
        mService.addTypeToConfigMaster(
            jobType,
            "job_type",
            userId.toString(),
            "1",
            "job_type_name",
            "gig_req",
            "-"
        ).enqueue(object : Callback<AddJobTypeResponse> {
            override fun onResponse(
                call: Call<AddJobTypeResponse>?,
                response: Response<AddJobTypeResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<AddJobTypeResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("addTypeToConf failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     * get in app data for particular user to our server
     */
    fun addIndToConfigMaster(
        successHandler: (AddJobTypeResponse) -> Unit,
        failureHandler: (String) -> Unit,
        industry: String?,
        type_id: String?,
        userId: Int?
    ) {
        mService.addIndustriesToConfigMaster(
            "industry",
            "job_type",
            userId.toString(),
            "1",
            industry,
            "gig_req",
            type_id
        ).enqueue(object : Callback<AddJobTypeResponse> {
            override fun onResponse(
                call: Call<AddJobTypeResponse>?,
                response: Response<AddJobTypeResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<AddJobTypeResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("addTypeToConf failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     * get in app data for particular user to our server
     */
    fun addWorkExp(
        successHandler: (AddExpResponse) -> Unit,
        failureHandler: (String) -> Unit,
        req: AddExpRequest?
    ) {
        val map: HashMap<String, String>? = HashMap()
        map?.put("requested_by", req?.requested_by?.toString() ?: "")
        map?.put("company_name", req?.company_name ?: "")
        map?.put("industry_type", req?.industry_type ?: "")
        map?.put("job_type", req?.job_type ?: "")
        map?.put("status", req?.status ?: "")
        map?.put("work_location", req?.work_location ?: "")
        map?.put("worker_id", req?.worker_id ?: "")
        map?.put("from_date", req?.from_date ?: "")
        if (req?.to_date != null) {
            map?.put("to_date", req.to_date)
        }
        if (req?.experience_id?.isNotEmpty() == true) {
            map?.put("experience_id", req.experience_id)
        }
        mService.addWorkExp(map).enqueue(object : Callback<AddExpResponse> {
            override fun onResponse(
                call: Call<AddExpResponse>?,
                response: Response<AddExpResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<AddExpResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("AddExpResponse failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}