package com.okaygo.worker.ui.fragments.login.model

import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation

data class AuthDetails(
    val authenticationContinuation: AuthenticationContinuation?,
    val username: String?
)