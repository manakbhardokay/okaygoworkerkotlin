package com.okaygo.worker.ui.fragments.availability

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ScreenUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.okaygo.worker.R
import com.okaygo.worker.adapters.AvailabilityAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.Availability
import com.okaygo.worker.data.modal.reponse.GetAvailabilityResponse
import com.okaygo.worker.data.modal.request.AvailbilityRequest
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import com.openkey.guest.ui.fragments.verification.PasswordModel
import kotlinx.android.synthetic.main.fragment_availability_dashboard.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * craeted by Davinder Goel OkayGo
 */
class AvailabilityDashBoardFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: OnBoardingModel
    private lateinit var viewModelPassword: PasswordModel
    private lateinit var viewModelJobList: JobListModel
    private var txtStartTime: AppCompatTextView? = null
    private var txtEndTime: AppCompatTextView? = null
    private var mCalendar: Calendar? = null
    private var mAvailabilityList: ArrayList<Availability>? = null
    private var mAvailabilityAdapter: AvailabilityAdapter? = null

    private var mStartTime: String? = null
    private var mEndTime: String? = null
    private var mSTime: Int? = 0
    private var isStartTime: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        viewModelPassword = ViewModelProvider(this).get(PasswordModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_availability_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OkayGoFirebaseAnalytics.availability_view(screenName())
        mCalendar = Calendar.getInstance()
        showInfoPopUp()
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAvailability(workerId, 100)
        viewModelPassword.getWorker(userId, false)
    }

    /**
     * set listener to required views
     */
    private fun setListeners() {
        imgBack?.setOnClickListener(this)
        imgAdd?.setOnClickListener(this)
        imgInfo?.setOnClickListener(this)


        chkFree?.setOnCheckedChangeListener({ compoundButton, isChecked ->
            if (isChecked) {
                OkayGoFirebaseAnalytics.availability_alltime()
                viewModelJobList.saveSuperAvailability(userId, null, 1, workerId)
                recylerAvailability?.setVisibility(View.GONE)
                imgAdd?.setVisibility(View.GONE)
            } else {
                viewModelJobList.saveSuperAvailability(userId, null, 0, workerId)
                recylerAvailability?.setVisibility(View.VISIBLE)
                imgAdd?.setVisibility(View.VISIBLE)
            }
        })
    }

    /**
     * selected bottom nav menu
     */
    private fun screenName(): String? {
        var screenName = ""
        when (Constants.BOTTOM_MENU_ITEM_SELECTED) {
            1 -> screenName = "findjobs"
            2 -> screenName = "earnings"
            3 -> screenName = "myjobs"
            4 -> screenName = "helps"
            5 -> screenName = "profile"
        }
        return screenName
    }

    /**
     * set adapter for availabilty
     */
    private fun setAdapter(reponse: GetAvailabilityResponse?) {
        if (mAvailabilityList == null) {
            mAvailabilityList = ArrayList()
        } else {
            mAvailabilityList?.clear()
        }
        reponse?.response?.content?.let { mAvailabilityList?.addAll(it) }
        if (mAvailabilityList?.isNotEmpty() == true && chkFree?.isChecked == false) {
            recylerAvailability?.visibility = View.VISIBLE
            imgAdd?.visibility = View.VISIBLE
        }
        mAvailabilityAdapter =
            activity?.let {
                AvailabilityAdapter(
                    it,
                    false,
                    mAvailabilityList,
                    onEditClick,
                    onDeleteClick,
                    onTggleClick
                )
            }
        val linearLayoutManager = LinearLayoutManager(activity)
        recylerAvailability?.setAdapter(mAvailabilityAdapter)
        recylerAvailability.setLayoutManager(linearLayoutManager)
    }


    /**
     * click listener when edit any exp
     */
    private val onEditClick: (Availability?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("OnEditAvail", it?.workdaysDetails.toString())
        addTme(it)
    }

    /**
     * click listener when edit any exp
     */
    private val onTggleClick: (Int?, Int?, Int?) -> Unit = { availabiltyId, it, pos ->
        Log.e("availabiltyId", availabiltyId.toString())
        Log.e("onTggleClick", it?.toString() ?: "")
        viewModel.updateAvailabilityStatus(availabiltyId, userId, it)
    }

    /**
     * click listener for delete exp
     */
    private val onDeleteClick: (Availability?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("onDeleteAvail", it?.workdaysDetails.toString())
        val builder = context?.let { it1 -> AlertDialog.Builder(it1) }
        builder?.setMessage(getString(R.string.want_delete))
        builder?.setCancelable(true)
        builder?.setPositiveButton("Yes") { dialog, id ->
            OkayGoFirebaseAnalytics.availability_delete(it?.availablityId?.toString())
            viewModel.deleteAvailability(it?.availablityId)
        }

        builder?.setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder?.create()
        alert?.show()
    }

    /**
     * add available time handling
     */
    private fun addTme(data: Availability?) {
        val view: View? = activity?.layoutInflater?.inflate(R.layout.bottomsheet_time_slot, null)
        val bottomSheetDialog = activity?.let { BottomSheetDialog(it) }
        val heightInPixels: Int = ScreenUtils.getScreenHeight() / 2
        val params =
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
        view?.let { bottomSheetDialog?.setContentView(it, params) }
        val txtTitle = view?.findViewById<AppCompatTextView>(R.id.txtTitle)
        val imgClose = view?.findViewById<AppCompatImageView>(R.id.imgClose)
        txtStartTime = view?.findViewById<AppCompatTextView>(R.id.txtTimeTo)
        val txtConfirm = view?.findViewById<AppCompatTextView>(R.id.txtConfirm)
        txtEndTime = view?.findViewById<AppCompatTextView>(R.id.txtTimeFrom)
        val chipGroup: ChipGroup? = view?.findViewById(R.id.chgDays)
        var tagName: String? = ""
        var availabilityId: Int? = null
        var jsonObject: JsonObject? = null
        if (data != null) {
            availabilityId = data.availablityId
            txtTitle?.text = "edit availability"
            txtStartTime?.setText(Utilities.convertTimeToAmPm(data.loginTime))
            txtEndTime?.setText(Utilities.convertTimeToAmPm(data.logoutTime))
            val jsonParser = JsonParser()
            jsonObject = jsonParser.parse(data.workdaysDetails).getAsJsonObject()
        }

        var id: Int? = 0

        for (i in 0..6) {
            when (i) {
                0 -> {
                    tagName = "M"
                    id = 2
                }
                1 -> {
                    tagName = "T"
                    id = 3
                }
                2 -> {
                    tagName = "W"
                    id = 4
                }
                3 -> {
                    tagName = "T"
                    id = 5
                }
                4 -> {
                    tagName = "F"
                    id = 6
                }
                5 -> {
                    tagName = "S"
                    id = 7
                }
                6 -> {
                    tagName = "S"
                    id = 1
                }
            }
            val chip = Chip(activity)

            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            chip.text = tagName
            chip.id = id ?: 0
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 1f
            chip.isClickable = true
            chip.isChipIconVisible = true
            chip.isCloseIconVisible = false
            chip.isCheckedIconVisible = false

            if (jsonObject?.get(id?.toString())?.getAsBoolean() == true) {
                chip.isChecked = true
            }
            chipGroup?.addView(chip)
        }

        txtConfirm?.setOnClickListener {
            var count = 0
            val days = HashMap<String, Boolean>()
            days.clear()
            for (i in 0 until chipGroup?.childCount!!) {
                val chip: Chip = chipGroup.findViewById(chipGroup.getChildAt(i).id)
                val d = arrayOf("2", "3", "4", "5", "6", "7", "1")
                if (chip.isChecked) {
                    count++
                    days[d[i]] = true
                } else {
                    days[d[i]] = false
                }
            }
            if (count == 0) {
                Utilities.showToast(activity, "Select atleast one day.")
            } else {
                val gson = Gson()
                val req: AvailbilityRequest? = AvailbilityRequest(
                    userId,
                    gson.toJson(days),
                    workerId,
                    availabilityId?.toString(),
                    getTimeSupportedFormat(mStartTime?.toString()),
                    getTimeSupportedFormat(mEndTime?.toString()),
                    1
                )
                viewModel.saveAvailability(req)
                if (data == null) {
                    OkayGoFirebaseAnalytics.availability_add(
                        mStartTime,
                        mEndTime,
                        yesOrNo(days, "2"),
                        yesOrNo(days, "3"),
                        yesOrNo(days, "4"),
                        yesOrNo(days, "5"),
                        yesOrNo(days, "6"),
                        yesOrNo(days, "7"),
                        yesOrNo(days, "1")
                    );
                } else {
                    OkayGoFirebaseAnalytics.availability_edit(
                        mStartTime,
                        mEndTime,
                        yesOrNo(days, "2"),
                        yesOrNo(days, "3"),
                        yesOrNo(days, "4"),
                        yesOrNo(days, "5"),
                        yesOrNo(days, "6"),
                        yesOrNo(days, "7"),
                        yesOrNo(days, "1")
                    );
                }
                bottomSheetDialog?.dismiss()
            }
        }

        txtStartTime?.setOnClickListener {
            isStartTime = true
            Utilities.HourTimePicker(activity, OnTimeSelection)
        }

        txtEndTime?.setOnClickListener {
            if (mSTime != -1) {
                Utilities.HourTimePicker(activity, OnTimeSelection)
                isStartTime = false
            } else {
                Utilities.showToast(
                    activity,
                    "Select start time"
                )
            }
        }

        imgClose?.setOnClickListener { bottomSheetDialog?.dismiss() }
        bottomSheetDialog?.show()
    }

    private fun yesOrNo(
        days: HashMap<String, Boolean>?,
        key: String
    ): String? {
        return if (days != null && days.containsKey(key)) {
            if (days?.get(key) == true) "yes" else "no"
        } else ""
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> activity?.onBackPressed()
            R.id.imgAdd -> addTme(null)
            R.id.imgInfo -> showInfoPopUp()
        }
    }

    /**
     * get supported time format for sending to api
     */
    private fun getTimeSupportedFormat(time: String?): String? {
        if (time?.trim({ it <= ' ' })?.contains(" ") == true) {
            if (time?.split(" ".toRegex()).toTypedArray().get(1).toString() == "AM") {
                if (time?.split(" ".toRegex()).toTypedArray().get(0).toInt() == 12) {
                    return "00:00:00"
                } else {
                    return time?.split(" ".toRegex()).toTypedArray().get(0).toInt()
                        .toString() + ":00:00"
                }
            } else {
                if (time?.split(" ".toRegex()).toTypedArray().get(0).toInt() == 12) {
                    return "12:00:00"
                } else {
                    return (time?.split(" ".toRegex()).toTypedArray().get(0)
                        .toInt() + 12).toString() + ":00:00"
                }
            }
        }
        return ""
    }

    /**
     * show availability info popup
     */
    private fun showInfoPopUp() {
        // Create custom dialog object
        val dialog = activity?.let { Dialog(it) }
        // Include dialog.xml file
        dialog?.setContentView(R.layout.dialog_extra_paise_kmaye)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
        //        dialog.setTitle("Custom Dialog");
        val OkayButton = dialog?.findViewById<View>(R.id.txtOkay) as AppCompatTextView

        // if decline button is clicked, close the custom dialog
        OkayButton.setOnClickListener { // Close dialog
            dialog.dismiss()
        }
        dialog.show()
    }

    /**
     * handle all api's response
     */
    private fun attachObservers() {
        viewModel.responseSaveAvailability.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.getAvailability(workerId, 20)
                }
            }
        })

        viewModelPassword.responseWorker.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    //                        Toast.makeText(AvailabilitySettings.this, "Global : "+workerResponseGS.getResponse().getContent().get(0).getGlobally_available(), Toast.LENGTH_SHORT).show();
                    if (it.response?.content?.get(0)?.globalAvailability == 1) {
                        chkFree?.setChecked(true)
                        recylerAvailability?.visibility = View.GONE
                        imgAdd?.visibility = View.GONE
                    } else {
                        chkFree?.setChecked(false)
                        recylerAvailability?.visibility = View.VISIBLE
                        imgAdd?.visibility = View.VISIBLE
                    }
                }
            }
        })

        viewModel.responseGetAvailability.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    setAdapter(it)
                }
            }
        })

        viewModel.responseUpdateAvailability.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                }
            }
        })

        viewModel.responseGlobalAvailability.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {

                }
            }
        })
        viewModel.responseDeleteAvailability.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel?.getAvailability(workerId, 20)
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    /**
     * handling when you click on time while adding
     */
    private val OnTimeSelection: (String?) -> Unit = { it ->
        var time = 0
        Log.e("hourOfDay", it ?: "")
        if (it?.contains("AM") == true) {
            if (it?.split(" ".toRegex()).toTypedArray()[0].toInt() == 12) {
                time = 0
            } else {
                time = it?.split(" ".toRegex()).toTypedArray()[0]?.toInt()
            }
        } else if (it?.contains("PM") == true) {
            if (it?.split(" ".toRegex()).toTypedArray()[0].toInt() == 12) {
                time = 12
            } else {
                time = it?.split(" ".toRegex()).toTypedArray()[0].toInt() + 12
            }
        }
        if (isStartTime == true) {
            txtStartTime?.text = it
            mSTime = time
            mStartTime = it ?: ""

        } else {
            txtEndTime?.text = it
            mSTime = time
            mEndTime = it ?: ""
        }
    }
}