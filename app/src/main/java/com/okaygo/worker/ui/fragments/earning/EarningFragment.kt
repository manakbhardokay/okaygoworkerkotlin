package com.okaygo.worker.ui.fragments.earning

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayoutMediator
import com.okaygo.worker.R
import com.okaygo.worker.adapters.PaymentViewPagerAdapter
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.ui.fragments.verification.PaymentModel
import kotlinx.android.synthetic.main.fragment_earning.*
import kotlinx.android.synthetic.main.fragment_notification.tabs
import kotlinx.android.synthetic.main.fragment_notification.viewPager

class EarningFragment : BaseFragment() {
    private lateinit var viewModel: PaymentModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PaymentModel::class.java)
        attachObservers()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_earning, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getTransectionStats(userId)
        viewPager?.setAdapter(activity?.let { PaymentViewPagerAdapter(it) })
        TabLayoutMediator(tabs, viewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                when (position) {
                    0 -> {
                        tab.text = "Jobs"
                    }
                    1 -> {
                        tab.text = "Referrals"
                    }
                }
            }).attach()
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseTransectionStats.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    txtAmountDue?.text = "₹ ${it.response?.toBePaid}"
                    if (it.response?.lastPaidAmount ?: 0.0 > 0.0) {
                        txtTransferedAmt?.visibility = View.VISIBLE
                        txtTransferedAmt?.text =
                            "₹ ${it.response?.lastPaidAmount} transferred to your A/C on ${
                                Utilities.getDateFromMili(
                                    it?.response?.lastPaidDate,
                                    "dd MMM"
                                )
                            }"
                    } else {
                        txtTransferedAmt?.visibility = View.GONE
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}