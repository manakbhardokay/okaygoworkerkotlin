package com.okaygo.worker.ui.fragments.on_boarding

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.hardware.Camera
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.request.UserDetailRequest
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import kotlinx.android.synthetic.main.fragment_selfy.*
import java.io.File

class SelfyFragment : BaseFragment(), View.OnClickListener {
    private val REQUEST_IMAGE_CAPTURE = 111
    private lateinit var viewModelOnBoard: OnBoardingModel
    private var mProfilePicPath: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelOnBoard = ViewModelProvider(this).get(OnBoardingModel::class.java)
        attachObservers()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_selfy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        txtSkipForNow?.setOnClickListener(this)
        txtClick?.setOnClickListener(this)
        txtClick?.setOnClickListener(this)
        imgBack?.setOnClickListener(this)
        btnSaveContinue?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> activity?.onBackPressed()
            R.id.btnSaveContinue -> {
                if (mProfilePicPath?.isEmpty() == false) {
                    val request: UserDetailRequest? = UserDetailRequest(
                        null,
                        null,
                        null,
                        null,
                        null,
                        userId.toString(),
                        null,
                        null,
                        mProfilePicPath, null, null, null, null
                    )
                    viewModelOnBoard.saveUserDetail(request)                       //save profilePhoto api call
                } else {
                    Utilities.showToast(activity, "Click A selfie First.")
                }
            }
            R.id.txtSkipForNow -> {
                OkayGoFirebaseAnalytics.on_boarding_selfie_details("skip")
                attachFragment(EducationLanguageFragment(), false)
            }
            R.id.txtClick -> {
                activity?.let {
                    if (ContextCompat.checkSelfPermission(
                            it,
                            Manifest.permission.CAMERA
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            it,
                            arrayOf(Manifest.permission.CAMERA),
                            201
                        )
                    } else {
                        launchCamera()
                    }
                }

            }
        }
    }

    private fun launchCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (activity?.getPackageManager()?.let { takePictureIntent.resolveActivity(it) } != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1 && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                takePictureIntent.putExtra(
                    "android.intent.extras.CAMERA_FACING",
                    Camera.CameraInfo.CAMERA_FACING_FRONT
                )
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                takePictureIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true)
                takePictureIntent.putExtra(
                    "android.intent.extras.LENS_FACING_FRONT",
                    1
                ) // Tested on API 24 Android version 7.0(Samsung S6)
            }
            startActivityForResult(
                takePictureIntent, REQUEST_IMAGE_CAPTURE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> if (resultCode == Activity.RESULT_OK) {
                val extras = data?.extras
                if (extras != null) {
                    val imageBitmap = extras["data"] as Bitmap?
                    val file: File? = Utilities.getFileFromBitmap(activity, imageBitmap)
                    Utilities.setImageByGlide(activity, imageBitmap, imgProPic)
                    Log.e("selfy image path", file?.absolutePath + "")
//                    val file1 = File(file?.absolutePath)
                    viewModelOnBoard.getDocLink(userId, file)
                }
            }
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModelOnBoard.responseGetDocLink.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    mProfilePicPath = it.response
                }

            }
        })

        viewModelOnBoard.responsePersonalDetail.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    OkayGoFirebaseAnalytics.on_boarding_selfie_details("save")
                    attachFragment(EducationLanguageFragment(), false)
                }
            }
        })

        viewModelOnBoard.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModelOnBoard.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}