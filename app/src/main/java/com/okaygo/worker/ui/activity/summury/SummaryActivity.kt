package com.okaygo.worker.ui.activity.summury

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.messaging.FirebaseMessaging
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.OnBoardingStatusResponse
import com.okaygo.worker.data.modal.request.CreateSessionRequest
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.BaseActivity
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.okaygo.worker.ui.fragments.add_exp.AddExperienceFragment
import com.okaygo.worker.ui.fragments.on_boarding.*
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.PasswordModel
import com.openkey.guest.ui.fragments.verification.SummaryModel
import kotlinx.android.synthetic.main.activiy_summary.*


class SummaryActivity : BaseActivity(), View.OnClickListener {
    private lateinit var viewModelWorker: PasswordModel  // this is for handle/get worker detail
    private lateinit var viewModel: SummaryModel
    private var isPersonalDetailDone = false
    private var isSelfyDone = false
    private var isExpDone = false
    private var isAvailablityDone = false
    private var isEduDone = false
    private var isSkillDone = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activiy_summary)
        Constants.IS_FROM_PROFILE = false
        viewModelWorker = ViewModelProvider(this).get(PasswordModel::class.java)
        viewModel = ViewModelProvider(this).get(SummaryModel::class.java)
        val intent = intent
        if (intent != null && intent?.hasExtra("very_first")) {
            if (intent?.getBooleanExtra("very_first", false) == true) {
                addFragment(PersonalDetailFragment(), false)
            }
        }
        attachObservers()
        setListeners()
        viewModelWorker.getWorker(Preferences.prefs?.getInt(Constants.ID, 0), false)
        viewModel.getOnBoardingStatus(Preferences.prefs?.getInt(Constants.ID, 0))
    }

    private fun setListeners() {
        txtPersonalDetails?.setOnClickListener(this)
        txtSelfy?.setOnClickListener(this)
        txtEduLang?.setOnClickListener(this)
        txtExp?.setOnClickListener(this)
        txtSkills?.setOnClickListener(this)
        txtFreeTime?.setOnClickListener(this)
        imgStartLooking?.setOnClickListener(this)
        txtStratLooking?.setOnClickListener(this)

        FirebaseMessaging.getInstance().token
            .addOnSuccessListener { instanceIdResult ->
                val token = instanceIdResult
                val sessionId: String? = Preferences.prefs?.getString(Constants.OG_SESSION_ID, "");
                if (sessionId == null || sessionId.isEmpty() == true) {
                    val createSessionRequest: CreateSessionRequest? = CreateSessionRequest(
                        token,
                        null,
                        null,
                        Preferences.prefs?.getInt(Constants.ID, 0),
                        null,
                        null,
                        null,
                        null,
                        null
                    )
                    viewModelWorker.createWorkerSession(createSessionRequest)
                }
            }
    }

    override fun onBackPressed() {
        val fragment: Fragment? = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is PersonalDetailFragment || fragment is EducationLanguageFragment || fragment is SelfyFragment || fragment is SkillsFragment || fragment is ExperienceFragment || fragment is AvailabilityFragment) {
            clearStack()
        } else if (fragment is AddExperienceFragment) {
            super.onBackPressed()
        } else {
            finish()
        }
    }

    /**
     * remove all added fragments to container
     */
    fun clearStack() {
        //Here we are clearing back stack fragment entries
        val backStackEntry = supportFragmentManager.backStackEntryCount
        if (backStackEntry > 0) {
            for (i in 0 until backStackEntry) {
                supportFragmentManager.popBackStackImmediate()
            }
        }

        //Here we are removing all the fragment that are shown here
        if (supportFragmentManager.fragments != null && supportFragmentManager.fragments.size > 0) {
            for (i in supportFragmentManager.fragments.indices) {
                val mFragment: Fragment? = supportFragmentManager.fragments[i]
                if (mFragment != null) {
                    supportFragmentManager.beginTransaction().remove(mFragment).commit()
                }
            }
        }
        viewModel.getOnBoardingStatus(                        // onboarding status api call
            Preferences.prefs?.getInt(
                Constants.ID,
                0
            )
        )
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtPersonalDetails -> {
                if (!isPersonalDetailDone) {
                    addFragment(PersonalDetailFragment(), false)
                } else {
                    Toast.makeText(this, "Already Done.", Toast.LENGTH_SHORT).show()
                }
            }

            R.id.txtSelfy -> {
                if (!isSelfyDone && isPersonalDetailDone) {
                    addFragment(SelfyFragment(), false)
                } else if (isSelfyDone) {
                    Toast.makeText(this, "Already Done.", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Fill personal details first", Toast.LENGTH_SHORT).show()
                }
            }


            R.id.txtEduLang -> {
                if (!isEduDone && isPersonalDetailDone) {
                    addFragment(EducationLanguageFragment(), false)
                } else if (isEduDone) {
                    Toast.makeText(this, "Already Done.", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Add personal details first", Toast.LENGTH_SHORT).show()
                }
            }

            R.id.txtExp -> {
                if (!isExpDone && isEduDone && isPersonalDetailDone) {
                    addFragment(ExperienceFragment(), false)
                } else if (isExpDone) {
                    Toast.makeText(this, "Already Done.", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Add Your Qualification details", Toast.LENGTH_SHORT)
                        .show()
                }
            }

            R.id.txtSkills -> {
                if (!isSkillDone && isPersonalDetailDone && isEduDone && isExpDone) {
                    Constants.IS_FROM_PROFILE_SKILLS = false
                    addFragment(SkillsFragment(), false)
                } else if (isSkillDone) {
                    Toast.makeText(this, "Already Done.", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Add a work experience first", Toast.LENGTH_SHORT).show()
                }
            }

            R.id.txtFreeTime -> {
                if (!isAvailablityDone && isPersonalDetailDone && isEduDone && isSkillDone && isExpDone) {
                    addFragment(AvailabilityFragment(), false)
                } else if (isAvailablityDone) {
                    Toast.makeText(this, "Already Done.", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Add skills first", Toast.LENGTH_SHORT).show()
                }
            }

            R.id.imgStartLooking,
            R.id.txtStratLooking -> {
                if (isPersonalDetailDone && isExpDone && isAvailablityDone && isEduDone && isSkillDone) {
                    val intent = Intent(this@SummaryActivity, DashBoardActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(
                        this@SummaryActivity,
                        "Complete onboarding first.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    /**
     * handle summary api response
     */
    private fun handleSummaryResponse(response: OnBoardingStatusResponse?) {
        response?.let {
            var count = 0
            if (it.response?.experience == true) {
                imgExpStatus.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@SummaryActivity,
                        R.drawable.ic_green_tick
                    )
                )
                count++
                isExpDone = true
            }
            if (it.response?.skills == true) {
                imgSkillsStatus.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@SummaryActivity,
                        R.drawable.ic_green_tick
                    )
                )
                count++
                isSkillDone = true
            }
            if (it.response?.qualification == true) {
                imgEduLangStatus.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@SummaryActivity,
                        R.drawable.ic_green_tick
                    )
                )
                count++
                isEduDone = true
            }
            if (it.response?.availabiliity == true) {
                imgFreeTimeStatus.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@SummaryActivity,
                        R.drawable.ic_green_tick
                    )
                )
                count++
                isAvailablityDone = true
            }
            if (it.response?.profile == true) {
                imgPersonalStatus.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@SummaryActivity,
                        R.drawable.ic_green_tick
                    )
                )
                count++
                isPersonalDetailDone = true
            }
            if (it.response?.profile_photo == true) {
                imgSelfyStatus.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@SummaryActivity,
                        R.drawable.ic_green_tick
                    )
                )
                count++
                isSelfyDone = true
            }
            val isForDeeplink = Preferences.prefs?.getBoolean(Constants.IS_FOR_DEEPLINK, false)
            if (isPersonalDetailDone && isExpDone && isAvailablityDone && isEduDone) {
                Preferences.prefs?.clearValue(Constants.IS_FOR_DEEPLINK)
                Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, true)
                startActivity(Intent(this, DashBoardActivity::class.java))
                finish()
            } else if (isForDeeplink == true) {
//                    startActivity(Intent(this, JonbDe::class.java))

            }

        }
    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {
        viewModel.response.observe(this, Observer {
            if (it?.code == Constants.SUCCESS) {
                handleSummaryResponse(it)
            }
        })

        viewModelWorker.responseWorker.observe(this, Observer {
            if (it?.code == (Constants.SUCCESS) && it?.response?.content?.isNotEmpty() == true) {
                Preferences.prefs?.saveValue(
                    Constants.EMPLOYER_ID,
                    it?.response?.content?.get(0)?.workerId
                )
            }
        })

        viewModelWorker.responseCreateSession.observe(this, Observer {
            it?.let {
                if (it?.code == Constants.SUCCESS) {
                    Preferences.prefs?.saveValue(Constants.OG_SESSION_ID, it?.response?.sessionId)
                }
            }
        })

        viewModelWorker.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(this, it)
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(this, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(this)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}