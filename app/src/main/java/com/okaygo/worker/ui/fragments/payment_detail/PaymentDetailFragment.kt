package com.okaygo.worker.ui.fragments.referral

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.okaygo.worker.R
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.ui.fragments.verification.PaymentDetailModel
import kotlinx.android.synthetic.main.fragment_payment_details.*

class PaymentDetailFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: PaymentDetailModel

    private var upi: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        viewModel = ViewModelProvider(this).get(PaymentDetailModel::class.java)
        attachObservers()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_payment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getUPI(workerId)
        edtUpi?.isEnabled = false
        setListeners()
    }

    /**
     * set click listeners for required views
     */
    private fun setListeners() {
        imgBackIcon?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtEdit?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)
    }


    /**
     * handle all api resposne
     */
    private fun attachObservers() {

        viewModel.responseSaveUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it?.response
                    edtUpi?.setText(upi)
                    Utilities.showToast(activity, "UPI Id is updated.")
                }
            }
        })

        viewModel.responseGetUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response
                    if (upi?.isEmpty() == false) {
                        edtUpi?.setText(upi)
                    }
                }
            }
        })


        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtEdit -> {
                edtUpi?.isEnabled = true
                linearBottomBtns?.visibility = View.VISIBLE
                txtEdit?.visibility = View.GONE
            }
            R.id.imgBackIcon -> activity?.onBackPressed()
            R.id.txtSave -> {
                upi = edtUpi?.text?.trim()?.toString()
                val count = upi?.split("@")
                if (upi?.isEmpty() == true) {
                    Utilities.showToast(activity, "Enter your UPI id.")
                } else if ((upi?.length) ?: 0 < 3 || upi?.contains("@") == false || (count?.size
                        ?: 0) != 2
                ) {
                    edtUpi?.setError("Invalid UPI id.")
                } else {
                    Dialogs.showAlertDialog(
                        activity,
                        "Check your UPI ID",
                        "Entered UPI ID is $upi. Do you want to save?",
                        "Yes",
                        "Edit",
                        onAlertPosBtnClick,
                        onAlertNagClick
                    )
                }
            }

            R.id.txtDiscard -> {
                viewModel.getUPI(workerId)
                edtUpi?.isEnabled = false
                linearBottomBtns?.visibility = View.GONE
                txtEdit?.visibility = View.VISIBLE
            }

//            R.id.txtTerms -> {
//                Utilities.showAlertTermsPolicy(
//                    activity,
//                    resources.getString(R.string.tc),
//                    resources.getString(R.string.referral_terms)
//                )
//            }
        }
    }


    private val onAlertNagClick: () -> Unit = {
    }

    private val onAlertPosBtnClick: () -> Unit = {
        viewModel.updateUPI(workerId, upi?.trim())
        linearBottomBtns?.visibility = View.GONE
        txtEdit?.visibility = View.VISIBLE
        edtUpi?.isEnabled = false
    }
}