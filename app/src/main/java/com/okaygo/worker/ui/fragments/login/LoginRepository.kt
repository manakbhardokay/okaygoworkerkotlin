package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.okaygo.worker.data.modal.InAppResponse
import com.okaygo.worker.ui.fragments.login.model.AuthDetails
import com.okaygo.worker.ui.fragments.login.model.AuthenticationSuccess
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object LoginRepository {
    private val mService = ApiHelper.getService()

    fun authenticationHandler(
        successHandler: (AuthenticationSuccess) -> Unit,
        authDetailHandler: (AuthDetails) -> Unit,
        failureHandler: (String) -> Unit,
        authenticationHandler: (AuthenticationHandler)
    ) {
        var authenticationHandler: AuthenticationHandler? = object : AuthenticationHandler {
            override fun onSuccess(
                cognitoUserSession: CognitoUserSession?,
                device: CognitoDevice?
            ) {
                Log.e("authenticationHandler", " -- Auth Success")
                val authSuccess = AuthenticationSuccess(cognitoUserSession, device)
                successHandler(authSuccess)
            }

            override fun getAuthenticationDetails(
                authenticationContinuation: AuthenticationContinuation,
                username: String
            ) {
                val authDetail = AuthDetails(authenticationContinuation, username)
                authDetailHandler(authDetail)
            }

            override fun getMFACode(multiFactorAuthenticationContinuation: MultiFactorAuthenticationContinuation) {}
            override fun onFailure(e: Exception) {
                failureHandler(e.localizedMessage)
            }

            override fun authenticationChallenge(continuation: ChallengeContinuation) {}
        }
    }

    /**
     * get in app data for particular user to our server
     */
    fun getInAppData(
        successHandler: (InAppResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getInAppData(userId).enqueue(object : Callback<InAppResponse> {
            override fun onResponse(
                call: Call<InAppResponse>?,
                response: Response<InAppResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<InAppResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("in app data  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}