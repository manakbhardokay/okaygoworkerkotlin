package com.okaygo.worker.ui.fragments.my_jobs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import com.okaygo.worker.R
import com.okaygo.worker.adapters.MyJobViewPagerAdpter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.JCount
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.applied_job.AppliedJobFragment
import com.okaygo.worker.ui.fragments.upcoming.PastJobFragment
import com.okaygo.worker.ui.fragments.upcoming.UpcomingFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.AppliedModel
import kotlinx.android.synthetic.main.fragment_my_job.*

class MyJobFragments : BaseFragment() {
    private lateinit var viewModelApplied: AppliedModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelApplied = ViewModelProvider(this).get(AppliedModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_job, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OkayGoFirebaseAnalytics.myjobs()
        viewModelApplied?.getJobCounts(userId)
        Constants.BOTTOM_MENU_ITEM_SELECTED = 3
        setupViewPager()
        val currentTab = Preferences.prefs?.getInt(Constants.CURRENT_TAB, 0)
        if (currentTab != 0) {
            if (currentTab == 1) {
                tabs?.getTabAt(0)?.select()
            } else if (currentTab == 2) {
                tabs?.getTabAt(1)?.select()
            } else if (currentTab == 3) {
                tabs?.getTabAt(2)?.select()
            }
            Preferences.prefs?.saveValue(Constants.CURRENT_TAB, 0)
        }
        tabClickEvent()

    }

    /**
     * seup view pager for tabs
     *
     * @param viewPager
     */
    private fun setupViewPager() {
        val adapter = MyJobViewPagerAdpter(childFragmentManager)
        adapter.addFragment(
            AppliedJobFragment(tabs),
            activity?.resources?.getString(R.string.applied) ?: ""
        )
        adapter.addFragment(UpcomingFragment(tabs), "Upcoming")
        adapter.addFragment(PastJobFragment(tabs), "Past")
        viewPager?.offscreenPageLimit = 2
        viewPager?.adapter = adapter
        tabs?.setupWithViewPager(viewPager)

    }

    fun changeTabText(response: JCount?) {
        if (response?.applied_job_count ?: 0 > 0) {
            tabs?.getTabAt(0)?.text = "Applied (${response?.applied_job_count})"
        } else {
            tabs?.getTabAt(0)?.text = "Applied"
        }
        if (response?.upcoming_job_count ?: 0 > 0) {
            tabs?.getTabAt(1)?.text = "Upcoming (${response?.upcoming_job_count})"
        } else {
            tabs?.getTabAt(1)?.text = "Upcoming"
        }
        if (response?.past_job_count ?: 0 > 0) {
            tabs?.getTabAt(2)?.text = "Past (${response?.past_job_count})"
        } else {
            tabs?.getTabAt(2)?.text = "Past"
        }
    }

    private fun tabClickEvent() {
        tabs?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    1 -> OkayGoFirebaseAnalytics.myjobs_upcoming()
                    2 -> OkayGoFirebaseAnalytics.myjobs_past()
                    else -> {
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModelApplied.responseJobCount.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    changeTabText(it.response)
                }
            }
        })

        viewModelApplied.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelApplied.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

    }
}
