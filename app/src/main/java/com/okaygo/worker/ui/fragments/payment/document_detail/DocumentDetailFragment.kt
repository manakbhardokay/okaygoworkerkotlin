package com.okaygo.worker.ui.fragments.referral

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.WorkerDetails
import com.okaygo.worker.data.modal.request.UpdateWorkerRequest
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.ui.fragments.verification.ExperinceModel
import com.openkey.guest.ui.fragments.verification.ProfileModel
import kotlinx.android.synthetic.main.fragment_documents.*
import kotlinx.android.synthetic.main.fragment_payment_details.imgBackIcon
import kotlinx.android.synthetic.main.fragment_payment_details.linearBottomBtns
import kotlinx.android.synthetic.main.fragment_payment_details.txtDiscard
import kotlinx.android.synthetic.main.fragment_payment_details.txtEdit
import kotlinx.android.synthetic.main.fragment_payment_details.txtSave

class DocumentDetailFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModelProfile: ProfileModel
    private lateinit var viewModelExperince: ExperinceModel

    private var mAdhaar: Int? = null
    private var mRC: Int? = null
    private var mDL: Int? = null
    private var mLAPTOP: Int? = null
    private var mSMARTPHONE: Int? = null
    private var mWifi: Int? = null
    private var mBike: Int? = null
    private var mPan: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModelExperince = ViewModelProvider(this).get(ExperinceModel::class.java)

        attachObservers()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_documents, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModelProfile.getWorkerByUserId(userId)
        setListeners()
        handleView(false)
    }

    /**
     * set click listeners for required views
     */
    private fun setListeners() {
        imgBackIcon?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtEdit?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)

        txtWifiYes?.setOnClickListener(this)
        txtWifiNo?.setOnClickListener(this)
        txtLaptopYes?.setOnClickListener(this)
        txtLaptopNo?.setOnClickListener(this)
        txtAdhaarYes?.setOnClickListener(this)
        txtAdhaarNo?.setOnClickListener(this)
        txtPanNo?.setOnClickListener(this)
        txtPanYes?.setOnClickListener(this)
        txtSmartYes?.setOnClickListener(this)
        txtSmartNo?.setOnClickListener(this)
        txtBikeYes?.setOnClickListener(this)
        txtBikeNo?.setOnClickListener(this)
        txtRCYes?.setOnClickListener(this)
        txtRCNo?.setOnClickListener(this)
        txtLicenceYes?.setOnClickListener(this)
        txtLicenceNo?.setOnClickListener(this)
    }


    private fun setYesActive(txt: AppCompatTextView?, selected: Boolean) {
        if (selected) {
            txt?.background = activity?.resources?.getDrawable(R.drawable.sp_eclips_blue_filled_40)
            activity?.let { ContextCompat.getColor(it, R.color.white) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        } else {
            txt?.background =
                activity?.resources?.getDrawable(R.drawable.sp_eclips_blue_border_40dp)
            activity?.let { ContextCompat.getColor(it, R.color.theme) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        }
    }

    private fun setNoActive(txt: AppCompatTextView?, selected: Boolean) {
        if (selected) {
            txt?.background = activity?.resources?.getDrawable(R.drawable.sp_eclips_blue_filled_40)
            activity?.let { ContextCompat.getColor(it, R.color.white) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        } else {
            txt?.background = null
            activity?.let { ContextCompat.getColor(it, R.color.text_black) }?.let {
                txt?.setTextColor(
                    it
                )
            }
        }
    }

    private fun setDataToInfo(response: WorkerDetails?) {

        if (response?.own_bike == 1) {
            setYesActive(txtBikeYes, true)
            setNoActive(txtBikeNo, false)
            mBike = 1
        } else {
            setNoActive(txtBikeNo, true)
            setYesActive(txtBikeYes, false)
            mBike = 0
        }

        if (response?.bike_license == 1) {
            mDL = 1
            setYesActive(txtLicenceYes, true)
            setNoActive(txtLicenceNo, false)

        } else {
            mDL = 0
            setNoActive(txtLicenceNo, true)
            setYesActive(txtLicenceYes, false)
        }

//        if (response?.car_license == 1) {
//            mDL = 1
//            setYesActive(txtLicenceYes, true)
//        } else {
//            mDL = 0
//            setNoActive(txtLicenceNo, true)
//        }

//        if (response?.own_laptop == 1) {
//            mLAPTOP = 1
//            setYesActive(txtLaptopYes, true)
//            setNoActive(txtLaptopNo, false)
//        } else {
//            mLAPTOP = 0
//            setNoActive(txtLaptopNo, true)
//            setYesActive(txtLaptopYes, false)
//        }

        if (response?.own_aadhar_card == 1) {
            mAdhaar = 1
            setYesActive(txtAdhaarYes, true)
            setNoActive(txtAdhaarNo, false)
        } else {
            mAdhaar = 0
            setNoActive(txtAdhaarNo, true)
            setYesActive(txtAdhaarYes, false)
        }
        if (response?.own_pan_card == 1) {
            mPan = 1
            setYesActive(txtPanYes, true)
            setNoActive(txtPanNo, false)
        } else {
            mPan = 0
            setNoActive(txtPanNo, true)
            setYesActive(txtPanYes, false)
        }

//        if (response?.own_smartphone == 1) {
//            mSMARTPHONE = 1
//            setYesActive(txtSmartYes, true)
//            setNoActive(txtSmartNo, false)
//        } else {
//            mSMARTPHONE = 0
//            setNoActive(txtSmartNo, true)
//            setYesActive(txtSmartYes, false)
//        }

        if (response?.own_vehicle_rc == 1) {
            mRC = 1
            setYesActive(txtRCYes, true)
            setNoActive(txtRCNo, false)
        } else {
            mRC = 0
            setNoActive(txtRCNo, true)
            setYesActive(txtRCYes, false)
        }

//        if (response?.own_wifi == 1) {
//            mWifi = 1
//            setYesActive(txtWifiYes, true)
//            setNoActive(txtLicenceNo, false)
//        } else {
//            mWifi = 0
//            setNoActive(txtWifiNo, true)
//            setYesActive(txtWifiYes, false)
//        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModelProfile.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {
                    setDataToInfo(it.response.content.get(0))
                }
            }
        })

        viewModelExperince.responseSaveData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    linearBottomBtns?.visibility = View.GONE
                    txtEdit?.visibility = View.VISIBLE
                    handleView(false)
                }
            }
        })

        viewModelProfile.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelProfile.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelExperince.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelExperince.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private fun handleView(isEnabld: Boolean) {
        txtWifiYes?.isEnabled = isEnabld
        txtWifiNo?.isEnabled = isEnabld
        txtLaptopYes?.isEnabled = isEnabld
        txtLaptopNo?.isEnabled = isEnabld
        txtAdhaarYes?.isEnabled = isEnabld
        txtAdhaarNo?.isEnabled = isEnabld
        txtPanNo?.isEnabled = isEnabld
        txtPanYes?.isEnabled = isEnabld
        txtSmartYes?.isEnabled = isEnabld
        txtSmartNo?.isEnabled = isEnabld
        txtBikeYes?.isEnabled = isEnabld
        txtBikeNo?.isEnabled = isEnabld
        txtRCYes?.isEnabled = isEnabld
        txtRCNo?.isEnabled = isEnabld
        txtLicenceYes?.isEnabled = isEnabld
        txtLicenceNo?.isEnabled = isEnabld
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtEdit -> {
                handleView(true)
                linearBottomBtns?.visibility = View.VISIBLE
                txtEdit?.visibility = View.GONE
            }
            R.id.imgBackIcon -> activity?.onBackPressed()
            R.id.txtSave -> {
                handleSave()
            }

            R.id.txtDiscard -> {
                handleView(false)
                viewModelProfile.getWorkerByUserId(userId)
                linearBottomBtns?.visibility = View.GONE
                txtEdit?.visibility = View.VISIBLE
            }

            R.id.txtWifiYes -> {
                setYesActive(txtWifiYes, true)
                setNoActive(txtWifiNo, false)
                mWifi = 1
            }
            R.id.txtWifiNo -> {
                setYesActive(txtWifiYes, false)
                setNoActive(txtWifiNo, true)
                mWifi = 0
            }
            R.id.txtLaptopYes -> {
                setYesActive(txtLaptopYes, true)
                setNoActive(txtLaptopNo, false)
                mLAPTOP = 1
            }
            R.id.txtLaptopNo -> {
                setYesActive(txtLaptopYes, false)
                setNoActive(txtLaptopNo, true)
                mLAPTOP = 0
            }
            R.id.txtAdhaarYes -> {
                setYesActive(txtAdhaarYes, true)
                setNoActive(txtAdhaarNo, false)
                mAdhaar = 1
            }
            R.id.txtAdhaarNo -> {
                setYesActive(txtAdhaarYes, false)
                setNoActive(txtAdhaarNo, true)
                mAdhaar = 0
            }
            R.id.txtPanYes -> {
                setYesActive(txtPanYes, true)
                setNoActive(txtPanNo, false)
                mPan = 1
            }
            R.id.txtPanNo -> {
                setYesActive(txtPanYes, false)
                setNoActive(txtPanNo, true)
                mPan = 0
            }
            R.id.txtBikeYes -> {
                setYesActive(txtBikeYes, true)
                setNoActive(txtBikeNo, false)
                mBike = 1
            }
            R.id.txtBikeNo -> {
                setYesActive(txtBikeYes, false)
                setNoActive(txtBikeNo, true)
                mBike = 0
            }

            R.id.txtLicenceYes -> {
                setYesActive(txtLicenceYes, true)
                setNoActive(txtLicenceNo, false)
                mDL = 1
            }
            R.id.txtLicenceNo -> {
                setYesActive(txtLicenceYes, false)
                setNoActive(txtLicenceNo, true)
                mDL = 0
            }
            R.id.txtSmartYes -> {
                setYesActive(txtSmartYes, true)
                setNoActive(txtSmartNo, false)
                mSMARTPHONE = 1
            }
            R.id.txtSmartNo -> {
                setYesActive(txtSmartYes, false)
                setNoActive(txtSmartNo, true)
                mSMARTPHONE = 0
            }

            R.id.txtRCYes -> {
                setYesActive(txtRCYes, true)
                setNoActive(txtRCNo, false)
                mRC = 1
            }
            R.id.txtRCNo -> {
                setYesActive(txtRCYes, false)
                setNoActive(txtRCNo, true)
                mRC = 0
            }
        }
    }

    private fun handleSave() {
        val request: UpdateWorkerRequest? = UpdateWorkerRequest(
            requestedBy = userId,
            own_aadhar_card = mAdhaar,
            own_bike = mBike,
            own_laptop = mLAPTOP,
            own_smartphone = mSMARTPHONE,
            own_pan_card = mPan,
            own_vehicle_rc = mRC,
            own_wifi = mWifi,
            bike_license = mDL
        )
        viewModelExperince.saveWorkerData(workerId, request)
    }
}