package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.AppliedJobResponse
import com.okaygo.worker.data.modal.reponse.JobCount
import com.okaygo.worker.data.modal.reponse.OfferAcceptResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class AppliedModel(app: Application) : MyViewModel(app) {
    var responseAppliedJob = MutableLiveData<AppliedJobResponse>()
    var responseDelete = MutableLiveData<SuccessResponse>()
    var response = MutableLiveData<SuccessResponse>()
    var responseInteriewSlot = MutableLiveData<SuccessResponse>()
    var responseNotJoining = MutableLiveData<OfferAcceptResponse>()
    var responseOfferAccpted = MutableLiveData<OfferAcceptResponse>()
    var responseJobCount = MutableLiveData<JobCount>()

    /**
     *
     */
    fun getAppliedJobs(userId: Int?, isLoader: Boolean = true) {
        isLoading.value = isLoader
        AppliedRepository.getAppliedJobs({
            isLoading.value = false
            responseAppliedJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    fun updateNotJoinin(interviewId: Int?, isOfferRejected: Int?, userId: Int?) {
        isLoading.value = true
        AppliedRepository.updateNotJoining({
            isLoading.value = false
            responseNotJoining.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, interviewId, isOfferRejected, userId)
    }

    fun offerAccepted(interviewId: Int?, isOfferAccepted: Int?, userId: Int?) {
        isLoading.value = true
        AppliedRepository.offerAccepted({
            isLoading.value = false
            responseOfferAccpted.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, interviewId, isOfferAccepted, userId)
    }

    fun deleteJob(assignId: Int?) {
        isLoading.value = true
        AppliedRepository.deleteJob({
            isLoading.value = false
            responseDelete.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, assignId)
    }

    fun getInterviewDetail(detailId: Int?, usrId: Int?) {
        isLoading.value = true
        AppliedRepository.getInterviewDetail({
            isLoading.value = false
            response.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, detailId, usrId
        )
    }

    fun getInterviewTimeSlot(detailId: Int?, usrId: Int?) {
        isLoading.value = true
        AppliedRepository.getInterviewTimeSlot({
            isLoading.value = false
            responseInteriewSlot.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, detailId, usrId
        )
    }

    fun getJobCounts(usrId: Int?) {
        isLoading.value = false
        AppliedRepository.getJobCounts({
            isLoading.value = false
            responseJobCount.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, usrId
        )
    }

}