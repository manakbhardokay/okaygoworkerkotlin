package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.AddExpResponse
import com.okaygo.worker.data.modal.reponse.AddJobTypeResponse
import com.okaygo.worker.data.modal.reponse.JobTypeResponse
import com.okaygo.worker.data.modal.request.AddExpRequest
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class AddExpModel(app: Application) : MyViewModel(app) {
    var responseIntrestedJobType = MutableLiveData<JobTypeResponse>()
    var responseJobTypeInd = MutableLiveData<JobTypeResponse>()
    var responseAddJobType = MutableLiveData<AddJobTypeResponse>()
    var responseAddExp = MutableLiveData<AddExpResponse>()

    /**
     * get all industries
     */
    fun getInterestedJobType() {
        isLoading.value = false
        AddExpRepository.getInterestedJobType({
            isLoading.value = false
            responseIntrestedJobType.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        })
    }

    /**
     * get job types for particular industries
     */
    fun getJobTypeForInd(inquery: String?) {
        isLoading.value = false
        AddExpRepository.getJobTypeForInd({
            isLoading.value = false
            responseJobTypeInd.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, inquery)
    }

    /**
     * add job type to server if not it is other
     */
    fun addTypeToConfigMaster(jobType: String?, userId: Int?) {
        isLoading.value = false
        AddExpRepository.addTypeToConfigMaster({
            isLoading.value = false
            responseAddJobType.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, jobType, userId)
    }

    /**
     * add indnstries if it is other
     */
    fun addIndToConfigMaster(industry: String?, typeId: String?, userId: Int?) {
        isLoading.value = false
        AddExpRepository.addIndToConfigMaster({
            isLoading.value = false
            responseAddJobType.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, industry, typeId, userId)
    }

    /**
     * add worker exp
     */
    fun addWorkExp(req: AddExpRequest?) {
        isLoading.value = true
        AddExpRepository.addWorkExp({
            isLoading.value = false
            responseAddExp.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, req)
    }
}