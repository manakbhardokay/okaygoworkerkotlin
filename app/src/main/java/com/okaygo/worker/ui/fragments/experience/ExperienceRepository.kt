package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.AllJobRoleResponse
import com.okaygo.worker.data.modal.reponse.WorkerDetailResponse
import com.okaygo.worker.data.modal.request.UpdateWorkerRequest
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object ExperienceRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun saveWorkerData(
        successHandler: (WorkerDetailResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?, request: UpdateWorkerRequest?
    ) {
        mService.saveWorkerData(workerId, request)
            .enqueue(object : Callback<WorkerDetailResponse> {
                override fun onResponse(
                    call: Call<WorkerDetailResponse>?,
                    response: Response<WorkerDetailResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<WorkerDetailResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("worker save failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getAllJobRole(
        successHandler: (AllJobRoleResponse) -> Unit,
        failureHandler: (String) -> Unit
    ) {
        mService.getAllJobRole()
            .enqueue(object : Callback<AllJobRoleResponse> {
                override fun onResponse(
                    call: Call<AllJobRoleResponse>?,
                    response: Response<AllJobRoleResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AllJobRoleResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("worker save failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })

    }
}