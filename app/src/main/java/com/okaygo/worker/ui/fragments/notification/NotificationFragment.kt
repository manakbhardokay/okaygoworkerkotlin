package com.okaygo.worker.ui.fragments.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayoutMediator
import com.okaygo.worker.R
import com.okaygo.worker.adapters.ViewPagerFragmentAdapter
import com.okaygo.worker.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgBack?.setOnClickListener { activity?.onBackPressed() }
        viewPager?.setAdapter(activity?.let { ViewPagerFragmentAdapter(it) })
        TabLayoutMediator(tabs, viewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                when (position) {
                    0 -> {
                        tab.text = "Notifications"
                    }
                    1 -> {
                        tab.text = "Alert"
                    }
                }
            }).attach()
    }
}