package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.UpiRespnse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object PaymentDetailRepository {
    private val mService = ApiHelper.getService()


    /**
     *get refer jobs data from server
     */
    fun getUPI(
        successHandler: (UpiRespnse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?
    ) {
        mService.getUPI(workerId)?.enqueue(object : Callback<UpiRespnse> {
            override fun onResponse(
                call: Call<UpiRespnse>?,
                response: Response<UpiRespnse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<UpiRespnse>?, t: Throwable?) {
                t?.let {
                    Log.e("referral failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *get refer worker list from server
     */
    fun addUpi(
        successHandler: (UpiRespnse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?,
        upi: String?
    ) {
        mService.updateUPI(upi, workerId)
            ?.enqueue(object : Callback<UpiRespnse> {
                override fun onResponse(
                    call: Call<UpiRespnse>?,
                    response: Response<UpiRespnse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<UpiRespnse>?, t: Throwable?) {
                    t?.let {
                        Log.e("upi failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }
}