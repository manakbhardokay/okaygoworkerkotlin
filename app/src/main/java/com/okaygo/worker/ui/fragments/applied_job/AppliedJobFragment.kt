package com.okaygo.worker.ui.fragments.applied_job

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ScreenUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.okaygo.worker.R
import com.okaygo.worker.adapters.MyJobAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.AppliedJob
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.NavigationActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.ui.fragments.verification.AppliedModel
import com.openkey.guest.ui.fragments.verification.JobDetailModel
import kotlinx.android.synthetic.main.bottom_sheet_interview_slot.*
import kotlinx.android.synthetic.main.fragment_applied_job.*

class AppliedJobFragment(val tabs: TabLayout?) : BaseFragment() {
    private lateinit var viewModel: AppliedModel
    private lateinit var viewModelJobDetail: JobDetailModel
    private var mAdapter: MyJobAdapter? = null
    private var mJobList: ArrayList<AppliedJob>? = null
    var mInterviewId = 0
    var isDialogFor = 0
    var isOfferRejected = 1
    private var mSlotsBottomSheet: BottomSheetDialog? = null
    private var mCurrentSlot = 0
    private var mJobType: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AppliedModel::class.java)
        viewModelJobDetail = ViewModelProvider(this).get(JobDetailModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_applied_job, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefresh?.setOnRefreshListener {
            viewModel.getAppliedJobs(userId)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAppliedJobs(userId)
    }

    private fun setAdapter() {
        val linearLayoutManager = LinearLayoutManager(context)
        mAdapter = MyJobAdapter(
            activity, 1, mJobList, onInterviewTimeClick, onJobDetailClick,
            onJobRemoveClick, onInterviewDetailClick, onJoiningDetailClick, onEnterOtpClick,
            onAddressClick, onYesClick, onNoClick, onDirectionClick, onConstraintClick
        )
        recylerAppliedJob?.layoutManager = linearLayoutManager
        recylerAppliedJob?.adapter = mAdapter
    }

    private val onConstraintClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }
    private val onDirectionClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }
    private val onNoClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        isDialogFor = 1
        mInterviewId = it?.interviewId ?: 0
        Dialogs.alertDialog(
            activity,
            resources?.getString(R.string.want_reject_offer),
            "Yes",
            "No",
            onPostiveBtnClick
        )
    }

    private val onPostiveBtnClick: () -> Unit = {
//        okayGoFirebaseAnalytics.interview_cancel(content.get(getAdapterPosition()).getJobId())
        if (isDialogFor == 1) {
            viewModel.updateNotJoinin(mInterviewId, isOfferRejected, userId)
        } else if (isDialogFor == 2) {
            viewModel.offerAccepted(mInterviewId, 1, userId)
        }
    }
    private val onYesClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        isDialogFor = 2
        mInterviewId = it?.interviewId ?: 0
        mJobType = it?.jobType
        Dialogs.alertDialog(
            activity,
            resources.getString(R.string.want_accept_offer),
            "Yes",
            "No",
            onPostiveBtnClick
        )

    }
    private val onAddressClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        val lat = it?.locationLat
        val lng = it?.locationLong
        activity?.let {
            if (ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(context, "Need location permission.", Toast.LENGTH_SHORT).show()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                        201
                    )
                } else {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ),
                        202
                    )
                }
            } else {
                Utilities.openAddressOnMap(
                    it,
                    lat + "," + lng
                )
            }
        }
    }

    private val onEnterOtpClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
    }
    private val onJoiningDetailClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        interviewJoiningDetail(false, it)
    }
    private val onInterviewTimeClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        viewModel.getInterviewTimeSlot(it?.jobDetailId, userId)
    }
    private val onInterviewDetailClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        interviewJoiningDetail(true, it)
    }
    private val onJobDetailClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        val intent: Intent? = Intent(activity, NavigationActivity::class.java)
        intent?.putExtra(Constants.NAV_SCREEN, 10)
        intent?.putExtra(Constants.JOB_DETAIL_ID, it?.jobId?.toString())
        startActivity(intent)
    }
    private val onJobRemoveClick: (AppliedJob?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        viewModel.deleteJob(it?.assignId)
    }

    private val onOKClick: () -> Unit = {
    }


    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseAppliedJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    swipeRefresh?.isRefreshing = false
                    if (it.response?.content?.isNotEmpty() == true) {
                        recylerAppliedJob?.setVisibility(View.VISIBLE)
                        imgNoData?.setVisibility(View.GONE)
                        txtWarning?.setVisibility(View.GONE)
                        if (mJobList == null) {
                            mJobList = ArrayList()
                        } else {
                            mJobList?.clear()
                        }
                        mJobList?.addAll(it.response.content)
                        tabs?.getTabAt(0)?.text = "Applied (${mJobList?.size})"
                        setAdapter()
                        mAdapter?.notifyDataSetChanged()
                    } else {
                        tabs?.getTabAt(0)?.text = "Applied"
                        imgNoData?.setVisibility(View.VISIBLE)
                        imgNoData?.setImageResource(R.drawable.ic_no_records)
                        txtWarning?.setText("No jobs found")
                        txtWarning?.setVisibility(View.VISIBLE)
                        recylerAppliedJob?.setVisibility(View.GONE)
                    }
                }
            }
        })

        viewModelJobDetail.responseJobDetail.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                }
            }
        })
        viewModel.responseInteriewSlot.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    showInterviewBottomSheet()
                }
            }
        })

        viewModel.responseNotJoining.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
//                    OkayGoFirebaseAnalytics.on_reject_job_offer()
                    Utilities.showToast(context, "Job Rejected.")
                    viewModel.getAppliedJobs(userId, false)
//                    UpdateTabLabel()
                }
            }
        })

        viewModel.responseDelete.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.getAppliedJobs(userId, false)
                }
            }
        })

        viewModel.responseOfferAccpted.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    val salary: Int =
                        it?.response?.content?.get(0)?.job_offered_amount?.toInt() ?: 0
                    OkayGoFirebaseAnalytics.on_accept_job_offer(
                        it?.response?.content?.get(0)?.jobDetailsId?.toString(),
                        mJobType,
                        "screen",
                        (salary * 0.36).toInt()
                    )
                    Utilities.showSuccessToast(activity, "Job Accepted.")
                    viewModel.getAppliedJobs(userId)
                    Utilities.jobSuccessDialog(
                        context as Activity?,
                        context?.resources?.getString(R.string.job_conf),
                        context?.resources?.getString(R.string.applied_confrmd_msg),
                        false, onOKClick
                    )

//                    UpdateTabLabel()
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    swipeRefresh?.isRefreshing = false
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelJobDetail.apiError.observe(this, Observer {
            if (isAdded) {
                swipeRefresh?.isRefreshing = false
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModelJobDetail.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private fun interviewJoiningDetail(
        isForInterview: Boolean, response: AppliedJob?
    ): BottomSheetDialog? {
        val view = (context as Activity).layoutInflater
            .inflate(R.layout.bottom_sheet_interview_joining_detail, null)
        val bottomSheetDialog = BottomSheetDialog(context as Activity)
        bottomSheetDialog.setCancelable(false)
        val txtTitle: AppCompatTextView = view.findViewById(R.id.txtTitle)
        val txtTagLine: AppCompatTextView = view.findViewById(R.id.txtTagLine)
        val txtInstructionTitle: AppCompatTextView = view.findViewById(R.id.txtInstructionTitle)
        val txtProcessTitle: AppCompatTextView = view.findViewById(R.id.txtProcessTitle)
        val txtPersonTitle: AppCompatTextView = view.findViewById(R.id.txtPersonContact)
        val txtInterviewMode: AppCompatTextView = view.findViewById(R.id.txtInterviewMode)
        val txtInterviewModeValue: AppCompatTextView = view.findViewById(R.id.txtInterviewModeValue)
        val txtContactPerson: AppCompatTextView = view.findViewById(R.id.txtContactName)
        val imgClose: AppCompatImageView = view.findViewById(R.id.imgCross)
        val txtInstruction: AppCompatTextView = view.findViewById(R.id.txtInstructions)
        val txtProcess: AppCompatTextView = view.findViewById(R.id.txtProcess)
        if (isForInterview) {
            txtProcessTitle.visibility = View.VISIBLE
            txtProcess.visibility = View.VISIBLE
            txtInstruction.visibility = View.VISIBLE
            txtInstructionTitle.visibility = View.VISIBLE
            txtTitle.text = "Interview Details"
            txtTagLine.text =
                "Please read the following instructions carefully before the interview."
            txtInstructionTitle.text = "Interview Instructions"
        } else {
            txtTitle.text = "Joining Details"
            txtTagLine.text =
                "Please read the following instructions carefully before the joining day."
            txtInstructionTitle.text = "Joining Instructions"
            txtProcessTitle.visibility = View.GONE
            txtProcess.visibility = View.GONE
            txtInstruction.visibility = View.VISIBLE
            txtInstructionTitle.visibility = View.VISIBLE
        }
        if (response?.contactCandidateName?.isEmpty() == true) {
            txtContactPerson.visibility = View.GONE
            txtPersonTitle.visibility = View.GONE
        } else {
            txtContactPerson.visibility = View.VISIBLE
            txtPersonTitle.visibility = View.VISIBLE
            txtContactPerson.setText(response?.contactCandidateName)
        }
        if (response?.joiningSpecialRequirement?.isEmpty() == false && !isForInterview) {
            txtInstruction.visibility = View.VISIBLE
            txtInstructionTitle.visibility = View.VISIBLE
            //            txtInstruction.setContent(response.getJoiningSpecialRequirement());
            Utilities.readMoreOption(activity)
                ?.addReadMoreTo(txtInstruction, response?.joiningSpecialRequirement)
        } else {
            txtInstruction.visibility = View.GONE
            txtInstructionTitle.visibility = View.GONE
        }
        if (response?.interviewProcess?.isEmpty() == false && isForInterview) {

            txtProcessTitle.visibility = View.VISIBLE
            Utilities.readMoreOption(activity)
                ?.addReadMoreTo(txtProcess, response?.interviewProcess)
        } else {
            txtProcessTitle.visibility = View.GONE
            txtProcess.visibility = View.GONE
        }
        if (response?.instructionsCandidate?.isEmpty() == false && isForInterview) {
            txtInstruction.visibility = View.VISIBLE
            txtInstructionTitle.visibility = View.VISIBLE
            Utilities.readMoreOption(activity)
                ?.addReadMoreTo(txtInstruction, response?.instructionsCandidate)
        } else {
            txtInstruction.visibility = View.GONE
            txtInstructionTitle.visibility = View.GONE
        }
        if (response?.interview_mode ?: 0 > 0 && isForInterview) {
            txtInterviewModeValue.visibility = View.VISIBLE
            txtInterviewMode.visibility = View.VISIBLE
            txtInterviewModeValue.setText(
                Utilities.getInterviewMode(response?.interview_mode ?: 0)
            )
        } else {
            txtInterviewModeValue.visibility = View.GONE
            txtInterviewMode.visibility = View.GONE
        }
        imgClose.setOnClickListener { bottomSheetDialog.dismiss() }
        val heightInPixels = ScreenUtils.getScreenHeight() / 1
        val params =
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
        bottomSheetDialog.setContentView(view, params)
        bottomSheetDialog.show()
        return bottomSheetDialog
    }

    fun showInterviewBottomSheet() {
        val view: View = (context as Activity).layoutInflater
            .inflate(R.layout.bottom_sheet_interview_slot, null)
        mSlotsBottomSheet = BottomSheetDialog(context as Activity)
//        val close = view1.findViewById<ImageView>(R.id.close)
//        val  txtSlotOne? = view1.findViewById<TextView>(R.id. txtSlotOne?)
//        val  txtSlotTwo? = view1.findViewById<TextView>(R.id. txtSlotTwo?)
//        val  txtSlotThree? = view1.findViewById<TextView>(R.id. txtSlotThree?)
//        val confirm = view1.findViewById<TextView>(R.id.confirm)
        txtConfirm?.setOnClickListener {
            if (mCurrentSlot != 0) {
//                OkayGoFirebaseAnalytics.on_interview_select_slot(
//                    content.get(pos).getJobDetailId(), "screen"
//                )
                Toast.makeText(context, "Slot selected.$mCurrentSlot", Toast.LENGTH_SHORT)
                    .show()
//                selectTheInterviewSlot(
//                    interviewSlotMasterGS.getInterviewId().toString() + "",
//                    mCurrentSlot.toString() + ""
//                )
            } else {
            }
        }
        txtSlotOne?.setOnClickListener {
            if (mCurrentSlot != 1) {
                txtSlotOne?.setBackgroundResource(R.drawable.sp_rounded_blue_light_fill_16dp)
                txtSlotTwo?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotThree?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                mCurrentSlot = 1
            } else if (mCurrentSlot == 1) {
                txtSlotOne?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotTwo?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotThree?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                mCurrentSlot = 0
            }
        }
        txtSlotTwo?.setOnClickListener {
            if (mCurrentSlot != 2) {
                txtSlotTwo?.setBackgroundResource(R.drawable.sp_rounded_blue_light_fill_16dp)
                txtSlotOne?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotThree?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                mCurrentSlot = 2
            } else if (mCurrentSlot == 2) {
                txtSlotOne?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotTwo?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotThree?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                mCurrentSlot = 0
            }
        }
        txtSlotThree?.setOnClickListener {
            if (mCurrentSlot != 3) {
                txtSlotThree?.setBackgroundResource(R.drawable.sp_rounded_blue_light_fill_16dp)
                txtSlotTwo?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotOne?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                mCurrentSlot = 3
            } else if (mCurrentSlot == 3) {
                txtSlotOne?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotTwo?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                txtSlotThree?.setBackgroundResource(R.drawable.sp_border_rounded_blue_16dp)
                mCurrentSlot = 0
            }
        }
//        if (interviewSlotMasterGS.getInterviewStartTime() != null && interviewSlotMasterGS.getInterviewEndTime() != null) {
//            var s1 = ""
//            s1 = s1 + General.getDateInGoodFormat(
//                interviewSlotMasterGS.getInterviewStartTime().split(" ").get(0)
//            ).toString() + "\n\n"
//            s1 = s1 + General.ConvertToAmPm(
//                interviewSlotMasterGS.getInterviewStartTime().split(" ").get(1)
//            )
//            s1 = "$s1 - " + General.ConvertToAmPm(
//                interviewSlotMasterGS.getInterviewEndTime().split(" ").get(1)
//            )
//             txtSlotOne?.text = s1
//        } else {
//             txtSlotOne?.visibility = View.GONE
//        }
//        if (interviewSlotMasterGS.getInterviewStartTime2() != null && interviewSlotMasterGS.getInterviewEndTime2() != null) {
//            var s1 = ""
//            s1 = s1 + General.getDateInGoodFormat(
//                interviewSlotMasterGS.getInterviewStartTime2().split(" ").get(0)
//            ).toString() + "\n\n"
//            s1 = s1 + General.ConvertToAmPm(
//                interviewSlotMasterGS.getInterviewStartTime2().split(" ").get(1)
//            )
//            s1 = "$s1 - " + General.ConvertToAmPm(
//                interviewSlotMasterGS.getInterviewEndTime2().split(" ").get(1)
//            )
//             txtSlotTwo?.text = s1
//        } else {
//             txtSlotTwo?.visibility = View.GONE
//        }
//        if (interviewSlotMasterGS.getInterviewStartTime3() != null && interviewSlotMasterGS.getInterviewEndTime3() != null) {
//            var s1 = ""
//            s1 = s1 + General.getDateInGoodFormat(
//                interviewSlotMasterGS.getInterviewStartTime3().split(" ").get(0)
//            ).toString() + "\n\n"
//            s1 = s1 + General.ConvertToAmPm(
//                interviewSlotMasterGS.getInterviewStartTime3().split(" ").get(1)
//            )
//            s1 = "$s1 - " + General.ConvertToAmPm(
//                interviewSlotMasterGS.getInterviewEndTime3().split(" ").get(1)
//            )
//             txtSlotThree?.text = s1
//        } else {
//             txtSlotThree?.visibility = View.GONE
//        }
        imgClose.setOnClickListener { mSlotsBottomSheet?.dismiss() }
        val heightInPixels = (ScreenUtils.getScreenHeight() / 2.5f).toInt()
        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
        mSlotsBottomSheet?.setContentView(view, params)
        mSlotsBottomSheet?.show()
    }

}