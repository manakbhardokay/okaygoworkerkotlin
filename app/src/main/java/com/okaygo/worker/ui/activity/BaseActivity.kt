package com.okaygo.worker.ui.activity

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.okaygo.worker.R
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences

/**
 * @author Davinder Goel.
 */
open class BaseActivity : AppCompatActivity() {
    protected var userId: Int? = null
    protected var workerId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        userId = Preferences.prefs?.getInt(Constants.ID, 0)
        workerId = Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0)
    }

    /**
     * replace fragments
     */
    fun replaceFragment(fragment: BaseFragment, addBackStack: Boolean) {
        val tag: String = fragment::class.java.simpleName
        val supportFragmentManager = supportFragmentManager
        val transaction: FragmentTransaction? = supportFragmentManager?.beginTransaction()
        if (addBackStack) {
//            transaction?.setCustomAnimations(R.anim.anim_in, R.anim.anim_out, R.anim.anim_in_reverse, R.anim.anim_out_reverse)
            transaction?.addToBackStack(tag)
        }
        transaction?.replace(R.id.container, fragment, tag)?.commitAllowingStateLoss()
    }

    //    //replace fragment without animation
//    fun replaceFragmentWithOutAnim(fragment: BaseFragment, addBackStack: Boolean) {
//        val tag: String = fragment::class.java.simpleName
//        val supportFragmentManager = supportFragmentManager
//        val transaction: FragmentTransaction? = supportFragmentManager?.beginTransaction()
//        if (addBackStack) {
//            transaction?.addToBackStack(tag)
//        }
//        transaction?.replace(R.id.container, fragment, tag)?.commitAllowingStateLoss()
//    }
//
    //add fragments
    fun addFragment(fragment: BaseFragment, addBackStack: Boolean) {
        val tag: String = fragment::class.java.simpleName
        val supportFragmentManager = supportFragmentManager
        val transaction: FragmentTransaction? = supportFragmentManager?.beginTransaction()
        if (addBackStack) {
//            transaction?.setCustomAnimations(R.anim.anim_in, R.anim.anim_out, R.anim.anim_in_reverse, R.anim.anim_out_reverse)
            transaction?.addToBackStack(tag)
        }
        transaction?.add(R.id.container, fragment, tag)?.commitAllowingStateLoss()
    }
//
//    fun startActivityWithAnimation(activity: Class<out BaseActivity>, bundle: Bundle?) {
//
//        val intent = Intent(this, activity)
//        if (bundle != null) {
//            intent.putExtra(Constants.INTENT_EXTRAS, bundle)
//        }
//        startActivity(intent)
//        overridePendingTransition(R.anim.anim_in, R.anim.anim_out)
//    }
}
