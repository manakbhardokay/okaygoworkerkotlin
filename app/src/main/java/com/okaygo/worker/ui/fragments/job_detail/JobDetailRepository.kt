package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.JobDetailResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object JobDetailRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun getJobDetail(
        successHandler: (JobDetailResponse) -> Unit,
        failureHandler: (String) -> Unit,
        jobId: Int?
    ) {
        mService.getJobDetail(jobId)?.enqueue(object : Callback<JobDetailResponse> {
            override fun onResponse(
                call: Call<JobDetailResponse>?,
                response: Response<JobDetailResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<JobDetailResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("addUserRole failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}