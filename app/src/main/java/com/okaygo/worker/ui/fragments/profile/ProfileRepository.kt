package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.GetCurrentEduResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.okaygo.worker.data.modal.reponse.UserDetailResponse
import com.okaygo.worker.data.modal.reponse.WorkerDetailResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object ProfileRepository {
    private val mService = ApiHelper.getService()


    /**
     *
     */
    fun getWorkerByUserId(
        successHandler: (WorkerDetailResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getWorkerByUserId(userId).enqueue(object : Callback<WorkerDetailResponse> {
            override fun onResponse(
                call: Call<WorkerDetailResponse>?,
                response: Response<WorkerDetailResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<WorkerDetailResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("session failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun updateLang(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        langId: Int?,
        userId: Int?
    ) {
        mService.updateLanguage(langId, userId)?.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(
                call: Call<SuccessResponse>?,
                response: Response<SuccessResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("lang failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun updateStatus(
        successHandler: (UserDetailResponse) -> Unit,
        failureHandler: (String) -> Unit,
        about_me: String?,
        userId: Int?
    ) {
        val map: HashMap<String, String>? = HashMap()
        map?.put("about_me", about_me ?: "")
        map?.put("user_id", userId?.toString() ?: "")
        mService.updateStatus(map)?.enqueue(object : Callback<UserDetailResponse> {
            override fun onResponse(
                call: Call<UserDetailResponse>?,
                response: Response<UserDetailResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<UserDetailResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("lang failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun getCurrentEdu(
        successHandler: (GetCurrentEduResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?
    ) {
        mService.getCurrentEdu(workerId)?.enqueue(object : Callback<GetCurrentEduResponse> {
            override fun onResponse(
                call: Call<GetCurrentEduResponse>?,
                response: Response<GetCurrentEduResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<GetCurrentEduResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("getCurrentEdu failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun uploadProfilePic(
        successHandler: (WorkerDetailResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        path: String?
    ) {
        val map: HashMap<String, String>? = HashMap()
        map?.put("user_id", userId?.toString() ?: "")
        map?.put("profile_photo", path ?: "")

        mService.uploadProfilePic(map)?.enqueue(object : Callback<WorkerDetailResponse> {
            override fun onResponse(
                call: Call<WorkerDetailResponse>?,
                response: Response<WorkerDetailResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<WorkerDetailResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("profile pic failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}