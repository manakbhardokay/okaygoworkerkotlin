package com.okaygo.worker.ui.fragments.earning

import android.graphics.drawable.StateListDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.google.android.material.tabs.TabLayoutMediator
import com.okaygo.worker.R
import com.okaygo.worker.adapters.EarningTabPagerAdapter
import com.okaygo.worker.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_earning_sub_tab.*

class EarningSubTabFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_earning_sub_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager?.setAdapter(activity?.let { EarningTabPagerAdapter(it) })
        TabLayoutMediator(tabs, viewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                when (position) {
                    0 -> {
                        tab.text = "All"
                    }
                    1 -> {
                        tab.text = "Pending"
                    }
                    2 -> {
                        tab.text = "Paid"
                    }
                }
            }).attach()

        val tabCount: Int? = tabs?.tabCount

        for (i in 0 until tabCount!!) {
            val tabView: View = (tabs?.getChildAt(0) as ViewGroup).getChildAt(i)
            val p: ViewGroup.MarginLayoutParams? =
                tabView.getLayoutParams() as ViewGroup.MarginLayoutParams
            p?.setMargins(32, 12, 32, 10)
            tabView.requestLayout()
            ViewCompat.setBackground(tabView, setImageButtonStateNew())
//            ViewCompat.setPaddingRelative(
//                tabView,
//                tabView.paddingStart,
//                tabView.paddingTop,
//                tabView.paddingEnd,
//                tabView.paddingBottom
//            );
        }
    }

    fun setImageButtonStateNew(): StateListDrawable {
        val states = StateListDrawable()
        context?.let {
            states.addState(
                intArrayOf(android.R.attr.state_selected),
                ContextCompat.getDrawable(it, R.drawable.sp_selected_tab)
            )
            states.addState(
                intArrayOf(-android.R.attr.state_selected),
                ContextCompat.getDrawable(it, R.drawable.sp_non_selected_tab)
            )
        }
        return states
    }
}