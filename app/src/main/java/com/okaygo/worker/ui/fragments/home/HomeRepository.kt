package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.api.response.FindJobCategories
import com.okaygo.worker.data.modal.request.JobCategoryRequest
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object HomeRepository {
    private val mService = ApiHelper.getService()


    fun getJobCategories(
        successHandler: (FindJobCategories) -> Unit,
        failureHandler: (String) -> Unit,
        req: JobCategoryRequest?
    ) {
        mService.getJobCategories(
            req?.user_id,
            req?.filter_od,
            req?.filter_pt,
            req?.filter_ft,
            req?.max_salary,
            req?.min_salary,
            req?.job_types,
            req?.jobs_after,
            req?.city,
            req?.experience,
            req?.jobs_before
        )?.enqueue(object : Callback<FindJobCategories> {
            override fun onResponse(
                call: Call<FindJobCategories>?,
                response: Response<FindJobCategories>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<FindJobCategories>?, t: Throwable?) {
                t?.let {
                    Log.e("job category  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}