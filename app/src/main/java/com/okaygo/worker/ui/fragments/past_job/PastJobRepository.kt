package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.AppliedJobResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object PastJobRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun getPastJobs(
        successHandler: (AppliedJobResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getPastJobs(userId, 500)
            ?.enqueue(object : Callback<AppliedJobResponse> {
                override fun onResponse(
                    call: Call<AppliedJobResponse>?,
                    response: Response<AppliedJobResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AppliedJobResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("applied failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

}