package com.okaygo.worker.ui.fragments.profile

import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler
import com.freshchat.consumer.sdk.Freshchat
import com.google.firebase.dynamiclinks.DynamicLink.AndroidParameters
import com.google.firebase.dynamiclinks.DynamicLink.GoogleAnalyticsParameters
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.okaygo.worker.R
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.data.modal.reponse.WorkerDetailResponse
import com.okaygo.worker.help.LanguageHelper
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.NavigationActivity
import com.okaygo.worker.ui.activity.login_onboarding.LoginOnBoardingActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.help.Preferences
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import com.openkey.guest.ui.fragments.verification.ProfileModel
import kotlinx.android.synthetic.main.fragment_help.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File
import java.util.*

class ProfileFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: ProfileModel
    private lateinit var viewModelOnBoarding: OnBoardingModel
    private var mCurrentSalary: Int? = null
    private val FILE_CHOOSER = 1212
    private val REQUEST_IMAGE_CAPTURE = 1211
    private var cvPath: String? = null
    private var isStatusEdit = false
    private var imageBitmap: Bitmap? = null
    private var isForProfilePic: Boolean? = false
    private var mCvLink: String? = null
    private var mFileExtension: String? = null
    private var mCvFileName: String? = null
    private var mCvFileNameUpload: String? = null
    private var message: String? = null
    private var mLangList: ArrayList<String>? = null
    private var mLang: String? = null
    private var isDialogForDownload = false

    private var mMimeType = ""
    private val detailsHandler: GetDetailsHandler = object : GetDetailsHandler {
        override fun onSuccess(cognitoUserDetails: CognitoUserDetails?) {
            // Store details in the AppHandler
            AuthHelper.setUserDetails(cognitoUserDetails)
        }

        override fun onFailure(exception: Exception?) {
            showDialogMessage(
                "Could not fetch user details!",
                AuthHelper.formatException(exception),
                false
            )
            signOut()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModelOnBoarding = ViewModelProvider(this).get(OnBoardingModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtFileName?.visibility = View.VISIBLE
        Constants.BOTTOM_MENU_ITEM_SELECTED = 5
//        init()
        setListeners()
        setSpnLangAdapter()
    }

    private fun init() {
        if (userId != null && AuthHelper.getPool() != null) {
            val user: CognitoUser? = AuthHelper.getPool().getUser(userId?.toString())
            user?.getDetailsInBackground(detailsHandler)
        }
    }


    override fun onResume() {
        super.onResume()
        viewModel.getWorkerByUserId(userId)
    }

    private fun setListeners() {
        txtShareApp?.setOnClickListener(this)
        txtUpdateCv?.setOnClickListener(this)
        txtUploadCv?.setOnClickListener(this)
        imgEditImage?.setOnClickListener(this)
        imgStatusEdit?.setOnClickListener(this)
        txtSkills?.setOnClickListener(this)
        txtReferral?.setOnClickListener(this)
        txtPersonalDetails?.setOnClickListener(this)
        txtEducation?.setOnClickListener(this)
        txtExperience?.setOnClickListener(this)
        txtJobPrefrence?.setOnClickListener(this)
        txtChangePassword?.setOnClickListener(this)
        txtLogout?.setOnClickListener(this)
        txtTermCondition?.setOnClickListener(this)
        txtPolicy?.setOnClickListener(this)
        txtFileName?.setOnClickListener(this)
        txtPayment?.setOnClickListener(this)
        txtRatingReview?.setOnClickListener(this)
        txtDocuments?.setOnClickListener(this)
        txtFileName?.setOnClickListener(this)
    }

    private fun setSpnLangAdapter() {
        mLangList = ArrayList()
        mLangList?.add("English")
        mLangList?.add("Hinglish")
        val spnAdapter: ArrayAdapter<String>? =
            activity?.let { ArrayAdapter(it, R.layout.item_lang_spinner, mLangList!!) }
        spnAdapter?.setDropDownViewResource(R.layout.item_lang_spinner)
        spnLang.adapter = spnAdapter
        if (Preferences.prefs?.getString(Constants.SELECTED_LANG, "en").equals("en")) {
            spnLang.setSelection(0)
        } else {
            spnLang.setSelection(1)
        }
        spnLang.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                var langId = 1
                if (parent.getItemAtPosition(position).toString() == "English") {
                    langId = 2
                }
                mLang = parent.getItemAtPosition(position).toString()
                viewModel.updateLang(langId, userId)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setProfileData(response: WorkerDetailResponse?) {
        if (response?.response?.content?.isEmpty() == true) {
            return
        }
        Constants.USER_UPI_ID = response?.response?.content?.get(0)?.upiId
        Constants.LAST_UPDATED_SALARY = response?.response?.content?.get(0)?.last_salary
        Constants.NOTICE_PERIOD = response?.response?.content?.get(0)?.notice_period
        Constants.TOTAL_EXP = response?.response?.content?.get(0)?.total_experience

        Preferences.prefs?.saveValue(
            Constants.LAST_SALARY,
            response?.response?.content?.get(0)?.last_salary
        )
        if (response?.response?.content?.get(0)?.averageRating ?: 0.0 == 0.0) {
            txtRating?.text = "New"
        } else {
            txtRating?.text = response?.response?.content?.get(0)?.averageRating?.toString()
        }

        if (response?.response?.content?.get(0)?.profilePhoto != null) {
            Utilities.setImageByGlideRounded(
                activity,
                response?.response?.content?.get(0)?.profilePhoto,
                imgProfile
            )
            imgEditImage?.setImageResource(R.drawable.ic_edit_blue)
        } else {
            imgEditImage?.setImageResource(R.drawable.ic_add)
        }
        txtWorkerName?.text =
            response?.response?.content?.get(0)?.firstName + " " + response?.response?.content?.get(
                0
            )?.lastName

        txtMobile?.text = response?.response?.content?.get(0)?.phoneNumber

        mCurrentSalary = response?.response?.content?.get(0)?.last_salary
        if (response?.response?.content?.get(0)?.aboutMe == null) {
            edtStatus?.setText("Add your status.")
        } else {
            edtStatus?.setText(response.response.content.get(0).aboutMe)
        }
        if (response?.response?.content?.get(0)?.cv_link != null) {
            mCvLink = response.response.content.get(0).cv_link
        } else {
            mCvLink = null
            txtUpdateCv?.setVisibility(View.GONE)
            txtUploadCv?.setVisibility(View.VISIBLE)
            txtFileName?.setVisibility(View.GONE)

        }

        if (response?.response?.content?.get(0)?.emailId?.isEmpty() == false) {
            txtEmail?.text = response.response.content.get(0).emailId
            txtEmail?.visibility = View.VISIBLE
        } else {
            txtEmail?.visibility = View.GONE
        }

        mCvFileName = response?.response?.content?.get(0)?.cv_file_name
        if (mCvFileName != null && mCvFileName?.isEmpty() == false) {
            if (mCvFileName?.contentEquals(".") == true) {
                mFileExtension = mCvFileName?.substring(mCvFileName?.lastIndexOf(".") ?: 0)
            }
            txtFileName?.text = mCvFileName + getString(R.string.cv_download_click)
            txtFileName?.setVisibility(View.VISIBLE)
            txtUploadCv?.setVisibility(View.GONE)
            txtUpdateCv?.setVisibility(View.VISIBLE)
        } else {
            txtFileName?.setVisibility(View.GONE)
            txtUploadCv?.setVisibility(View.VISIBLE)
            txtUpdateCv?.setVisibility(View.GONE)
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseLanguage.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    var changedLang = "en"
                    if (it.response != null) {
                        changedLang = if (mLang == "English") {
                            "en"
                        } else {
                            "hi"
                        }
                        Preferences.prefs?.saveValue(Constants.SELECTED_LANG, changedLang)

                        activity?.let { it1 -> LanguageHelper.setLocale(it1, changedLang) }
                        txtFileName?.setText(
                            mCvFileName + activity?.resources?.getString(R.string.cv_download_click)
                        )
                    }
                }
            }
        })
        viewModel.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    setProfileData(it)
                }
            }
        })

        viewModel.responseUpdateStatus.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Utilities.showToast(activity, "Status Updated.")
                }
            }
        })

        viewModel.responseProfilePic.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Utilities.showSuccessToast(activity, "Profile pic updated.")
                    viewModel.getWorkerByUserId(userId, false)
                }
            }
        })

        viewModelOnBoarding.responseGetDocLink.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (isForProfilePic == true) {
                        viewModel.uploadProfilePic(userId, it.response)
                    } else {
                        viewModelOnBoarding.uplaodCV(it.response, workerId, mCvFileNameUpload)
                    }
                }
            }
        })
        viewModelOnBoarding.responseUploadCV.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.getWorkerByUserId(userId)
                    Utilities.showToast(
                        activity,
                        "File Uploaded Sucessfully!"
                    )

                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private fun showDialogMessage(
        title: String?,
        body: String?,
        exit: Boolean?
    ) {
        var userDialog: AlertDialog? = null
        val builder =
            activity?.let { AlertDialog.Builder(it) }
        builder?.setTitle(title)?.setMessage(body)
            ?.setNeutralButton("OK") { dialog, which ->
                try {
                    userDialog?.dismiss()
                    if (exit == true) {
                        exit()
                    }
                } catch (e: java.lang.Exception) {
                    // Log failure
                    Log.e("Expception", e.localizedMessage + "")
                }
            }
        userDialog = builder?.create()
        userDialog?.show()
    }

    private fun exit() {
        val intent = Intent(activity, LoginOnBoardingActivity::class.java)
        activity?.startActivity(intent)
        activity?.finish()
    }

    private fun signOut() {
        if (AuthHelper.getPool() != null) {
            val cu = AuthHelper.getPool().getUser(userId?.toString())
            val currentUser = AuthHelper.getPool().currentUser
            Log.e("Auth helper user", cu.userId + ", " + currentUser)
            currentUser.signOut()
            cu.signOut()
        }
        AuthHelper.setUser(null)
        Preferences.prefs?.clearValue(Constants.OG_SESSION_ID)
        Preferences.prefs?.clearValue(Constants.USER_ID)
        Preferences.prefs?.clearValueInt(Constants.ID)
        Preferences.prefs?.clearValue(Constants.IS_LOGGED_IN)
        Preferences.prefs?.clearValue(Constants.DONE_ONBOARDING)
        AuthHelper.clearCurrUserAttributes()
        Freshchat.resetUser(activity)
        exit()
    }

    private fun showDialogForRefferApp() {
        val dialog = activity?.let { Dialog(it) }
        dialog?.setContentView(R.layout.dialog_reffer_app)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
        val close = dialog?.findViewById<View>(R.id.imgClose) as AppCompatImageView
        val txtRefferApp =
            dialog?.findViewById<View>(R.id.txtRefferApp) as AppCompatTextView
        close?.setOnClickListener { dialog.dismiss() }
        txtRefferApp?.setOnClickListener {
            dialog.dismiss()
            val url =
                "https://www.okaygo.in/?invitedby=" + userId
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(url))
                .setDomainUriPrefix("https://okaygoapps.page.link")
                .setAndroidParameters(
                    AndroidParameters.Builder("com.okaygo.worker")
                        .setMinimumVersion(9)
                        .build()
                )
                .setGoogleAnalyticsParameters(
                    GoogleAnalyticsParameters.Builder()
                        .setSource("app_referal")
                        .setMedium("app_referal")
                        .setCampaign("share_with_friends")
                        .build()
                )
                .buildShortDynamicLink()
                .addOnSuccessListener { shortDynamicLink ->
                    if (shortDynamicLink != null) {
                        message =
                            "Download OkayGo Jobs app ${shortDynamicLink.shortLink} to apply for thousands of high paying full time, part time, single day and work from home jobs near you."
                        activity?.let { it1 ->
                            Utilities.shareApp(
                                it1,
                                message,
                                "Share OkayGo Jobs App"
                            )
                        }
                    }
                }
                .addOnFailureListener {
                    if (message == null) {
                        message =
                            "Download OkayGo Jobs app ${Constants.PLAY_STORE_URL} to apply for thousands of high paying full time, part time, single day and work from home jobs near you."
                    }
                    activity?.let { it1 ->
                        Utilities.shareApp(
                            it1,
                            message,
                            "Share OkayGo Jobs App"
                        )
                    }
                }
        }
        dialog.show()
    }

    private fun launchCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
    }

    override fun onClick(v: View?) {
        Constants.IS_FROM_PROFILE = true
        when (v?.id) {
            R.id.txtShareApp -> showDialogForRefferApp()
            R.id.imgEditImage -> {
                if (Utilities.checkStoragePermission(activity) && Utilities.checkCameraPermission(
                        activity
                    )
                ) {
                    launchCamera()
                }
            }
            R.id.txtUploadCv -> showFileChooserCV()

            R.id.txtUpdateCv -> {
                if (Utilities.checkStoragePermission(activity)) {
                    Dialogs.showAlertDialog(
                        activity,
                        resources.getString(R.string.replace_cv),
                        resources.getString(R.string.replace_cv_msg),
                        "Yes",
                        "No",
                        onAlertPosBtnClick,
                        onAlertNagClick
                    )
                }
            }

            R.id.imgStatusEdit -> {
                if (!isStatusEdit) {
                    edtStatus?.setEnabled(true)
                    Utilities.showToast(activity, "Now you can edit. 100 Char Max.")
                    edtStatus?.setTextSize(16f)
                    imgStatusEdit?.setImageResource(R.drawable.ic_done_black)
                    isStatusEdit = true
                } else {
                    var x: String = edtStatus?.getText().toString()
                    x = x.replace(" ".toRegex(), "")
                    if (x.length > 5) {
                        edtStatus?.setEnabled(false)
                        edtStatus?.setText(
                            edtStatus?.getText().toString().replace("\n".toRegex(), "")
                        )
                        viewModel.updateStatus(edtStatus?.getText().toString(), userId)
                        edtStatus?.setTextSize(12f)
                        imgStatusEdit?.setImageResource(R.drawable.ic_pencil_blue)
                        isStatusEdit = false
                    } else {
                        Utilities.showToast(
                            activity,
                            "Invalid Status."
                        )
                    }
                }
            }

            R.id.txtSkills -> {
                Constants.IS_FROM_PROFILE_SKILLS = true
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.SKILLs)
                startActivity(intent)
            }

            R.id.txtReferral -> {
                Constants.IS_FROM_PROFILE_SKILLS = true
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.REFFERAL)
                startActivity(intent)
            }
            R.id.txtPayment -> {
                Constants.IS_FROM_PROFILE_SKILLS = true
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.PAYMENT_DETAIL)
                startActivity(intent)
            }
            R.id.txtDocuments -> {
                Constants.IS_FROM_PROFILE_SKILLS = true
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.DOCUMENT_DETAIL)
                startActivity(intent)
            }

            R.id.txtTermCondition -> {
                Utilities.showAlertTermsPolicy(
                    activity,
                    resources.getString(R.string.tc),
                    resources.getString(R.string.tc_all)
                )
            }
            R.id.txtPolicy -> {
                Utilities.showAlertTermsPolicy(
                    activity,
                    resources.getString(R.string.pp),
                    resources.getString(R.string.pp_text)
                )
            }

            R.id.txtPersonalDetails -> {
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.PERSONAL_DETAIL)
                startActivity(intent)
            }

            R.id.txtEducation -> {
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.EDUCATION_LANGUAGE)
                startActivity(intent)
            }

            R.id.txtExperience -> {
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.EXPERIENCE)
                startActivity(intent)
            }

            R.id.txtJobPrefrence -> {
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.JOB_PREFREBCE)
                startActivity(intent)
            }

            R.id.txtChangePassword -> {
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.CHANGE_PASSWORD)
                startActivity(intent)
            }
            R.id.txtRatingReview -> {
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.RATING_REVIEW)
                startActivity(intent)
            }

            R.id.txtLogout -> logOutUser()
            R.id.txtFileName -> {
                isDialogForDownload = true
                Dialogs.showAlertDialog(
                    activity,
                    resources.getString(R.string.cv_download),
                    resources.getString(R.string.cv_download_msg),
                    "Yes",
                    "No",
                    onAlertPosBtnClick,
                    onAlertNagClick
                )
            }
        }
    }

    private val onAlertNagClick: () -> Unit = {
        isDialogForDownload = false
    }

    private val onAlertPosBtnClick: () -> Unit = {
        if (Utilities.checkStoragePermission(activity)) {
            if (isDialogForDownload) {
                Utilities.downloadAnyFile(activity, mCvLink, mCvFileName)
                isDialogForDownload = false
            } else {
                showFileChooserCV()
            }
        }
    }

//    private var receiver: BroadcastReceiver = object : BroadcastReceiver() {
//        override fun onReceive(context: Context?, intent: Intent) {
//            val action = intent.action
//            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == action) {
//                val downloadId = intent.getLongExtra(
//                    DownloadManager.EXTRA_DOWNLOAD_ID, 0
//                )
//                openDownloadedAttachment(downloadId)
//            }
//        }
//    }

//    /**
//     * Used to open the downloaded attachment.
//     *
//     * @param context    Content.
//     * @param downloadId Id of the downloaded file to open.
//     */
//    private fun openDownloadedAttachment(
//        downloadId: Long
//    ) {
//        val downloadManager =
//            context?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
//        val query = DownloadManager.Query()
//        query.setFilterById(downloadId)
//        val cursor: Cursor = downloadManager.query(query)
//        if (cursor.moveToFirst()) {
//            val downloadStatus: Int? =
//                cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
//            val downloadLocalUri: String? =
//                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
//            val downloadMimeType: String? =
//                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE))
//            if (downloadStatus == DownloadManager.STATUS_SUCCESSFUL && downloadLocalUri != null) {
//                openDownloadedAttachment(
//                    Uri.parse(downloadLocalUri),
//                    downloadMimeType
//                )
//            }
//        }
//        cursor.close()
//    }
//
//    /**
//     * Used to open the downloaded attachment.
//     *
//     *
//     * 1. Fire intent to open download file using external application.
//     *
//     * 2. Note:
//     * 2.a. We can't share fileUri directly to other application (because we will get FileUriExposedException from Android7.0).
//     * 2.b. Hence we can only share content uri with other application.
//     * 2.c. We must have declared FileProvider in manifest.
//     * 2.c. Refer - https://developer.android.com/reference/android/support/v4/content/FileProvider.html
//     *
//     * @param context            Context.
//     * @param attachmentUri      Uri of the downloaded attachment to be opened.
//     * @param attachmentMimeType MimeType of the downloaded attachment.
//     */
//    private fun openDownloadedAttachment(
//        attachmentUri: Uri?,
//        attachmentMimeType: String?
//    ) {
//        var uri: Uri? = attachmentUri
//        if (uri != null) {
//            // Get Content Uri.
//            if (ContentResolver.SCHEME_FILE.equals(uri.scheme)) {
//                // FileUri - Convert it to contentUri.
//                val file = File(uri.path)
//                uri =
//                    activity?.let {
//                        FileProvider.getUriForFile(
//                            it,
//                            "com.okaygo.worker.provider",
//                            file
//                        )
//                    }
//            }
//            val openAttachmentIntent = Intent(Intent.ACTION_VIEW)
//            openAttachmentIntent.setDataAndType(uri, attachmentMimeType)
//            openAttachmentIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
//            try {
//                context?.startActivity(openAttachmentIntent)
//            } catch (e: ActivityNotFoundException) {
//                Utilities.showToast(
//                    context,
//                    "Unable to open file"
//                )
//            }
//        }
//    }
//

//    private fun DownloadAnyFile(activity: Activity?,url: String?) {
//
//        mMimeType = Utilities.getMimeType(url) ?: ""
//        Log.e("DOWNLOAD_MImE", mMimeType + "")
//        val request: DownloadManager.Request = DownloadManager.Request(Uri.parse(url))
//        activity?.registerReceiver(
//            receiver, IntentFilter(
//                DownloadManager.ACTION_DOWNLOAD_COMPLETE
//            )
//        )
//
//        //request.setDescription("Some descrition");
//        request.setTitle(mCvFileName)
//        request.setDestinationInExternalPublicDir(
//            Environment.DIRECTORY_DOWNLOADS,
//            "$mCvFileName"
//        )
//
//        // get download service and enqueue file
//        val manager: DownloadManager? =
//            activity?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
//        request.setMimeType(mMimeType)
//
//        request.allowScanningByMediaScanner()
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
//        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI)
//        manager?.enqueue(request)
//    }

    private fun logOutUser() {
        activity?.let {
            val alert = AlertDialog.Builder(it)
            alert.setMessage(R.string.want_to_logout)
            alert.setCancelable(true)

            alert.setPositiveButton(
                "Yes"
            ) { dialog, id -> signOut() }

            alert.setNegativeButton(
                "No"
            ) { dialog, id -> dialog.cancel() }

            val alert11 = alert.create()
            alert11.show()
        }
    }

    /**
     * file choser for cv
     */
    fun showFileChooserCV() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        val mimeTypes = arrayOf(
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "image/*",
            "application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        //        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                FILE_CHOOSER
            )
        } catch (ex: ActivityNotFoundException) {
            // Potentially direct the user to the Market with a Dialog
            Utilities.showToast(activity, "Please install a File Manager.")
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val extras = data?.extras
            if (extras != null) {
                isForProfilePic = true
                imageBitmap = extras["data"] as Bitmap?
                imgProfile?.setImageBitmap(imageBitmap)
                val file: File? = Utilities.getFileFromBitmap(activity, imageBitmap)
//                val file1 = File(file?.absolutePath)
                viewModelOnBoarding.getDocLink(userId, file)
                imgEditImage?.setImageResource(R.drawable.ic_edit_blue)
            }
//        } else if (requestCode == ProfileFragment.PIC_CROP && resultCode == Activity.RESULT_OK) {
//            val extras = data?.extras
//            var thePic: Bitmap? = null
//            if (extras != null) {
//                thePic = extras.getParcelable("data")
//                companyImage.setImageBitmap(thePic)
//                editImage.setImageResource(R.drawable.ic_edit_blue)
//            }
//        } else if (requestCode == 18 && resultCode == Activity.RESULT_OK) {
//            Toast.makeText(mActivity, "Refreshing...", Toast.LENGTH_SHORT).show()
//            getUserData()
        } else if (requestCode == FILE_CHOOSER && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            Log.e("Work Experience", "File Uri: " + uri.toString())
//            E/Work Experience: File Uri: content://com.android.providers.downloads.documents/document/10007
            // Get the path
            cvPath = Utilities.getFilePathForN(uri, activity)
            if (cvPath != null) {
                val file = File(cvPath)
                val file_size: Int = (file.length() / (1024 * 1024)).toString().toInt()
                if (file_size < 1) {
                    val name: Array<String>? = cvPath?.split("/".toRegex())?.toTypedArray()
                    mCvFileName = name?.get(name.size - 1)
                    mCvFileNameUpload = name?.get(name.size - 1)
                    isForProfilePic = false

                    viewModelOnBoarding.getDocLink(userId, file)
                } else {
                    Utilities.showToast(activity, getString(R.string.file_size))
                }
                Log.e("Work Experience", "File Name: $mCvFileName")
                Log.e("Work Experience", "File Path: $cvPath")
                Log.e("Work Experience", "File file_size : ${file.length() / 1024}KB")
            }
        }
    }
}