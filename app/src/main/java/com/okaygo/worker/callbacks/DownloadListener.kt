package com.okaygo.worker.callbacks

interface DownloadListener {

    fun onDownloadComplete(download: Boolean, pos: Int)
    fun downloadProgress(status: Int)
}