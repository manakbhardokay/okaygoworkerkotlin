package com.okaygo.worker.date_picker;

public enum PickerField {
    YEAR,
    MONTH
}