package com.okaygo.worker.data.modal.reponse

import java.io.Serializable

data class RequiredSkills(
    val available: Boolean?,
    val required: Boolean?,
    val mandatory: Boolean?,
    val skillName: String?,
    val skillValue: String?,
    val skillId: Int?
) : Serializable