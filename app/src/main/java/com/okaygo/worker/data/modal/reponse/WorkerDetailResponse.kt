package com.okaygo.worker.data.modal.reponse

data class WorkerDetailResponse(
    val code: Int?,
    val message: String?,
    val response: WDResponse?
)

data class WDResponse(val content: ArrayList<WorkerDetails>?)

