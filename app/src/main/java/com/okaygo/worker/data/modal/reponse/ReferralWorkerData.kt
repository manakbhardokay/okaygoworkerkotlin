package com.okaygo.worker.data.modal.reponse

data class ReferralWorkerData(
    val approvalStatus: String?,
    val assignId: Int?,
    val feedbackStatus: String?,
    val firstName: String?,
    val interviewId: Int?,
    val jobDetailsId: Int?,
    val jobId: Int?,
    val joiningDate: String?,
    val joiningStatus: String?,
    val lastName: String?,
    val claimStatus: String?,
    val enquiryStatus: String?,
    val referral_id: String?,
    val userId: Int?,
    val referralClaimAmount: Int?,
    val workerId: Int?,
    val toBeClaimed: Int?
)