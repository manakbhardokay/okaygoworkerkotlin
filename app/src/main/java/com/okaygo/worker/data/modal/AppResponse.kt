package com.okaygo.worker.data.modal

data class AppResponse(
    val user_id: Int?,
    val worker_age: Int?,
    val worker_category: String?,
    val worker_city: String?,
    val worker_cv_status: Int?,
    val worker_education: String?,
    val worker_english_level: String?,
    val worker_exp: Double?,
    val worker_gender: String?,
    val worker_interview_count: Int?,
    val worker_job_apply_count: Int?,
    val worker_job_type: String?,
    val worker_joined_count: Int?,
    val worker_last_activity: String?,
    val worker_last_salary: String?,
    val worker_onboard_status: Int?,
    val worker_selected_count: Int?
)