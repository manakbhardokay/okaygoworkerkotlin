package com.okaygo.worker.data.modal.reponse

data class DueResponse(
    val transactionId: String?,
    val amountDue: String?,
    val amountPaid: String?,
    val insertedOn: String?,
    val comments: String?,
    val userId: Int?,
    val paid_on: String?
)