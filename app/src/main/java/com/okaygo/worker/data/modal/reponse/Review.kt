package com.okaygo.worker.data.modal.reponse

data class Review(
    val employerId: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val jobId: Int?,
    val ratings: Double?,
    val review: String?,
    val reviewId: Int?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?
)