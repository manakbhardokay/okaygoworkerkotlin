package com.okaygo.worker.data.modal.reponse

data class SuperAvailailityResponse(
    val code: Int,
    val message: String,
    val response: AvailailityResponse
)