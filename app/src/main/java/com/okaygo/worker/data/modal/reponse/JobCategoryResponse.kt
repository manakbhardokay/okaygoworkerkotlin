package com.okaygo.worker.data.modal.reponse

data class JobCategoryResponse(
    val code: Int?,
    val message: String?,
    val response: CategoryResponse?
)
