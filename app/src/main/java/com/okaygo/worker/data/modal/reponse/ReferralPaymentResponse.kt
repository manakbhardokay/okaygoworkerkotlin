package com.okaygo.worker.data.modal.reponse

data class ReferralPaymentResponse(
    val code: Int?,
    val message: String?,
    val response: RResponse?
)

data class RResponse(
    val content: ArrayList<ReferralPayment>?,
    val empty: Boolean?,
    val first: Boolean?,
    val last: Boolean?,
    val number: Int?,
    val numberOfElements: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)

