package com.okaygo.worker.data.modal.reponse

data class IDResponse(
    val company_id: Int?,
    val employer_id: Int?,
    val on_board: Int?,
    val role_type: Int?,
    val user_id: Int?,
    val worker_id: Int?
)