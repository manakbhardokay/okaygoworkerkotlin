package com.okaygo.worker.data.modal.reponse

data class JobInterviewDetails(
    val insertedOn: String?,
    val interviewAddress: Any?,
    val interviewGoogleLocation: Any?,
    val interviewLat: Any?,
    val interviewLong: Any?,
    val interviewProcess: Any?,
    val interviewUrl: Any?,
    val jobDetailId: Int?,
    val jobInterviewId: Int?,
    val joiningSpecialRequirement: String?,
    val preferredInterviewDays: String?,
    val updatedOn: String?
)
