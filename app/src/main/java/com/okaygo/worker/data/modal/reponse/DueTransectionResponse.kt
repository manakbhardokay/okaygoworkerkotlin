package com.okaygo.worker.data.modal.reponse

data class DueTransectionResponse(
    val code: Int?,
    val message: String?,
    val response: DueResponse?
)