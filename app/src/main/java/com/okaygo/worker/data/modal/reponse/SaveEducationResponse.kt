package com.okaygo.worker.data.modal.reponse

data class SaveEducationResponse(
    val code: Int?,
    val message: String?,
    val response: EduLangResponse?
)

