package com.okaygo.worker.data.modal.request

data class AddExpRequest(
    val requested_by: Int?,
    val company_name: String?,
    val industry_type: String?,
    val job_type: String?,
    val status: String?,
    val work_location: String?,
    val worker_id: String?,
    val from_date: String?,
    val to_date: String?,
    val experience_id: String?
)