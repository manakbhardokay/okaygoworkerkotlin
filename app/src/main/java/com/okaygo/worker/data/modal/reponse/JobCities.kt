package com.okaygo.worker.data.modal.reponse

data class JobCities(
    val cityId: Int?,
    val city_name: String?,
    val jobId: Int?,
    val new_city_id: String?,
    val pincode: Int?,
    val state_name: String?
)