package com.okaygo.worker.data.modal

data class BottomNavScreenChange(val screen: Int?)