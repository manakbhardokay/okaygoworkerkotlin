package com.okaygo.worker.data.modal.request

data class AddUserRoleRequest(
    val role_type: String? = "4",
    val country_code: String? = "91",
    val phone_number: String?,
    val referred_by_code: String?,
    val referral_code: String? = "dfghj"
)