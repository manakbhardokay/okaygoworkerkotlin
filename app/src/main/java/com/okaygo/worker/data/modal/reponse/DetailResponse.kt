package com.okaygo.worker.data.modal.reponse

data class DetailResponse(

    var content: ArrayList<DetailContent>?,
    var totalElements: Int?,
    var last: Boolean?,
    var totalPages: Int?,
    var size: Int?,
    var number: Int?,
    var numberOfElements: Int?,
    var first: Boolean?,
    var empty: Any?
)
