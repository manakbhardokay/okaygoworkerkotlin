package com.okaygo.worker.data.modal.reponse

data class WhatsAppResponse(
    val whatsappNumber: String?,
    val userId: Int?,
    val permission: Int?,
    val message: String?
)