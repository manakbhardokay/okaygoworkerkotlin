package com.okaygo.worker.data.modal.reponse

data class JobPayout(
    val insertedOn: String?,
    val jobId: Int?,
    val leaderPayoutAmount: Any,
    val leaderPayoutDetails: Any,
    val okaygoCommissionPerJoining: Double,
    val okaygoCommissionTerm: Int,
    val okaygoInvoicingPeriod: Any,
    val okaygoReplacementClause: Any,
    val remoteRecPayoutAmount: Any,
    val remoteRecPayout_details: Any,
    val updatedOn: String
)
