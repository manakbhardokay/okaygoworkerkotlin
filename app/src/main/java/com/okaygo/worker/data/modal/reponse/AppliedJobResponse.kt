package com.okaygo.worker.data.modal.reponse

data class AppliedJobResponse(
    val code: Int?,
    val message: String?,
    val response: AppliedResponse?
)

data class AppliedResponse(
    val content: ArrayList<AppliedJob>?
)
