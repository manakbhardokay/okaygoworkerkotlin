package com.okaygo.worker.data.modal.reponse

data class RaiseEnquiry(
    val enquiryComments: String?,
    val enquiryRequest: String?,
    val enquiryStatus: String?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val referralEnquiryId: Int?,
    val referral_id: String?,
    val updatedBy: Int?,
    val updatedOn: String?
)