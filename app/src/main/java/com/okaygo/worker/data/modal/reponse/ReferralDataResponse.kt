package com.okaygo.worker.data.modal.reponse

data class ReferralDataResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<ReferralData>?
)