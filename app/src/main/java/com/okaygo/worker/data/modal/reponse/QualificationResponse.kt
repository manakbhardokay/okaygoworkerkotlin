package com.okaygo.worker.data.modal.reponse

data class QualificationResponse(
    val code: Int?,
    val message: String?,
    val response: EduResponse?
)
