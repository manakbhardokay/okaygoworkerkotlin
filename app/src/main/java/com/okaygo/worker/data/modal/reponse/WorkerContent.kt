package com.okaygo.worker.data.modal.reponse

data class WorkerContent(
    val availableFt: Int?,
    val availableOd: Int?,
    val availablePt: Int?,
    val averageRating: Int?,
    val breach: Double?,
    val cv_file_name: String?,
    val cv_link: String?,
    val english_known_level: Int?,
    val expected_salary: String?,
    val expected_salary_type: String?,
    val globalAvailability: Int?,
    val hasExperience: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val int_ext: String?,
    val interested_cat: String?,
    val interested_roles: Any?,
    val isKyc: Any?,
    val isLeader: Int?,
    val last_salary: Int?,
    val last_salary_type: String?,
    val no_exp: Int?,
    val paytmNumber: Any?,
    val preferred_current_language: Int?,
    val preferred_job_type: Any?,
    val preferred_remark_location: Any?,
    val qualificationId: Int?,
    val recent_gig: Any?,
    val spokenLanguage: Any?,
    val superToggle: Int?,
    val totalGigs: Int?,
    val totalReviews: Int?,
    val total_experience: Any?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?,
    val on_board: Int?,
    val workerId: Int?
)
