package com.okaygo.worker.data.modal.request

class AddSkillsRequest : ArrayList<Skills>()

data class Skills(
    val skillsName: String?,
    val skillsTypeId: Int?,
    val userId: Int?,
    val userSkillsId: Int? = 0
)