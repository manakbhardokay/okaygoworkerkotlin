package com.okaygo.worker.data.modal.reponse

data class FAQ(
    val insertedBy: String?,
    val insertedOn: String?,
    val status: String?,
    val id: Int?,
    val categorySubType: String?,
    val categoryType: String?,
    val typeKey: String?,
    val typeValue: String?,
    val typeDesc: String?,
    val other: String?
)
