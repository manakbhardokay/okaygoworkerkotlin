package com.okaygo.worker.data.modal.request

data class JobCategoryRequest(
    val user_id: Int?,
    val filter_od: String?,
    val filter_pt: String?,
    val filter_ft: String?,
    val max_salary: String?,
    val min_salary: String?,
    val job_types: String?,
    val jobs_after: String?,
    val city: String?,
    val experience: String?,
    val jobs_before: String?
)