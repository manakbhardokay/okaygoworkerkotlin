package com.okaygo.worker.data.modal.reponse

data class RefferJob(
    val previewLink: String?,
    val shortLink: String?,
    val warning: ArrayList<Warning>?
)