package com.okaygo.worker.data.modal.reponse

data class SuccessResponse(
    val code: Int?,
    val message: String?,
    val response: String?
)