package com.okaygo.worker.data.modal.reponse

data class CheckOutResponse(
    val code: Int?,
    val message: String?,
    val response: JobCheckOutResponse?
)
