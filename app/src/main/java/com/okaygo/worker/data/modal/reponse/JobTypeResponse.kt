package com.okaygo.worker.data.modal.reponse

data class JobTypeResponse(
    val code: Int?,
    val message: String?,
    val response: TypeResponse?
)
