package com.okaygo.worker.data.modal.reponse

data class AddExp(
    val companyName: String?,
    val designation: Any?,
    val experienceId: Int?,
    val experienceInMonth: Double?,
    val fromDate: String?,
    val industryId: Any?,
    val industryType: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val jobType: Int?,
    val otherIndustry: Any?,
    val status: Int?,
    val toDate: Any?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val workLocation: String?,
    val workerId: Int?
)