package com.okaygo.worker.data.modal.reponse

data class GetCurrentEduResponse(
    val code: Int?,
    val message: String?,
    val response: CResponse?
)

data class CResponse(
    val content: ArrayList<CurrentStatus>?,
    val empty: Boolean?,
    val first: Boolean?,
    val last: Boolean?,
    val number: Int?,
    val numberOfElements: Int?,
    val size: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)
