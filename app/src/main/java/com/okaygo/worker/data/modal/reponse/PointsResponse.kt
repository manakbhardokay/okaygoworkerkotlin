package com.okaygo.worker.data.modal.reponse

data class PointsResponse(
    val pointType: Int?,
    val reviewId: Int?,
    val reviewPointId: Int?,
    val status: Int?
)