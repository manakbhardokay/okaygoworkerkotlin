package com.okaygo.worker.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [LocationData::class], version = 1, exportSchema = false)
abstract class OkayGoDatabase : RoomDatabase() {

    abstract fun locationDao(): LocationDao

    companion object {
        private var instance: OkayGoDatabase? = null

        fun getInstance(mContext: Context): OkayGoDatabase {

            if (instance == null)
                instance = Room.databaseBuilder(
                    mContext.applicationContext, OkayGoDatabase::class.java,
                    "okaygo_jobs_database"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build()

            return instance!!
        }

        private val roomCallback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }

        fun destroyInstance() {
            instance = null
        }
    }
}