package com.okaygo.worker.data.modal.reponse

data class AddUserRolePhoneNumberResponse(
    val code: Int?,
    val message: String?,
    val response: AddUserResponse?
)