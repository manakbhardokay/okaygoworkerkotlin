package com.okaygo.eflex.data.modal.reponse

import com.okaygo.worker.data.modal.reponse.UpdateResponse

data class UResponse(
    val content: UpdateResponse?,
    val empty: Any,
    val first: Any,
    val last: Any,
    val number: Any,
    val numberOfElements: Any,
    val pageable: Any,
    val size: Any,
    val sort: Any,
    val totalElements: Any,
    val totalPages: Any
)