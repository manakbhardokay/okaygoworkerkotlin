package com.okaygo.worker.data.modal.reponse

data class AddJobTypeResponse(
    val code: Int?,
    val message: String?,
    val response: JobType?
)
