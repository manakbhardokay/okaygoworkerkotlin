package com.okaygo.worker.data.modal.reponse

data class EmployerPointsResponse(
    val code: Int?,
    val message: String?,
    val response: PointsResponse?
)
