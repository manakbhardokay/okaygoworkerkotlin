package com.okaygo.worker.data.modal.reponse

data class OnBoardingStatusResponse(
    val code: Int?,
    val message: String?,
    val response: TaskStatus?
)