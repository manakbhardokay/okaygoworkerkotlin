package com.okaygo.worker.data.modal.reponse

import java.io.Serializable

data class ReqiredSkillsResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<RequiredSkills>?
) : Serializable
