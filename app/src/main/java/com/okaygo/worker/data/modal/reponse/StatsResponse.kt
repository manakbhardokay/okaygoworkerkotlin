package com.okaygo.worker.data.modal.reponse

data class StatsResponse(
    val code: Int?,
    val message: String?,
    val response: Stats?
)

