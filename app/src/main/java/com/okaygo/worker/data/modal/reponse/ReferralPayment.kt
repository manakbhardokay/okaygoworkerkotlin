package com.okaygo.worker.data.modal.reponse

data class ReferralPayment(
    val brandName: String?,
    val companyLogo: Any?,
    val companyName: String?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val isWorkerPaid: Int?,
    val jobId: Int?,
    val referralId: String?,
    val referralJopType: String?,
    val referralName: String?,
    val totalAmount: Int?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?
)
