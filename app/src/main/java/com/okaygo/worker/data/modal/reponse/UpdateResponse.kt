package com.okaygo.worker.data.modal.reponse

data class UpdateResponse(
    val app_name: String?,
    val is_force_update: Boolean?,
    val update_msg: String?,
    val version: String?,
    val version_code: Int?
)