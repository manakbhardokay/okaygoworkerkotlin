package com.okaygo.worker.data.modal.reponse

data class RaiseEnquiryResponse(
    val code: Int?,
    val message: String?,
    val response: RaiseEnquiry?
)
