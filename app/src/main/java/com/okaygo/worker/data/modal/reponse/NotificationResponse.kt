package com.okaygo.worker.data.modal.reponse

data class NotificationResponse(
    val code: Int?,
    val message: String?,
    val response: NotiResponse?
)

data class NotiResponse(
    val content: ArrayList<Notifications>?
)
