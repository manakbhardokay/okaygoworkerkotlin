package com.okaygo.worker.data.modal

import com.google.gson.annotations.SerializedName

data class SocialMediaLoginExistsRequest(

    @field:SerializedName("sns_type")
    var snsType: Int? = null,

    @field:SerializedName("sns_id")
    var snsId: String? = null
)