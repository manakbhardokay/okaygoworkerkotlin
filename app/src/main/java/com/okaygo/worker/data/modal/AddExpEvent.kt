package com.okaygo.worker.data.modal

data class AddExpEvent(val isAdded: Boolean?)