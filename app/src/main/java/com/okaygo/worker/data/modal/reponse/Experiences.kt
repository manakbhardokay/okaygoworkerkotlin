package com.okaygo.worker.data.modal.reponse

import android.os.Parcel
import android.os.Parcelable

data class Experiences(
    val companyName: String?,
    val designation: String?,
    val experienceId: Int?,
    val experienceInMonth: Double?,
    val fromDate: String?,
    val industryId: Int?,
    val industryName: String?,
    val industryType: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val jobType: Int?,
    val jobTypeName: String?,
    val otherIndustry: Any?,
    val status: Int?,
    val toDate: String?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val workLocation: String?,
    val workerId: Int?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        TODO("designation"),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        TODO("otherIndustry"),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(companyName)
        parcel.writeValue(experienceId)
        parcel.writeValue(experienceInMonth)
        parcel.writeString(fromDate)
        parcel.writeValue(industryId)
        parcel.writeString(industryName)
        parcel.writeValue(industryType)
        parcel.writeValue(insertedBy)
        parcel.writeString(insertedOn)
        parcel.writeValue(jobType)
        parcel.writeString(jobTypeName)
        parcel.writeValue(status)
        parcel.writeString(toDate)
        parcel.writeValue(updatedBy)
        parcel.writeString(updatedOn)
        parcel.writeString(workLocation)
        parcel.writeValue(workerId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Experiences> {
        override fun createFromParcel(parcel: Parcel): Experiences {
            return Experiences(parcel)
        }

        override fun newArray(size: Int): Array<Experiences?> {
            return arrayOfNulls(size)
        }
    }
}