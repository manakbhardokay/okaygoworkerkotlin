package com.okaygo.worker.data.modal

data class InAppResponse(
    val code: Int?,
    val message: String?,
    val response: AppResponse?
)
