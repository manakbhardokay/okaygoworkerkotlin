package com.okaygo.worker.data.modal.reponse

data class RatingReviewResponse(
    val code: Int?,
    val message: String?,
    val response: RatingResponse?
)
