package com.okaygo.worker.data

data class LocationEvent(
    val lat: Double?,
    val lng: Double?,
    val address: String?,
    val city: String?
)