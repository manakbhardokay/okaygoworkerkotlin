package com.okaygo.worker.data.modal.reponse

data class AllJobRoleResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<JobRoleResponse>?
)
