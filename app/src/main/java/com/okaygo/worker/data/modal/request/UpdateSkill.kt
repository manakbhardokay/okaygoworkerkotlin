package com.okaygo.worker.data.modal.request

import android.os.Parcel
import android.os.Parcelable

data class UpdateSkill(
    val skillsName: String? = null,
    val skillsTypeId: Int? = null,
    val userId: Int? = null,
    val userSkillsId: Int? = 0
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(skillsName)
        parcel.writeValue(skillsTypeId)
        parcel.writeValue(userId)
        parcel.writeValue(userSkillsId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpdateSkill> {
        override fun createFromParcel(parcel: Parcel): UpdateSkill {
            return UpdateSkill(parcel)
        }

        override fun newArray(size: Int): Array<UpdateSkill?> {
            return arrayOfNulls(size)
        }
    }
}
