package com.okaygo.worker.data.modal.reponse

data class ChekinResonse(
    val code: Int?,
    val message: String?,
    val response: Response?
)

data class Response(
    val content: ArrayList<CheckIn>?,
    val empty: Boolean,
    val first: Boolean,
    val last: Boolean,
    val number: Int,
    val numberOfElements: Int,
    val size: Int,
    val totalElements: Int,
    val totalPages: Int
)
