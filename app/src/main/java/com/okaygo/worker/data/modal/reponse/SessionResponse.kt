package com.okaygo.worker.data.modal.reponse

data class SessionResponse(
    val androidVersion: String?,
    val appVersion: String?,
    val brand: String?,
    val deviceRegistrationId: String?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val ipAddress: String?,
    val lastSeenLat: Double?,
    val lastSeenLong: Double?,
    val loggedOn: Any?,
    val macAddress: String?,
    val sessionId: Int?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?
)