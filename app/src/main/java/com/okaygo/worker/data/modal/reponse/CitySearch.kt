package com.okaygo.worker.data.modal.reponse

data class CitySearch(
    val city_id: Int?,
    val district: String?,
    val stateName: String?
)