package com.okaygo.worker.data.modal.reponse

import com.okaygo.worker.data.api.response.SkillResponse

data class AllSkillResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<SkillResponse>?
)