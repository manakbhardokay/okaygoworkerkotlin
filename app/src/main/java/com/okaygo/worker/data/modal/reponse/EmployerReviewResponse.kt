package com.okaygo.worker.data.modal.reponse

data class EmployerReviewResponse(
    val code: Int?,
    val message: String?,
    val response: ReviewResponse?
)

data class ReviewResponse(
    val content: ArrayList<ReviewContent>?,
    val empty: Boolean?,
    val first: Boolean?,
    val last: Boolean?,
    val number: Int?,
    val numberOfElements: Int?,
    val size: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)
