package com.okaygo.worker.data.modal.reponse

data class CheckIn(
    val assignId: Int?,
    val checkInId: Int?,
    val endedOn: Any?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val isEnded: Int?,
    val isExtended: Int?,
    val isNoShow: Int?,
    val isPaid: Int?,
    val isStarted: Int?,
    val isVerifiedByOtp: Int?,
    val isWorkerPaid: Int?,
    val jobDetailsId: Int?,
    val jobId: Int?,
    val joiningStatus: Any?,
    val otp: String?,
    val startedOn: Any?,
    val status: Int?,
    val transactionUniqueId: String?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?,
    val workerInvoiceId: Int?,
    val workerTransactionId: Any?
)
