package com.okaygo.worker.data.modal.reponse

data class PersonalDetailResponse(
    val code: Int?,
    val message: String?,
    val response: PersonalDetailContent?
)

data class PersonalDetailContent(
    val content: ArrayList<PersonalDetail>?,
    val empty: Any?,
    val first: Any?,
    val last: Any?,
    val number: Any,
    val numberOfElements: Any?,
    val pageable: Any?,
    val size: Any?,
    val sort: Any?,
    val totalElements: Any?,
    val totalPages: Any?
)
