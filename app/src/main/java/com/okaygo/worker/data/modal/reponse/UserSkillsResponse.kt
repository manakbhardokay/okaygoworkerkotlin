package com.okaygo.worker.data.api.response

data class UserSkillsResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<UserSkills>?
)