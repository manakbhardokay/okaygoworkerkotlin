package com.okaygo.worker.data.modal.reponse

data class AllIdResponse(
    val code: Int?,
    val message: String?,
    val response: IDResponse?
)
