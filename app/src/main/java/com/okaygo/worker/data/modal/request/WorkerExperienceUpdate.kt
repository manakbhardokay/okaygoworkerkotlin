package com.okaygo.worker.data.modal.request

data class WorkerExperienceUpdate(
    val companyName: String? = null,
    val designation: String? = null,
    val experienceId: Int? = null,
//    val fromDate: String? = null,
    val industryId: Int? = null,
    val industryType: Int? = null,
    val jobType: Int? = null,
    val otherIndustry: String? = null,
    val status: Int? = null,
    val toDate: String? = null,
    val workLocation: String? = null
)