package com.okaygo.worker.data.modal.reponse

data class WorkerSession(
    val code: Int?,
    val message: String?,
    val response: SessionResponse?
)
