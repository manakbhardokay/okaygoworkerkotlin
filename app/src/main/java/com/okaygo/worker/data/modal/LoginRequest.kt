package com.okaygo.worker.data.modal

import com.google.gson.annotations.SerializedName

data class LoginRequest(

    @field:SerializedName("sns_type")
    var snsType: Int? = null,

    @field:SerializedName("password")
    var password: String? = null,

    @field:SerializedName("email")
    var email: String? = null,

    @field:SerializedName("firstName")
    var firstName: String? = null,

    @field:SerializedName("lastName")
    var lastName: String? = null,

    @field:SerializedName("sns_id")
    var snsId: String? = null,

    @field:SerializedName("is_email_verified")
    var isEmailVerified: Int? = null
)