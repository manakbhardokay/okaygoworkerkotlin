package com.okaygo.worker.data.api.response

import com.okaygo.worker.data.modal.reponse.JobCategories

data class FindJobCategoriesResponse(
    val experiencedTypeJobs: ArrayList<JobCategories>?,
    val interestedCatJobs: ArrayList<JobCategories>?,
    val otherJobs: ArrayList<JobCategories>?
)