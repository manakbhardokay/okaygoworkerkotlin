package com.okaygo.worker.data.modal.reponse

data class FindJobResponse(
    val code: Int?,
    val message: String?,
    val response: JobResponse?
)