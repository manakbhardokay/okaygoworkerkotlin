package com.okaygo.worker.data.modal.reponse

data class JobCount(
    val code: Int?,
    val message: String?,
    val response: JCount?
)
