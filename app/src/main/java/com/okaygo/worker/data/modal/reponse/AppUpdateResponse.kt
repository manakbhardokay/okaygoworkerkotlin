package com.okaygo.eflex.data.modal.reponse

data class AppUpdateResponse(
    val code: Int?,
    val message: String?,
    val response: UResponse?
)

