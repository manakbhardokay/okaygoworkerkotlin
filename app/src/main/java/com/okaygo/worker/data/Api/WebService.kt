package com.openkey.guest.data.Api

import com.okaygo.eflex.data.modal.reponse.AppUpdateResponse
import com.okaygo.worker.data.api.response.FindJobCategories
import com.okaygo.worker.data.api.response.OtherSkills
import com.okaygo.worker.data.api.response.UserSkillsResponse
import com.okaygo.worker.data.modal.InAppResponse
import com.okaygo.worker.data.modal.direction_api_response.DirectionApiResponse
import com.okaygo.worker.data.modal.reponse.*
import com.okaygo.worker.data.modal.request.AddSkillsRequest
import com.okaygo.worker.data.modal.request.QuestionareRequestItem
import com.okaygo.worker.data.modal.request.UpdateWorkerRequest
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @author Davinder Goel.
 */
interface WebService {

    @GET("inapp/getData")
    fun getInAppData(@Query("user_id") user_id: Int?): Call<InAppResponse>

    //
//    @POST("user/addUserRolePhoneNumber")
//    fun addUserRolePhoneNumber(@Body request: AddUserRoleRequest?): Call<InAppResponse>
    @FormUrlEncoded
    @POST("user/addUserRolePhoneNumber")
    fun addUserRolePhoneNumber(@FieldMap params: Map<String, String>): Call<AddUserRolePhoneNumberResponse>

    @GET("user/userAllIds")
    fun getAllIds(@Query("user_id") user_id: Int?): Call<AllIdResponse>

    @GET("configmaster/getJobsAppDetails")
    fun getAppUpdateFlag(): Call<AppUpdateResponse>

    @GET("workermaster/")
    fun getWorker(@Query("user_id") user_id: Int?): Call<WorkerResponse>

    @POST("user/session")
    fun createWorkerSession(
        @Query("device_registration_id") device_registration_id: String?,
        @Query("ip_address") ip_address: String?,
        @Query("mac_address") mac_address: String?,
        @Query("user_id") user_id: Int?,
        @Query("last_seen_long") last_seen_long: Double?,
        @Query("last_seen_lat") last_seen_lat: Double?,
        @Query("app_version") app_version: String?,
        @Query("android_version") android_version: String?,
        @Query("brand") brand: String?
    ): Call<WorkerSession>

    @GET("workermaster/summary")
    fun getOnBoardingProcess(@Query("user_id") user_id: Int?): Call<OnBoardingStatusResponse>

    @FormUrlEncoded
    @POST("user/")
    fun savePersonalDetail(@FieldMap params: Map<String, String>?): Call<PersonalDetailResponse>

    @FormUrlEncoded
    @POST("workermaster/experience")
    fun addWorkExp(@FieldMap params: Map<String, String>?): Call<AddExpResponse>

    @GET("job/getDistrictByKey")
    fun searchCity(@Query("key") key: String?): Call<CityStateResponse>?

    @GET("configmaster/?category_type=qualification_type")
    fun getEducationQualification(): Call<QualificationResponse>?

    @POST("workermaster/")
    fun saveEducationLang(
        @Query("requested_by") requested_by: Int?,
        @Query("qualification_id") qualification_id: Int?,
        @Query("english_known_level") english_known_level: Int?,
        @Query("worker_id") worker_id: Int?
    ): Call<SaveEducationResponse>?

    @GET("configmaster/?category_sub_type=industry&rows=100&inserted_by=0&status=1")
    fun fetchInterestedJobTypes(): Call<JobCategoryResponse>

    @GET("workermaster/experience")
    fun getWorkerExp(
        @Query("worker_id") worker_id: Int?,
        @Query("rows") rows: Int?
    ): Call<ExperienceResponse>

    @GET("configmaster/?category_type=job_type&rows=100&type_desc=job_type_name&type_key=gig_req&inserted_by=0")
    fun getInterestedJobType(): Call<JobTypeResponse>

    @GET("configmaster/inquery")
    fun getJobTypeForInd(@Query("inquery") inquery: String?): Call<JobTypeResponse>

    @POST("configmaster/")
    fun addTypeToConfigMaster(
        @Query("category_sub_type") category_sub_type: String?,
        @Query("category_type") category_type: String?,
        @Query("requested_by") requested_by: String?,
        @Query("status") status: String?,
        @Query("type_desc") type_desc: String?,
        @Query("type_key") type_key: String?,
        @Query("type_value") type_value: String?
    ): Call<AddJobTypeResponse>

    @POST("configmaster/")
    fun addIndustriesToConfigMaster(
        @Query("category_sub_type") category_sub_type: String?,
        @Query("category_type") category_type: String?,
        @Query("requested_by") requested_by: String?,
        @Query("status") status: String?,
        @Query("type_desc") type_desc: String?,
        @Query("type_key") type_key: String?,
        @Query("type_value") type_value: String?
    ): Call<AddJobTypeResponse>


    @DELETE("workermaster/experience")
    fun deleteExp(@Query("experience_id") experience_id: Int?): Call<SuccessResponse>

    @Multipart
    @POST("userdocs/getDocLink")
    fun getDocLink(
        @Part("user_id") user_id: RequestBody,
        @Part file: MultipartBody.Part?
    ): Call<SuccessResponse>

    @POST("workermaster/AddUpdateCVLink")
    fun uploadCv(
        @Query("link") link: String?,
        @Query("worker_id") worker_id: Int?,
        @Query("cv_file_name") cv_file_name: String?
    ): Call<SuccessResponse>

    @FormUrlEncoded
    @POST("user/")
    fun uploadProfilePic(@FieldMap params: Map<String, String>?): Call<WorkerDetailResponse>

    @POST("workermaster/interestedCat")
    fun saveInterestedCat(
        @Query("worker_id") worker_id: Int?,
        @Query("categories") categories: String?
    ): Call<SuccessResponse>

    @POST("workermaster/markNoExp")
    fun setNoExp(
        @Query("no_exp") no_exp: Int?,
        @Query("worker_id") worker_id: Int?
    ): Call<SuccessResponse>

    @POST("workermaster/addUpdateLastSalary")
    fun updateLastSalary(
        @Query("salary") salary: Int?,
        @Query("worker_id") worker_id: Int?
    ): Call<SuccessResponse>

    @GET("workermaster/")
    fun getSelectedJobCat(@Query("user_id") user_id: Int?): Call<SelectedJobCatResponse>

    @POST("workermaster/addOtherSkills")
    fun addSkills(
        @Query("inserted_by") userId: Int?,
        @Query("skills_name") skills_name: String?
    ): Call<OtherSkills>?

    @POST("workermaster/addWorkerSkills")
    fun saveSkills(
        @Body request: AddSkillsRequest?,
        @Query("inserted_by") userId: Int?
    ): Call<AllSkillResponse>?

    @GET("workermaster/getskills")
    fun getAllSkills(
        @Query("job_category_id") job_category_id: String?,
        @Query("job_type_id") job_type_id: String?
    ): Call<AllSkillResponse>?

    @GET("workermaster/getUserSkills")
    fun getUserSkills(@Query("user_id") user_id: Int?): Call<UserSkillsResponse>?

    @FormUrlEncoded
    @POST("workermaster/availability")
    fun saveAvailability(@FieldMap params: Map<String, String>?): Call<AvailabilityResponse>

    @FormUrlEncoded
    @POST("workermaster/")
    fun saveGlobalAvailability(@FieldMap params: Map<String, String>?): Call<SuperAvailailityResponse>

    @FormUrlEncoded
    @POST("workermaster/")
    fun saveSuperAvailability(@FieldMap params: Map<String, String>?): Call<SuperAvailailityResponse>

    @GET("workermaster/availability")
    fun getAvailability(
        @Query("worker_id") workerId: Int?,
        @Query("rows") rows: Int?
    ): Call<GetAvailabilityResponse>

    @POST("workermaster/availability")
    fun updateAvailabilityStatus(
        @Query("availablity_id") availablity_id: Int?,
        @Query("requested_by") requested_by: Int?,
        @Query("active_status") active_status: Int?
    ): Call<GetAvailabilityResponse>

    @DELETE("workermaster/availability")
    fun deleteAvailability(@Query("availablity_id") availablity_id: Int?): Call<SuccessResponse>

    @GET("user/whatsapp")
    fun getWhatsAppSubscription(@Query("user_id") user_id: Int?): Call<WhatsAppSubscriptionResponse>?

    @POST("user/whatsapp")
    fun whatsAppSubscribe(
        @Query("mobile_number") mobile_number: String?,
        @Query("permission") permission: Int?,
        @Query("user_id") user_id: Int?
    ): Call<WhatsAppSubscriptionResponse>?

    @GET("job/getCategoryIdCount")
    fun getJobCategories(
        @Query("user_id") userId: Int?,
        @Query("filter_od") filter_od: String?,
        @Query("filter_pt") filter_pt: String?,
        @Query("filter_ft") filter_ft: String?,
        @Query("max_salary") max_pay: String?,
        @Query("min_salary") min_pay: String?,
        @Query("job_types") job_types: String?,
        @Query("jobs_after") jobs_after: String?,
        @Query("city") city: String?,
        @Query("experience") experience: String?,
        @Query("jobs_before") jobs_before: String?
    ): Call<FindJobCategories>?


    @GET("/job/interestedCatFindJobsNew")
    fun getJobsAccordingToCat(
        @Query("user_id") userId: Int?,
        @Query("job_category_id") job_category_id: Int?,
        @Query("filter_od") filter_od: String?,
        @Query("filter_pt") filter_pt: String?,
        @Query("filter_ft") filter_ft: String?,
        @Query("max_pay") max_pay: String?,
        @Query("min_pay") min_pay: String?,
        @Query("job_types") job_types: String?,
        @Query("jobs_after") jobs_after: String?,
        @Query("jobs_before") jobs_before: String?,
        @Query("city") city: String?,
        @Query("experience") experience: String?,
        @Query("page_no") page_no: Int?,
        @Query("rows") rows: Int?
    ): Call<FindJobResponse>?

    @POST("extWorker/assignExtToJob")
    fun matchJob(
        @Query("job_id") job_id: Int?,
        @Query("user_ids") user_id: String?
    ): Call<JobMatchResponse>?

    @POST("job/assigner/accept")
    fun applyJob(
        @Query("assign_id") assignId: Int?,
        @Query("requested_by") userId: Int?,


        @Query("refer_by") refer_by: Int?
    ): Call<SuccessResponse>?

    @POST("/job/assigner/accept")
    fun acceptInterviewSlot(
        @Query("assign_id") assign_id: Int?,
        @Query("preferred_interview_date") preferred_interview_date: String?,
        @Query("requested_by") requested_by: Int?
    ): Call<SuccessResponse>?

    @GET("job/interestedJobCity")
    fun getJobCities(@Query("user_id") user_id: Int?): Call<JobCititesResponse>?

    @POST("deeplink/getReferLinkForAJob")
    fun getRefferJobLink(
        @Query("job_detail_id") job_detail_id: Int?,
        @Query("job_id") job_id: Int?,
        @Query("refer_by") refer_by: Int?
    ): Call<RefferJobResponse>?

    @POST("job/assigner/reject")
    fun rejectJob(
        @Query("assign_id") assignId: Int?,
        @Query("requested_by") userId: Int?
    ): Call<FindJobResponse>?

    @GET("transactions/worker/jobs")
    fun getPaymentTransection(
        @Query("user_id") userId: Int?,
        @Query("rows") rows: Int?,
        @Query("paymentStatus") paymentStatus: Int? = null
    ): Call<PaymentTransectionResponse>?

    @GET("transactions/workerAppInvoices")
    fun getPaymentTransectionPT_FT(
        @Query("user_id") userId: Int?,
        @Query("rows") rows: Int?
    ): Call<PaymentTransectionResponse>?

    @GET("transactions/workerAppInvoices/stats")
    fun getTransectionStats(
        @Query("user_id") userId: Int?
    ): Call<StatsResponse>?

    @GET("transactions/worker/referral")
    fun getReferralTransection(
        @Query("user_id") userId: Int?,
        @Query("paymentStatus") paymentStatus: Int? = null,
        @Query("page_no") page_no: Int?,
        @Query("rows") rows: Int?
    ): Call<ReferralPaymentResponse>?

    @GET("transactions/worker/due")
    fun getDueTransection(
        @Query("user_id") userId: Int?
    ): Call<DueTransectionResponse>?

//    @GET("transactions/worker/stats")
//    fun getTransectionStats(
//        @Query("user_id") userId: Int?
//    ): Call<StatsResponse>?

    @GET("configmaster/?category_type=worker_faqs_type&page_no=0&rows=500&status=1")
    fun getFaq(): Call<FaqResponse>

    @GET("youtube/getVideoList")
    fun getYoutubeVideos(): Call<YoueTubeResponse>?

    @GET("user/")
    fun getUserDetail(@Query("user_d") user_id: Int?): Call<UserDetailResponse>

    @FormUrlEncoded
    @POST("user/")
    fun updateStatus(@FieldMap params: Map<String, String>?): Call<UserDetailResponse>

    @GET("workermaster/V2/workerByUserId")
    fun getWorkerByUserId(@Query("user_id") user_id: Int?): Call<WorkerDetailResponse>

    @POST("workermaster/change_preferred_language")
    fun updateLanguage(
        @Query("language_id") language_id: Int?,
        @Query("user_id") user_id: Int?
    ): Call<SuccessResponse>?

    @GET("workermaster/")
    fun getCurrentEdu(
        @Query("worker_id") worker_id: Int?
    ): Call<GetCurrentEduResponse>

    @POST("workermaster/interestedCat/")
    fun saveJobCat(
        @Query("worker_id") worker_id: Int?,
        @Query("categories") categories: String?
    ): Call<SuccessResponse>

    @GET("job/appliedJobs")
    fun getAppliedJobs(
        @Query("user_id") userId: Int?,
        @Query("rows") rows: Int?
    ): Call<AppliedJobResponse>

    @GET("job/V2/getdetailedjob")
    fun getJobDetail(@Query("job_id") job_id: Int?): Call<JobDetailResponse>?

    @GET("job/upcomingJobs")
    fun getUpcommingJobs(
        @Query("user_id") user_id: Int?,
        @Query("rows") rows: Int?
    ): Call<AppliedJobResponse>?

    @GET("job/upcomingJobsOD")
    fun getUpcommingODJobs(
        @Query("user_id") user_id: Int?,
        @Query("work_type") work_type: Int?,
        @Query("rows") rows: Int?
    ): Call<AppliedJobResponse>?

    @GET("job/pastJobs")
    fun getPastJobs(
        @Query("user_id") user_id: Int?,
        @Query("rows") rows: Int?
    ): Call<AppliedJobResponse>?

    @POST("interview/")
    fun updateForNotJoining(
        @Query("interview_id") interview_id: Int?,
        @Query("is_offer_rejected") is_offer_rejected: Int?,
        @Query("requested_by") requested_by: Int?
    ): Call<OfferAcceptResponse>?

    @POST("interview/")
    fun offerAccepted(
        @Query("interview_id") interview_id: Int?,
        @Query("is_offer_accepted") is_offer_rejected: Int?,
        @Query("requested_by") requested_by: Int?
    ): Call<OfferAcceptResponse>?

    @POST("job/deleteAssignRequest")
    fun deleteJob(@Query("assignId") assignId: Int?): Call<SuccessResponse>?

    @GET("interview/getInterviewByJobdetailsAndUserId")
    fun getInterviewDetail(
        @Query("job_detail_id") job_detail_id: Int?,
        @Query("user_id") user_id: Int?
    ): Call<SuccessResponse>?

    @GET("interview/")
    fun getInterviewTimeSlot(
        @Query("job_detail_id") job_detail_id: Int?,
        @Query("user_id") user_id: Int?
    ): Call<SuccessResponse>?

    @GET("notifications/unread/")
    fun getNotificationCount(
        @Query("user_id") user_id: Int?
    ): Call<SuccessResponse>?

    @GET("notifications/")
    fun getNotifications(
        @Query("user_id") user_id: Int?,
        @Query("is_alert") is_alert: Int?,
        @Query("is_task") is_task: Int?,
        @Query("page_no") page_no: Int?,
        @Query("rows") rows: Int?

    ): Call<NotificationResponse>?

    @POST("notifications/delete")
    fun deleteNotification(
        @Query("notification_id") notification_id: Int?,
        @Query("requested_by") requested_by: Int?
    ): Call<NotificationResponse>?

    @GET("job/assigner/assigned")
    fun getAssignedJobDetail(
        @Query("user_id") user_id: Int?,
        @Query("job_details_id") job_details_id: Int?
    ): Call<SuccessResponse>?

    @GET("referral/getReferralData")
    fun getReferralData(
        @Query("user_id") user_id: Int?
    ): Call<ReferralDataResponse>?

    @GET("referral/getReferralDetails")
    fun getReferralWorkerData(
        @Query("user_id") user_id: Int?,
        @Query("job_id") job_id: Int?
    ): Call<ReferralWorkerDataResponse>?

    @POST("referral/claimReferral")
    fun claimReferral(
        @Query("user_id") user_id: Int?,
        @Query("refer_by") refer_by: Int?,
        @Query("claim_amount") claim_amount: Int?,
        @Query("job_id") job_id: Int?
    ): Call<CliamResponse>?

    @POST("referral/raiseEnquiry")
    fun raiseEnquiry(
        @Query("requested_by") requested_by: Int?,
        @Query("enquiry_comments") enquiry_comments: String?,
        @Query("enquiry_request") enquiry_request: String?,
        @Query("referral_id") referral_id: String?
    ): Call<RaiseEnquiryResponse>?

    @GET("workermaster/jobTravelMode")
    fun getTravalMode(
        @Query("job_id") assignId: Int?
    ): Call<SuccessResponse>?

    @GET("job/checkIn/")
    fun getCheckInData(
        @Query("assign_id") assignId: Int?,
        @Query("user_id") user_id: Int?
    ): Call<ChekinResonse>?

    @POST("workermaster/jobTravelMode")
    fun setTravalMode(
        @Query("job_id") assignId: Int?,
        @Query("user_id") userId: Int?,
        @Query("worker_id") worker_id: Int?,
        @Query("travel_mode") travel_mode: Int?
    ): Call<SuccessResponse>?

    @POST("job/checkIn/")
    fun setCheckInData(
        @Query("check_in_id") check_in_id: Int?,
        @Query("updated_by") updated_by: Int?,
        @Query("otp") otp: String?
    ): Call<CheckOutResponse>?

    @POST("interview/verifyOtp")
    fun verifyInterviewOtp(
        @Query("interview_id") interview_id: Int?,
        @Query("requested_by") requested_by: Int?,
        @Query("otp") otp: String?
    ): Call<SuccessResponse>?

    @POST("job/checkOut")
    fun jobCheckOut(
        @Query("check_in_id") check_in_id: Int?,
        @Query("updated_by") updated_by: Int?
    ): Call<CheckOutResponse>?

    @POST("reviews/employer")
    fun saveReview(
        @Query("employer_id") employer_id: Int?,
        @Query("job_id") job_id: Int?,
        @Query("ratings") ratings: Int?,
        @Query("requested_by") requested_by: Int?,
        @Query("review") review: String?
    ): Call<SaveReviewResponse>?

    @GET("configmaster/?category_type=rating_point_employer&rows=20")
    fun getEmployerReviewOpt(
        @Query("type_key") type_key: String?
    ): Call<EmployerReviewResponse>?

    @FormUrlEncoded
    @POST("reviews/employer/points")
    fun saveEmployerReviewPt(@FieldMap params: Map<String, String>?): Call<EmployerPointsResponse>

    @POST("workermaster/ODWorker/liveLocation/save")
    fun saveLocationForOd(
        @Query("user_id") user_id: Int?,
        @Query("latitude") latitude: Double?,
        @Query("speed") speed: Float?,
        @Query("worker_name") worker_name: String?,
        @Query("time_millies") time_millies: Long?,
        @Query("longitude") longitude: Double?,
        @Query("job_id") job_id: Int?,
        @Query("distance") distance: String?,
        @Query("eta") eta: String?
    ): Call<ODLocationResponse>

    @GET("question_controller/getJobQuestion")
    fun getJobQuestion(
        @Query("job_id") job_id: Int?
    ): Call<QuestionareResponse>

    @POST("question_controller/workerJobQuestion")
    fun saveJobQuestion(
        @Query("job_id") job_id: Int?,
        @Query("job_details_id") job_details_id: Int?,
        @Query("user_id") user_id: Int?,
        @Body request: ArrayList<QuestionareRequestItem?>
    ): Call<SaveQuestionareResponse>

    @POST("admin/V2/addWorker/{worker_id}")
    fun saveWorkerData(
        @Path("worker_id") worker_id: Int?,
        @Body request: UpdateWorkerRequest?
    ): Call<WorkerDetailResponse>

    @GET("maps/api/directions/json")
    fun getDirectionApi(
        @Query("origin") origin: String?,
        @Query("destination") destination: String?,
        @Query("key") key: String?
    ): Call<DirectionApiResponse>

    @GET("/job/assigner/requiredSkills")
    fun getRequiredSkills(
        @Query("job_id") job_id: Int?,
        @Query("worker_id") worker_id: Int?
    ): Call<ReqiredSkillsResponse>

    @GET("/configmaster/getAllJobType")
    fun getAllJobRole(): Call<AllJobRoleResponse>

    @GET("/workermaster/getUPI")
    fun getUPI(
        @Query("worker_id") worker_id: Int?
    ): Call<UpiRespnse>

    @POST("/workermaster/addEditUPI")
    fun updateUPI(
        @Query("upi") upi: String?,
        @Query("worker_id") worker_id: Int?
    ): Call<UpiRespnse>

    @GET("/job/getJobCounts")
    fun getJobCounts(
        @Query("user_id") user_id: Int?
    ): Call<JobCount>

    @GET("/reviews/workerV2")
    fun getRatingReview(
        @Query("worker_id") worker_id: Int?
    ): Call<RatingReviewResponse>

    @POST("/workermaster/ODWorker/liveLocation/save")
    fun saveLiveLocation(
        @Query("user_id") user_id: Int?,
        @Query("worker_name") worker_name: String?,
        @Query("time_millies") time_millies: Long?,
        @Query("latitude") latitude: Double?,
        @Query("longitude") longitude: Double?,
        @Query("speed") speed: Float?,
        @Query("job_id") job_id: Int?,
        @Query("eta") eta: String?,
        @Query("distance") distance: String?
    ): Call<ODLocationResponse>
}