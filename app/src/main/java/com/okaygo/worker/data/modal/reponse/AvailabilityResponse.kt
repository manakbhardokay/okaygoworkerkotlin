package com.okaygo.worker.data.modal.reponse

data class AvailabilityResponse(
    val code: Int?,
    val message: String?,
    val response: Availability?
)
