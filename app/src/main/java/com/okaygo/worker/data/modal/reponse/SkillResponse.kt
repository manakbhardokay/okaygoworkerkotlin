package com.okaygo.worker.data.api.response

data class SkillResponse(
    val job_category_id: Int?,
    val job_type_id: Int?,
    val job_type_name: String?,
    val skills_master_id: Int?,
    val skills_name: String?,
    val skills_type_id: Int?,
    val status: Int?
)