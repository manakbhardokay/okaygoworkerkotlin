package com.okaygo.worker.data.modal.request

data class QuestionareRequestItem(
    val answer: Int?,
    val inserted_by: Int?,
    val job_Question_id: Int?,
    val updated_by: Int?,
    val user_id: Int?

)