package com.okaygo.worker.data.modal.reponse

data class DetailSkill(
    val insertedBy: Int,
    val insertedOn: String,
    val jobDetailsId: Int,
    val jobId: Int,
    val jobSkillsId: Int,
    val skillName: String,
    val skillValue: String,
    val skillsTypeId: Int?,
    val updatedBy: Int,
    val updatedOn: String
)
