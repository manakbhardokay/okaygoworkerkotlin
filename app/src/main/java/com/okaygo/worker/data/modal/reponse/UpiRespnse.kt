package com.okaygo.worker.data.modal.reponse

data class UpiRespnse(
    val code: Int?,
    val message: String?,
    val response: String?
)