package com.okaygo.worker.data.modal.request

data class AvailbilityRequest(
    val userId: Int?,
    val workdays_details: String?,
    val worker_id: Int?,
    val availablity_id: String?,
    val login_time: String?,
    val logout_time: String?,
    val active_status: Int?
)