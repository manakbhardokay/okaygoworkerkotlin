package com.okaygo.worker.data.modal.reponse

data class RatingResponse(
    val averageRating: Double?,
    val reviews: ReviewsList?
)