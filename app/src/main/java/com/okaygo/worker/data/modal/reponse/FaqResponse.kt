package com.okaygo.worker.data.modal.reponse

data class FaqResponse(
    val code: Int?,
    val message: String?,
    val response: FResponse?
)

data class FResponse(val content: ArrayList<FAQ>?)