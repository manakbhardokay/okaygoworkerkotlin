package com.okaygo.worker.data.modal.reponse

data class WorkerResponse(
    val code: Int?,
    val message: String?,
    val response: WResponse?
)
