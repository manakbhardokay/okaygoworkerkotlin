package com.okaygo.worker.data.modal.reponse

data class LocationResponse(
    val latitude: String?,
    val live_loc_id: Int?,
    val longitude: String?,
    val speed: String?,
    val time_millies: Long?,
    val user_id: Int?,
    val worker_name: String?
)