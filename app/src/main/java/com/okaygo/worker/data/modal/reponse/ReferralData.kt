package com.okaygo.worker.data.modal.reponse

data class ReferralData(
    val brandName: String?,
    val company: String?,
    val count: Int?,
    val jobDetailsId: Int?,
    val jobId: Int?,
    val jobTitle: String?
)