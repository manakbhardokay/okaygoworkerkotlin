package com.openkey.guest.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

/**
 * @author Openkey Inc.
 */
open class MyViewModel(app: Application) : AndroidViewModel(app) {
    var apiError = MutableLiveData<String>()
    var isLoading = MutableLiveData<Boolean>()
}