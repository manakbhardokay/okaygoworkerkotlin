package com.okaygo.worker.data.modal.reponse

data class Warning(
    val warningCode: String?,
    val warningMessage: String?
)