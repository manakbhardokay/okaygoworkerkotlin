package com.okaygo.worker.data.modal.reponse

data class Reviews(
    val brandName: String?,
    val companyLogo: String?,
    val companyName: String?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val jobDetailsId: Int?,
    val ratings: Double?,
    val review: String?,
    val reviewId: Int?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?,
    val workType: Int?,
    val workerId: Int?
)
