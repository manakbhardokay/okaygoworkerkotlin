package com.okaygo.worker.data.api.response

data class UserSkills(
    val insertedBy: Int?,
    val insertedOn: String?,
    val skillsName: String?,
    val skillsTypeId: Int?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?,
    val userSkillsId: Int?
)