package com.okaygo.worker.data.modal.reponse

data class SaveReviewResponse(
    val code: Int?,
    val message: String?,
    val response: Review?
)
