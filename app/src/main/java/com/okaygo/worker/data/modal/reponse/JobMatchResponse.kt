package com.okaygo.worker.data.modal.reponse

data class JobMatchResponse(
    val code: Int?,
    val message: String?,
    val response: MatchResponse?
)