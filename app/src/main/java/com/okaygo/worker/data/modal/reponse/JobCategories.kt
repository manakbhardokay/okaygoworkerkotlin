package com.okaygo.worker.data.modal.reponse

import android.os.Parcel
import android.os.Parcelable

data class JobCategories(
    val count: Int?,
    val id: Int?,
    val type_desc: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(count)
        parcel.writeValue(id)
        parcel.writeString(type_desc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<JobCategories> {
        override fun createFromParcel(parcel: Parcel): JobCategories {
            return JobCategories(parcel)
        }

        override fun newArray(size: Int): Array<JobCategories?> {
            return arrayOfNulls(size)
        }
    }
}