package com.okaygo.worker.data.modal.reponse

data class JobCatResponse(
    val content: ArrayList<SelectedJobCat>?,
    val empty: Boolean?,
    val first: Boolean?,
    val last: Boolean?,
    val number: Int?,
    val numberOfElements: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)