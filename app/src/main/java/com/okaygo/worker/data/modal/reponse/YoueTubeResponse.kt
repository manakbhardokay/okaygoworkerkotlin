package com.okaygo.worker.data.modal.reponse

data class YoueTubeResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<Youtube>?
)