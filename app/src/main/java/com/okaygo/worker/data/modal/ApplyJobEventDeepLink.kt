package com.okaygo.worker.data.modal

import com.okaygo.worker.data.modal.reponse.DetailContent
import java.io.Serializable

data class ApplyJobEventDeepLink(
    val content: DetailContent?,
    val cvFileName: String?,
    val assignId: Int?
) : Serializable