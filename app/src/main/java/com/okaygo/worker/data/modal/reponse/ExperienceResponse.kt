package com.okaygo.worker.data.modal.reponse

data class ExperienceResponse(
    val code: Int?,
    val message: String?,
    val response: ExpResponse?
)
