package com.okaygo.worker.data.modal.reponse

data class Stats(
    val lastPaidAmount: Double?,
    val lastPaidDate: Long?,
    val toBePaid: Double?
)