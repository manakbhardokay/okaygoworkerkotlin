package com.okaygo.worker.data.modal

data class ErrorResponse(
    val timestamp: Long?,
    val message: String?,
    val error: String?,
    val details: String?
)