package com.okaygo.worker.data.modal.request

data class UpdateWorkerRequest(
    val addressLine1: String? = null,
    val addressLine2: String? = null,
    val availableFt: Int? = null,
    val availableOd: Int? = null,
    val availablePt: Int? = null,
    val bike_license: Int? = null,
    val city: String? = null,
    val cityId: Int? = null,
    val countryCode: String? = null,
    val cvLink: String? = null,
    val cvName: String? = null,
    val dateOfBirth: String? = null,
    val emailId: String? = null,
    val english_known_level: Int? = null,
    val expected_salary: Int? = null,
    val expected_salary_type: Int? = null,
    val firstName: String? = null,
    val gender: String? = null,
    val globalAvailability: Int? = null,
    val hasExperience: Int? = null,
    val interested_cat: String? = null,
    val interested_roles: String? = null,
    val jobInterests: ArrayList<JobInterest>? = null,
    val languages_known: String? = null,
    val lastName: String? = null,
    val last_salary: Int? = null,
    val last_salary_type: Int? = null,
    val notice_period: String? = null,
    val own_aadhar_card: Int? = null,
    val own_bike: Int? = null,
    val own_laptop: Int? = null,
    val own_pan_card: Int? = null,
    val own_smartphone: Int? = null,
    val own_vehicle_rc: Int? = null,
    val own_wifi: Int? = null,
    val paytmNumber: String? = null,
    val phoneNumber: String? = null,
    val pincode: Int? = null,
    val postalCode: String? = null,
    val preferred_location: String? = null,
    val profilePhoto: String? = null,
    val profilePhotoName: String? = null,
    val qualificationId: Int? = null,
    val requestedBy: Int? = null,
    val skills: ArrayList<UpdateSkill>? = null,
    val specialisation: String? = null,
    val spokenLanguage: String? = null,
    val state: String? = null,
    val total_experience: String? = null,
    val upi_id: String? = null,
    val userDocuments: ArrayList<UserDocument>? = null,
    val userGoogleLocation: String? = null,
    val userStatus: Int? = null,
    val whatsappNumber: String? = null,
    val workerAvailabilities: ArrayList<WorkerAvailability>? = null,
    val workerExperiences: ArrayList<WorkerExperienceUpdate>? = null,
    val year_of_passing: Int? = null
)

data class JobInterest(
    val jobInterestId: Int? = null,
    val jobType: Int? = null
)


data class UserDocument(
    val b64EncodedDocument: String? = null,
    val documentId: Int? = null,
    val documentName: String? = null,
    val documentTypeId: Int? = null,
    val fileMimeType: String? = null
)

data class WorkerAvailability(
    val active_status: Int? = null,
    val availabilityId: Int? = null,
    val availabilityType: Int? = null,
    val loginTime: LoginTime? = null,
    val logoutTime: LogoutTime? = null,
    val workdaysDetails: String? = null
)


data class LoginTime(
    val date: Int? = null,
    val day: Int? = null,
    val hours: Int? = null,
    val minutes: Int? = null,
    val month: Int? = null,
    val seconds: Int? = null,
    val time: Int? = null,
    val timezoneOffset: Int? = null,
    val year: Int? = null
)

data class LogoutTime(
    val date: Int? = null,
    val day: Int? = null,
    val hours: Int? = null,
    val minutes: Int? = null,
    val month: Int? = null,
    val seconds: Int? = null,
    val time: Int? = null,
    val timezoneOffset: Int? = null,
    val year: Int? = null
)