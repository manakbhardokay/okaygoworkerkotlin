package com.okaygo.worker.data.modal.reponse

data class OfferAcceptResponse(
    val code: Int?,
    val message: String?,
    val response: OfferResponse?
)

data class OfferResponse(
    val content: ArrayList<AcceptOffer>?,
    val empty: Boolean?,
    val first: Boolean,
    val last: Boolean,
    val number: Int,
    val numberOfElements: Int,
    val totalElements: Int,
    val totalPages: Int
)
