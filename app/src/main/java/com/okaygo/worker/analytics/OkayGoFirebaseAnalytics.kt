package com.okaygo.worker.analytics

import android.os.Bundle
import android.util.Log
import com.okaygo.worker.BuildConfig
import com.okaygo.worker.help.utils.Constants
import com.openkey.guest.application.OkayGo
import com.openkey.guest.help.Preferences

object OkayGoFirebaseAnalytics {
    const val ON_BOARDING_MOBILE_NUMBER = "onboarding_mobile_no"
    const val LOGIN_MOBILE_NUMBER = "login_mobile_no"
    private val TAG = OkayGoFirebaseAnalytics::class.java.simpleName
    private const val ON_BOARDING_PERSONAL_DETAILS_KEY = "onboarding_personal_details"
    private const val ON_BOARDING_INTRESTED_CAT_KEY = "onboarding_interested_cat"
    private const val ON_BOARDING_AADHAR_CARD_KEY = "onboarding_aadhar_card"
    private const val ON_BOARDING_EDUCATION_KEY = "onboarding_education"
    private const val ON_BOARDING_EXPERIENCE_KEY = "onboarding_experience"
    private const val ON_BOARDING_SKILLS_KEY = "onboarding_skills"
    private const val ON_BOARDING_AVAILIBILITY_KEY = "onboarding_availability"
    private const val ON_BOARDING_SELFIE_KEY = "onboarding_selfie"
    private const val ON_JOB_APPLY = "job_apply"
    private const val ON_INTERVIEW_SELECT_SLOT = "interview_select_slot"
    private const val ON_INTERVIEW_ENTER_OTP = "interview_enter_otp"
    private const val ON_JOB_OFFER_ACCEPTED = "job_offer_accepted"
    private const val ON_JOB_OFFER_REJECTED = "job_offer_rejected"
    private const val ON_BOARDING_PASSWORD = "onboarding_password"
    private const val LOGIN_PASSWORD = "login_password"
    private const val ON_FIND_JOBS = "find_jobs"
    private const val ON_JOB_DETAIL = "job_detail_view"
    private const val ON_COMPANY_DETAIL = "company_detail_view"
    private const val FIND_JOB_FILTER_APPLIED = "find_jobs_filters_applied"
    private const val FIND_JOBS_CHANGE_JOB_TYPE = "find_jobs_change_job_type"
    private const val MY_JOBS = "myjobs"
    private const val MY_JOBS_UPCOMING = "myjobs_upcoming"
    private const val MY_JOBS_PAST = "myjobs_past"
    private const val INTERVIEW_CANCLE = "interview_cancel"
    private const val AVAILBILITY_ODAY_ON = "availability_today_on"
    private const val AVAILBILITY_ODAY_OFF = "availability_today_off"
    private const val AVAILBILITY_VIEW = "availability_view"
    private const val AVAILBILITY_ADD = "availability_add"
    private const val AVAILBILITY_EDIT = "availability_edit"
    private const val AVAILBILITY_DELETE = "availability_delete"
    private const val AVAILBILITY_ALL_TIME = "availability_alltime"
    private const val IN_APP_NOTIFICATION_VIEW = "inapp_notification_view"
    private const val IN_APP_ALERT_VIEW = "inapp_alert_view"
    private const val CLICK_ON_HELP = "help_section"
    private const val CLICK_ON_VIDEO = "help_video"
    private const val DEEPLINK_CLICK = "deeplink_click"
    private const val DEEPLINK_CLICK_REFER_JOB = "refer_job_deeplink_click"
    private const val FIND_JOB_CATEGORY = "find_job_categories"
    private const val JOB_REFFERAL = "job_referral"
    private const val POST_APPLICATION_ALL_INFO_COMPLETE = "post_application_all_info_has_complete"
    private const val POST_APPLICATION_EDIT_INFO = "post_application_edit_info"
    private const val POST_APPLICATION_EDIT_EXP = "post_application_edit_experience"
    private const val POST_APPLICATION_ON_QUESTIONNAIRE = "post_application_on_questionnaire"
    private const val POST_APPLICATION_SUBMIT_QUESTIONNAIRE =
        "post_application_submit_questionnaire"
    private const val VIEW_INVOICE = "view_invoice"

    var mBundle: Bundle? = null

    init {
        mBundle = Bundle()
    }

    fun commonData() {
        if (mBundle == null) {
            mBundle = Bundle()
        }
        mBundle?.putString(
            "employee_id",
            Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0).toString() + ""
        )
        mBundle?.putString("utm_source", Preferences.prefs?.getString(Constants.UTM_SOURCE, ""))
        mBundle?.putString("utm_medium", Preferences.prefs?.getString(Constants.UTM_MEDIUM, ""))
        mBundle?.putString("utm_content", Preferences.prefs?.getString(Constants.UTM_CONTENT, ""))
        mBundle?.putString("utm_campaign", Preferences.prefs?.getString(Constants.UTM_CAMPAIGN, ""))
        mBundle?.putString("utm_term", Preferences.prefs?.getString(Constants.UTM_TERM, ""))
        mBundle?.putString("anid", Preferences.prefs?.getString(Constants.ANID, ""))
    }

    fun deeplinkClick(
        utmSource: String?,
        utm_medium: String?,
        utm_content: String?,
        utm_campaign: String?,
        utm_term: String?
    ) {
        if (mBundle == null) {
            mBundle = Bundle()
        }
        mBundle?.putString("is_from_deeplink", true.toString() + "")
        mBundle?.putString(
            "employee_id",
            Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0).toString() + ""
        )
        mBundle?.putString("user_id", Preferences.prefs?.getInt(Constants.ID, 0)?.toString() + "")
        mBundle?.putString("utm_source", utmSource)
        mBundle?.putString("utm_medium", utm_medium)
        mBundle?.putString("utm_content", utm_content)
        mBundle?.putString("utm_campaign", utm_campaign)
        mBundle?.putString("utm_term", utm_term)
        mBundle?.putString("anid", Preferences.prefs?.getString(Constants.ANID, "").toString() + "")
        OkayGo.firebaseAnalytics?.logEvent(DEEPLINK_CLICK, mBundle)
    }

    fun deeplinkClickReferJob(
        job_id: String?,
        job__detail_id: String?,
        refer_by: String?,
        utmSource: String?,
        utm_medium: String?,
        utm_content: String?,
        utm_campaign: String?,
        utm_term: String?
    ) {
        if (mBundle == null) {
            mBundle = Bundle()
        }
        mBundle?.putString("is_from_deeplink", true.toString() + "")
        mBundle?.putString(
            "employee_id",
            Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0).toString() + ""
        )
        mBundle?.putString("user_id", Preferences.prefs?.getInt(Constants.ID, 0).toString() + "")
        mBundle?.putString("job_id", job_id)
        mBundle?.putString("job_detail_id", job__detail_id)
        mBundle?.putString("refer_by", refer_by)
        mBundle?.putString("utm_source", utmSource)
        mBundle?.putString("utm_medium", utm_medium)
        mBundle?.putString("utm_content", utm_content)
        mBundle?.putString("utm_campaign", utm_campaign)
        mBundle?.putString("utm_term", utm_term)
        mBundle?.putString("anid", Preferences.prefs?.getString(Constants.ANID, "").toString() + "")
        OkayGo.firebaseAnalytics?.logEvent(DEEPLINK_CLICK_REFER_JOB, mBundle)
    }

    fun on_boarding_personal_details() {
        commonData()
        Log.d(TAG, "Personal OnBoarding Logged.")
        OkayGo.firebaseAnalytics?.logEvent(
            ON_BOARDING_PERSONAL_DETAILS_KEY,
            mBundle
        )
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(
                ON_BOARDING_PERSONAL_DETAILS_KEY,
                mBundle
            )
        }

    }

    fun viewInvoice(invoiceNo: String?) {
        commonData()
        mBundle?.putString("invoiceNo", invoiceNo)
        OkayGo.firebaseAnalytics?.logEvent(VIEW_INVOICE, mBundle)
    }


    fun on_boarding_interested_cat() {
        commonData()
        Log.d(TAG, "intrsted act OnBoarding Logged.")
        OkayGo.firebaseAnalytics?.logEvent(
            ON_BOARDING_INTRESTED_CAT_KEY,
            mBundle
        )
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(
                ON_BOARDING_INTRESTED_CAT_KEY,
                mBundle
            )
        }
    }

    fun postApplicationAllInfoComplete(jobId: Int?) {
        commonData()
        Log.d(TAG, "post application all info Logged.")
        mBundle?.putString("jobId", jobId?.toString())
        OkayGo.firebaseAnalytics?.logEvent(
            POST_APPLICATION_ALL_INFO_COMPLETE,
            mBundle
        )
    }

    fun postApplicationEditInfo(jobId: Int?) {
        commonData()
        Log.d(TAG, "post application edit info Logged.")
        mBundle?.putString("jobId", jobId?.toString())
        OkayGo.firebaseAnalytics?.logEvent(
            POST_APPLICATION_EDIT_INFO,
            mBundle
        )
    }

    fun postApplicationEditExp(jobId: Int?) {
        commonData()
        Log.d(TAG, "post application edit exp Logged.")
        mBundle?.putString("jobId", jobId?.toString())
        OkayGo.firebaseAnalytics?.logEvent(
            POST_APPLICATION_EDIT_EXP,
            mBundle
        )
    }

    fun postApplicationOnQuestionnaire(jobId: Int?) {
        commonData()
        Log.d(TAG, "post application on questionare Logged.")
        mBundle?.putString("jobId", jobId?.toString())
        OkayGo.firebaseAnalytics?.logEvent(
            POST_APPLICATION_ON_QUESTIONNAIRE,
            mBundle
        )
    }

    fun postApplicationSubmitQuestionnaire(jobId: Int?) {
        commonData()
        Log.d(TAG, "post application submit questionare Logged.")
        mBundle?.putString("jobId", jobId?.toString())
        OkayGo.firebaseAnalytics?.logEvent(
            POST_APPLICATION_SUBMIT_QUESTIONNAIRE,
            mBundle
        )
    }

    fun clickOnHelpVideo(videoName: String?) {
        commonData()
        mBundle?.putString("video_title", videoName)
        Log.d(TAG, "Personal OnBoarding Logged.")
        OkayGo.firebaseAnalytics?.logEvent(CLICK_ON_VIDEO, mBundle)

    }

    fun clickOnHelp() {
        commonData()
        Log.d(TAG, "Personal OnBoarding Logged.")
        OkayGo.firebaseAnalytics?.logEvent(CLICK_ON_HELP, mBundle)

    }


    fun on_boarding_education_details() {
        commonData()
        Log.d(TAG, "Education Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_BOARDING_EDUCATION_KEY, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(ON_BOARDING_EDUCATION_KEY, mBundle)
        }
    }

    fun on_boarding_experience_details(is_having_experience: Boolean) {
        commonData()
        mBundle?.putString("is_added_experience", is_having_experience.toString())
        Log.d(TAG, "Experience Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_BOARDING_EXPERIENCE_KEY, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(ON_BOARDING_EXPERIENCE_KEY, mBundle)
        }

    }

    fun on_boarding_skills_details() {
        commonData()
        Log.d(TAG, "Experience Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_BOARDING_SKILLS_KEY, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(ON_BOARDING_SKILLS_KEY, mBundle)
        }

    }

    fun on_boarding_availibility_details(is_all_time_free: Boolean) {
        commonData()
        mBundle?.putString("is_all_time_free", is_all_time_free.toString())
        Log.d(TAG, "Availibility Event Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_BOARDING_AVAILIBILITY_KEY, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(
                ON_BOARDING_AVAILIBILITY_KEY,
                mBundle
            )
        }

    }

    fun on_boarding_selfie_details(skipped_or_saved: String?) {
        commonData()
        mBundle?.putString("selfie", skipped_or_saved)
        Log.d(TAG, "Selfie Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_BOARDING_SELFIE_KEY, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(ON_BOARDING_SELFIE_KEY, mBundle)
        }

    }

    fun on_job_apply(
        work_type: String?,
        job_type: String?,
        job_id: String?,
        from: String?
    ) {
        commonData()
        mBundle?.putString("work_type", work_type)
        mBundle?.putString("job_type", job_type)
        mBundle?.putString("job_id", job_id)
        mBundle?.putString("from", from)
        Log.d(TAG, "Job Apply Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_JOB_APPLY, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(ON_JOB_APPLY, mBundle)
        }

    }

    fun on_interview_select_slot(job_id: String?, from: String?) {
        commonData()
        mBundle?.putString("job_id", job_id)
        mBundle?.putString("from", from)
        Log.d(TAG, "Slot Selected Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_INTERVIEW_SELECT_SLOT, mBundle)

    }

    fun on_interview_enter_otp(job_id: String?, from: String?) {
        commonData()
        mBundle?.putString("job_id", job_id)
        mBundle?.putString("from", from)
        Log.d(TAG, "Enter OTP Interview Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_INTERVIEW_ENTER_OTP, mBundle)

    }

    fun on_accept_job_offer(
        job_id: String?,
        job_type: String?,
        from: String?,
        comission: Int
    ) {
        // from should be outside or inside
        commonData()
        mBundle?.putString("job_id", job_id)
        mBundle?.putString("job_type", job_type)
        mBundle?.putString("from", from)
        mBundle?.putInt("comission", comission)
        Log.d(TAG, "Accept Job Offer Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_JOB_OFFER_ACCEPTED, mBundle)

    }

    fun on_reject_job_offer(
        job_id: String?,
        job_type: String?,
        from: String?
    ) {
        // from should be outside or inside
        commonData()
        mBundle?.putString("job_id", job_id)
        mBundle?.putString("job_type", job_type)
        mBundle?.putString("from", from)
        Log.d(TAG, "Reject Job Offer Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_JOB_OFFER_REJECTED, mBundle)

    }

    fun onboarding_mobile_no(mobile_no: String?) {
        commonData()
        mBundle?.putString("mobile_no", mobile_no)
        Log.d(
            TAG,
            "onboarding_mobile_no Logged."
        )
        OkayGo.firebaseAnalytics?.logEvent(ON_BOARDING_MOBILE_NUMBER, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(ON_BOARDING_MOBILE_NUMBER, mBundle)
        }

    }

    fun login_mobile_no(mobile_no: String?) {
        commonData()
        mBundle?.putString("mobile_no", mobile_no)
        OkayGo.firebaseAnalytics?.logEvent(LOGIN_MOBILE_NUMBER, mBundle)
    }

    fun onboarding_password() {
        commonData()
        OkayGo.firebaseAnalytics?.logEvent(ON_BOARDING_PASSWORD, mBundle)
        if (BuildConfig.FLAVOR == "aws") {
            OkayGo.fbLogger?.logEvent(ON_BOARDING_PASSWORD, mBundle)
        }
    }

    fun login_password() {
        commonData()
        OkayGo.firebaseAnalytics?.logEvent(LOGIN_PASSWORD, mBundle)
    }

    fun find_jobs(jobCat: String?) {
        commonData()
        mBundle?.putString("job_category", jobCat)
        Log.d(TAG, "find_jobs Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_FIND_JOBS, mBundle)

    }

    fun job_detail_view(
        job_category: String?,
        job_id: String?,
        job_type: String?,
        job_location_id: String?,
        from: String?
    ) {
        commonData()
        mBundle?.putString("job_category", job_category)
        mBundle?.putString("job_id", job_id)
        mBundle?.putString("job_type", job_type)
        mBundle?.putString("job_location_id", job_location_id)
        mBundle?.putString("from", from)
        Log.d(TAG, "job_detail_view Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_JOB_DETAIL, mBundle)
    }

    fun company_detail_view(company_id: String?, from: String?) {
        commonData()
        mBundle?.putString("company_id", company_id)
        mBundle?.putString("from", from)
        Log.d(TAG, "company_detail_view Logged.")
        OkayGo.firebaseAnalytics?.logEvent(ON_COMPANY_DETAIL, mBundle)

    }

    fun find_jobs_filters_applied(
        job_exp_ids: String?,
        job_cities_id: String?,
        job_end_time: String?,
        pay_min: String?,
        pay_max: String?
    ) {
        commonData()
        //        mBundle.putString("job_title_ids", job_title_ids);
//        mBundle.putString("job_start_time", job_start_time);
//        mBundle.putString("job_end_time", job_end_time);
//        mBundle.putString("distance_min", distance_min);
//        mBundle.putString("distance_max", distance_max);
        mBundle?.putString("job_exp_ids", job_exp_ids)
        mBundle?.putString("job_cities_id", job_cities_id)
        mBundle?.putString("pay_min", pay_min)
        mBundle?.putString("pay_max", pay_max)
        Log.d(
            TAG,
            "find_jobs_filters_applied Logged."
        )
        OkayGo.firebaseAnalytics?.logEvent(FIND_JOB_FILTER_APPLIED, mBundle)

    }

    fun find_jobs_change_job_type(job_category: String?) {
        commonData()
        mBundle?.putString("job_category", job_category)
        Log.d(
            TAG,
            "find_jobs_change_job_type Logged."
        )
        OkayGo.firebaseAnalytics?.logEvent(FIND_JOBS_CHANGE_JOB_TYPE, mBundle)

    }

    fun myjobs() {
        commonData()
        Log.d(TAG, "myjobs Logged.")
        OkayGo.firebaseAnalytics?.logEvent(MY_JOBS, mBundle)

    }

    fun myjobs_upcoming() {
        commonData()
        Log.d(TAG, "myjobs_upcoming Logged.")
        OkayGo.firebaseAnalytics?.logEvent(MY_JOBS_UPCOMING, mBundle)

    }

    fun myjobs_past() {
        commonData()
        Log.d(TAG, "myjobs_past Logged.")
        OkayGo.firebaseAnalytics?.logEvent(MY_JOBS_PAST, mBundle)

    }

    fun interview_cancel(job_id: String?) {
        commonData()
        mBundle?.putString("job_id", job_id)
        Log.d(TAG, "interview_cancel Logged.")
        OkayGo.firebaseAnalytics?.logEvent(INTERVIEW_CANCLE, mBundle)

    }

    fun availability_today_on(current_time: String?) {
        commonData()
        mBundle?.putString("current_time", current_time)
        Log.d(
            TAG,
            "availability_today_on Logged."
        )
        OkayGo.firebaseAnalytics?.logEvent(AVAILBILITY_ODAY_ON, mBundle)

    }

    fun availability_today_off(current_time: String?) {
        commonData()
        mBundle?.putString("current_time", current_time)
        Log.d(
            TAG,
            "availability_today_off Logged."
        )
        OkayGo.firebaseAnalytics?.logEvent(AVAILBILITY_ODAY_OFF, mBundle)

    }

    fun availability_view(from: String?) {
        commonData()
        mBundle?.putString("from", from)
        Log.d(TAG, "availability_view Logged.")
        OkayGo.firebaseAnalytics?.logEvent(AVAILBILITY_VIEW, mBundle)

    }

    fun availability_add(
        availability_time_from: String?,
        availability_time_to: String?,
        availablity_mon: String?,
        availability_tue: String?,
        availability_wed: String?,
        availability_thu: String?,
        availability_fri: String?,
        availability_sat: String?,
        availability_sun: String?
    ) {
        commonData()
        mBundle?.putString("availability_time_from", availability_time_from)
        mBundle?.putString("availability_time_to", availability_time_to)
        mBundle?.putString("availablity_mon", availablity_mon)
        mBundle?.putString("availability_tue", availability_tue)
        mBundle?.putString("availability_wed", availability_wed)
        mBundle?.putString("availability_thu", availability_thu)
        mBundle?.putString("availability_fri", availability_fri)
        mBundle?.putString("availability_sat", availability_sat)
        mBundle?.putString("availability_sun", availability_sun)
        Log.d(TAG, "availability_add Logged.")
        OkayGo.firebaseAnalytics?.logEvent(AVAILBILITY_ADD, mBundle)

    }

    fun availability_edit(
        availability_time_from: String?,
        availability_time_to: String?,
        availablity_mon: String?,
        availability_tue: String?,
        availability_wed: String?,
        availability_thu: String?,
        availability_fri: String?,
        availability_sat: String?,
        availability_sun: String?
    ) {
        commonData()
        mBundle?.putString("availability_time_from", availability_time_from)
        mBundle?.putString("availability_time_to", availability_time_to)
        mBundle?.putString("availablity_mon", availablity_mon)
        mBundle?.putString("availability_tue", availability_tue)
        mBundle?.putString("availability_wed", availability_wed)
        mBundle?.putString("availability_thu", availability_thu)
        mBundle?.putString("availability_fri", availability_fri)
        mBundle?.putString("availability_sat", availability_sat)
        mBundle?.putString("availability_sun", availability_sun)
        Log.d(TAG, "availability_edit Logged.")
        OkayGo.firebaseAnalytics?.logEvent(AVAILBILITY_EDIT, mBundle)

    }

    fun availability_delete(availability_id: String?) {
        commonData()
        mBundle?.putString("availability_id", availability_id)
        Log.d(TAG, "availability_delete Logged.")
        OkayGo.firebaseAnalytics?.logEvent(AVAILBILITY_DELETE, mBundle)

    }

    fun availability_alltime() {
        commonData()
        Log.d(
            TAG,
            "availability_alltime Logged."
        )
        OkayGo.firebaseAnalytics?.logEvent(AVAILBILITY_ALL_TIME, mBundle)

    }

    fun inapp_notification_view() {
        commonData()
        Log.d(
            TAG,
            "inapp_notification_view Logged."
        )
        OkayGo.firebaseAnalytics?.logEvent(IN_APP_NOTIFICATION_VIEW, mBundle)

    }

    fun inapp_alert_view() {
        commonData()
        Log.d(TAG, "inapp_alert_view Logged.")
        OkayGo.firebaseAnalytics?.logEvent(IN_APP_ALERT_VIEW, mBundle)

    }

    fun job_refferal(
        refferBy: String?,
        jobId: String?,
        from: String?
    ) {
        commonData()
        mBundle?.putString("reffer_by", refferBy)
        mBundle?.putString("jobId", jobId)
        mBundle?.putString("from", from)
        Log.e(TAG, "job_refferal Logged.")
        OkayGo.firebaseAnalytics?.logEvent(JOB_REFFERAL, mBundle)
    }

    fun find_job_categories() {
        commonData()
        Log.e(TAG, "find_job_categories Logged.")
        OkayGo.firebaseAnalytics?.logEvent(FIND_JOB_CATEGORY, mBundle)
    }

}