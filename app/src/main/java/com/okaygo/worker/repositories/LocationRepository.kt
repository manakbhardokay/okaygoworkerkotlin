package com.okaygo.worker.repositories

import android.content.Context
import com.okaygo.worker.data.database.LocationDao
import com.okaygo.worker.data.database.LocationData
import com.okaygo.worker.data.database.OkayGoDatabase
import com.okaygo.worker.help.utils.subscribeOnBackground

class LocationRepository(context: Context) {

    var db: LocationDao = OkayGoDatabase.getInstance(context).locationDao()

    fun insertLocation(locationData: LocationData) {
        try {
            subscribeOnBackground {
                db.insertLocationDetails(locationData)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

//    fun fetchLocation(): List<LocationData>? {
//        var locData: List<LocationData>? = null
//        try {
//            subscribeOnBackground {
//                locData = db.fecthAllLocData()
//            }
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
//        return locData
//    }
}