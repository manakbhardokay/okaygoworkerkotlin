package com.okaygo.worker.services

class LocationDTO(
    var userId: Int?,
    var workerName: String?,
    var latitude: Double?,
    var longitude: Double?,
    var speed: Float?,
    var timeMillies: Long?,
    var eta: String?,
    var distance: String?
)