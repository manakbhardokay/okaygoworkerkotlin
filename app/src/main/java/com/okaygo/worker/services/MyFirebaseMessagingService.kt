/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.okaygo.worker.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.freshchat.consumer.sdk.Freshchat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.okaygo.worker.R
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.TrackingUtility
import com.okaygo.worker.services.location.LocationTrackerService
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private var mDataMap: Map<String, String>? = null
    private var mTitle: String? = "OkayGo"
    private var mMessage: String? = null
    private var mPayloadType: String? = null

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.e("Token", "Refreshed token: $s")
    }

    private fun sendMessage() {
        val intent = Intent("custom-event-notification")
        // You can also include some extra data.
        intent.putExtra("message", "This is my message!")
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        /*
         **************** check user logged in or not
         */
        Log.e("data", remoteMessage.data.toString())
        if (Freshchat.isFreshchatNotification(remoteMessage)) {
            Freshchat.handleFcmMessage(applicationContext, remoteMessage)
        } else {
            if (mPayloadType.equals("silent", true)) {
                if (TrackingUtility.hasLocationPermissions(this) && !TrackingUtility.isServiceRunning(
                        applicationContext,
                        LocationTrackerService::class.java
                    )
                ) {
                    Intent(this, LocationTrackerService::class.java).also {
                        this.startService(it)
                    }
                }
            } else {
                sendMessage()

                // Check if message contains a data payload.
                if (remoteMessage.notification != null) {
                    mTitle = remoteMessage.notification?.title
                    mMessage = remoteMessage.notification?.body
                } else {
                    mTitle = remoteMessage.data["title"]
                    mMessage = remoteMessage.data["body"]
                }
                if (remoteMessage.data.isNotEmpty()) {
                    mDataMap = remoteMessage.data
                    val payloadType = mDataMap?.get("payloadType")
                }
                sendPush()
            }

        }
    }

    /**
     * *************** send push for normal events *****************
     */
    private fun sendPush() {
        val mChannelId = getString(R.string.default_notification_channel_id)
        val mImportance = NotificationManager.IMPORTANCE_HIGH
        val mChannelName = getString(R.string.app_name)
        val notiData = Gson().toJson(mDataMap)
        val mIntent = Intent(this, DashBoardActivity::class.java)
        mIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        mIntent.putExtra(Constants.NOTIFICATION_DATA, notiData)
        val NOTIID = System.currentTimeMillis().toInt()
        mIntent.action = Intent.ACTION_MAIN
        mIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pendingIntent = PendingIntent.getActivity(
            this, NOTIID /* SyncFriendsRequest code */, mIntent, PendingIntent.FLAG_UPDATE_CURRENT
                    or PendingIntent.FLAG_ONE_SHOT
        )
        val defaultSoundUri =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder =
            NotificationCompat.Builder(
                this,
                mChannelId
            ) //.setSmallIcon(getNotificationIcon())
                .setContentTitle(mTitle)
                .setContentText(mMessage)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.ic_logo_transarent)
            notificationBuilder.color = resources.getColor(R.color.colorPrimary)
        } else {
            notificationBuilder.setSmallIcon(R.drawable.ic_logo_transarent)
        }
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        /*
         **********************   push for android oreo version *************
         */if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                mChannelId, mChannelName, mImportance
            )
            notificationManager?.createNotificationChannel(mChannel)
        }
        //handle notification manager notify may null issue
        notificationManager?.notify(
            NOTIID /* ID of notification */,
            notificationBuilder.build()
        )
    }
}