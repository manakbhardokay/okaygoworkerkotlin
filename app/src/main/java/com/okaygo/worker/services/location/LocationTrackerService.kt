package com.okaygo.worker.services.location

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.Looper
import android.os.PowerManager
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import com.google.android.gms.location.*
import com.okaygo.worker.data.database.LocationData
import com.okaygo.worker.data.modal.direction_api_response.DirectionApiResponse
import com.okaygo.worker.data.modal.reponse.ODLocationResponse
import com.okaygo.worker.help.utils.Constants
import com.okaygo.worker.help.utils.TrackingUtility
import com.okaygo.worker.repositories.LocationRepository
import com.okaygo.worker.ui.activity.LauncherActivity
import com.openkey.guest.data.Api.ApiHelper
import com.openkey.guest.help.Preferences
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class LocationTrackerService : LifecycleService() {

    companion object {
        private val TAG = LocationTrackerService::class.java.simpleName
        private const val NOTIFICATION_CHANNEL_ID = "channel_01"
        private const val NOTIFICATION_ID = 1101
        private const val EXTRA_STARTED_FROM_NOTIFICATION = "started_from_notification"
        private const val SMALLEST_DISPLACEMENT_100_METERS = 100F
        private const val INTERVAL_TIME = 1000L
        private const val FASTEST_INTERVAL_TIME = 500L

    }

    private var mEta: String? = "Calculating..."
    private var mDistance: String? = "Calculating..."

    private lateinit var locationRequest: LocationRequest
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var location: Location
    private lateinit var locationRepository: LocationRepository
    private var wakelock: PowerManager.WakeLock? = null

    override fun onCreate() {
        super.onCreate()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationRepository = LocationRepository(this)

        if (TrackingUtility.hasLocationPermissions(this)) {
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)
                    onNewLocation(locationResult!!.lastLocation)
                }
            }
            createLocationRequest()
            requestLocationUpdates()
        } else {
            removeLocationUpdates()
        }
        acquireWakeLock();

    }

    private fun acquireWakeLock() {
        val pm = getSystemService(POWER_SERVICE) as PowerManager
        if (pm != null) {
            wakelock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, javaClass.canonicalName)
            wakelock?.acquire(10000)
        }
    }

    private fun releaseWakeLock() {
        if (wakelock != null && wakelock?.isHeld == true) {
            wakelock?.release()
            wakelock = null
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startForegroundService()
        return super.onStartCommand(intent, flags, START_NOT_STICKY)
    }


    override fun onDestroy() {
        removeLocationUpdates()
        releaseWakeLock();
        super.onDestroy()
    }


    private fun requestLocationUpdates() {
//        Toast.makeText(this, "requestLocationUpdates ", Toast.LENGTH_SHORT).show()

        Log.v(TAG, "Requesting location updates")
        try {
            fusedLocationClient.requestLocationUpdates(
                locationRequest, locationCallback, Looper.getMainLooper()
            )
        } catch (unlikely: SecurityException) {
            Log.e(TAG, unlikely.message!!)
        }
    }


    private fun removeLocationUpdates() {
        Log.v(TAG, "Removing location updates")
        try {
            fusedLocationClient.removeLocationUpdates(locationCallback)
            stopSelf()
        } catch (unlikely: SecurityException) {
            Log.e(TAG, unlikely.message!!)
        }
    }

    private fun onNewLocation(location: Location) {
        Log.e(TAG, "new Location " + location.latitude)
        this.location = location
        val locationId = location.latitude.toString()
//        Toast.makeText(
//            this,
//            "Your location changed and latitude " + location.latitude,
//            Toast.LENGTH_SHORT
//        ).show()
        locationRepository.insertLocation(
            LocationData(
                location.latitude,
                location.longitude,
                "",
                Date().toString()
            )
        )
//        EventBus.getDefault().post(location)
//        val request = LocationDataRequest()
        val userId = Preferences.prefs?.getInt(Constants.ID, 0)
        val name = Preferences.prefs?.getString(Constants.userName, "")
        val jobId = Preferences.prefs?.getInt(Constants.TRACK_JOB_ID, 0)
//        request.add(
//            LocationDataRequestItem(
//                location.latitude.toString(),
//                location.longitude.toString(),
//                System.currentTimeMillis(),
//                userId
//            )
//        )
        val origin = location.latitude.toString() + "," + location.longitude
        val dest = Preferences.prefs?.getString(Constants.JOB_DEST, "0.0,0.0")
        val mapKey = Preferences.prefs?.getString(Constants.MAP_KEY, "")
        getDirectionDuration(origin, dest, mapKey)

        updateLocationData(
            userId,
            name,
            System.currentTimeMillis(),
            location.latitude,
            location.longitude,
            location.speed,
            jobId,
            mEta, mDistance
        )
    }

    fun updateLocationData(
        user_id: Int?,
        worker_name: String?,
        time_millies: Long?,
        latitude: Double?,
        longitude: Double?,
        speed: Float?,
        job_id: Int?,
        eta: String?,
        distance: String?
    ) {
        Log.e("updateLocationData", "Dashboard")

        val mService = ApiHelper.getService()
        mService.saveLiveLocation(
            user_id,
            worker_name,
            time_millies,
            latitude,
            longitude,
            speed,
            job_id,
            eta,
            distance
        )
            .enqueue(object : Callback<ODLocationResponse> {
                override fun onResponse(
                    call: Call<ODLocationResponse>?,
                    response: Response<ODLocationResponse>?
                ) {
                    Log.e("location api  success::", "Dashboard true")
                }

                override fun onFailure(call: Call<ODLocationResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("location api  failure::", it.message.orEmpty() + " Dashboard")

                    }
                }
            })
    }

    fun getDirectionDuration(
        origin: String?,
        dest: String?,
        key: String?
    ) {
        Log.e("Direction api", "Service")

        val mService = ApiHelper.getDirectionService()
        mService.getDirectionApi(
            origin, dest, key
        )
            .enqueue(object : Callback<DirectionApiResponse> {
                override fun onResponse(
                    call: Call<DirectionApiResponse>?,
                    response: Response<DirectionApiResponse>?
                ) {
                    response?.body()?.let {

                        if (it?.routes?.isEmpty() == false) {
                            if (it.routes?.get(0)?.legs?.isEmpty() == false) {
                                mEta = it.routes?.get(0)?.legs?.get(0)?.duration?.text
                                mDistance = it.routes?.get(0)?.legs?.get(0)?.distance?.text
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<DirectionApiResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("location api  failure::", it.message.orEmpty() + " Dashboard")

                    }
                }
            })
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            smallestDisplacement = 5f
            interval = 10 * 60 * 1000
            fastestInterval = 5 * 60 * 1000

        }
    }


    private fun startForegroundService() {

        val notificationManager = getSystemService(NOTIFICATION_SERVICE)
                as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(notificationManager)
        }
        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setAutoCancel(false) //para que siempre este visible si lo tocamos
            .setOngoing(true) //swipe away
            .setContentTitle("Location Tracking")
            .setContentText("Keep Going......!!!")
            .setContentIntent(getMainActivity())


        startForeground(NOTIFICATION_ID, notificationBuilder.build())

    }

    private fun getMainActivity() = PendingIntent.getActivity(
        this,
        0,
        Intent(this, LauncherActivity::class.java),
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(notificationManager: NotificationManager) {
        val channel = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            EXTRA_STARTED_FROM_NOTIFICATION,
            NotificationManager.IMPORTANCE_HIGH
        )
        notificationManager.createNotificationChannel(channel)
    }

}