package com.okaygo.worker.services

import android.app.*
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.okaygo.worker.R
import com.openkey.guest.application.OkayGo
import org.greenrobot.eventbus.EventBus

/**
 * Starts location updates on background and publish LocationUpdateEvent upon
 * each new location result.
 */
class CurrentLocationService : Service() {
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null

    //endregion
    //Location Callback
    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            val currentLocation = locationResult.lastLocation

            Log.e(
                "Locations",
                currentLocation.latitude.toString() + "," + currentLocation.longitude
            )
            //Share/Publish Location
            //Publish Location

            //            location.latitude = currentLocation.getLatitude();
//            location.longitude = currentLocation.getLongitude();
//            location.speed = currentLocation.getSpeed();
//            EventBus.getDefault().post(new LocationUpdateEvent(location));
            EventBus.getDefault().post(currentLocation)
        }
    }

    //onCreate
    override fun onCreate() {
        super.onCreate()
        initData()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        prepareForegroundNotification()
        startLocationUpdates()
        return START_STICKY
    }

    private fun startLocationUpdates() {
        mFusedLocationClient?.requestLocationUpdates(
            locationRequest,
            locationCallback, Looper.myLooper()
        )
    }

    private fun prepareForegroundNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                "4574",
                "Location Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
        val notificationIntent = Intent(this, LauncherActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            2014,
            notificationIntent, 0
        )
        val notification =
            NotificationCompat.Builder(this, "4574")
                .setContentTitle(getString(R.string.app_name))
                .setContentTitle("Location tracking is on for your OD Job")
                .setSmallIcon(R.drawable.ic_logo_transarent)
                .setColor(ContextCompat.getColor(this, R.color.yellow))
                .setContentIntent(pendingIntent)
                .build()
        startForeground(5014, notification)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun initData() {
        locationRequest = LocationRequest.create()
        locationRequest?.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        locationRequest?.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(OkayGo.appContext)
    }

    override fun onDestroy() {
        super.onDestroy()
        mFusedLocationClient?.removeLocationUpdates(locationCallback)
        //        stopForeground(true);
    }

    companion object {
        //region data
        private const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    }
}