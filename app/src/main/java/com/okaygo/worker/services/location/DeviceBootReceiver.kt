package com.okaygo.worker.services.location

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build


class DeviceBootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action != null && intent.action.equals("android.intent.action.BOOT_COMPLETED")) {
            val serviceIntent = Intent(context, LocationTrackerService::class.java)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context!!.startForegroundService(serviceIntent)
            } else {
                context!!.startService(serviceIntent)
            }
        }
    }
}