package com.okaygo.worker.services.location

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Settings
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.okaygo.worker.R
import com.okaygo.worker.help.utils.TrackingUtility

class GPSLocationReceiver : BroadcastReceiver() {
    private val CHANNEL_ID: String = "OKAY_GO_TRACKING"

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == "android.location.PROVIDERS_CHANGED") {
            if (!TrackingUtility.isLocationEnabled(context)) {
                showGPSNotification(context)
            } else {
                NotificationManagerCompat.from(context!!).apply {
                    cancel(1212)
                }
                if (TrackingUtility.isServiceRunning(
                        context!!,
                        LocationTrackerService::class.java
                    )
                ) {
                    val pushIntent = Intent(context, LocationTrackerService::class.java)
                    context.startService(pushIntent)
                }
            }
        }
    }

    private fun showGPSNotification(context: Context?) {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val builder = NotificationCompat.Builder(context!!, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_logo_transarent)
            .setContentTitle("Location Disabled")
            .setContentIntent(pendingIntent)
            .setContentText("Please enable your location.")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        with(NotificationManagerCompat.from(context)) {
            createNotificationChannel(context)
            // notificationId is a unique int for each notification that you must define
            notify(1212, builder.build())
        }
    }

    private fun createNotificationChannel(context: Context?) {
        // Create the NotificationChannel, but only on API 26 because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, "Okay_Go_loc_job", importance).apply {
                description = "Okay Go Location Tracking"
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}