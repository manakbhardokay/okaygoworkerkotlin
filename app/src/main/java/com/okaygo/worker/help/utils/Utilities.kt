package com.okaygo.worker.help.utils

import android.Manifest
import android.app.*
import android.content.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.provider.OpenableColumns
import android.text.*
import android.text.format.DateFormat
import android.text.method.ScrollingMovementMethod
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.NumberPicker
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.devs.readmoreoption.ReadMoreOption
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.okaygo.worker.R
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.help.views.NunitoSemiBoldTextView
import com.okaygo.worker.ui.activity.login_onboarding.LoginOnBoardingActivity
import com.okaygo.worker.ui.fragments.login.model.AuthDetails
import com.okaygo.worker.ui.fragments.login.model.AuthenticationSuccess
import com.openkey.guest.help.Preferences
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.reflect.Method
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author Davinder Goel.
 */
object Utilities {
    private var msg: Toast? = null
    private var dialog: Dialog? = null
    var weekDays = arrayOf(
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    )

    fun isMyServiceRunning(serviceClass: Class<*>, activity: Activity?): Boolean {
        val manager: ActivityManager =
            activity?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.getClassName()) {
                return true
            }
        }
        return false
    }

    fun getDateFromMili(time: Long?, format: String? = "MMM dd"): String? {
        val formatter =
            SimpleDateFormat(format, Locale.ENGLISH)

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = time ?: 0L
        return formatter.format(calendar.time)
    }

    fun convertStringDateToDesireFormat(
        actualDate: String?,
        format: String?
    ): String? {
        var monthYear = ""
        val outputFormat =
            SimpleDateFormat(format, Locale.ENGLISH)
        val inputFormat =
            SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        monthYear = try {
            val date = inputFormat.parse(actualDate)
            outputFormat.format(date)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            ""
        }
        return monthYear
    }

    /**
     * Used to get MimeType from url.
     *
     * @param url Url.
     * @return Mime Type for the given url.
     */
    fun getMimeType(url: String?): String? {
        var type: String? = null
        val extension: String = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            val mime: MimeTypeMap = MimeTypeMap.getSingleton()
            type = mime.getMimeTypeFromExtension(extension)
        }
        return type
    }

    fun getTimeFromMili(time: Long): String? {
        val cal = Calendar.getInstance()
        cal.timeInMillis = time
        return DateFormat.format("hh:mm a", cal).toString()
    }

    fun getEduFromId(eduId: String): String? {
        val id = eduId.toInt()
        var education = ""
        education = when (id) {
            1 -> "Below 10th"
            2 -> "10th Std"
            3 -> "12th Std"
            4 -> "Diploma"
            5 -> "In College"
            6 -> "Graduate"
            7 -> "Post Graduate"
            else -> "Any"
        }
        return education
    }

    fun getExpFromId(expId: Int): String? {
        var experience = ""
        experience = when (expId) {
            40 -> "Fresher"
            41 -> "1-2 yrs experience"
            42 -> "3-5 yrs experience"
            43 -> "5+ yrs experience"
            else -> "Fresher"
        }
        return experience
    }


    fun readMoreOption(activity: Activity?): ReadMoreOption? {
        return ReadMoreOption.Builder(activity) //                .textLength(3, ReadMoreOption.TYPE_LINE) // OR
            .textLength(400, ReadMoreOption.TYPE_CHARACTER)
            .moreLabel("see more")
            .lessLabel("  less")
            .moreLabelColor(Color.BLUE)
            .lessLabelColor(Color.BLUE)
            .labelUnderLine(false)
            .expandAnimation(true)
            .build()
    }

    /**
     * snack bar for error messages
     */
    fun showSnackBar(view: View, msg: String?, activity: Activity?) {
        val mSnackBar = msg?.let { Snackbar.make(view, it, Snackbar.LENGTH_SHORT) }
        val textView = mSnackBar?.view?.findViewById(R.id.snackbar_text) as TextView
        mSnackBar.view.setBackgroundColor(Color.WHITE)
        activity?.let { textView.setTextColor(ContextCompat.getColor(it, R.color.colorAccent)) }
        mSnackBar.show()
    }


    fun getInterviewMode(intervie_wMode: Int): String? {
        var interviewMode = ""
        when (intervie_wMode) {
            1 -> interviewMode = "Profile shared"
            2 -> interviewMode = "Telephonic"
            3 -> interviewMode = "Face to face"
            4 -> interviewMode = "Profile rejected"
        }
        return interviewMode
    }


    public fun isValidEmail(email: String?): Boolean {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    fun getAppVersion(activity: Activity?): String? {
        try {
            val pInfo: PackageInfo? =
                activity?.packageManager?.getPackageInfo(activity.packageName, 0)
            return pInfo?.versionName

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return null
    }

    /**
     * Show toast.
     *
     * @param context the context
     * @param toast   String value which needs to shown in the toast.
     * @description if you want to print a toast just call this method and pass
     * what you want to be shown.
     */
    fun showToast(context: Context?, toast: String): Toast? {
        if (context != null && msg == null || msg?.view?.windowVisibility != View.VISIBLE) {
            msg = Toast.makeText(context, toast, Toast.LENGTH_LONG)
            msg?.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 200)
            msg?.show()
        }
        return msg
    }

    fun showSuccessToast(activity: Activity?, toastMsg: String?) {
        val inflater: LayoutInflater? = activity?.getLayoutInflater()
        val layout: View? = inflater?.inflate(
            R.layout.layout_toast,
            activity.findViewById(R.id.custom_toast_container) as ViewGroup?
        )
        val tv = layout?.findViewById<View>(R.id.txtMsg) as AppCompatTextView
        tv.text = toastMsg
        val toast = Toast(activity)
        toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 200)
        toast.duration = Toast.LENGTH_LONG
        toast.view = layout
        toast.show()
    }

    /**
     * hide soft keyboard
     *
     * @param activity
     */
    fun hideKeyboard(activity: Activity?) {
        val inputManager: InputMethodManager? =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        // check if no view has focus:
        val v = activity?.currentFocus ?: return
        inputManager?.hideSoftInputFromWindow(v.windowToken, 0)
    }

    /**
     * Is online.
     *
     * @param context the context
     * @return True, if device is having a Internet connection available.
     */
    fun isOnline(context: Context?): Boolean {
        val cm: ConnectivityManager? =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        val netInfo = cm?.activeNetworkInfo
        return (netInfo != null && netInfo.isConnectedOrConnecting)
    }

    /**
     * open address on google map
     */
    fun openAddressOnMap(context: Context?, address: String?) {
        val map = "http://maps.google.co.in/maps?q=$address"
        val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
        context?.startActivity(i)
    }

    /**
     * @param context     context
     * @param phoneNumber the phone number which needs to be called
     */
    fun callToNumber(context: Activity?, phoneNumber: String?) {

        try {
            if (!TextUtils.isEmpty(phoneNumber)) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("tel:$phoneNumber")
                context?.startActivity(intent)
            }
        } catch (e: Exception) {
            print(e.printStackTrace())
        }

    }

    /**
     * show Loader view
     *
     * @param activity
     */
    @Synchronized
    fun showLoader(activity: Activity?) {
        if (activity != null) {
            hideLoader()
            dialog = Dialog(activity, android.R.style.Theme_Translucent)
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog?.setContentView(R.layout.loader_layout)
            dialog?.setCancelable(false)
            dialog?.show()
        }
    }

    /**
     * hide Loader view
     */
    @Synchronized
    fun hideLoader() {
        /**
         * due to handle huge crashes on hide loader
         * we are unable to identify this crash, so we handle it by try-catch block
         * IllegalArgumentException: View=com.android.internal.policy.impl.PhoneWindow$DecorView
         * not attached to window manager
         */
        if (dialog?.isShowing == true) {
            dialog?.dismiss()
        }
    }

    /**
     * convert String UTC Timestamp in string with  desire format
     */
    fun covertStringUTCToStringTime(dateStr: String, format: String): String {
        var ourDate = dateStr
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(ourDate)

            val dateFormatter = SimpleDateFormat(format) //this format changeable
            dateFormatter.timeZone = TimeZone.getDefault()
            ourDate = dateFormatter.format(value)

            //Log.d("ourDate", ourDate);
        } catch (e: Exception) {
            ourDate = "00:00"
        }

        return ourDate
    }


    fun covertStringCountryToStringTime(dateStr: String, format: String, timeZone: String): String {
        var ourDate = dateStr
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(ourDate)

            val dateFormatter = SimpleDateFormat(format) //this format changeable
            dateFormatter.timeZone = TimeZone.getTimeZone(timeZone)
            ourDate = dateFormatter.format(value)

            //Log.d("ourDate", ourDate);
        } catch (e: Exception) {
            ourDate = "00:00"
        }

        return ourDate
    }

    fun getDate(time: Long): String {
        var newDate: String = "00;00"
        val ourDate = time

        val date = Date(ourDate)
        try {
            // val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val formatter = SimpleDateFormat("dd-MM-yyyy")
            newDate = formatter.format(date)

            //Log.d("ourDate", ourDate);
        } catch (e: Exception) {
            newDate = "00:00"
        }

        return newDate
    }

    private fun enableLocation(activity: Activity?): Boolean {
        val service = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER)
        return enabled
    }


    /**
     * clear all previous added fragments fro stack
     */
    fun clearAllBackStack(activity: FragmentActivity?) {
        val fm = activity?.supportFragmentManager
        if (fm != null) {
            Log.e("backstack", "not null")
            for (i in 0 until fm.backStackEntryCount) {
                fm.popBackStack()
            }
        }
    }

    /**
     * stop multiclick to a view
     * @param view
     * @param delayTime
     */
    fun stopMultiClick(view: View?) {
        view?.isEnabled = false
        val handler = Handler()
        handler.postDelayed({ view?.isEnabled = true }, 300)
    }

    /**
     * set image from url to view by glide
     */
    fun setImageByGlide(activity: Activity?, imageUrl: String?, view: AppCompatImageView?) {
        activity?.let {
            view?.let { it1 ->
                Glide.with(it)
                    .load(imageUrl)
                    .apply(
                        RequestOptions().placeholder(
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_selfie
                            )
                        ).error(ContextCompat.getDrawable(it, R.drawable.ic_selfie))
                    )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(it1)
            }
        }
    }

    /**
     * set image from url to view by glide
     */
    fun setImageByGlideRounded(activity: Activity?, imageUrl: String?, view: AppCompatImageView?) {
        activity?.let {
            view?.let { it1 ->
                Glide.with(it)
                    .load(imageUrl)
                    .apply(
                        RequestOptions().placeholder(
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_placeholder_co
                            )
                        ).error(ContextCompat.getDrawable(it, R.drawable.ic_placeholder_co))
                    )
                    .circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(it1)
            }
        }
    }

    /**
     * set image from Bitmap to view by glide
     */
    fun setImageByGlide(activity: Activity?, image: Bitmap?, view: AppCompatImageView?) {
        activity?.let {
            view?.let { it1 ->
                Glide.with(it)
                    .load(image)
                    .apply(
                        RequestOptions()
                            .placeholder(ContextCompat.getDrawable(it, R.drawable.ic_selfie))
                            .error(ContextCompat.getDrawable(it, R.drawable.ic_selfie))
                    )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(it1)
            }
        }
    }

    /**
     * set image from Bitmap to view by glide
     */
    fun setImageByGlide(activity: Activity?, image: Drawable?, view: AppCompatImageView?) {
        activity?.let {
            view?.let { it1 ->
                Glide.with(it)
                    .load(image)
                    .apply(
                        RequestOptions()
                            .placeholder(ContextCompat.getDrawable(it, R.drawable.ic_selfie))
                            .error(ContextCompat.getDrawable(it, R.drawable.ic_selfie))
                    )
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(it1)
            }
        }
    }

    /**
     * set image from Bitmap to view by glide
     */
    fun setImageByGlideRounded(activity: Activity?, image: Drawable?, view: AppCompatImageView?) {
        activity?.let {
            view?.let { it1 ->
                Glide.with(it)
                    .load(image)
                    .apply(
                        RequestOptions()
                            .placeholder(ContextCompat.getDrawable(it, R.drawable.ic_selfie))
                            .error(ContextCompat.getDrawable(it, R.drawable.ic_selfie))
                    )
                    .circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(it1)
            }
        }
    }

    fun enableSystemLocationSettingDialog(activity: Activity?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val manager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                val alertDialog: AlertDialog.Builder? = AlertDialog.Builder(activity)
                alertDialog?.setMessage("You have to enable your location for access your key. Do you want to enable it?")
                alertDialog?.setCancelable(false)
                alertDialog?.setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        val intent =
                            Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        activity?.startActivity(intent)
                    }
                })

                alertDialog?.setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog?.cancel()
                    }
                })
                val dialog: AlertDialog? = alertDialog?.create()
                dialog?.show()
            }
        }
    }

    fun getAuthHandler(
        onAuthSuccess: (AuthenticationSuccess?) -> Unit?,
        onAuthenticationDetails: (AuthDetails?) -> Unit?,
        onAuthFailure: (Exception?) -> Unit?
    ): AuthenticationHandler {
        val authenticationHandler: AuthenticationHandler = object : AuthenticationHandler {
            override fun onSuccess(
                cognitoUserSession: CognitoUserSession?,
                device: CognitoDevice?
            ) {
                Log.e("TAG", " -- Auth Success")
                val authHandler = AuthenticationSuccess(cognitoUserSession, device)
                onAuthSuccess(authHandler)
            }

            override fun getAuthenticationDetails(
                authenticationContinuation: AuthenticationContinuation,
                username: String
            ) {
                Locale.setDefault(Locale.US)
                val authDetails = AuthDetails(authenticationContinuation, username)
                onAuthenticationDetails(authDetails)
            }

            override fun getMFACode(continuation: MultiFactorAuthenticationContinuation) {}
            override fun authenticationChallenge(continuation: ChallengeContinuation) {}
            override fun onFailure(e: Exception?) {
                onAuthFailure(e)
            }
        }
        return authenticationHandler
    }

    fun appVersion(activity: Activity): String? {
        val version: String
        version = try {
            val pInfo =
                activity.packageManager.getPackageInfo(activity.packageName, 0)
            pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            "1.0.0"
        }
        return version
    }

    fun getFormatedDate(date: String?): String? {
        return if (date != null) {
            val year = date.split("-".toRegex()).toTypedArray()[0]
            val month = date.split("-".toRegex()).toTypedArray()[1]
            val day = date.split("-".toRegex()).toTypedArray()[2]
            val Months = arrayOf(
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            )
            day + "-" + Months[month.toInt() - 1] + "-" + year
        } else {
            null
        }
    }

    fun getFormatedDateDay(date: String?): String? {
        return if (date != null) {
            val day = date.split("-".toRegex()).toTypedArray()[0]
            val month = date.split("-".toRegex()).toTypedArray()[1]
            val year = date.split("-".toRegex()).toTypedArray()[2]
            val Months = arrayOf(
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            )
            day + "-" + Months[month.toInt() - 1] + "-" + year
        } else {
            null
        }
    }

    fun checkStoragePermission(activity: Activity?): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity?.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || activity?.checkSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Log.e("STORAGE PERMISSION", "Permission is granted")
                activity?.let {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        1101
                    )
                }
                return false
            }
            return true
        }
        return true
    }

//    fun locationPermisiion(activity: Activity?): Boolean {
//        activity?.let {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (ContextCompat.checkSelfPermission(
//                        it,
//                        Manifest.permission.ACCESS_COARSE_LOCATION
//                    ) != PackageManager.PERMISSION_GRANTED
//                    || ContextCompat.checkSelfPermission(
//                        it,
//                        Manifest.permission.ACCESS_FINE_LOCATION
//                    ) != PackageManager.PERMISSION_GRANTED
//                ) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                        ActivityCompat.requestPermissions(
//                            it, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
//                            5011
//                        )
//                    } else {
//                        ActivityCompat.requestPermissions(
//                            it,
//                            arrayOf(
//                                Manifest.permission.ACCESS_FINE_LOCATION,
//                                Manifest.permission.ACCESS_COARSE_LOCATION
//                            ),
//                            5011
//                        )
//                    }
//                    return false
//                }
//                return true
//            }
//            return true
//        }
//        return true
//    }


    fun checkCameraPermission(activity: Activity?): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity?.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Log.e("STORAGE PERMISSION", "Permission is granted")
                activity?.let {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(
                            Manifest.permission.CAMERA
                        ),
                        1102
                    )
                }
                return false
            }
            return true
        }
        return true
    }

    fun showFileChooserCV(activity: Activity?, requestCode: Int?) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        val mimeTypes = arrayOf(
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "image/*",
            "application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        //        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            activity?.startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                requestCode ?: 0
            )
        } catch (ex: ActivityNotFoundException) {
            // Potentially direct the user to the Market with a Dialog
            showToast(activity, "Please install a File Manager.")
        }
    }

    fun showSalaryPopUp(
        activity: Activity?,
        onConfirmClick: (Int?) -> Unit?
    ) {
        activity?.let {
            val dialog = Dialog(it)
            dialog.setContentView(R.layout.dialog_salary)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val salary: TextInputEditText = dialog.findViewById(R.id.salary)
            val cancel = dialog.findViewById<TextView>(R.id.cancel)
            val confirm = dialog.findViewById<TextView>(R.id.confirm)
            salary.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                }

                override fun afterTextChanged(s: Editable) {
                    if (!s.toString().startsWith("\u20B9")) {
                        salary.setText("\u20B9 ")
                        Selection.setSelection(salary.text, salary.text?.length ?: 0)
                    }
                }
            })
            cancel.setOnClickListener {
                dialog.dismiss()
            }
            confirm.setOnClickListener {
                val sym = "\u20B9"
                var lastSalary = 0
                if (!salary.text.toString().isEmpty()) {
                    val enterSalary =
                        salary.text.toString().trim { it <= ' ' }.split(sym.toRegex())
                            .toTypedArray()
                    if (enterSalary.size > 0) {
                        lastSalary = enterSalary[1].trim { it <= ' ' }.toInt()
                    }
                    if (lastSalary > 0) {
                        dialog.dismiss()
                        onConfirmClick(lastSalary)
                    }
                }
            }
            dialog.show()
        }
    }

    fun getDateInMonthYear(date: String?): String? {
        return if (date != null) {
            val year = date.split("-".toRegex()).toTypedArray()[0]
            val month = date.split("-".toRegex()).toTypedArray()[1]
            val Months = arrayOf(
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            )
            Months[month.toInt() - 1] + "-" + year
        } else {
            null
        }
    }

    // Use Below Method Working fine for Android N.
    fun getFilePathForN(
        uri: Uri?,
        context: Context?
    ): String? {
        val returnCursor =
            uri?.let { context?.contentResolver?.query(it, null, null, null, null) }
        if (returnCursor != null) {
            try {
                /*
                 * Get the column indexes of the data in the Cursor,
                 *     * move to the first row in the Cursor, get the data,
                 *     * and display it.
                 * */
                val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                val sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE)
                returnCursor.moveToFirst()
                val name = returnCursor.getString(nameIndex)
                val size = java.lang.Long.toString(returnCursor.getLong(sizeIndex))
                val file = File(context?.filesDir, name)
                try {
                    val inputStream = context?.contentResolver?.openInputStream(uri)
                    val outputStream = FileOutputStream(file)
                    var read = 0
                    val maxBufferSize = 1 * 1024 * 1024
                    val bytesAvailable =
                        inputStream?.available() ?: 0

                    //int bufferSize = 1024;
                    val bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    val buffers = ByteArray(bufferSize)
//                    while ((read = inputStream != null ? inputStream.read(buffers) : 0) != -1) {
//                        outputStream.write(buffers, 0, read);
//                    }
                    while ((inputStream?.read(buffers) ?: 0).also {
                            read = it
                        } != -1
                    ) {
                        outputStream.write(buffers, 0, read)
                    }
                    Log.e("File Size", "Size " + file.length())
                    inputStream!!.close()
                    outputStream.close()
                    Log.e("File Path", "Path " + file.path)
                } catch (e: java.lang.Exception) {
                    Log.e("Exception", e.message ?: "")
                }
                return file.path
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        return null
    }

    fun showAlertTermsPolicy(
        activity: Activity?,
        title: String?,
        body: String?
    ) {
        activity?.let {
            val dialog = Dialog(it)
            dialog.setContentView(R.layout.dialog_term_policy)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val txtTitle: AppCompatTextView = dialog.findViewById(R.id.txtTitle)
            val txtBody: AppCompatTextView = dialog.findViewById(R.id.txtText)
            val imgClose = dialog.findViewById<AppCompatImageView>(R.id.imgClose)
            txtTitle.text = title
            txtBody.text = body
            txtBody?.setMovementMethod(ScrollingMovementMethod())

            imgClose?.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
        }
    }

    fun HourTimePicker(
        context: Context?,
        onTimeSelection: (String?) -> Unit?
    ) {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_hour_picker)
        val npHour = dialog.findViewById<View>(R.id.npHour) as NumberPicker
        val npAmPm = dialog.findViewById<View>(R.id.npAmPm) as NumberPicker
        val txtOk = dialog.findViewById<View>(R.id.txtOk) as AppCompatTextView
        val txtCancel = dialog.findViewById<View>(R.id.txtCancel) as AppCompatTextView
        npHour.minValue = 1
        npHour.maxValue = 12
        npAmPm.minValue = 1
        npAmPm.maxValue = 2

        npAmPm.setFormatter {
            if (it == 2) {
                "PM"
            } else {
                "AM"
            }
        }

        try {
            val method: Method = npAmPm.javaClass.getDeclaredMethod(
                "changeValueByOne",
                Boolean::class.javaPrimitiveType
            )
            method.setAccessible(true)
            method.invoke(npAmPm, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        txtOk.setOnClickListener {
            var hour = npHour.value.toString() + ""
            hour = if (npAmPm.value == 1) {
                "$hour AM"
            } else {
                "$hour PM"
            }
            onTimeSelection(hour)
            dialog.dismiss()
        }
        txtCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    fun convertTimeToAmPm(time: String?): String? {
        val time: Int? = time?.split(":".toRegex())?.toTypedArray()?.get(0)?.toInt()
        if (time == 0) {
            return 12.toString() + " AM"
        } else if (time ?: 0 < 12) {
            return "$time AM"
        } else if (time ?: 0 == 12) {
            return "$time PM"
        } else if (time ?: 0 > 12 && time ?: 0 < 24) {
            return (time ?: 0 - 12).toString() + " PM"
        } else if (time ?: 0 == 24) {
            return 12.toString() + " AM"
        }

        return ""
    }


    fun convertStringDateToDayMonth(actualDate: String?): String? {
        var dayMonth: String? = ""
        val outputFormat = SimpleDateFormat("dd MMMM", Locale.ENGLISH)
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        try {
            val date = inputFormat.parse(actualDate)
            val output = outputFormat.format(date)
            if (output.contains(" ")) {
                val dayDate = output.split(" ".toRegex()).toTypedArray()
                var day = dayDate[0]
                day = if (day.endsWith("1")) {
                    if (day == "11") {
                        day + "th"
                    } else {
                        day + "st"
                    }
                } else if (day.endsWith("2")) {
                    if (day == "12") {
                        day + "th"
                    } else {
                        day + "nd"
                    }
                } else if (day.endsWith("3")) {
                    if (day == "13") {
                        day + "th"
                    } else {
                        day + "rd"
                    }
                } else {
                    day + "th"
                }
                dayMonth = day + " " + dayDate[1]
            } else {
                dayMonth = output
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            dayMonth = ""
        }
        return dayMonth
    }

    fun convertTimeInAmPm(actualTime: String?): String? {
        var convertedTime = ""
        val inputFormat = SimpleDateFormat("KK:mm:ss", Locale.ENGLISH)
        val outPutFormat = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
        var time: Date? = null
        try {
            time = inputFormat.parse(actualTime)
            convertedTime = outPutFormat.format(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return convertedTime
    }

    fun getWeekdayFromDate(inDate: String?): String? {
        var day = ""
        val inFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        var date: Date? = null
        try {
            date = inFormat.parse(inDate)
            val outFormat = SimpleDateFormat("EEEE", Locale.ENGLISH)
            day = outFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return day
    }

    fun getDateAfterParticularDays(givenDate: String?, days: Int?): String? {
        val date = SimpleDateFormat("yyyy-MM-dd").parse(givenDate)
        val c = Calendar.getInstance()
        c.time = date
        c.add(Calendar.DATE, days ?: 0)
        val expDate = c.time
        val dateFormat: SimpleDateFormat = SimpleDateFormat("dd-MMM-yyyy")
        return dateFormat.format(expDate)
    }

    fun compareDates(givenDate: String?, days: Int?): Boolean {
        val date = SimpleDateFormat("yyyy-MM-dd").parse(givenDate)
        val c = Calendar.getInstance()
        c.time = date
        c.add(Calendar.DATE, days ?: 0)
        val expDate = c.time
        Log.e("CURRENTDATE", Date()?.toString() + " == " + expDate.toString())
        if (Date().after(expDate)) {
            return true
        }
        return false
    }

    fun getNextFourDays(
        day: String?,
        dayCount: Int
    ): ArrayList<String>? {
        var index = -1
        var count = 0
        for (i in weekDays.indices) {
            if (weekDays.get(i).equals(day, ignoreCase = true)) {
                index = i
                break
            }
        }
        if (index == -1) {
            return null
        }
        val nextFourDays = ArrayList<String>()
        var j = index
        while (count < dayCount) {
            if (j >= weekDays.size) {
                j = 0
            }
            nextFourDays.add(weekDays.get(j))
            count++
            j++
        }
        return nextFourDays
    }

    fun getDayMonth(day: String?): String? {
        return convertDateToMonthDay(getNextDateFromDay(day))
    }

    fun convertDateToMonthDay(actualDate: String?): String? {
        var monthYear = ""
        val outputFormat = SimpleDateFormat("MMM dd", Locale.ENGLISH)
        val inputFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH)
        monthYear = try {
            val date = inputFormat.parse(actualDate)
            outputFormat.format(date)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            ""
        }
        return monthYear
    }


    fun getNextDateFromDay(dayName: String?): String? {
        val c = Calendar.getInstance()
        c.add(Calendar.DAY_OF_MONTH, 1)
        var date: Date? = null
        date = when (dayName?.toUpperCase()) {
            "MONDAY" -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.MONDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
            "TUESDAY" -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.TUESDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
            "WEDNESDAY" -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.WEDNESDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
            "THURSDAY" -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.THURSDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
            "FRIDAY" -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.FRIDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
            "SATURDAY" -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.SATURDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
            "SUNDAY" -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.SUNDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
            else -> {
                while (c[Calendar.DAY_OF_WEEK] != Calendar.MONDAY) {
                    c.add(Calendar.DATE, 1)
                }
                c.time
            }
        }

        Log.e("next date", date.toString() + "")
        return date.toString() + ""
    }

    fun getInterviewSlotTime(day: String?): String? {
        return convertDateToDesireFormat(getNextDateFromDay(day), "yyyy-MM-dd HH:mm:ss")
    }

    fun convertDateToDesireFormat(
        actualDate: String?,
        format: String?
    ): String? {
        var result = ""
        val outputFormat = SimpleDateFormat(format, Locale.ENGLISH)
        val inputFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH)
        result = try {
            val date = inputFormat.parse(actualDate)
            outputFormat.format(date)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            ""
        }
        return result
    }

    fun getCuisineFromId(expId: Int): String? {
        var cuisine = ""
        cuisine = when (expId) {
            32 -> "Indian"
            33 -> "Continental"
            34 -> "Chinese"
            35 -> "Bakery"
            else -> "Other"
        }
        return cuisine
    }

    fun getEnglishLevel(id: Int, activity: Activity?): String? {
        var level = ""
        level = when (id) {
            1 -> activity?.resources?.getString(R.string.no_eng) ?: ""
            2 -> activity?.resources?.getString(R.string.thoda_eng) ?: ""
            3 -> activity?.resources?.getString(R.string.good_eng) ?: ""
            else -> activity?.resources?.getString(R.string.no_eng) ?: ""
        }
        return level
    }

    fun jobSuccessDialog(
        activity: Activity?,
        title: String?,
        msg: String?,
        navigation: Boolean,
        onPosClick: () -> Unit?
    ) {
        if (activity == null) {
            return
        }
        // Create custom dialog object
        val dialog = Dialog(activity)
        // Include dialog.xml file
        dialog.setContentView(R.layout.dailog_apply_success)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        //        dialog.setTitle("Custom Dialog");
        val OkayButton = dialog.findViewById<View>(R.id.txtOk) as TextView
        val txtMsg = dialog.findViewById<View>(R.id.txtDesc) as TextView
        val refer_title: NunitoSemiBoldTextView = dialog.findViewById(R.id.refer_title)
        txtMsg.text = msg
        refer_title.setText(title)

        // if decline button is clicked, close the custom dialog
        OkayButton.setOnClickListener { // Close dialog
            if (navigation) {
                Constants.navController?.navigate(R.id.navigation_my_jobs)
            }
            onPosClick()
            dialog.dismiss()
        }
        dialog.show()
    }

    fun getCurrentTime(): String? {
        val formatter = SimpleDateFormat("HH:mm:ss a", Locale.getDefault())
        val date = Date()
        return formatter.format(date)
    }

    fun shareApp(
        activity: Activity,
        msg: String?,
        subject: String?
    ) {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        shareIntent.putExtra(Intent.EXTRA_TEXT, msg)
        activity.startActivity(Intent.createChooser(shareIntent, "Share via"))
    }

    fun getDisplaySalary(
        minSalary: Int?,
        maxSalary: Int?,
        amount: Double?,
        isOnDemand: Boolean,
        amountPer: String?
    ): String? {
        Log.e("minSalary", minSalary?.toString() ?: "")
        Log.e("maxSalary", maxSalary?.toString() ?: "")
        val minSal = (minSalary)?.div(1000)
        val maxSal = (maxSalary)?.div(1000)
        var salaryType = amountPer
        if (salaryType == null) {
            salaryType = if (isOnDemand) {
                "Daily"
            } else {
                "p.m."
            }
        }
        return if (maxSal ?: 0 < 1 || isOnDemand) {
            "Rs. ${String.format(
                "%.2f",
                amount
            )}/ ${salaryType}"
//            "Rs. $amount/ $salaryType"
        } else {
            if (minSal ?: 0 > 99) {
                val mMinSal = minSal?.div(100.0)
                val mMaxSal = maxSal?.div(100.0)
                if (mMinSal == mMaxSal) {
                    "Rs. " + mMaxSal + "L /" + salaryType
                } else {
                    "Rs. " + mMinSal + "L - Rs. " + mMaxSal + "L /" + salaryType
                }
            } else {
                if (minSal == maxSal) {
                    "Rs. " + minSal + "k /" + salaryType
                } else {
                    "Rs. " + minSal + "k - Rs. " + maxSal + "k /" + salaryType
                }
            }
        }
    }

    fun getFileFromBitmap(context: Context?, bitmap: Bitmap?): File? {
        return try {
            val file = File(
                context?.cacheDir,
                System.currentTimeMillis().toString() + ".png"
            )
            file.createNewFile()

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap?.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
            val bitmapdata = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
            file
        } catch (ex: java.lang.Exception) {
            null
        }
    }


    fun getDaysDiff(sDate: String?): Int? {
        if (sDate == null || sDate.isEmpty()) {
            return 0
        }
        //        String strThatDay = "1985/08/25";
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        var d: Date? = null
        try {
            d = formatter.parse(sDate) //catch exception
        } catch (e: ParseException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        val lastDate = Calendar.getInstance()
        lastDate.time = d
        val today = Calendar.getInstance()
        val diff =
            today.timeInMillis - lastDate.timeInMillis //result in millis
        val days = diff / (24 * 60 * 60 * 1000)
        return days.toInt()
    }

    fun logoutUser(userId: Int?, activity: Activity?) {
        if (userId != null && userId > 0) {
            showToast(activity, "Session Expired")
            AuthHelper.getPool().getUser(userId?.toString()).signOut()
            Preferences.prefs?.clearAllValue()
        }
        Preferences?.prefs?.saveValue(Constants.ID, 0)
        val intent = Intent(activity, LoginOnBoardingActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        activity?.startActivity(intent)
        activity?.finish()
    }


    fun isThisDateIsToday(input: String?): Boolean? {
        if (input == null) {
            return false
        }
        val calendar = Calendar.getInstance()
        var date = "" + calendar[Calendar.YEAR]
        date = if (calendar[Calendar.MONTH] < 9) {
            date + "-0" + (calendar[Calendar.MONTH] + 1)
        } else {
            date + "-" + (calendar[Calendar.MONTH] + 1)
        }
        date = if (calendar[Calendar.DAY_OF_MONTH] < 10) {
            date + "-0" + calendar[Calendar.DAY_OF_MONTH]
        } else {
            date + "-" + calendar[Calendar.DAY_OF_MONTH]
        }
        return if (input == date) {
            true
        } else {
            false
        }
    }

    fun setSpecificTextColor(
        context: Context,
        s: SpannableString?,
        fullValue: String?,
        value: String?
    ): SpannableString? {
        val start = fullValue?.indexOf(value ?: "")
        val end = (start ?: 0) + (value?.length ?: 0)
        if ((start ?: 0) < 0 || end < 0) {
            return s
        }
        s?.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, R.color.dark_green)),
            start ?: 0,
            end,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return s
    }

    private var receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val action = intent.action
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == action) {
                val downloadId = intent.getLongExtra(
                    DownloadManager.EXTRA_DOWNLOAD_ID, 0
                )
                openDownloadedAttachment(context as Activity?, downloadId)
            }
        }
    }

    /**
     * Used to open the downloaded attachment.
     *
     * @param context    Content.
     * @param downloadId Id of the downloaded file to open.
     */
    private fun openDownloadedAttachment(
        activity: Activity?,
        downloadId: Long
    ) {
        val downloadManager =
            activity?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val query = DownloadManager.Query()
        query.setFilterById(downloadId)
        val cursor: Cursor = downloadManager.query(query)
        if (cursor.moveToFirst()) {
            val downloadStatus: Int? =
                cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
            val downloadLocalUri: String? =
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
            val downloadMimeType: String? =
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE))
            if (downloadStatus == DownloadManager.STATUS_SUCCESSFUL && downloadLocalUri != null) {
                openDownloadedAttachment(
                    activity,
                    Uri.parse(downloadLocalUri),
                    downloadMimeType
                )
            }
        }
        cursor.close()
    }

    /**
     * Used to open the downloaded attachment.
     *
     *
     * 1. Fire intent to open download file using external application.
     *
     * 2. Note:
     * 2.a. We can't share fileUri directly to other application (because we will get FileUriExposedException from Android7.0).
     * 2.b. Hence we can only share content uri with other application.
     * 2.c. We must have declared FileProvider in manifest.
     * 2.c. Refer - https://developer.android.com/reference/android/support/v4/content/FileProvider.html
     *
     * @param context            Context.
     * @param attachmentUri      Uri of the downloaded attachment to be opened.
     * @param attachmentMimeType MimeType of the downloaded attachment.
     */
    private fun openDownloadedAttachment(
        activity: Activity?,
        attachmentUri: Uri?,
        attachmentMimeType: String?
    ) {
        var uri: Uri? = attachmentUri
        if (uri != null) {
            // Get Content Uri.
            if (ContentResolver.SCHEME_FILE.equals(uri.scheme)) {
                // FileUri - Convert it to contentUri.
                val file = File(uri.path)
                uri =
                    activity?.let {
                        FileProvider.getUriForFile(
                            it,
                            "com.okaygo.worker.provider",
                            file
                        )
                    }
            }
            val openAttachmentIntent = Intent(Intent.ACTION_VIEW)
            openAttachmentIntent.setDataAndType(uri, attachmentMimeType)
            openAttachmentIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            try {
                activity?.startActivity(openAttachmentIntent)
            } catch (e: ActivityNotFoundException) {
                Utilities.showToast(
                    activity,
                    "Unable to open file"
                )
            }
        }
    }


    fun downloadAnyFile(activity: Activity?, url: String?, fileName: String?) {
        if (checkStoragePermission(activity)) {
            val mMimeType = getMimeType(url) ?: ""
            Log.e("DOWNLOAD_MImE", mMimeType + "")
            val request: DownloadManager.Request = DownloadManager.Request(Uri.parse(url))
            activity?.registerReceiver(
                receiver, IntentFilter(
                    DownloadManager.ACTION_DOWNLOAD_COMPLETE
                )
            )

            //request.setDescription("Some descrition");
            request.setTitle(fileName)
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                "$fileName"
            )

            // get download service and enqueue file
            val manager: DownloadManager? =
                activity?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            request.setMimeType(mMimeType)

            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI)
            manager?.enqueue(request)
        }
    }

    fun openPdf(activity: Activity?, url: String?) {
        try {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            activity?.startActivity(browserIntent)
        } catch (e: Exception) {
            e.printStackTrace()
            activity?.finish()
        }
    }

    /**
     * show alert dialog for app update
     */
    fun appUpdateAlert(activity: Activity, isForceUpdate: Boolean, updateMsg: String) {
        if (Constants.isAppUpdateDialogVisible == false) {
            val dialog = Dialog(activity)
            dialog.setContentView(R.layout.dialog_app_update)
            dialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val msg = dialog.findViewById<TextView>(R.id.txtMsg)
            val later = dialog.findViewById<TextView>(R.id.txtLater)
            val update = dialog.findViewById<TextView>(R.id.txtUpdate)
            msg.setText(updateMsg)
            if (isForceUpdate) {
                later.setVisibility(View.GONE)
            } else {
                later.setVisibility(View.VISIBLE)
            }
            later.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View) {
                    Constants.isAppUpdateDialogVisible = false
                    dialog.dismiss()
                }
            })
            update.setOnClickListener(object : View.OnClickListener {
                override fun onClick(view: View) {
                    Constants.isAppUpdateDialogVisible = false
                    dialog.dismiss()
                    try {
                        activity.startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(Constants.PLAY_STORE_URL)
                            )
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
            Constants.isAppUpdateDialogVisible = true
            dialog.show()
        }
    }


}