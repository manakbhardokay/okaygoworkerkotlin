package com.okaygo.worker.help.views

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager

class CustomGridLayoutManager(context: Context?, orientation: Int, reverseLayout: Boolean) :
    LinearLayoutManager(context, orientation, reverseLayout) {
//    private var isScrollEnabled = true
//    fun setScrollEnabled(flag: Boolean) {
//        isScrollEnabled = flag
//    }

    override fun canScrollHorizontally(): Boolean {
        return false
    }

    override fun canScrollVertically(): Boolean {
        return false
    }


    //
    //    @Override
    //    public boolean canScrollVertically() {
    //        //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
    //        return isScrollEnabled && super.canScrollVertically();
    //    }
}