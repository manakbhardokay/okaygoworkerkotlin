package com.okaygo.worker.help.utils

import android.os.Build
import androidx.navigation.NavController
import com.okaygo.worker.BuildConfig


/**
 * @author Davinder Goel
 */
object Constants {
    const val BASE_URL = BuildConfig.BASE_URL
    const val userName = "user_name"
    var IS_SPLASH = false
    const val IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch"
    const val IS_LOGGED_IN = "IS_LOGGED_IN"
    const val USERNAME_FORGOT = "username_forgot"

    const val UTM_ACCESS_STATUS = "is_utm_accessed"
    const val IS_WHATSAPP_SUB_DIALOG = "whatsapp_subscribe_dialog"
    const val CUR_LAT = "CURRENT_LAT"
    const val CUR_LNG = "current_lng"
    const val IS_AUTO_START_ENABLED = "is_auto_start_enabled"

    //    const val IS_CHOSE_PASS = "is_chose_pass"
//    const val SAVE_USERNAME = "save_username"
    const val IS_FIRST_TIME = "is_first_time"
    const val SELECTED_LANG = "selected_lang"
    const val LANG_SELECTION = "lang_selection"
    const val REFERED_BY = "refered_by"

    //    const val ID = "user_id_okaygo"
    const val TRACK_JOB_ID = "track_job_id"
    const val JOB_DEST = "job_Dest"
    const val MAP_KEY = "mapkey"
    const val ID = "id"
    const val OLD_ID = "id"
    var isToggle = false
    const val HOME_JOB_FILTER = "job_filter"
    const val LAST_SALARY = "last_salary"
    const val PLAY_STORE_URL =
        "https://play.google.com/store/apps/details?id=com.okaygo.worker&hl=en"

    //Filtter
    var exp_active: Boolean = false
    var city_active = false
    var pay_active = false
    var time_active = false
    var Minimum_Pay = 0
    var Maximum_Pay = 50000
    var start_time = 0
    var end_time = 0
    var exp_filters: String? = null
    var city_filters: String? = null
    var job_type_active = false
    var job_type_ids: String? = null
    var exp_filters_count = 0
    var city_filters_count = 0
    var job_type_count = 0
    var Active = false

    var BOTTOM_MENU_ITEM_SELECTED = 0

    //For google analytics
    const val UTM_SOURCE = "utm_source"
    const val UTM_MEDIUM = "utm_medium"
    const val UTM_CONTENT = "utm_content"
    const val UTM_CAMPAIGN = "utm_campaign"
    const val UTM_TERM = "utm_term"
    const val ANID = "anid"
    const val ACCESS_TOKEN = "access_token"
    const val MOBILE_NO = "MOBILE_NO"
    const val DONE_ONBOARDING = "done_onboard"
    const val CURRENT_TAB = "current_tab"

    const val EXIST = "exist"
    const val USERNAME = "username"
    const val OG_SESSION_ID = "OgSessionId"
    const val USER_ID = "user_id"
    const val IS_USER_LOGGED_IN = "is_user_logged_in"
    const val EMPLOYER_ID = "employer_id"

    var FAQ_TITLE = ""
    var FAQ_DESC = ""

    var navController: NavController? = null
    var IS_UPDATE_DIALOG_DISPLAYED = false
    const val IS_FOR_DEEPLINK = "is_for_deep_link"

    //Response Code
    const val SUCCESS = 1000
    const val API_ERROR = 1001
    const val API_SESSION_EXPIRED = 1005;

    val manufacturer = Build.MANUFACTURER
    val model = Build.MODEL
    val osVersion = Build.VERSION.RELEASE
    val brand = "$manufacturer-$model"
    const val SAVE_CAT_ID = "save_cat_id"
    const val IS_FOR_EDIT = "IS_FOR_EDIT"
    const val EXP_DATA = "exp_data"

    //    const val IS_FROM_PROFILE = "is_from_profile"
    var IS_FROM_PROFILE = false
    var IS_FROM_PROFILE_SKILLS = false
    const val CURRENT_SCREEN = "current_screen"

    const val FCM_TOKEN = "fcm_token"
    const val JOB_ID = "Job_id"
    const val JOB_DETAIL_ID_DEEPLINK = "Job_detail_id_deeplink"
    const val WORKER_ID = "WORKER_id"
    const val JOB_DETAIL_DATA = "user_detail_data"
    const val JOB_DETAIL_ID = "user_detail_id"
    const val APP_DIRECT = "app_direct"
    const val DOWNLOAD_AND_APPLY = "download_app_and_apply_for_job"
    const val INTERVIEW_SLOT = "select_interview_slot"
    const val INTERVIEW_DETAILS = "interview_details"
    const val OFFER_DETAIL = "job_offer_details"
    const val IS_FOR_ONBOARDING = "job_offer_details"
    var IS_FOR_INTERVIEW = false
    var IS_DEEPLINK = 0

    const val SELECTED_JOB_CAT_DATA = "seletced_job_cat_data"
    const val NAV_SCREEN = "nav_screen"
    const val POST_NAV_SCREEN = "POST_nav_screen"
    const val POST_APPLY_JOB_DATA = "apply_job_data"
    const val POST_APPLY_REQUIRED_DATA = "apply_required_data"
    const val POST_Assign_id = "POST_Assign_id"

    var isOnDemand: String? = null
    var isPt: String? = null
    var isFt: String? = null
    var maxPay: String? = null
    var minMay: String? = null
    var jobType: String? = null
    var cityFilter: String? = null
    var expFilter: String? = null

    var jobAfter: String? = null
    var jobBefore: String? = null
    const val YOUTUBE_RESPONSE = "youtube_response"

    const val NAVIGATION_DATA = "NAVI_data"

    const val OD_APPLIED = 9901
    const val OD_REJECTED = 9902
    const val PT_FT_APPLIED = 9903
    const val PT_FT_REJECTED = 9904
    const val PT_FT_SELECT_A_SLOT = 9905
    const val PT_FT_SHORTLISTED = 9906
    const val PT_FT_SLOT_SELECTED = 9907
    const val PT_FT_INTERVIEW_DONE_RESULT_AWAITED = 9908
    const val PT_FT_INTERVIEW_CANCELLED = 9910
    const val PT_FT_JOB_OFFERED = 9909

    //Social Login
    const val SOCIAL_LOGIN_ID = 1301
    const val GOOGLE = 301
    const val FB = 302
    const val LINKEDIN = 303
    const val OKAYGO = 304

    //Fragments
    const val VIDEO_PLAYER = 1
    const val JOB_LIST = 2
    const val SKILLs = 3
    const val PERSONAL_DETAIL = 4
    const val EDUCATION_LANGUAGE = 5
    const val EXPERIENCE = 6
    const val JOB_PREFREBCE = 7
    const val CHANGE_PASSWORD = 8
    const val FAQ = 9
    const val JOB_DETAIL = 10
    const val NOTIFICATION = 11
    const val AVAILABILITY_DASHBOARD = 12
    const val REFFERAL = 13
    const val PAYMENT_DETAIL = 14
    const val DOCUMENT_DETAIL = 15
    const val WEBVIEW = 16
    const val RATING_REVIEW = 17

    //    const val COMPLETE_INFO = 14
    const val INTERVIEW_SCHEDUAL_OD = 21

    const val COMPLETE_INFO = 31
    const val UNCOMPLETE_INFO = 32
    const val NO_REQUIREMENT = 33
    const val ADD_EXP = 34

    var USER_SPECIALIZATION = 117
    var USER_SKILLS = 118
    var USER_EDUCATION = 101
    var USER_EXP = 115
    var USER_OWNERSHIP = false
    var USER_LAST_SALARY = 116
    var USER_TOTAL_EXP = 103
    var USER_NOTICE = 107
    var USER_CV = 105
    var USER_LANG = 104
    var USER_LAPTOP = 106
    var USER_SMARTPHONE = 108
    var USER_WIFI = 109
    var USER_BIKE = 110
    var USER_DL = 111
    var USER_RC = 112
    var USER_ADHAAR = 113
    var USER_PAN = 114

    const val QUEST_JOB_ID = "JOB_ID_QUESTION"

    const val NOTIFICATION_DATA = "notification_data"
    var isLocationPermissionJobId = 0
    var isApiCalled = false

    const val SKILL_LIST = "SKILL_LIST"
    const val EDU_ID = "EDU_ID"
    const val LANG_ID = "LANG_ID"
    const val RC_STATUS = "RC_STATUS"
    const val DL_STATUS = "DL_STATUS"
    const val LAPTOP_STATUS = "LAPTOP_STATUS"
    const val SMARTPHONE_STATUS = "SMARTPHONE_STATUS"
    const val BIKE_STATUS = "BIKE_STATUS"
    const val WIFI_STATUS = "WIFI_STATUS"
    const val ADHAAR_STATUS = "ADHAAR_STATUS"
    const val PAN_STATUS = "ADHAAR_STATUS"

    var DEEPLINK_JOB_CAT_ID = 0
    var DEEPLINK_JOB_TYPE_ID = 0
    var DEEPLINK_JOB_ID = 0
    var DEEPLINK_JOB_DETAIL_ID = 0
    var DEEPLINK_JOB_TYPE = ""
    var DEEPLINK_WORK_TYPE = ""
    var DEEPLINK_REFFER_BY: Int? = null

    var USER_UPI_ID: String? = null

    var LAST_UPDATED_SALARY: Int? = null
    var NOTICE_PERIOD: String? = null
    var TOTAL_EXP: String? = null
    var isAppUpdateDialogVisible = false


    const val WEB_URL = "WEB_URL"
}