package com.okaygo.worker.help.utils

import android.content.Context
import android.os.AsyncTask
import android.os.Environment
import android.util.Log
import com.okaygo.worker.callbacks.DownloadListener
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class FileDownlaod(
    val context: Context?,
    val yourUrl: String?,
    val fileName: String?,
    val listener: DownloadListener
) : AsyncTask<Boolean, Void, Boolean>() {
    override fun doInBackground(vararg booleans: Boolean?): Boolean {


        var apkStorage: File? = null
        var outputFile: File? = null
        try {
            val url = URL(yourUrl) //Create Download URl
            val c: HttpURLConnection =
                url.openConnection() as HttpURLConnection //Open Url Connection
            c.setRequestMethod("GET") //Set Request Method to "GET" since we are grtting data
            c.connect() //connect the URL Connection

            //If Connection response is not OK then show Logs
            if (c.getResponseCode() !== HttpURLConnection.HTTP_OK) {
                Log.e(
                    "Downlaod File", "Server returned HTTP " + c.getResponseCode()
                        .toString() + " " + c.getResponseMessage()
                )
            }


            //Get File if SD card is present
//            if (CheckForSDCard().isSDCardPresent()) {
            apkStorage = File(
                Environment.getExternalStorageDirectory().toString()
                        + "/load"
            )
//            )
//            } else Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show()

            //If File is not present create directory
            if (!apkStorage.exists()) {
                apkStorage.mkdir()
                Log.e("Download dir", "Directory Created.")
            }
            outputFile =
                File(
                    apkStorage,
                    Utilities.getCurrentTime() + " CV"
                ) //Create Output file in Main File

            //Create New File if not present
            if (!outputFile.exists()) {
                outputFile.createNewFile()
                Log.e("File created", "File Created")
            }
            val fos =
                FileOutputStream(outputFile) //Get OutputStream for NewFile Location
            val ist: InputStream = c.getInputStream() //Get InputStream for connection
            val buffer = ByteArray(1024) //Set buffer type
            var len1 = 0 //init length
            while (ist.read(buffer).also({ len1 = it }) != -1) {
                fos.write(buffer, 0, len1) //Write new file
            }

            //Close all connection after doing task
            fos.close()
            ist.close()
            return true
        } catch (e: java.lang.Exception) {

            //Read exception if something went wrong
            e.printStackTrace()
            outputFile = null
            Log.e("download error", "Download Error Exception " + e.message)
            return false
        }


//        var lenghtOfFile: Long = 0
//        var status = 0
//        val client = OkHttpClient()
//        val request = Request.Builder()
//            .url(yourUrl)
//            .build()
//        var response: Response? = null
//
//
//        try {
//            response = client.newCall(request).execute()
//
//        } catch (e: IOException) {
//            e.printStackTrace()
//        }
//
//        lenghtOfFile = response?.body()?.contentLength()?:0
//        var inputStream: InputStream?=null
//
//        try {
//            assert(response?.body() != null)
//            inputStream = response?.body()?.byteStream()
//
//            val buff = ByteArray(1024 * 4)
//            var downloaded: Long = 0
//
//
//            val folder = File(Environment.getExternalStorageDirectory().toString() + "/OkayGo/")
//            if (!folder.exists()) {
//                folder.mkdir()
//            }
//
//            val documentFile = File("$folder/$fileName")
//            documentFile.parentFile.mkdirs()
//            try {
//                documentFile.createNewFile()
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }
//
//            var output: OutputStream? = null
//            try {
//                output = FileOutputStream(documentFile, false)
//            } catch (e: FileNotFoundException) {
//                e.printStackTrace()
//            }
//
//            while (true) {
//
//                val readed = inputStream?.read(buff)
//
//                if (readed == -1) {
//                    break
//                }
//                if (isCancelled) {
//                    break
//                }
//                downloaded += readed?.toLong()?:0
//                status = (downloaded * 100 / lenghtOfFile).toInt()
//
//                listener.downloadProgress(status)
//
//                output!!.write(buff, 0, readed?:0)
//
//            }
//
//            output!!.flush()
//            output.close()
//            return true
//
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }

    }


    override fun onPreExecute() {
        super.onPreExecute()
        Log.e("Downloading Started", "true")

    }


    override fun onProgressUpdate(vararg values: Void) {
        super.onProgressUpdate(*values)
    }

    override fun onPostExecute(objects: Boolean?) {
        super.onPostExecute(objects)
        if (objects == true) {
            listener.onDownloadComplete(true, 0)
        } else {
            listener.onDownloadComplete(false, 0)
        }
    }

    override fun onCancelled(aBoolean: Boolean?) {
        super.onCancelled(aBoolean)
        val folder = File(Environment.getExternalStorageDirectory().toString() + "/OkayGo/")
        val documentFile = File("$folder/$fileName")
        documentFile.delete()
    }

    override fun onCancelled() {
        super.onCancelled()
    }

}
