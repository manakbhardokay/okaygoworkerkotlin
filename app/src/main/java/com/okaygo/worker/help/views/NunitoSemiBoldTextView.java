package com.okaygo.worker.help.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public class NunitoSemiBoldTextView extends AppCompatTextView {

    private Context context;
    private AttributeSet attrs;
    private int defStyle;

    public NunitoSemiBoldTextView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public NunitoSemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init();
    }

    public NunitoSemiBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        this.attrs = attrs;
        this.defStyle = defStyle;
        init();
    }

    private void init() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/nunito_semibold.ttf");
        this.setTypeface(font);
    }
}